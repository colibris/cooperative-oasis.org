import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import LazyLoad from 'react-lazyload'
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import CardPost from '../components/CardPost'

class BlogIndex extends React.Component {
  state = {
    filterCategories: [],
    currentPosts: []
  }

  componentDidMount() {
    this.setState({
      currentPosts: get(this, 'props.data.remark.posts')
    })
  }

  categoryFilter(data) {
    const filters = []
    const component = this
    data.forEach(function(value, i) {
      filters.push(
        <span key={i}>
          <input
            type="checkbox"
            aria-label="Filter"
            name={value}
            id={value}
            onChange={component.handleInputChange}
          />
          <label htmlFor={value}>
            <span className="badge">{value}</span>
          </label>
        </span>
      )
    })
    return filters
  }

  handleInputChange = event => {
    const category = event.target.name
    const allPosts = get(this, 'props.data.remark.posts')
    let currentCategories = this.state.filterCategories

    // Add or remove category from currentCategories
    if (currentCategories.includes(category)){
      currentCategories = currentCategories.filter(currentCategory => currentCategory != category)
    } else {
      currentCategories.push(category)
    }

    // Display category posts (or allPosts if no category filtered)
    let currentPosts = allPosts
    if (currentCategories.length > 0) {
      currentPosts = allPosts.filter(item => {
        const postCategories = item.post.frontmatter.categories
        return (
          postCategories.some( cat => currentCategories.includes(cat))
        )
      })
    }

    this.setState({
      filterCategories: currentCategories,
      currentPosts: currentPosts,
    })
  }

  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const allPosts = get(this, 'props.data.remark.posts')
    const posts = this.state.currentPosts

    const duplicateFilters = allPosts.map(item => item.post.frontmatter.categories)
    const uniqFilters = [
      ...new Set([].concat.apply([], duplicateFilters)),
    ].filter(Boolean)

    const cardPosts = []
    posts.forEach((data, i) => {
      cardPosts.push(
        <LazyLoad height={1} offset={500} once={true} key={i}>
          <CardPost data={data.post} site={site} isIndex={true} key={i} />
        </LazyLoad>
      )
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={get(site, 'title')}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            { property: 'og:title', content: get(site, 'title') },
            { property: 'og:type', content: 'website' },
            { property: 'og:description', content: get(site, 'description') },
            { property: 'og:url', content: get(site, 'url') },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/logo.png`,
            },
          ]}
        />
        <div className="articles">
          <div className="container">
            <div className="row">
              <div className="col-3 text-right">
                <h1 className="underlined">Actus</h1>
              </div>
              <div className="col-9"></div>
            </div>
            <div className="checkbox-filter mb-5">
              <h4> Filtrer en cochant ou décochant les catégories :</h4>
              {this.categoryFilter(uniqFilters)}
            </div>
            {cardPosts}
          </div>
        </div>
      </Layout>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query BlogQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    remark: allMarkdownRemark(
      filter: {
        frontmatter:{
          type: {eq : "blog"}
        }
      },
      sort: {
        fields: [frontmatter___date],
        order: DESC
      }
    ) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            categories
            date(formatString: "DD.MM.YYYY")
            type
            image {
              childImageSharp {
                resize(width: 600) {
                  src
                }
              }
            }
            description
          }
        }
      }
    }
  }
`
