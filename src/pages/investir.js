import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import LazyLoad from 'react-lazyload'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import CardOasis from '../components/CardOasis'

class InvestirIndex extends React.Component {

  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const oasis = get(this, 'props.data.oasis.posts')
    const textContent = get(this, 'props.data.textContent')

    const tabCardOasis = []
    oasis.forEach((data, i) => {
      tabCardOasis.push(
        <LazyLoad height={1} offset={500} once={true} key={i}>
          <CardOasis data={data.post} site={site} isIndex={true} key={i} isInvestir={true}/>
        </LazyLoad>
      )
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={textContent.frontmatter.title}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            {
              property: 'og:title',
              content: get(textContent, 'frontmatter.title'),
            },
            { property: 'og:type', content: 'article' },
            {
              property: 'og:description',
              content: get(textContent, 'excerpt'),
            },
            {
              property: 'og:url',
              content: get(site, 'url') + get(textContent, 'frontmatter.path'),
            },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/logo.png`,
            },
          ]}
        />
        <div className="contained-page container">
          <div className="page-header">
            <Link style={{ boxShadow: 'none' }} to="/investir/">
              <h1>{textContent.frontmatter.title}</h1>
            </Link>
          </div>
          <div
            className="page-content"
            dangerouslySetInnerHTML={{ __html: textContent.html }}
          />
        </div>
        <div className="oasis pt-5">
          <div id="oasis-cards" className="container main-section">
            <h2>Les oasis financées et à financer</h2>
            <div className="grid">{tabCardOasis}</div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default InvestirIndex

export const pageQuery = graphql`
  query InvestirQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    oasis: allMarkdownRemark(
      filter: { frontmatter: { type: { eq: "oasis" }, montant: {ne: ""} } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            avancement
            montant
            departement
            image {
              childImageSharp {
                resize(width: 600) {
                  src
                }
              }
            }
            description
          }
        }
      }
    }
    textContent: markdownRemark(
      frontmatter: { path: { eq: "/investir-text/" } }
    ) {
      id
      html
      excerpt
      frontmatter {
        title
        date(formatString: "DD.MM.YYYY")
        layout
        path
      }
    }
  }
`
