import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import LazyLoad from 'react-lazyload'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import CardOasis from '../components/CardOasis'

class OasisIndex extends React.Component {

  handleOasisScrollListeners() {
    const tabs = document.querySelector('#tabs-container')
    const TabsDistanceToTop = tabs.offsetTop
    let mainNavLinks = document.querySelectorAll('.tab-nav-link')
    let navHeight = 50

    let debounce = function(func, wait, immediate) {
      var timeout
      return function() {
        var context = this,
          args = arguments
        var later = function() {
          timeout = null
          if (!immediate) func.apply(context, args)
        }
        var callNow = immediate && !timeout
        clearTimeout(timeout)
        timeout = setTimeout(later, wait)
        if (callNow) func.apply(context, args)
      }
    }

    let switchFixedNav = debounce(function() {
      let bodyDistanceToTop = document.body.getBoundingClientRect().top
      if (bodyDistanceToTop < -(TabsDistanceToTop + navHeight)) {
        tabs.classList.add('fixed-tabs')
      } else {
        tabs.classList.remove('fixed-tabs')
      }
    }, 25)


    let setActiveTab = debounce(function() {
      let fromTop = window.scrollY
      mainNavLinks.forEach(link => {
        let section = document.querySelector(link.hash)

        if (
          section.offsetTop - navHeight <= fromTop &&
          section.offsetTop - navHeight + section.offsetHeight > fromTop
        ) {
          link.classList.add('active')
        } else {
          link.classList.remove('active')
        }
      })
    }, 25);

    switchFixedNav();
    setActiveTab()
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleOasisScrollListeners)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleOasisScrollListeners)
  }

  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const oasis = get(this, 'props.data.oasis.posts')
    const location = this.props.location
    const tabCardOasis = []
    oasis.forEach((data, i) => {
      tabCardOasis.push(
        <LazyLoad height={1} offset={500} once={true} key={i}>
          <CardOasis
            data={data.post}
            site={site}
            isIndex={true}
            key={i}
            isInvestir={false}
          />
        </LazyLoad>
      )
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={get(site, 'title')}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            { property: 'og:title', content: get(site, 'title') },
            { property: 'og:type', content: 'website' },
            { property: 'og:description', content: get(site, 'description') },
            { property: 'og:url', content: get(site, 'url') },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/logo.png`,
            },
          ]}
        />
        <div className="oasis">
          <div className="oasis-header">
            <div className="container">
              <div className="row">
                <div className="col-md-3 text-right">
                  <h1 className="underlined">Les Oasis</h1>
                </div>
                <div className="col-md-9">
                  <h2>Découvrez les lieux de vie de demain</h2>
                  <div className="desc-page-oasis">
                    Partout en France, des collectifs citoyens inventent de nouveaux lieux de vie écologiques et solidaires. Nous les appelons les oasis.
                    <br />
                    <br />
                    Découvrez leur définition, naviguez sur une carte des 900 lieux du réseau et laissez-vous inspirer par quelques projets...
                    <br />
                    <br />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="tabs-container" className="oasis-tabs-container">
            <ul className="nav nav-tabs nav-justified oasis-tabs">
              <li className="nav-item">
                <a
                  href="#oasis-definition"
                  className={
                    location.hash === '#oasis-definition' || ''
                      ? 'tab-nav-link nav-link active'
                      : 'tab-nav-link nav-link'
                  }
                  title="Définition"
                >
                  Définition
                </a>
              </li>
              <li className="nav-item">
                <a
                  href="#oasis-map"
                  className={
                    location.hash === '#oasis-map'
                      ? 'tab-nav-link nav-link active'
                      : 'tab-nav-link nav-link'
                  }
                  title="Carte"
                >
                  Carte
                </a>
              </li>
              <li className="nav-item">
                <a
                  href="#oasis-cards"
                  className={
                    location.hash === '#oasis-cards'
                      ? 'tab-nav-link nav-link active'
                      : 'tab-nav-link nav-link'
                  }
                  title="Découvrez-les"
                >
                  Liste
                </a>
              </li>
            </ul>
          </div>
          <div id="oasis-definition" className="main-section oasis-definition">
            <div className="container">
              <div className="page-content bg-white p-3 ">
                <h2>Qu'est-ce qu'une oasis ?</h2>
                <p>Les oasis sont des lieux de vie et d’activité écologiques et collectifs.
                On y expérimente un mode de vie sobre au service de la préservation des écosystèmes et de la nature.
                ll existe des <strong>« oasis de vie »</strong> où habitent plusieurs foyers (écohameaux, habitats participatifs...), des <strong>« oasis ressource »</strong> abritant une ou plusieurs activités économiques (ferme collective, tiers lieux multiactivités…) et des <strong>« oasis de vie et ressource »</strong>, associant les deux, habitat et activités.</p>
                <p>Ces lieux sont d’une extrême diversité de taille (de deux à une trentaine de familles, de quelques milliers de m2 à des dizaines d’hectares), de localisation (urbain, périurbain, rural…), de forme (une seule maison pour tous ou une par foyers, réhabilitation ou neuf, habitat en dur ou léger…) et de nature (simple habitat, artisanat, ferme, élevage, résidence artistique, école…).</p>
                <p>Imaginé il y a près de vingt ans par Pierre Rabhi et redéfini par l'association Colibris en 2014, le terme d’oasis n’est pas un label mais est déposé juridiquement depuis 2017 comme une marque collective. À travers elle, les lieux peuvent se définir a priori comme oasis mais un règlement de marque sécurise les cas d’abus ou d’actes contraires aux valeurs.</p>
                <p>La carte ci-dessous présente plus de 1000 lieux recensés dans une base de donnée interopérable qui est le fruit d'une collaboration entre la Coopérative Oasis et Habitat Participatif France. Cette base de données et cette carte sont sous licence libre CC-BY-SA.
                </p>
              </div>
            </div>
          </div>
          <div id="oasis-map" className="main-section oasis-map">
            <iframe
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
              }}
              src="https://basededonnees-habitatparticipatif-oasis.gogocarto.fr/annuaire?iframe=1#/carte/@46.33,2.50,5z?cat=all"
              frameBorder="0"
              marginHeight="0"
              marginWidth="0"
            ></iframe>
          </div>
          <div id="oasis-cards" className="container main-section">
            <h2>Découvrez des oasis</h2>
            <div className="grid">{tabCardOasis}</div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default OasisIndex

export const pageQuery = graphql`
  query OasisQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    oasis: allMarkdownRemark(
      filter: {
        frontmatter:{
          type:{eq : "oasis"}
        }
      },
      sort: {
        fields: [frontmatter___date],
        order: DESC
      }) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            avancement
            montant
            departement
            image {
              childImageSharp {
                resize(width: 600) {
                  src
                }
              }
            }
            description
          }
        }
      }
    }
  }
`
