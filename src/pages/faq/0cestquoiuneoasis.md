---
title: "Qu'est-ce qu'une oasis ?"
date: "2018-04-19T22:12:03.284Z"
layout: post
type: faq
path: "/faq/c-est-une-oasis/"
categories:
 - Définitions
---

Partout en France et dans le monde, des citoyennes et des citoyens réinventent de nouveaux lieux de vie et d'activités appelés oasis : écohameaux, habitats participatifs écologiques, tiers-lieux...

Depuis 2015, Colibris facilite la création et le développement de centaines de projets par différents outils.
Aujourd'hui ce réseau rassemble plus de 700 lieux et permet l'échange d'expériences et de compétences.

Ces lieux se reconnaissent dans **5 valeurs clés qui définissent les intentions des oasis** : la souveraineté alimentaire, la sobriété énergétique, 	la gouvernance partagée, la mutualisation de biens et de services et enfin l’ouverture sur l’extérieur.

Pour en savoir plus, [découvrez le Projet Oasis de Colibris](https://www.colibris-lemouvement.org/projets/projet-oasis).
