---
title: "Pourquoi avoir créé la Coopérative Oasis ?"
date: "2018-04-18T22:12:03.284Z"
layout: post
type: faq
path: "/faq/pourquoi-la-cooperative/"
categories:
 - Définitions
---

La Coopérative accompagne les citoyens et les citoyennes qui veulent vivre en écolieux collectifs : les oasis.

Ainsi nous soutenons la création des lieux de vie écologiques et solidaires qui catalysent un changement de société plus global. Après avoir créé de nombreux outils pour les oasis (accompagnements, formations, plateformes numériques, centres de ressources...) nous nous sommes rendus compte qu'un de leur besoin n'était pas encore satisfait : le financement.

Les banques comprennent très mal, ou pas du tout, ces projets. Plusieurs collectifs ont souhaité la création d'un outil citoyen qui permet de s'affranchir du système des banques classiques. C'est pour répondre à ce besoin que nous avons choisi de créer la Coopérative Oasis.

Pour en savoir plus, [découvrez le pourquoi de la coopérative en détails en cliquant ici](/qui-sommes-nous).
