---
title: "Comment investir dans la Coopérative et qu'est-ce que j'y gagne ?"
date: "2018-04-16T22:12:03.284Z"
layout: post
type: faq
path: "/faq/comment-s-impliquer/"
categories:
 - Projet
---

La Coopérative Oasis a pour but de donner du sens à votre argent en lui permettant de financer des oasis qui en ont besoin.

Vous prenez des parts et devenez ainsi investisseur solidaire dans la Coopérative. Vous recevez une attestation de possession de parts qui vous permettra à l'avenir de récupérer votre argent.

Le modèle économique de la Coopérative ne permettra sans doute pas de verser des dividendes, mais votre placement dans une entreprise de l'économie sociale et solidaire et d'utilité sociale vous permet de bénéficier de réduction d'impôts à hauteur de 18% ou 25% du montant investi (18% minimum garanti et 25% pour 2018 - en attente du décret d'application).

Cet outil est donc très intéressant financièrement pour des personnes qui paient des impôts sur le revenu car elles peuvent bénéficier des exonérations prévues par l'Etat.

Si vous investissez 1 000 euros dans la Coopérative, vous pourrez donc déduire au moins 180 euros en tout de vos impôts, à répartir sur une durée de 7 ans. Au bout de 7 ans, vous pouvez récupérer vos 1 000 euros.

Pour en savoir plus, [voir la page Participer](/participer).
