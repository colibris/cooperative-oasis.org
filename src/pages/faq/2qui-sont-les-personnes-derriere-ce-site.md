---
title: "Qui sont les personnes derrière ce projet ?"
date: "2018-04-17T22:14:01.284Z"
layout: post
type: faq
path: "/faq/qui-sont-les-personnes-derriere-ce-projet/"
categories:
 - Projet
---

La Coopérative Oasis a été créée en 2018 à la suite du projet Oasis porté par l'association Colibris.

Mathieu Labonne, alors directeur de Colibris, a mené la réflexion sur le sujet en consultant de nombreuses personnes et a ensuite monté la SCIC. La SCIC est composée de 6 collèges, qui permettent d'intégrer des parties prenantes très variées : oasis bénéficiaires, bénévoles/prestataires/salariés, partenaires, accompagnateurs de projet, investisseurs solidaires...

Actuellement la Coopérative est administrée par un CA de 8 personnes issues de différents collèges.

[Découvrez les différents comités de la Coopérative sur la page "qui sommes-nous"](/qui-sommes-nous).
