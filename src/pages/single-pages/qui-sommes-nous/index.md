---
title: "Qui sommes-nous ?"
date: "2018-01-29T22:12:03.284Z"
layout: qui-sommes-nous
path: "/qui-sommes-nous/"
---

## L'histoire de la Coopérative Oasis

Partout en France et dans le monde, des collectifs citoyens créent de nouveau lieux écologiques et solidaires : écohameaux, habitats participatifs écologiques, tiers-lieux… **Ces lieux, nous les appelons des oasis.** Ils inventent un vivre-ensemble alliant protection des écosystèmes, autonomie, partage, convivialité…

A la suite du mouvement Oasis en tous lieux initié par Pierre Rabhi au début des années 2000, **Colibris a lancé fin 2014 un vaste projet pour faciliter l’émergence et le développement de ces lieux**. Mathieu Labonne, alors directeur de Colibris,  conçoit et porte alors ce projet. Il est rejoint par Gabrielle Paoli en 2016 qui anime le réseau grandissant des oasis.

En 2018, avec l'appui du comité stratégique, Mathieu impulse la création d'outil citoyen spécifique pour accompagner le financement de ces lieux. Mathieu et Gabrielle s'entourent alors de plusieurs partenaires, dont Dominique Schalck qui conseille de créer une SCIC, Ingrid Avot, Nicolas Granier et Thomas Lefrancq qui constituent ensemble le premier conseil d'administration en janvier 2018. Cette coopérative répond aux difficultés des oasis à accéder à du financement bancaire car ces projets collectifs sont complexes et ont des besoins spécifiques. Elle permet à des citoyens d'investir pour soutenir des projets concrets et utiles et ainsi de donner du sens leur épargne.

![desoasis](./img/bandeauoasis2.jpg)

**En 2020,  l’ensemble du projet Oasis “essaime” de Colibris et est désormais entièrement porté par la Coopérative Oasis**. Colibris reste associée et membre du CA. L'organisation de la Coopérative Oasis est particulièrement adaptée à la démarche de bien commun et de mutualisation qui préside les choix de ce projet depuis ses débuts et elle permet ainsi de mettre les oasis au centre de la gouvernance de leur réseau.
C’est aujourd’hui plus de 900 lieux qui sont membres, actifs ou non, de ce vaste réseau, et certains sont associés de la Coopérative.

Grâce à ce grand projet, ce sont des centaines d’oasis qui ont vu le jour ou sont aujourd’hui en création. La Coopérative Oasis s’appuie notamment sur un ensemble d’outils développés au fil des années : le Festival Oasis, le réseau des “compagnons oasis”, des rencontres et formations (notamment le MOOC « Concevoir une oasis », suivi par plus de 32 000 personnes en 2016 et 2017), un centre de ressources en ligne… Tous les outils génériques sont gratuits et mis à disposition des citoyens.

[Télécharger l'histoire du projet Oasis avant 2020](./Historique_projet_Oasis_avant2020.pdf).

![desoasis](./img/bandeauoasis.jpg)

## Une société coopérative (SCIC) pour organiser la vie de l'écosytème des oasis

Une SCIC se compose de collèges. **La Coopérative Oasis comporte 6 collèges** : celui des fondateurs, celui des producteurs (salariés, bénévoles, prestataires permanents), celui des oasis bénéficiaires, celui des investisseurs solidaires (vous potentiellement !), celui des partenaires (experts, partenaires financiers…) et celui des accompagnateurs de projet. Cette gouvernance collégiale garantit une forme d’intelligence collective via la prise en compte des points de vue de toutes les parties prenantes du projet. Notre SCIC a également choisi de fonctionner grâce à des outils de gouvernance partagée, notamment la décision par consentement.

- [Télécharger les statuts de la SCIC](./Statuts_Coopérative_Oasis.pdf).
- [Télécharger le dossier de presse des actions de financement de la Coopérative](./Dossier-presse-janvier-2021.pdf).

## Les instances de la Coopérative Oasis

Découvrez ci-dessous **4 instances principales de la Coopérative Oasis** : le conseil d'administration, le comité d'engagement, le comité stratégique et l'équipe opérationnelle (salariés et prestataires récurrents).
