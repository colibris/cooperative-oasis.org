---
title: "Créer son oasis"
date: "2020-05-06T22:12:03.284Z"
layout: page
path: "/creer-son-oasis/"
---

## Grandir grâce à un réseau fort
**La Coopérative Oasis anime le réseau des oasis**. Tenir la carte des oasis à jour, organiser des rencontres nationales et régionales régulières, étoffer le réseau des partenaires... Un panel d'activité dont l'enjeu est de "**se compter pour compter**" afin de permettre à la vie collective et écologique de se renforcer aujourd'hui en France. Aujourd'hui, des liens très forts unissent un ensemble d’habitants d’écolieux très variés et leur permettent de **s'entraider**.

Au coeur de cette vie de réseau, le **Festival Oasis** tient une place particulière. Cet événement principalement destiné aux oasis leur permet chaque année de se retrouver pour d’apprendre, échanger des bonnes et mauvaises pratiques, faire la fête… En octobre 2019, 400 participants de 85 oasis différentes se sont par exemple retrouvées au Château de Jambville dans les Yvelines.

![Festival](./img/bandeaufestival.jpg)


## Un soutien gratuit via des outils numériques et un centre de ressources en ligne

La Coopérative Oasis a développé  de nombreux **outils génériques et gratuits** afin de soutenir les collectifs se lançant dans l'aventure d'une oasis.
Un site collaboratif, le **[wiki des oasis](https://cooperative-oasis.org/wiki/?PagePrincipale)**, concentre toutes ces ressources : carte des fonciers disponibles, base de données de statuts juridiques ou de techniques d'animation, carte des personnes en recherche d'un collectif et de collectifs en recherche de nouveaux habitants...

Un cours en ligne en trois parties, intitulé “Concevoir son oasis”, créé en 2016 est aussi accessible gratuitement en ligne sur [l'Université des colibris](https://colibris-universite.org/formations).

![desoasis](./img/bandeausoutenir.jpg)


## Un accompagnement personnalisé et des financements

Pour répondre à la difficulté des collectifs à avoir accès aux prêts bancaires, la Coopérative propose un **accompagnement global et sur mesure de chaque oasis**. Il inclut à la fois un **prêt** sans intérêt ou à un taux très faible, sous forme de compte courant d’associés ou d’apport avec droit de reprise, et plusieurs jours par an de **soutien technique et humain en fonction des besoins de l’oasis** (juridique, gouvernance, relations humaines, urbanisme, modèle économique...).

En complément, la Coopérative Oasis anime le réseau des **[compagnons oasis](https://cooperative-oasis.org/wiki/?LesCompagnons)**, qui sont rassemblés dans le collège des accompagnateurs de la SCIC. Ce sont des professionnels indépendants, travaillant en réseau collaboratif, dont la mission est de faciliter l’éclosion et le développement d’oasis partout en France. Ils proposent des formules accessibles d’accompagnement bienveillant et compétent. Les compagnons oasis sont ainsi en mesure de s’adapter aux besoins de chaque Oasis..
