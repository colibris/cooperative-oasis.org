---
title: "Investir"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/investir-text/"
---

***La Coopérative Oasis permet de financer des oasis partout en France qui en ont besoin. Celles-ci sont présentées au bas de cette page. Découvrez comment placer votre épargne pour contribuer à l'éclosion de ces projets.***.

## Pourquoi mettre ses économies au service de lieux écologiques ?

Aujourd'hui, les habitats collectifs écologiques ont du mal à obtenir des financements auprès du système bancaire traditionnel, qui comprend mal ces projets.

En parallèle, l'épargne que nous gardons sur un compte en banque est souvent utilisée de façon opaque, au service de projets qui peuvent être contraires à des valeurs personnelles de bien commun et d'écologie (industries polluantes, armement, agroalimentaire…).

La Coopérative Oasis répond à ce double enjeu en permettant à chacune et chacun de placer son épargne dans une coopérative citoyenne. Elle sert ainsi à **financer des écolieux collectifs qui inventent le monde de demain**.

La Coopérative Oasis est une société coopérative d'intérêt collectif adhérente de Finansol, l'association de promotion de la finance solidaire, et dispose de **l'agrément ESUS (entreprise solidaire d'utilité sociale)**.

- [Télécharger le dossier de presse de la Coopérative Oasis](./Dossier-presse-janvier-2021.pdf).



## Comment investir dans la Coopérative Oasis ?

**Placer son épargne dans la Coopérative consiste à prendre des parts dans cette coopérative et à en devenir associé**. Ce n’est donc pas un don, mais bien un investissement qui peut être récupéré à tout moment.

Pour devenir associé, il suffit de remplir un bulletin de souscription et de nous le faire parvenir accompagné d'un chèque ou d'un virement. Toutes les infos relatives à la démarche sont inscrites sont sur le bulletin de souscription. Dès réception, nous vous renvoyons un certificat de propriété de parts ainsi que toutes les informations relatives à votre entrée dans la structure et aux possibilités de déduction fiscale.

Si vous êtes imposable, l’utilité sociale de la Coopérative vous permet en effet de **déduire de votre impôt sur le revenu 25% du capital investi s'il y reste pendant 7 ans**. Ce nouveau dispositif fiscal mis en place à l'été 2020 est reconduit en 2021.

Prendre des parts dans la Coopérative Oasis vous permet d’intégrer le collège des investisseurs solidaires, un des six collèges de la SCIC. Vous pourrez notamment participer à des week-ends organisés pour les associés dans les oasis financées !

- [Télécharger le bulletin de souscription personne physique](./Bulletin-personne-physique.pdf).
- [Télécharger le bulletin de souscription personne morale](./Bulletin-personne-morale.pdf).
- [Télécharger le RIB de la Coopérative Oasis](./RIB_Cooperative_Oasis.pdf).
- [Télécharger les statuts de la SCIC](../qui-sommes-nous/Statuts_Coopérative_Oasis.pdf).


## Contactez-nous pour investir dans la Coopérative
<iframe class="auto-resize" width="100%" height="440" scroll="no" frameborder="0" src="https://colibris-wiki.org/chatons/wakka.php?wiki=ContacTCooperativeOasis/iframe"></iframe>
