---
title: "Demain en main, une oasis bretonne tournée vers l'avenir"
date: "2020-11-12T06:40:32.169Z"
layout: post
type: blog
path: "/demainenmainavenir/"
image: ./DemainenMainSattler3.jpg
categories:
  - histoires d'oasis
---

**[Demain en main](https://demainenmain.fr/) est un projet collectif de 7 familles situé à Locoal-Mendon, dans le Morbihan. Il a pour objectif la réalisation d’un écosystème complet de village rural intégrant habitat et activités économiques. Brut nous présente ce projet en partie [financé par la Coopérative Oasis](https://www.cooperative-oasis.org/oasis/demain-en-main/)...**.


<iframe width="560" height="315" src="https://www.youtube.com/embed/z6noyGNLQ9g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
