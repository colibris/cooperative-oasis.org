---
title: "Une tribune d'habitat Participatif France après le confinement"
date: "2020-05-23T06:40:32.169Z"
layout: post
type: blog
path: "/tribune-hpf/"
image: ./tribunehpf.jpg
categories:
  - tribunes
---

_Nous relayons cette tribune de notre partenaire privilégié : Habitat Participatif France._



## L’arrivée du confinement nous a tous sidérés

Nous avons pu observer, à partir de nombreux témoignages d’habitants, comment les habitats participatifs, de par leur organisation propre, ont “fait faceˮ à la crise sanitaire : services mutuels supplémentaires et interactions sociales ; présence de relais de proximité diminuant le poids de l’isolement ressenti par les séniors ; présence d’espaces communs différenciés permettant de multiples opportunités d’organisation qui allègent et soutiennent.

## Habitants et professionnels acteurs de l’habitat participatif, nous observons ce que les évènements ont révélé

Ces lieux de vie collective vont à l’encontre du repli sur soi, de la peur de l’autre. Ces habitats créent du commun avec des moments de vie collective, des solidarités entre habitants et avec l’extérieur, des pratiques inclusives, des comportements écoresponsables de préservation de l’environnement. La crise actuelle illustre la résilience des habitats participatifs. Plus largement, ils répondent à de nouvelles aspirations mises au jour par cette crise de société.

## Habitat Participatif France appelle à des choix de société, politiques et économiques, pour la réalisation et la construction de projets en habitat participatif plus nombreux

Cela nécessite de prendre de nouvelles orientations au sein des organisations de l’habitat social, au sein des secteurs économiques de la construction immobilière et du logement. Nous invitons les élus, les militants politiques, les militants associatifs... à entrer dans la mise en œuvre d’une politique du logement où l’habitat participatif serait proposé largement à toute la population. Nous réaffirmons l’objectif d’accès de tous à une forme d’habitat qui favorise la solidarité des territoires, qui permet la mixité sociale et engage la participation citoyenne pour un mieux-vivre ensemble.

## 6 propositions pour développer l'habitat participatif

**1ère proposition : Fixer à 2 % la part des logements en habitat participatif**
Fixer à 2 % la part des logements en habitat participatif (constructions et rénovations programmés) qui seront réalisés sur les trois prochaines années.
Habitat Participatif France souligne la nécessité de rénover le parc existant comme un axe de plus en plus essentiel de la revitalisation urbaine et rurale :
400 nouveaux projets d’habitat participatif représentant 6 000 logements neufs...
600 projets de rénovation, notamment énergétique, correspondant à la transformation d’habitats anciens en habitats participatifs, représentant 8 000 logements réhabilités dans le cadre des opérations de renouvellement urbain (Cœur de ville, Cœur de Bourg, Quartiers Prioritaires de la Ville...) d’intervention sur des copropriétés dégradées, ou de revitalisation et densification d’habitat rural.

**2ème proposition : Impliquer les citoyens dans la gouvernance du développement territorial (ville et campagne) et de la production de logements**
L’habitat participatif doit être un axe important de mobilisation, tant des habitants disposant d’une bonne expérience de ces projets et de ce mode d’habiter, que de l’intelligence collective des citoyens qui y aspirent :
Dans les instances de concertation consultées lors de l’élaboration des documents d’urbanisme Plan Locaux de l’Habitat (PLH, PLiH), Plan Local d’Urbanisme (PLU, PLUi), Charte Promoteur...
Dans les instances de concertation consultées lors de la mise en place des projets d’aménagement : Opération de Revitalisation de Territoire (ORT), Orientation d’Aménagement et de Programmation (OAP), Zone d’Aménagement Concerté (ZAC).
Dans les conseils de quartier installés dans le cadre des programmes nationaux mis en œuvre par l’Agence Nationale de Rénovation Urbaine et l’Agence Nationale d’Amélioration de l’Habitat : Programme National de Rénovation Urbaine (PNRU), Nouveau Programme de Renouvellement Urbain (NPNRU), programme de requalification des quartiers anciens dégradés (PNRQUAD).
Dans les Comités Régionaux de l’Habitat et de l’Hébergement (CRHH), et dans les Conseils de Développement.
Dans la gouvernance d’outils foncier innovants (Organismes de Foncier Solidaire...).

**3ème proposition : Réserver à l’habitat participatif 10 % des droits à construire ou à rénover dans les programmes des opérations d’aménagement et/ou de rénovation**
 - Dès la programmation des opérations citées: ORT, OAP, ZAC, PNRU, NPNRU, PNRQUAD, Programmes Cœurs de Ville et Cœur de Bourg...
 - Par l’émergence d’Appels à Projet dédiés à l’Habitat Participatif, initiés par les collectivités territoriales ou leurs opérateurs, prenant en compte les spécificités de ces opérations (implication de collectifs citoyens dans la conception, la réalisation et la gestion) notamment par des procédures négociées limitant la mise en concurrence de groupes d’habitants et sortant ces lots de la logique de concours de charge foncière.
 - En engageant une dizaine d’opérations d’aménagement “post-carbone” fondées sur une conception environnementale à grande échelle dans le cadre de projets urbains participatifs limitant l’artificialisation des sols, où l’habitant devient acteur aux côtés de la puissance publique à toutes les phases du projet de la conception, de la maitrise foncière, de l’aménagement et de la gestion.

**4ème proposition : Prendre en compte les spécificités de l’habitat participatif dans l’application du droit commun**
 - Lever les freins au développement des projets participatifs en autopromotion, notamment en SCIA et en coopératives d’habitants : freins juridiques (Garantie), fiscaux (Régime de TVA), bancaires (accès aux prêts lors des cessions de parts de SCIA maintenues en jouissance) et leur permettre un accès égal aux financements (PLS, ANAH, etc.), éligibilité à l’APL pour les ménages détenant du patrimoine sous forme de parts sociales...
 - Faciliter le développement de l’habitat participatif en locatif social, en harmonisant le principe des Pré-Cal (Commissions d’attribution préparatoires), en donnant accès aux aides à l’accession sociale aux projets réalisés en autopromotion (financement, taux de TVA, exonération de taxe foncière...), et aux prêts CDC pour financer les espaces communs de ces projets, en favorisant la programmation de projets d’habitat participatif en locatif social, en quartiers prioritaires de la ville (régime dérogatoire...).
 - Diffuser et faciliter la location-accession en SCI-APP en harmonisant l’approche de cette forme progressive d’accession à la propriété, en la rendant éligible aux subventions ANRU quand les projets en SCI-APP sont développés en Quartier Prioritaire de la Ville,
 - Consolider les montages en coopératives d’habitants : en adaptant le PLS au contrat coopératif (accès aux financements CDC) en leur donnant accès aux prêts Gaïa, en maintenant à 25 ans l’exonération de la taxe foncière sur les propriétés bâties, en rendant éligibles à l’APL les ménages détenant du patrimoine sous forme de parts sociales, en rendant cessibles les apports en industrie.

**5ème proposition : Mobiliser les outils de portage foncier**
 - Mobiliser l’ensemble des outils territoriaux de portage foncier (EPF, EPFL,...) pour l’acquisition de fonciers (terrains ou immeubles constructibles ou réhabilitables rapidement) à vocation de projet d’habitat participatif. Intégrer des objectifs de programmation en faveur de l’habitat participatif dans les conventions entre ces Établissements et les collectivités locales.
 - Mobiliser la solidarité dans les outils fonciers : sensibles à la non-spéculation, les candidats à l’habitat participatif peuvent pour un grand nombre être acquis à la dissociation du bâti et du foncier proposée par les Organismes de foncier solidaire, voire les Offices Foncier Libre. Pour autant, le mouvement Habitat Participatif France souhaite que soient prises en compte les spécificités des différents statuts (SCIA en jouissance, coopératives d’habitants) et qu’il soit associé à la gouvernance de ces outils, afin qu’au-delà de leur seule contribution à l’économie du marché du logement, ils soient de véritables outils d’un développement urbain, citoyen, solidaire.

**6ème proposition : Créer un fonds dédié à l’habitat participatif**
 - Abondement en subvention : dans le cadre des programmes d’action de l’Agence Nationale de Cohésion des Territoires (PNRU, NPNRU, PNRQUAD, Programmes Cœurs de Ville et Cœur de Bourg), financement des études pré-opérationnelles et les missions d’Assistance à Maitrise d’Usage de 100 projets lancés sur la période 2020/2022.
 - Aides remboursables sous condition de ressources pour appuyer la participation des ménages les plus modestes dans le cadre des projets à maîtrise d’ouvrage citoyens-autopromotion (SCIA & Coopératives d’habitants...).
 - Soutien à l’accession sociale en habitat participatif (neuf et rénovation) par des primes sous condition de ressources et de territoires prioritaires, principalement dans le cadre d’OFS, pour favoriser l’accession sociale tout en pérennisant les aides publiques investies.
 - Soutien financier pour les projets en zone rurale mettant en œuvre de nouvelles formes de coopérations territoriales (matériaux bio-sourcés, habitats légers, autonomie énergétique et alimentaire, mixité fonctionnelle...).
