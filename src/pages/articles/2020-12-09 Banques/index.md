---
title: "Ne laissons pas les banques détruire le monde avec notre argent"
date: "2020-12-09T06:40:32.169Z"
layout: post
type: blog
path: "/SortirBanques/"
image: ./bnp4.jpg
categories:
  - tribunes
---

**Plaçons notre épargne dans les oasis ! Il est aujourd’hui possible de régénérer des sols et d’entretenir du patrimoine à l’abandon en mettant ses économies à disposition de projets vertueux. Et aujourd’hui plus que jamais, cela est nécessaire. Et c’est [ce que propose la Coopérative Oasis](https://cooperative-oasis.org/investir/)...**

![BNP 4](./bnp4.jpg)

##Les 6 plus grosses banques françaises ont une empreinte carbone qui représente près de 8 fois celle de la France

Depuis des années, les banques investissent massivement nos économies dans des projets fossiles émetteurs de carbone. En **octobre 2020**, Oxfam France a publié un [nouveau rapport intitulé « Banques : des engagements climat à prendre au 4ème degré »](https://www.oxfamfrance.org/climat-et-energie/banques-des-engagements-climat-a-prendre-au-4eme-degre/) dans lequel sont analysées la température et les émissions de gaz à effet de serre issues des activités de financement et d’investissement des six principales banques françaises.

![Oxfam](./oxfam2.jpg)

Le rapport révèle que les banques françaises n’ont pas pris de mesures suffisantes pour respecter l’objectif de limiter le réchauffement de la planète à 1,5°C, inscrit dans l’Accord de Paris, et nous amènent au contraire sur une trajectoire de plus de 4 degrés. **Les 6 banques françaises – BNP Paribas, Crédit Agricole, Société Générale, Banque Populaire Caisse d’Epargne, Crédit Mutuel et la Banque Postale – ont une empreinte carbone qui représente près de 8 fois les émissions de gaz à effet de serre de la France entière**. Et à elles seules, BNP Paribas et Société Générale émettent chacune 2 fois plus que l’ensemble du territoire français.

![BNP 3](./bnp3.jpg)

##Les oasis divisent par deux l’émission de gaz à effet de serre de leurs habitants

Oasis, écohameau, écovillage, habitat participatif, ferme collective… Urbains, ruraux ou périurbains, lieux d’habitat et / ou de production, allant de 1 à 30 foyers vivant sur place… Ce qui lie les [1 000 lieux écologiques et collectifs qui existent aujourd’hui en France](https://cooperative-oasis.org/les-oasis/#oasis-map), si différents les uns des autres, ce sont des valeurs communes : l’autonomie, le collectif, la résilience, la solidarité.

![Carbone 1](./Carbone1.jpg)

Au-delà de ce que chacun de ces lieux apporte à ses membres et à son territoire, il contribue aussi à réduire notre empreinte climatique. En 2016, Colibris et Carbone 4 ont réalisé une étude sur l’empreinte carbone d’un habitant d’Oasis. Le résultat est clair : **un habitant d’oasis émet deux fois moins de gaz à effet de serre qu’un Français moyen**. La construction écologique des habitations, la mutualisation des espaces et des outils, ou encore un mode de vie plus sobre sont les causes de ces émissions réduites.

##Mettre son épargne à disposition des oasis contribue à réduire notre emprunte carbone

Or **ces projets vertueux, les banques ne les financent pas, ou très mal.**  Pour répondre à ce besoin de financement, la Coopérative Oasis propose donc à chacune et chacun de placer son épargne en toute sécurité. Elle réinjecte ensuite cet argent dans une économie réparatrice en la prêtant à des lieux écologiques et collectifs pour qu’ils achètent un terrain, financent des travaux ou lancent une activité économique.

![Finance solidaire](./finance-solidaire.jpg)

La démarche est simple : n'importe quel citoyen ou citoyenne peut investir en prenant des parts dans la Coopérative.  Cet investissement peut être récupéré à tout moment. Et parce que cet investissement est reconnu d'utilité publique par l'État (agrément ESUS), tout investisseur peut bénéficier d’une déduction de 25 % de la somme investie sur son impôt sur le revenu, à condition qu'il conserve ses parts durant 7 années au moins.

##En deux ans, 200 investisseurs ont déjà placé près de  2M d’euros qui ont permis de financer 12 projets

Didier a placé 2 000€ en 2020, après en avoir entendu parler par un ami : « _Avec le confinement, j'ai pu mettre un peu d'argent de côté, et je souhaitais le placer en lui donnant du sens, plutôt que de chercher une rentabilité à outrance. En effet, la période actuelle nous rappelle que privilégier un mode de vie coopératif vaut bien mieux qu'une course au profit. Outre l'avantage fiscal, c'est aussi un tremplin pour soutenir des actions et des acteurs engagés, voire… s'engager soi-même._ »

![Oasis financee](./Oasisfinancee2.jpg)

_Week-end organisé à l'oasis de Grain&Sens pour les investisseurs de la Coopérative Oasis_

Anne-Marie a quant à elle placé une partie de son épargne en 2019 : « _J’ai placé 15 000€ dans la Coopérative en 2019 car j’en avais assez de ne pas savoir où les banques plaçaient mon argent. La Coopérative nous informe de tous les prêts octroyés et nous permet même d’aller voir tous les lieux ayant abouti grâce à nous… enfin ça, c’était avant le COVID !_ »

Après avoir découvert les oasis au festival oasis 2020, Michèle a placé 100 000€ : « _Je place mon argent à la Coopérative Oasis pour faire avec ce que j’ai toujours fait : soutenir l’avenir._ »


_Article écrit par Gabrielle Paoli_
