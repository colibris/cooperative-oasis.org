---
title: Des soirées rencontre pour parler de la coopérative
date: "2018-05-22T22:40:32.169Z"
layout: post
type: blog
path: "/evenements-lancement/"
image: ./evenement-lancement.jpg
categories:
  - événements
---

_Pour répondre aux questions de celles et ceux qui envisagent de participer à la Coopérative Oasis, des événements sont organisés dans plusieurs villes pour présenter le projet, répondre aux questions... et bien sûr passer un moment convivial autour des oasis !_

![Rencontre coopérative](./evenement-lancement.jpg)

Retrouvez la coopérative Oasis à de multiples occasions en juin et juillet 2018 :

* * *
**Le festival de l'habitat participatif à Forcalquier le 3 juin à 15h**

Dans le cadre du Festival Régional de l'habitat participatif de PACA organisé par Regain, une conférence d'une heure est prévue pour présenter la coopérative oasis.

Gratuit, 15h à 16h, Couvent des Cordeliers, 04300 Forcalquier.

[Informations et inscription](http://regain-hg.org/2-non-categorise/558-programme-du-frhap)

* * *
**Soirée rencontre à Lyon le 12 juin**

Venez découvrir la Coopérative et poser vos questions à Thomas Lefrancq, membre de l'oasis du Château partagé et administrateur de la Coopérative, et Jean-Philippe Cieslak, chargé de mission à la coopérative oasis.

Gratuit, 20h - 22h, Auditorium de la Nef, Immeuble Woopa, 8 avenue des Canuts, 69 120 Vaulx-en-Velin

[Inscription](https://colibris.cc/oasis/wakka.php?wiki=AjoutParticipantSoireeCooperative)

* * *
**Soirée rencontre à Montpellier le 15 juin**

Venez découvrir la Coopérative et poser vos questions à Nicolas Granier, membre du conseil d'administration de la coopérative Oasis, et Isabelle Casteras, directrice innovation d'une grande banque et membre du comité d'engagement de la coopérative.

Gratuit, 19h - 21h, 6 Rue des Augustins, Salle de conférence du 1er étage (salle Grand Lac), 34000 Montpellier
Proche Tram Comédie, face au manège de l' Esplanade

[Inscription](https://colibris.cc/oasis/wakka.php?wiki=AjoutParticipantSoireeCooperative)

* * *
**Soirée rencontre à Paris le 28 juin**

Venez découvrir la Coopérative et poser vos questions à Mathieu Labonne, président et fondateur de la coopérative Oasis, et Jean-Philippe Cieslak, chargé de mission à la coopérative.

Gratuit, 19h - 21h, Jardin 21, Parc de La Villette, 12 Rue Ella Fitzgerald, 75019 Paris
Tram 3b (station Ella Fitzgerald) ou Métro 5 (Porte de Pantin) - 7 (Porte de la Vilette) - 10 min à pied par le Parc de La Villette.

[Inscription](https://colibris.cc/oasis/wakka.php?wiki=AjoutParticipantSoireeCooperative)

* * *
**Les Rencontres Nationales de l'habitat participatif le 6 juillet à Nantes**

Dans le cadre des rencontres nationales de l'habitat participatif organisées à Nantes par la Coordinaction, un atelier d'une heure est prévu pour présenter la coopérative oasis. Il sera animé par Ingrid Avot, compagnon oasis et membre du conseil d'administration de la coopérative oasis, et Gabrielle Paoli, chargée de mission oasis chez Colibris et membre du conseil d'administration de la Coopérative.

14h - 17h, École nationale supérieure d'architecture de Nantes - salle 1A03
6 quai François Mitterrand, 44262 Nantes cedex 2

Payant via l'achat d'un billet pour les rencontres nationales de l'habitat participatif.

[Infos et Inscription](https://www.coordinaction.net/?RNHP2018)

* * *
**Soirée rencontre à Strasbourg le 11 juillet**

Venez découvrir la Coopérative et poser vos questions à Gabrielle Paoli, chargée de mission oasis chez Colibris et membre du conseil d'administration de la Coopérative, et Jean-Philippe Cieslak, chargé de mission à la coopérative.

Gratuit, 19h - 21h, Maison des associations de Strasbourg
1ª place des Orphelins
67000 Strasbourg
Trams A et D arrêt Porte de l'Hôpital ou Bus 10 arrêt Austerlitz

[Inscription](https://colibris.cc/oasis/wakka.php?wiki=AjoutParticipantSoireeCooperative)
