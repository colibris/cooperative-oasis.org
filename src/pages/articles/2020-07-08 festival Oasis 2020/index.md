---
title: "Participez au festival oasis 2020 !"
date: "2020-07-08T06:40:32.169Z"
layout: post
type: blog
path: "/festivaloasis2020/"
image: ./Festival1.jpg
categories:
  - événements
---

**Le festival oasis 2020 aura lieu du 28 septembre au 4 octobre au château de Jambville (Yvelines)**.
Une semaine de formation, suivie de deux jours de partages et de fête, permettront à plusieurs centaines de personnes de se réunir autour de leur rêve et de leur expérience de vie collective et écologique…
<br/>

_Attention : le Festival est complet (plus de 400 participants). Il n'est plus possible de s'inscrire._


<br/>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://frama.link/FestivalOasis2020" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Découvrir les infos détaillées sur le wiki
    </a>
</div>
</div>

<br/>

## Se réunir pour avancer chacun de son côté

Il y a un an, Sarah et Reynald se sont installés en Ardèche pour rejoindre les Hameaux Légers. Ils les ont rencontrés autour d’un verre au festival oasis 2018 et ont été si emballés par leur projet… qu’ils ont fini par changer de vie pour s’engager à leurs côtés. Claire, elle, s’est établie dans la communauté de l’Arche Saint-Antoine, en Isère, pour une année. Elle est arrivée là après avoir entendu Margalida, cofondatrice de l’Arche de Saint Antoine, parler de non-violence et de vivre-ensemble au festival oasis 2019.

Le festival oasis n’est pas un speed-dating géant pour écolieux… mais presque : c’est la rencontre annuelle des écolieux français. Des centaines de personnes vivant déjà en oasis ou souhaitant y vivre s’y retrouvent pour partager leurs expériences, leurs réussites, leurs découragements, leurs envies…

![festival Oasis 2019](./Festival2.jpg)

## « Je me suis dit qu’on était tous dans le même bateau »

Nicolas Pinus est venu au festival oasis 2019 en curieux. Il vit à l'Aérium, une oasis située dans le parc national des Cévennes, où habitent une vingtaine d’adultes et une demi-douzaine d’enfants. Le lieu de 3 500 mètres carrés de bâtiments et quatre hectares de terrain se situe dans la montagne, dans un ancien sanatorium. Ce qui a enrichi le plus Nicolas, c’est d’entendre les témoignages de ceux qui ont trente ans de communauté derrière eux : « _cela m’aide à relativiser, à me dire qu’on est tous dans le même bateau_ ». 

Clément Leguidcoq, lui, a acheté un lieu à Saint-Omer-de-Blain au nord de Nantes, un presbytère qui a 200 ans avec trois habitations et un théâtre, pour créer une oasis ! Il est venu au festival 2019 pour rechercher des informations sur la gouvernance, mais aussi pour rencontrer de futurs habitants. « _5 familles, ce serait chouette, avec des enfants qui courent partout, un lieu culturel, un jardin partagé, la collecte des eaux de pluies et une phytoépuration_ », rêve-t-il.

## Parler et jouer ensemble… avec des mots et des pratiques différentes

Des jeunes citadins, des militants anarchistes, des moines, des retraités qui veulent vieillir ensemble, des mères célibataires.... « _Un seul repas au Festival Oasis permet de naviguer dans des mondes traditionnellement séparés_, explique Mathieu, directeur de la Coopérative Oasis. _Pendant deux jours, des personnes éloignées se réunissent dans un lieu sublime. Elles parlent et jouent autour de questions communes mais avec des mots, des cultures, des pratiques très différentes ! C’est très riche et parfois très drôle..._ »

![festival Oasis 2019](./Festival3.jpg)

## Un festival 2020 en deux parties sur une semaine complète

En 2020, le festival oasis revient au château de Jambville en deux parties qui se succèdent :

**Le camp oasis - du lundi 28 septembre au vendredi 2 octobre 2020.**
Plus de cent personnes se réunissent toute la semaine pour suivre treize formations et ateliers variés, mais aussi télétravailler dans un espace de coworking éphémère et se retrouver tous aux repas et le soir pour échanger et tisser des liens.
Retrouvez [**le programme des ateliers et formations**](./Programme-CampOasis2020-v2.pdf) en cliquant ici.


**Le festival oasis - du vendredi 2 au dimanche 4 octobre 2020.**
Près de 400 personnes issues d'une centaine d'oasis partout en France se retrouvent le week-end pour échanger, se former, faire la fête ! Chacune et chacun est libre de venir juste au camp oasis, juste au Festival Oasis ou aux
deux !
Le programme précis sera communiqué avant fin août.

Nous vous encourageons à venir pour un maximum de jours et profiter ainsi des belles formations proposées du lundi au vendredi.

<br/>
<br/>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://frama.link/FestivalOasis2020" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Découvrir les infos détaillées sur le wiki
    </a>
</div>
</div>

<br/>
<br/>


## Petit rappel sur le week-end du Festival Oasis 2019 en chiffres...
- 380 participants
- 85 oasis et habitats participatifs représentés
- 32 présentations d'écolieux
- 21 accélérateurs de projet
- 20 formations et partages
- 5 cercles samoan avec 5 intervenants : Corinne Morel-Darleux, Maxime de Rostolan,
Nicolas Voisin, Margalida Réus et Franck Nathié
- 3 sessions de jeux de confiance et de coopération avec Pascal Deru et Jean-marie Ramel
- 1 forum ouvert avec Frédéric Bosqué et Vincent Tardieu
- 1 auberge de la parole
- 1 bal folk

<iframe width="560" height="315" src="https://www.youtube.com/embed/bPmM5t2GNu4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
