---
title: "Le Moulin Bleu : être résilient dans un monde qui s'effondre"
date: "2020-05-10T06:40:32.169Z"
layout: post
type: blog
path: "/moulin-bleu-etre-resilient/"
image: ./moulinbleu1.jpg
categories:
  - oasis financées
---

_Le collectif est né au sein d'une eco-colocation de 15 personnes en région parisienne  : la Maison Bleue.
Le lieu est un ancien moulin et ses dépendances, 800m2 habitable sur un terrain de 13ha au bord du Loir (Saint-Jean Froidmentel, Loir-et-Cher). En partie grâce à un prêt de la Coopérative Oasis, le mercredi 11 mars 2020, le collectif a signé l'acte d'achat du lieu : Le Moulin Bleu était né. Récit à plusieurs voix d'une course folle, guidée par le plaisir d'être ensemble et le besoin d'être exemplaire._

![Moulin bleu](./moulinbleu1.jpg)

## De la coloc au moulin

En ce mois d'avril 2020, alors que la France entière est encore plongée dans le "grand confinement", les 13ha de l'ancien moulin de Saint-Jean-Froidmentel voient se balader de nouveaux et inhabituels habitants. Une quinzaine de jeunes gens et autant de moutons, encore ébahis de la rapidité avec laquelle ils se sont retrouvés là.

"L'idée du Moulin Bleu est née avec la Maison bleue, une éco-colocation créée en 2015 à Bourg-la-Reine, raconte Édouard, à l'initiative de cette dernière. L'ambition d'aller un jour à la campagne était inscrite dans la fondation de cette coloc', on se savait "en transition"". Trois ans plus tard, l'idée revient sur la table. Pour toutes et tous, le moment est venu de déménager et de donner une autre envergure à l'aventure collective, écologique et amicale déjà bien entamée. Ils sont alors 19 copains à rêver ensemble.

Camille, co-fondatrice du Moulin bleu, rappelle l'intention commune : "Notre envie était forte d'incarner nos valeurs dans un mode de vie résilient et alternatif. Cette exemplarité passait pour nous par de nombreux domaines : le zéro déchet, l'économie d'énergie, une mobilité réduite, la production d'une partie de notre consommation alimentaire..." C'est notamment ces deux derniers points, la mobilité et la résilience alimentaire, qui ont poussé le collectif à quitter Bourg-la-Reine pour s'installer dans une zone plus rurale.

![Moulin bleu](./moulinbleu2.jpg)

## Un projet  à dix-neuf personnes monté en un an (et sans chef)

En un an, au rythme d'un week-end complet de travail tous les mois, le groupe est passé de l'idée à l'installation. Avec une culture chevillée au corps : celle de refuser un chef, de travailler en autogestion. Comment ?  En lissant les caractères des uns et des autres. "Même si on peut pas empêcher que certains prennent plus de place que d'autres, on a essayé de faire en sorte que certains groupes de travail ne chapotent pas les autres, explique Camille. Par exemple on a voulu créer un groupe en charge de faire un échéancier du projet. Certains y ont vu un groupe "coordination" et l'ont mal vécu. On a discuté et trouvé une solution pour sécuriser ceux qui avaient besoin d'une vision globale et ceux qui ne voulaient pas de coordination." Les processus sont plus lourds, les décisions passent souvent par de longues discussions ; cependant, même s'il est élevé, le prix est volontiers payé.

Quand le moulin a été identifié comme potentiel lieu, certains se sont mis en arrière plan, tout en restant investisseurs. Si, à terme, le Moulin abritera 24 habitants, le noyau dur n'est aujourd'hui encore constitué que de 7 personnes, qui ont porté l'achat. Âgées de 25 à 34 ans, les 5 filles et les 2 garçons sont chercheur en biologie, consultante, professeur d'écologie et de yoga, chômeuse, conseillère de collectivité territoriale, chef d'entreprise...

![Moulin bleu](./moulinbleu2.jpg)

## Habiter le territoire au-delà des murs de l'oasis

Avec la mise en place du confinement, le groupe dans sa quasi intégralité (soit les 7 habitants et les investisseurs), se sont installés sur place. Une occasion en or d'accélérer l'installation. Découverte, nettoyage, réhabilitation, premiers aménagements... la prise en main du lieu par les habitants depuis la mi-mars 2020 laisse déjà entrevoir les potentielles futures activités pour ce Moulin. "On se dit qu'on composera avec le territoire avant tout - ses caractéristiques, ses habitants, ses besoins... Il est donc encore difficile de dire ce qu'on fera exactement ici" explique Flore, la dernière en date à avoir rallié l'aventure. Les envies et les compétences ne manquent pourtant pas :  production agricole, production d'électricité, espace bien-être, brasserie locale, atelier de sérigraphie participative, restaurant bio...

Car le plus important pour le collectif n'est pas simplement de se donner un cadre de vie agréable, mais de contribuer à impulser une dynamique locale de transition. Alors les habitants ne se précipitent pas. "On est au pied de la Beauce(1), avec nos bonnes intentions et notre envie de changer de modèle ! On ne peut pourtant pas arriver comme ça avec des solutions toutes faites d'urbains privilégiés... alors on observe, on s'imprègne, et après on verra ce qu'on fera" conclue Flore.

![Moulin bleu](./moulinbleu3.jpg)

## Splendeur et misère du collectif

Une chose est sûre pourtant : il y aura de l'habitat collectif au Moulin Bleu.

Les 7 gîtes et les 5 chambres de la maison principale permettent aujourd'hui aux personnes qui le souhaitent) d'avoir un espace privé. De moments qui réunissent l'intégralité du groupe, il n'y a guère que le repas du soir, sans obligation. Il s'agit néanmoins d'un moment essentiel, où toutes et tous se retrouvent pour faire un point sur la journée, sur ce qui a avancé et les choses qui restent à faire. Le dimanche, une réunion de deux heures suit le brunch et précède la session hebdomadaire collective de ménage.

Depuis leur installation au Moulin, les 15 compagnons ont déjà traversé des moments difficiles. "Par exemple, nous avons dû nous poser la question de recevoir d'autres personnes après le début du confinement. Chacun a été affecté de manière différente par cette question, et cela a généré de fortes tensions, qui ne sont toujours pas résolues" raconte Camille. Si un cercle de parole régulier permet à chacun et chacune d'exprimer et d'écouter les émotions qui les traverse, celui-ce ne suffit pas toujours.

"Vivre en collectif suppose des avantages et des inconvénients, reprend Camille. Quant à moi, tant que les avantages sont plus forts, je m'y tiens. Mais je ne dis pas que c'est fait pour tout le monde !" Au-delà de la vie quotidienne, la mutualisation des forces permet souvent de voir en plus grand. "Quand j'habitais seule, la vie de tous les jours était plus simple, c'est vrai... explique Flore, mais je ne me serais jamais acheté un moulin !"

L'aventure du Moulin Bleu commence donc tambour battant. Rythmée par les hauts et bas de la vie quotidienne, elle reste l'horizon rêvé de quinze amis déterminés.

![Moulin bleu](./moulinbleu4.jpg)
