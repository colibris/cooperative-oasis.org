---
title: "De la révolution industrielle à la révolution écologique : le Moulinage de Chirols"
date: "2020-01-21T06:40:32.169Z"
layout: post
type: blog
path: "/revolution-chirols/"
image: ./article-chirols-1.jpg
categories:
  - oasis financées
---

Né en 2015, le collectif du Moulinage de Chirols a décidé de réhabiliter une ancienne usine ardéchoise de fil de soie. L’objectif ? En faire un lieu ouvert regroupant activités artistiques et artisanales, habitat participatif et espaces communs publics. Intérêt général, refus de la spéculation, préservation des écosystèmes et engagement citoyen sont les maîtres-mots des 23 personnes qui ont signé l’acte d’achat au printemps 2019…

![Moulinage de Chirols](./article-chirols-1.jpg)

## De l’espace pour accueillir la diversité

_« Tout est parti d’une blague. On est tombé sur cette énorme usine abandonnée au cœur du village de Chirols et j’ai dit à Juliette « Tiens on pourrait acheter ça ! » Sauf que, de fil en aiguille… »_ Guy, co-fondateur du projet, se souvient qu’au début, en 2015, ils ne sont que quatre : deux artistes du spectacle, lui et sa compagne Juliette, et deux architectes-paysagistes, Antoine et Sylvain. Ensemble, ils rêvent de redonner vie à ce lieu emblématique de la vallée.

Quatre ans plus tard, le Moulinage de Chirols existe plus que jamais. Et derrière ce nom enchanteur, se trouvent désormais : un collectif de vingt-trois personnes déjà presque toutes installées dans les environs ; une coopérative d’habitants propriétaire du lieu acheté pour 200 000€ au printemps 2019 ; des espaces communs déjà réhabilités et occupés : dortoir et sanitaires,  salle des fêtes, foyer, espace d’art graphique, menuiserie, bureaux temporaires… le tout pour accueillir les premiers chantiers participatifs.

![Moulinage de Chirols](./article-chirols-2.jpg)

Depuis quelques années, le Moulinage de Chirols fait beaucoup parler de lui. Il est vrai que le projet collectionne les originalités. La taille d’abord : 4 500m2 de bâti. _« La dimension extravagante du lieu exige le mélange et la réunion de gens et d’activités d’horizons, d’âges, de tonalités différentes. Avec un tel volume, on est obligé de rêver grand… et ensemble »_ nous confie Guy, comédien, metteur en scène et musicien.

## Le Moulinage de Chirols : les espaces en projet

Un espace commun d’activités de 2400m² à usage collectif et ouvert au public comprenant  :
- un pôle culturel avec une salle de spectacle de 200 places pour des résidences d’artistes et des formations
- un espace de « coworking »
- une cantine participative associative privilégiant les produits bio et locaux
- un espace d’hébergement temporaire pouvant accueillir une trentaine de résidents (artistes, étudiants…) ou de bénévoles

Un espace d’habitat participatif de 1900m² pour 30 résidents permanents, privilégiant la mixité sociale, avec des espaces domestiques partagés (buanderie, épicerie participative, atelier).

Une ruche d’activités intégrant des artisans locaux (menuiserie, sculpture, transformation de fruits et légumes), un atelier participatif associatif, et des activités audiovisuelles et numériques.

Un terrain commun d’environ 2000 m² en “faysses” (cultures en terrasses traditionnelles), pour créer des potagers bio en permaculture et des poulaillers.

![Moulinage de Chirols](./article-chirols-3.jpg)

## Un patrimoine en héritage

L’envergure du patrimoine que représente cette ancienne usine est une deuxième singularité. À la pointe de la technologie, le moulinage et ses terrasses cultivées en vignes et en fruitiers ont fait vivre pendant près d’un siècle une bonne partie de la vallée. Et ce,  jusqu’à leur fermeture dans les années 2000. _« Bien plus que réhabiliter des murs anciens, nous souhaitons prolonger la lignée de tous ceux qui ont construit, travaillé et vécu ce lieu pendant des années. Inscrivant nos pas dans les leurs, nous essayons d’être à la pointe en montrant la capacité des territoires ruraux à se réinventer avec des solutions écologiques et sociales vertueuses »_ raconte Alexandre, paysagiste, qui a rejoint le groupe en 2018.

Une artiste et une ethnologue du collectif travaillent d’ailleurs sur le recueil des mémoires. _« Certains viennent jusqu’à nous, pour nous faire visiter l’usine à travers leurs yeux et leurs souvenirs. Autrement, on toque aux portes et on demande aux gens de nous raconter… »_ explique Juliette, chanteuse, comédienne et co-fondatrice du projet. Le lien avec les habitants locaux n’a pour autant pas toujours été facile. _« Au début, on sentait qu’ils n’y croyaient pas du tout. Et puis, il y a parfois un fond de méfiance à l’égard des néo-ruraux, perçus comme pas très sérieux… mais ils nous ont vu mouiller la chemise, travailler, avancer, et leur regard a changé. »_ continue Juliette.

![Moulinage de Chirols](./article-chirols-4.jpg)

## La joie du collectif

Guy, Juliette, Alexandre, Anna, Jean-Philippe, Christelle…. tous s’accordent pour dire que la force de cette initiative réside dans le nombre de personnes impliquées. Cependant, comme pour toute initiative collective, dans le nombre réside aussi les obstacles. _« C’est difficile, parfois, de prendre des décisions à vingt. On a l’impression de parler la même langue, et puis, tout d’un coup, un même mot résonne différemment dans vingt têtes différentes. Des fois on a l’impression que ça n’avance pas, on a les bras ballants, on a envie de partir au loin… »_ raconte Guy. Loin d’une organisation hiérarchique descendante, les membres du Moulinage s’administrent en effet grâce au consentement et à des méthodes qu’ils se sont peu à peu appropriées, au fil de la pratique.

![Moulinage de Chirols](./article-chirols-5.jpg)

Le Moulinage ne se réduit cependant pas aux 23 porteurs de projet du « premier cercle ». Très vite, s’est constitué autour de l’ancienne usine un réseau de partenaires, au premier rang desquels figure l’équipe municipale très engagée. _« Il y a aussi des dizaines de bénévoles qui viennent nous aider sur le chantier. C’est très précieux. Désormais et pour toujours, ils sont ici chez eux »_ explique Juliette.

Architecte, ethnologue, comédienne, communicante, paysagiste, menuisier… Si bon nombre d’horizons différents convergent au Moulinage, une pratique les rassemble tous : la fête ! Chants, danses, improvisations, spectacles, soirée… la bonne humeur et la joie semblent envelopper le travail au quotidien. _« Il y a une générosité et une joie ici, qui invitent à s’engager totalement – et donnent le sentiment d’être à sa place. Vivre, avancer et apprendre ensemble sont plus important que l’objectif. Je sais pas encore combien de temps je resterai, mais je sais que j’aurai beaucoup grandi »_ poursuit Alexandre.

![Moulinage de Chirols](./article-chirols-6.jpg)

## Rester éveillé
À l’heure où l’on se demande lequel des gestes du quotidien ou de la lutte politique radicale permettra de sauver cette planète, chacun au Moulinage apporte sa réponse par l’action. _« Je suis artiste : je m’éclate dans mon travail, j’y mets tout le sens et l’engagement que je veux. Cependant, avec le Moulinage, j’atteins un autre niveau. J’ai le sentiment d’une action véritablement politique. Nous construisons un monde réel et viable fondé sur la liberté et l’engagement. Rien de plus, et pourtant, déjà, tout. »_ explique Juliette. Pour Guy, l’entreprise du Moulinage se joue également à un niveau intime. _« Tout ce qu’on vit ici est à la fois très doux et très violent. J’ai le sentiment de ne pas m’endormir. Et c’est une crainte que j’ai, de m’endormir… Avec ce projet, je reste éveillé. »_

<iframe src="https://www.podcastics.com/player/extended/978/34088/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

_Article écrit par Gabrielle Paoli et à paraître dans KAIZEN n° 49, mars/avril 2020. Le Moulinage de Chirols est financé et accompagné par la Coopérative Oasis._
