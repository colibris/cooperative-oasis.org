---
title: "Vie en collectif : prévenir la souffrance des individus"
date: "2021-04-13T06:40:32.169Z"
layout: post
type: blog
path: "/prevenir_souffrance/"
image: ./SaintAntoine1.jpg
categories:
  - tribunes
---

**En 1987, Margalida Reus a cofondé la [communauté de l’Arche de Saint-Antoine en Isère](https://cooperative-oasis.org/oasis/archesaintantoine/), à Saint-Antoine-l'Abbaye. Depuis plus de trente ans, elle y vit avec une quarantaine de personnes et y expérimente tous les jours la non-violence, outil de transformation de soi et de la société. À l’occasion du [Festival Oasis 2020](https://cooperative-oasis.org/revivrefestivaloasis2020/), Margalida a interpellé les collectifs sur leur capacité à accueillir et à gérer la souffrance des personnes, ainsi qu’à construire des lieux d’épanouissement individuel…**

![Arche de Saint Antoine](./SaintAntoine1.jpg)

Margalida rappelle qu’au sein des oasis existe une **tension permanente entre besoins du groupe et besoins de l’individu**. Cette tension est par définition un endroit de souffrance, de conflit mais aussi de créativité et d’épanouissement. Sans cette tension qu’il s’agit de rendre féconde, ni les personnes, ni les groupes ne peuvent grandir.

Pour Margalida, **la réussite d’une oasis dépend du bien-être individuel et collectif**. Ce bonheur semble conditionné par la **capacité des individus à avoir un certain sens de l’attention**, du soin, du don. D’autre part, le collectif doit **toujours faire passer l’épanouissement individuel avant le projet théorique**. Pour cela, le groupe doit pouvoir créer les conditions d’expression libre de chacun. “_Un saint triste est un triste saint_, rappelle Margalida. _Si les habitants d’une oasis ne sont pas heureux, le lieu tiendra peut-être debout mais aura échoué : un monde dans lequel la personne est opprimée par l’idéologie aura été reproduit._”

<iframe width="560" height="315" src="https://www.youtube.com/embed/c8Ph5-DiazA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
