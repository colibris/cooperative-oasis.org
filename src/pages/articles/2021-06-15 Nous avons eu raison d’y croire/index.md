---
title: "« Nous avons eu raison d’y croire ! » : le joyeux bilan des investisseurs de la Coopérative Oasis"
date: "2021-06-15T06:40:32.169Z"
layout: post
type: blog
path: "/raison-y-croire/"
image: ./epargne1.jpg
categories:
  - tribunes
---

**En trois ans et demi, le nombre de sociétaires de la Coopérative Oasis a été multiplié par dix. Les quelques investisseurs solidaires de la première heure ont été rejoints par des centaines d’autres, qui soutiennent tous les jours les oasis avec joie et détermination... [et si vous aussi, vous rejoigniez cette aventure unique en France ?](https://cooperative-oasis.org/investir/)**

![Raison d'y croire](./epargne1.jpg)

En janvier 2018, une trentaine de citoyens et l’association Colibris réunissaient 18 500 € pour créer [la Coopérative Oasis](https://cooperative-oasis.org/qui-sommes-nous/). Ces **aventuriers de la première heure** étaient convaincus qu’ils arriveraient à monter une structure capable de **collecter de l’épargne pour l’apporter temporairement à des écolieux** n’ayant pas accès à un prêt bancaire. Une première en France où les collectifs peinent encore à voir le jour malgré leur inestimable contribution sociale et environnementale.

## Une vingtaine d’oasis soutenues par près de 300 investisseurs
Moins de quatre ans plus tard, **l’espérance a porté ses fruits** et une fabuleuse histoire s’écrit un peu plus chaque jour... La **quatrième assemblée générale de la Coopérative Oasis**, qui a eu lieu le 9 juin 2021 à Oasis21, un tiers-lieu à Paris 19e, membre du réseau des oasis, a dressé un joyeux bilan :
 - De 31 en janvier 2018, la Coopérative Oasis est passée à 349 sociétaires en juin 2021 ;
 - De 18 500 € au départ, le capital est passé à 2 450 000 € aujourd’hui ;
 - 680 000 € ont été investis en plus sous d’autres formes que des parts sociales ;
 - 2,4 M€ ont été apportés à 19 oasis partout en France ;
 - Plusieurs dizaines d'accompagnements personnalisés ont été réalisés auprès des habitants ou porteurs de projets.

 ![Raison d'y croire](./Pepiniere.jpg)

## Les sociétaires renoncent aux dividendes pour les mettre au service des oasis

En plus d’accompagner des collectifs grâce à l’épargne solidaire, la Coopérative Oasis **anime le réseau des 1 000 écolieux**, notamment avec le soutien de l’Ademe. Gestion au quotidien de la [carte des oasis](https://cooperative-oasis.org/les-oasis/), organisation d’un [festival national pour 500 personnes](https://cooperative-oasis.org/revivrefestivaloasis2020/) vivant ou souhaitant vivre en oasis, lancement de [travaux de recherche sur l’impact social et la relation aux territoires](https://cooperative-oasis.org/vit-on-mieux/)… Autant d’activités qui contribuent au développement du réseau et à la bonne santé économique de la coopérative. Après une première année d’existence déficitaire, la coopérative a en effet réussi à générer quelques bénéfices plus tôt que prévu dans son existence.

![Raison d'y croire](./Evolution_resultat.JPG)
_Evolution du résultat (en orange les produits et en bleu els charges)_

Le 9 juin dernier, les **sociétaires ont de ce fait unanimement décidé que ces bénéfices devraient être systématiquement et intégralement réinvestis** au service des oasis plutôt que d’être redistribués aux sociétaires sous forme de dividendes.

En 2021, [quatre oasis ont déjà été soutenues](https://cooperative-oasis.org/deja-300-investisseurs/). Mais de **nombreux collectifs attendent encore un accompagnement essentiel** pour voir le jour. Créateurs de bien commun et de solidarité à l’échelle locale, ces lieux sont essentiels, [soutenez-les](https://cooperative-oasis.org/investir/) !

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Je rejoins l’aventure !
    </a>
</div>
</div>

_Article écrit par Gabrielle Paoli / Coopérative Oasis_
