---
title: "Le temps des oasis"
date: "2020-06-10T06:40:32.169Z"
layout: temps-oasis
type: blog
path: "/le-temps-des-oasis/"
image: ./temps-oasis-couv.png
categories:
  - histoires d'oasis
---

Depuis des années, des centaines de lieux développent des solutions efficaces pour habiter joyeusement cette planète tout en préservant les écosystèmes. En ville ou à la campagne, tous ont en commun de remettre le collectif, l’autonomie, la sobriété et la solidarité territoriale au coeur de nos vies.

À l’heure où la crise du coronavirus nous fait nous rendre compte qu’une autre trajectoire est possible, et qu’elle est désirable, ces oasis montrent que l’on peut bien vivre autrement.

Et si c’était pour vous ?
