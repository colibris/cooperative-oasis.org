---
title: L'oasis Grain&Sens soutenue par la Coopérative
date: "2019-02-01T22:40:32.169Z"
layout: post
type: blog
path: "/financement-grain-sens/"
image: ./grainetsens-signature.jpg
categories:
  - oasis financées
---

_Grain&Sens est un lieu de vie et de partage dont la volonté commune est de trouver une manière plus juste d'habiter, de cultiver la terre, d'éduquer, de se réaliser au sein d'un projet collectif. C'est sur le domaine de Lavenant, comprenant 15ha de prés et de bois, dont une majestueuse hêtraie, que les 7 adultes et 4 enfants on décidé d'établir leur rêve. En janvier 2019, la Coopérative Oasis a prêté 100 000€ issus de l'épargne citoyenne au projet afin d'acheter le domaine et d'accompagner le collectif pendant 7 ans autour, entre autres, de sa gouvernance. Entretien avec Romain, Astrid, Victoria et Jonah, 4ans..._

![Collectif Grain&Sens](./grainetsens-signature.jpg)


* * *
**D'où venez vous ?**

_**Romain**_ : J'ai grandi en région parisienne à Bures-sur-Yvette, j'ai fait mes études à Paris puis vécu à Bordeaux avant de partir faire un tour du monde en vélo à la rencontre des acteurs de l'agroécologie. Revenu en France j'ai passé 2ans à Montpellier à fonder des jardins partagés avant de m'installer en Ardèche... avec Grain&Sens.

_**Victoria** : [It is a long road ! Born in Texas, USA, grew up in Germany and England, studied in New Jersey and New York... After university my travel in India and Nepal brought me to farming in California and eventually to creating community gardens outside of Montpellier, France. Today when people ask me where I am from I say Boffres, Ardeche.]_ Je viens de loin ! Je suis née au Texas, j'ai grandi en Allemagne et en Angleterre, j'ai étudié dans le New Jerseay et à New-York... Après l'université, mon voyage en Inde et au Népal m'a conduit jusqu'à une ferme californienne. J'ai ensuite créé une communauté de jardins partagés près de Montpellier. Aujourd'hui, quand les gens me demandent d'où je viens, je réponds que je viens de Boffres, en Ardèche.

_**Astrid**_ : Je suis née à Saint-Etienne, j'y ai vécu jusqu'à l'âge de 20 ans et puis j'ai continué mes études en Angleterre. J'ai commencé à travailler dans un établissement financier américain en Angleterre puis en Suisse. La carrière s'annonçait bien avec beaucoup de "réussite", mais ce n'était pas "ma" définition de la réussite. J'ai donc quitté le milieu de la banque, l'appartement exécutif pour partir travailler en tant que bénévole dans une ONG au Népal. J'ai ensuite changé de carrière pour me lancer dans l'enseignement des langues. Je suis maintenant installée en Ardèche avec le collectif de Grain&Sens.

_**Jonah**_ : Je suis né dans la France.


**Un moment précis, une phrase, une vision ont-ils opéré comme un déclic, et vous ont poussé à véritablement vous lancer dans l'aventure ?**

_**Astrid**_ : Au Népal, en observant la façon de vivre des populations locales, dans la solidarité et le partage. Leur rapport avec une nature encore pure et sauvage. C'est certainement à ce moment là que j'ai "inconsciemment" eu un premier déclic. Le deuxième déclic conscient a été le moment de la fin de vie de ma grand-mère. C'était dans une maison de retraite. Ça m'a profondément touchée et rappelé à quel point on a besoin de changer notre façon de vivre en France.

_**Romain**_ : A Sadhana Forest à Auroville (Inde), une communauté de 100 à 200 bénévoles unis pour la reforestation de zones déboisées. Cette communauté rayonne d'une grande humanité, dans la solidarité, le partage et l'échange pour grandir ensemble, avec la forêt. C'est cette expérience qui m'a convaincu d'un possible en collectif.

_**Victoria** : [A farm in California, First Generation Farmers, that runs on volunteer power and feeds people by donation. Their table at the market states "Take what you need, pay what you can", making local organic food accesible for all. They also offer support in the form of land and training to young aspiring farmers who do not come from an agricultural background. FGF showed me that a young group of people can come together with a dream, create something beautiful and make a difference in the lives of many.  I don't have to be wiser or exceptional or inspirational... Every and anybody can make the world a better place. It is all about deciding where we contribute our energy.]_ "First Generation Farmers"; une ferme en Californie qui accueille des volontaires et nourrit les gens contre donations. Au marché, une pancarte indique : "Prenez ce dont vous avez besoin, payez ce que vous pouvez."  Ils rendent une nourriture locale biologique accessible à tous. Et ils accompagnent les néo-ruraux dans leur installation. FGF m'a montré qu'un groupe de personnes partageant un rêve pouvait créer quelque chose de beau et changer la vie de beaucoup de gens. Et pas besoin pour cela d'être sage, exceptionnel ou hors du commun. Chacun peut faire de cette terre un monde meilleur, il suffit de savoir où mettre son énergie.


**Pourquoi vivre en collectif ?**

_**Jonah**_ : Parce qu'on peut faire des trucs ensemble, et rigoler. Et aussi on peut jouer, des fois aller à la poste.

_**Victoria** : [Living in a collective confronts me with aspects of myself, my history and habits... it guides me in my personal development.]_ Vivre en collectif me renvoie à moi-même - mon histoire, mes façons de fonctionner... et cela me guide dans mon cheminement personnel.

_**Romain**_ :  Pour l'échange, le partage, le lien. Pour la sérendipité dans les rencontres et le lien avec la nature.

_**Astrid**_ : Pour créer notre propre façon de vivre. A plusieurs on multiplie nos forces et notre créativité.


**Comment vous êtes-vous rencontrés ?**

_**Romain**_ : A Saint-Bauzille-de-Montmel, une ville proche de Montpellier. C'est une langue qui unit nos familles : l'anglais. Astrid et Victoria ont démarré des activités de langues dans les jardins partagés que nous créions et Jonah et Elio allaient dans la même crèche bilingue. Un week-end, l'idée est venue à nous...

![Collectif Grain&Sens](./grainetsens-article2.jpg)

**Un premier souvenir vécu à Grain&Sens, qui vous fait vous dire que vous êtes au bon endroit ?**

_**Jonah**_ : Quand on a eu les moutons.

_**Astrid**_ : Le chantier participatif qui a permis la transformation d'une caravane en poulailler. C'était de beaux moments de partage et de bonheur - travailler ensemble dans la joie pour un but... commun.


**Ce que vous faites en ce moment et dont vous êtes fiers ?**

_**Romain**_ : Transmettre l'agroécologie aux woofers, aux visiteurs... et surtout à moi-même, en expérimentant chaque jour.

_**Astrid**_ : Transmettre l'envie d'apprendre l'anglais, donner l'enthousiasme de découvrir cette langue et une nouvelle culture. A la fin d'un camp, lorsque Victoria et moi-même, nous voyons que tous les participants ont acquis une confiance en eux dans la langue et qu'ils se sont régalés en apprenant l'anglais, nous avons tout gagné.


**Un problème rencontré, déjà réglé ou non ?**

Romain : La gouvernance partagée. Ça n'est pas évident à mettre en œuvre... Et nous avons toujours repoussé ce chantier, du fait de la priorité d'autres choses à faire, mais aussi parce que des mécanismes se sont déjà mis en place naturellement. Mais il va falloir nous y mettre...


**Quelle nature est autour de vous ?**

_**Astrid**_ : A 750m d'altitude, orientés plein Est, on voit les Alpes à l'horizon. Le ciel, les couleurs, la végétation change au fil des jours et des saisons pour le bonheur de nos yeux. On est entourés de forêts mxites, de prairies et d'animaux sauvages.

_**Romain**_ : Je me lève le matin sous les arbres avec vue sur les 3 becs et le Diois où le soleil se lève. Le lieu est entouré de hêtres centenaires, de châtaigniers vieillissant et de douglas perchés à la cime de la forêt. En bas, renards et blaireaux ont leurs terriers et à la sources les biches et chevreuils passent se ressourcer au printemps. Le soir, la lune se lève sur les Alpes, les chouettes hululent et en allant se coucher, Séva, 1 an et demi, lève les yeux au ciel, pointe son doigt et dit "Moon", "la Lune"!

**Une réussite en terme d'organisation, de fonctionnement de votre collectif ?**

_**Romain**_ : Depuis le début, l'ensemble du groupe, face à chaque défi, a conscience que la solution viendra par une transformation personnelle avant tout.

_**Astrid**_ : Notre communication : nos réunions sont fondées sur la communication non-violente et l'écoute.


**Qu'est-ce qui vous relie ?**

_**Romain**_ : L'ouverture au monde via l'anglais et nos expériences à l'étranger ainsi que notre besoin de lien à la nature qui donne sens à notre projet chaque jour.

_**Astrid**_ : Notre envie d'apprendre. Notre esprit d'aventure.

**Qu'est-ce qui fait votre diversité ?**

_**Astrid**_ : Notre multiculturalisme. Nous sommes d'origines anglaise, allemande, américaine, française, italienne. On a des vécus différents, des parcours différents (ingénieurs, artistes, scientifiques, linguistes..)... et il nous semble que c'est une force.


**Comment caractériseriez-vous le processus engagé auprès de la coopérative oasis, comment l'avez-vous vécu ?**

_**Romain**_ : Comme à beaucoup de moments dans la création du projet, dans la proactivité vers ce que nous désirions, le lien a la Coopérative s'est fait naturellement, elle est venue à nous. Ce fut un formidable soutien pour défendre la crédibilité du projet auprès des collectivités, des banques et des acteurs locaux. Ce qui a participé au rayonnement du projet dès son montage.

**À quoi va servir l'argent prêté par la coopérative ? Et quel sera l'accompagnement prodigué par cette coopérative ?**

_**Romain**_ : La somme prêtée à Grain&Sens va permettre de financer le lancement des activités du lieux (boulange paysanne, jardins en agroécologie, camps d'immersion en anglais, accueil de stage et de formations) par l'achat du mobilier et du matériel agricole ainsi que les travaux de remise aux normes du lieu. Une partie de la somme sera également gardée en trésorerie afin de palier aux éventuels aléas du lancement les premières années. En outre, la coopérative nous accompagnera sur les domaines que l'on doit encore travailler : notre gouvernance, notre design en permaculture, notre modèle économique...

**Et demain ?**

_**Astrid**_ : Demain, Grain&Sens aura acquis une belle expérience dans le monde du collectif et dans la transition écologique. Nous pourrons accueillir de nouvelles familles, peut-être inspirer de nouveaux projets à se lancer dans l'aventure.

_**Romain**_ : Demain se fera en intelligence collective, vers une écologie où l'humain trouve sa juste place. C'est un lieu vers cette transition que nous voulons proposer comme possible à la société.
