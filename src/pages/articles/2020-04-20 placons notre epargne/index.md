---
title: "Réinventons le monde, plaçons notre épargne !"
date: "2020-04-20T06:40:32.169Z"
layout: post
type: blog
path: "/placons-notre-epargne/"
image: ./epargne4.jpg
categories:
  - tribunes
---

_Une tribune de Gabrielle Paoli, coordinatrice du réseau des Oasis._

« C’était un printemps incertain » Les Années, Virginia Woolf, 1937

Il existe aujourd’hui en France des lieux un peu magiques. Je le sais, cela fait plus de trois ans que je les arpente. Pour ce qui est et pour ce qui vient, ces lieux sont à la fois des refuges, et des lanternes.

La Coopérative Oasis les accompagne depuis plus de deux ans en leur octroyant les prêts que les banques traditionnelles leur refusent. Pour apporter cet essentiel soutien, la Coopérative a besoin que des citoyens mettent à disposition l’épargne dont ils ne se servent pas. Car il est possible que nos économies fertilisent les sols et les imaginaires. Et aujourd’hui plus que jamais, cela est même nécessaire.

## Les oasis, une constellation de royaumes écologiques en France

![Epargne](./epargne1.jpg)_Kandinsky, "Plusieurs cercles, 1926. "Pour ce qui est et pour ce qui vient, ces oasis sont à la fois des refuges, et des lanternes"_

Ces lieux soutenus par la Coopérative Oasis portent les noms qu’ils veulent bien se donner : oasis, écohameau, écovillage, habitat participatif, ferme collective, communauté… Aujourd’hui, à l’heure où le monde s’arrête, ils sont près de mille à s’être regroupés dans un même réseau. Urbains, ruraux ou périurbains, lieux d’habitat et / ou de production, allant de 1 à 30 foyers vivant sur place… Ce qui lie ces royaumes autonomes et si différents les uns des autres, ce sont des valeurs communes, dont les dénominations même sonnent comme de petits trésors : l’autonomie, le collectif, la résilience, la solidarité.

Des valeurs communes oui, mais pour des incarnations différentes. Chaque oasis donne corps à l’autonomie, au collectif ou à la solidarité à sa façon, en fonction de la sensibilité des personnes qui la constituent, de la terre et du vent qui la bercent, des dynamiques propres à chacune. Et cela donne une constellation bigarrée et réjouissante...

![Epargne](./epargne2.jpg)_"À l’Arche Saint-Antoine (Isère), j’ai découvert une communauté de 40 personnes rénover une ancienne abbaye abandonnée depuis 14 ans"_

## Des lieux producteurs de biens communs

Depuis plus de trois ans que j’anime le réseau de ces écolieux pour Colibris et désormais pour la Coopérative, j’ai la chance d’aller les voir souvent, de les arpenter. Et je mesure ce qu’ils apportent.

À la Ressourcerie du Pont (Gard), j’ai vu un groupe d'artistes militants racheter une usine de 3 500m2 et la dédier à la collecte, à la revalorisation et la revente d’objets. À la clef : 6 emplois créés sur une commune sinistrée par le chômage et plus d’une cinquantaine de tonnes de déchets recyclés par an.

![Epargne](./epargne3.jpg)_"À la Ressourcerie du Pont (Gard), j’ai vu un groupe d'artistes militants racheter une usine de 3 500m2 et la dédier à la collecte, à la revalorisation et la revente d’objets."_

À l’Arche Saint-Antoine (Isère), j’ai découvert une communauté de 40 personnes rénover une ancienne abbaye abandonnée depuis 14 ans et la transformer en une des maisons d’accueil les plus importantes de l’Isère, avec un passage d’à peu près 4 000 personnes par an. Le tout au service de la transmission de son expérience du vivre-ensemble et des valeurs de non-violence portées par Lanza del Vasto.

À Grain&Sens (Ardèche), quatre familles ont pu sauver 15 ha de forêt de la vente à un exploitant de bois et maintenir de l'agriculture biologique sur 5 ha de prairie cultivable. Ils ont également su mettre cette nature à disposition d'enfants du monde entier à l'occasion d'écocamps franco-anglais.

![Epargne](./epargne4.jpg)_À Grain&Sens (Ardèche), quatre familles ont su mettre une nature préservée à disposition d'enfants du monde entier à l'occasion d'écocamps franco-anglais."(Crédit [Ed Carr](https://edcarr.com))_

Au-delà de ce que chaque lieu apporte à ses membres et à son territoire, il contribue aussi à réduire notre empreinte climatique. En 2016, Colibris et Carbone 4[1] ont réalisé une étude sur l’empreinte carbone d’un habitant d’Oasis. Le résultat est clair : un habitant d’oasis émet deux fois moins de gaz à effet de serre qu’un Français moyen. La construction écologique des habitations, la mutualisation des espaces et des outils, ou encore un mode de vie plus sobre sont les causes de ces émissions réduites.

## Les oasis ne suffisent pas mais sont nécessaires : elles sont des refuges et des lanternes pour demain

Depuis le confinement lié à l’épidémie de COVID19, je ne vais plus voir les lieux, je les appelle. Comme on le prévoyait, la résilience, la solidarité et le soin leur permettent de traverser cette crise dans des conditions souvent privilégiées, avec nature, espace et approvisionnement alimentaire partiellement autonome. Comme on s’en doutait aussi, cette crise d'ampleur révèle des fragilités insoupçonnées, qu’il faudra régler [2].

![Epargne](./epargne5.jpg)_La Ferme de Chenèvre, une oasis d'une vingtaine de personnes dans le Jura (Crédit Fabrice Tendrevet)_

Tous ces lieux sont un peu magiques donc. Mais ils ne sont pas la solution miracle. D’abord parce qu’ils portent leurs maux – tensions relationnelles, fatigue, difficile intégration dans le territoire qui les accueillent, fragile équilibre économique. Comme dans la vie, il y a dans les oasis des difficultés, parfois insurmontables. Ensuite parce que vivre ainsi n’est pas fait pour tout le monde, et c'est tant mieux, la diversité est mère de richesse ! Enfin, parce que le monde est complexe et la crise écologique systémique : la réponse ne saurait être simple et uniforme.

Cependant, ces lieux font incontestablement partie des solutions . Ils inventent des nouveaux modes de production, de coopération, d’animation des villes et villages, en prenant aussi en compte les besoins non humains. Et surtout, quels que soient leurs contours et les gens qui les portent, ces oasis prennent soin - de la terre, des végétaux, des animaux, des hommes.

Ils sont à la fois des refuges pour le présent et des lanternes pour l'avenir.

![Epargne](./epargne6.jpg)_Chaque année, toutes les oasis se rencontrent pour un festival annuel, le festival oasis, ici en 2019 au Château de Jambville.(Crédit [Clément Osé](http://www.clementose.art/))_

Parsemer la France d’oasis ne nous abstiendra pas de mener la lutte politique à l’échelle nationale pour changer les lois communes, de soutenir les plus fragiles jour après jour, de prolonger une réflexion scientifique à l’échelle mondiale, de prier. Mais parsemer la France d’oasis participera précieusement à l’inévitable mise en place d’un monde habitable, à la sortie de cette crise du COVID-19.

## Financer le monde d'après

Chacun n’a pas vocation à habiter en écolieu donc, mais chacun peut participer au développement de cette partie de la solution. Comment ? En mettant à disposition de ces oasis l'épargne qui dort à la banque où elle sert à on-ne-sait-quoi et peut être exposée à des pertes liées aux aléas de la spéculation boursière.

La démarche est simple : n'importe quel citoyen ou citoyenne peut investir en prenant des parts dans la Coopérative, allant de 1 000€ à... autant qu’il ou elle veut ! Cet investissement peut être récupéré à tout moment. Parce que cet investissement est reconnu d'utilité public par l'État (note : agrément ESUS), tout investisseur peut bénéficier d’une déduction de 18 % de la somme investie sur son impôt sur le revenu, à condition qu'il conserve ses parts durant 7 années au moins.

![Epargne](./epargne7.jpg)_Aux Sens de Theus (Hautes-Alpes), Pauline et Jean-Pascal ont bénéficié d'un prêt de 50 000€ pour l'achat essentiel d'un alambic._

## Concrètement, à quoi servira votre épargne ?

Pendant qu'il est dans la Coopérative, nous prêtons cet argent à des collectifs pour qu’ils achètent un terrain, fassent des travaux, remboursent une famille qui s’en va et la remplacent… En plus de deux ans, nous avons collecté 1M d’euros et prêté à 9 projets.

Grain&Sens, cette joyeuse tribu ardéchoise j'ai déjà évoquée plus haut, a par exemple bénéficié en janvier 2019 d'un prêt de 100 000€ sur sept ans. Sans ce soutien, ils n'auraient probablement pas pu acheter le lieu ni donner jamais à leurs rêves une réalité concrète. Nous les accompagnons en outre sur la structuration de leur modèle économique et l'organisation de leur collectif. Nous avons aussi également mis à leur disposition notre réseau pour leur permettre d'organiser une formation en permaculture sur leur lieu avec Warren Brush... Ils ont ainsi pu étoffer leur cercle de sympathisants, prolonger l'aménagement de leurs espaces et écouter les histoires du plus enchanteur des permaculteurs américains...    

![Epargne](./epargne8.jpg)_Le Moulinage de Chirols s'est vu accorder un prêt pour financer les travaux de réhabilitation de l'ancienne usine déstinée à devenir transformée en écolieu_

Beaucoup d'autres projets attendent encore, qui n'existeront peut-être jamais sans le soutien de chacun et chacune d'entre nous.

Aujourd’hui, nous parlons du monde de l’après pandémie. Il sera probablement celui des bouleversements économiques, de la souffrance sociale et des difficultés à s’en sortir. Il sera aussi probablement celui de la réinvention personnelle et collective. Ces oasis sont une partie de cette réinvention du monde. Nous sommes convaincus qu’il faut, plus que jamais, mettre les moyens disponibles à leur service.

Ainsi, si vous, vos amis, vos proches ont la sécurité matérielle qui vous et leur permet demain de conserver des économies, c'est aujourd'hui plus que jamais que nous avons besoin de votre contribution. Ne les laissez pas nourrir le système bancaire qui nous a, en partie, mené dans cette galère. Donnez à votre épargne – est-ce possible ? – le pouvoir de réinventer le monde.

Et je continuerai, moi, à vous raconter des histoires d'oasis...
