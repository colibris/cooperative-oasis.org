---
title: "L’écohameau nous a permis de recréer une vie de village"
date: "2021-03-11T06:40:32.169Z"
layout: post
type: blog
path: "/Verfeil-recreer-village/"
image: ./Verfeil0.jpg
categories:
  - histoires d'oasis
---

_15 adultes et 12 enfants vivent aujourd’hui dans un [écohameau situé à Verfeil-sur-Seye](http://verfeil-eco.over-blog.org/), une commune de 400 habitants dans le Tarn-et-Garonne. Les maisons bioclimatiques ont toutes été en partie construites par les membres de ce collectif passionné d'éco-construction et de bœufs musicaux.._

![Verfeil](./Verfeil0.jpg)Crédit photo Mathieu Hoffmann

## “Ni obligation, ni interdit”
Lorsqu’on a déjà eu l’occasion de se promener dans l’allée centrale de l’écohameau de Verfeil, on ne s’étonne pas qu’il se définisse comme « un lieu de vie et d’échange recherchant l’équilibre entre l’humain et la nature, l’individuel et le collectif ». Un vent de liberté souffle bel et bien sur ce collectif de 15 adultes et 12 enfants installé en plein cœur du Tarn-et-Garonne.

_« À part quelques règles sur les animaux domestiques, il n’y a ni obligation ni interdit ici »_ explique Elsa. Les habitants participent aux tâches communes (réunions, travaux en extérieur, journée collective du samedi…) par plaisir ou parce qu’ils s’en sentent responsables. _« L’équilibre entre tous les aspects de nos vies est fondamental ; si le groupe évolue au détriment des individus, ça ne fonctionnera pas, l’inverse non plus. Et puis… l’écologie joyeuse, c’est pas que des mots : on est aussi ici pour être heureux ! »_ précise Elsa. En témoignent les nombreux événements festifs, organisés ou spontanés, comme les soirées de bœufs musicaux et la chorale du lundi – pour ceux qui veulent !

![Verfeil](./Verfeil1.png)

## Un collectif en mouvement

Il ne reste aujourd’hui personne du groupe d'éco-constructeurs d’origine, qui avait conçu un projet d’écovillage en zone rurale et l’avait soumis à la commune de Verfeil, enthousiaste. En 2010, un deuxième collectif avait finalement acheté le terrain avec ses fonds propres ; seuls deux foyers de ce deuxième groupe habitent aujourd’hui sur place.

Parmi les 15 adultes habitant à Verfeil, on trouve des métiers aussi différents les uns des autres que professeur de piano, chant et français, traductrice, créatrice de bijoux, cadre en Mairie, puéricultrice, écoconstructeur, restaurateur d’objets d’art… La plupart d’entre eux ont en effet gardé un métier à l’extérieur, l’exercent parfois en temps partiel, afin d’avoir du temps à consacrer aux activités du lieu.

Ce collectif bigarré a fait le choix d’une gouvernance collective exigeante et intégrée : la totalité des décisions liées au lieu sont prises au consentement au cours du cercle hebdomadaire. _« Nous n’avons jamais eu besoin d’utiliser la majorité au 2/3 prévue en cas de blocage, ça fonctionne bien »_ explique Sarah. Cette gestion intégrée des parties communes est contrebalancée par une grande liberté laissée à chaque foyer dans la construction et la gestion de sa maison individuelle, grâce au modèle juridique traditionnel de copropriété choisi : l’ASL.

![Verfeil](./Verfeil4.png)

## L’écoconstruction en commun

Libre donc à chaque foyer de choisir son mode de construction et son esthétique, mais toujours dans le cadre d’une charte constructive écologique commune : ossature bois, isolation paille et enduit terre, matériaux biosourcés et locaux. « L’écoconstruction était vraiment à la base du projet » explique Stéphan, constructeur membre du [Réseau français de la construction terre paille](https://www.rfcp.fr/). Il est le seul de l’écohameau à avoir construit sa maison seul, de A à Z. Les huit autres maisons ont été soit réalisées en « autoconstruction accompagnée », mixant autoconstruction et artisans, soit complètement construites par des artisans.

Avoir une maison au sein de l’écohameau de Verfeil demande un investissement pour l’achat du terrain (35 000€ avec les frais de notaire) et la construction de la maison (entre 300€ et 1000€ du m2 pour une maison bioclimatique). Une fois cet investissement de départ effectué, les foyers vivent souvent avec peu de dépenses du fait des faibles consommations énergétiques des bâtiments bioclimatiques. Deux stères de bois grand maximum sont nécessaires pour le chauffage d’un foyer, soit moins de 150€ par an pour un grand confort d’habitat. À Verfeil, la sobriété énergétique va de pair avec les économies !

![Verfeil](./Verfeil3.png)

## Bienfaits et difficultés de la vie collective

Le défi principal de l’écohameau n’est pas technique mais bien relationnel. _« On a traversé des conflits,_ raconte Elsa. _On n’en est pas toujours sortis indemnes, il y a eu de la casse individuelle et collective, il faut le dire. Mais le groupe a beaucoup évolué grâce à ces ruptures. À titre personnel, ça m’a appris à gérer des situations que j’avais tendance à éviter, c’est aussi très enrichissant. »_

Pour Alice aussi, le collectif coûte et apporte en même temps. _« Je me suis installée en collectif pour ne pas vieillir seule... et pour vivre dans une maison écologique, raconte Alice, la doyenne de l’écohameau. Mais j’ai vite découvert que c’était beaucoup plus que ça, que c’était une expérience de vie. Il y a eu des moments très difficiles pour moi. Le collectif m’a poussée à me remettre en question profondément, j’en ai souffert. Maintenant que j’ai passé ces paliers, je suis très heureuse d’avoir fait ce choix. »_

![Verfeil](./Verfeil2.png)

## Recréer la vie de village

Mère de deux enfants, Sarah a pris la décision de s’installer en collectif suite au décès de son mari. _« Au début, j’ai essayé de continuer comme avant – même travail, même maison, même train de vie… mais je n’ai pas tenu longtemps. J’avais besoin de changer et le collectif me donnait l’impression de pouvoir être épaulée tout en donnant en échange. Et puis j’avais besoin d’un projet fou dans lequel mettre mon énergie. Alors j’ai tapé « écovillage France » sur internet et je suis tombée sur le blog de Verfeil. Une semaine plus tard, je participais à la journée d’accueil. »_

Les enfants de Sarah ont donc rejoint la dizaine d’autres enfants habitant déjà sur place.
Tous vont à l’école ou au collège à l’extérieur et partagent leur quotidien dans l’écohameau. _“Les enfants ont une vie de village : ils explorent le lieu, passent de maison en maison, grandissent avec bien plus d’enfants que leurs frères et soeurs, sont entourés de nombreux adultes… et tout cela dans une sécurité totale”_ explique Elsa. Bien sûr, chaque parent éduque un peu différemment, sur la question des écrans par exemple. _“On a appris à en discuter entre nous et à trouver des solutions, raconte Sarah. Aujourd’hui, j’ai une confiance totale quand mes enfants sont chez d’autres adultes qui font à leur façon. C’est aussi ça la vie de village, la découverte de la diversité…”_


_Article écrit par Gabrielle Paoli / Coopérative Oasis et publié dans le numéro papier de Kaizen de mai/juin 2021_
