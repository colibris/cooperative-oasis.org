---
title: "Quand une oasis réinvente l'herboristerie"
date: "2019-11-28T10:40:32.169Z"
layout: post
type: blog
path: "/oasis-reinvente-herboristerie/"
image: ./senstheus-3.jpg
categories:
  - oasis financées
---

_Article initialement publié sur le magazine de Colibris._

Les Sens de Theus est une ferme en agriculture biologique située dans la Hautes-Alpes. Pauline et Jean-Pascal, paysans herboristes, y produisent et transforment ensemble des plantes à parfums, aromatiques et médicinales. Le lieu est également un lieu de vie, d’échange, et de formation pour tous les passionnés de plantes. En juin 2019, le couple a emprunté à la Coopérative Oasis 50 000€ sur 1 an sans taux d'intérêt. Cette somme leur a permis d'acheter une chaudière et un alambic nécessaires à leur activité, en attendant le remboursement de ceux-ci par une subvention européenne...

![Pauline et Jean-Pascal](./senstheus-1.jpg)

## Du ski de fond aux herbes aromatiques

Pauline est ingénieure agronome, Jean-Pascal plombier de formation. Tous deux pratiquent le ski de fond de haut niveau. En 2015, un peu lassés de leurs métiers respectifs et aspirant à une vie plus sobre et proche de la montagne qu’ils connaissent bien et aiment intensément, Pauline et Jean-Pascal décident de changer de vie. Elle, quitte son poste de chargée de projet faune et flore à la SNCF ; lui, arrête de travailler comme plombier pendant la période estivale mais garde son métier de technicien d’une équipe de ski américaine.

"Les plantes nous ont toujours beaucoup attirés, mais on ne pensait pas qu’une ferme de plantes aromatiques et médicinales était viable. On s’est donc d'abord orientés vers le maraichage. Cependant, au fil de nos visites et de nos rencontres, nous avons changé d'avis et décidé de tenter le coup. En plus des formations que nous avons faites, nous sommes donc allés travailler 6 mois dans une ferme de plantes médicinales avant de nous lancer » explique Pauline. Très vite, ils se rendent compte que ce domaine correspond pleinement à leurs attentes tant en terme de lien à la nature que de stimulation intellectuelle.

Un peu désarmé par les prix exorbitants des terres en Haute-Savoie, le couple y cherche pourtant un lieu pendant 2 ans. Saisis par l’opportunité d’un terrain de 3,5ha avec une petite maison, mis en vente juste à côté de la maison de famille de Pauline dans les Hautes-Alpes, ils décident finalement de changer de région et de l’acquérir.

Fin 2017, le couple s’installe sur son nouveau lieu de vie et d’activité qu’il appelle « Les Sens de Theus ».

![Jean-Pascal](./senstheus-2.jpg)

## Le soin des gens et de leur intimité

Aujourd’hui, la ferme « Les sens de Théus » produit et transforme des plantes aromatiques et médicinales. Le cœur de l’activité de ces « paysans herboristes » est la « phytothérapie » - le soin par les plantes. Celle-ci consistent en huiles essentielles, eaux florales, teintures mères (préparations traditionnelles, base de la pharmacopée traditionnelles) et gemmothérapie (le soin par les bourgeons de plantes). Pauline et Jean-Pascal produisent aussi des tisanes et des cosmétiques.

Certaines plantes sont cueillies dans la nature, d’autres sont cultivées en agriculture biologique. Pour obtenir des plantes ayant des propriétés puissantes, chaque étape est fondamentale et nécessite une maîtrise d’un processus propre à chaque variété - la culture, la cueillette, la transformation, le stockage, le transport et la diffusion…

![Pauline](./senstheus-3.jpg)


« Mais l'herboristerie ne se résume pas à la biologie pure. Émotions, énergies, philosophie… les plantes parlent des hommes » raconte Pauline. « En vendant ces produits, on entre dans l’intimité des gens. Ils viennent nous voir avec leurs enjeux physiques et émotionnels du moment. », renchérit Jean-Pascal.

Pour cette raison, les « Sens de Theus » ne se limite pas à être un lieu de production ; formations et accueil de gîte prendront bientôt place à la ferme.

![En pleine montage](./senstheus-4.jpg)


## Herboriste : un métier interdit et un savoir traditionnel perdu

Depuis 1941, la loi interdit de parler de plantes à tous ceux qui ne sont pas détenteurs d’un diplôme de pharmacien. Devenue illégale pour beaucoup de paysans, la transmission de ce savoir-faire, traditionnellement orale, s’est donc largement perdue en deux générations. La figure du docteur éduqué venu de la ville s’est imposée face à celle de la femme paysanne dotée d’un savoir populaire.

« Nous sommes officiellement « exploitant agricole, producteur et transformateur de plantes à parfum, aromatiques et médicinales ». Et même si, dans notre cœur, nous sommes « paysan herboriste », nous n’avons officiellement pas le droit de vendre certaines plantes ni celui de parler des propriétés des plantes » explique Jean-Pascal.

Comme il n’existe pas de cursus clair pour apprendre la connaissance médicinale des plantes, Jean-Pascal et Pauline sont donc allés à la rencontre de ceux qui font encore ce travail en France pour apprendre « sur le tas ». Afin d’obtenir une forme de reconnaissance de son métier, et malgré le fait qu'il ne soit pas officiellement reconnu, Pauline a également décidé de passer le diplôme d’herbaliste à l’Ecole Lyonnaise de Plantes Médicinales. Aujourd'hui, le couple s’entoure d’un réseau de professionnels naturopathes et docteurs en phytothérapie pour échanger sur leur métier. « On n’a jamais fini d’apprendre. On travaille avec à peu près 90 plantes différentes… ! » s’exclame Pauline.

![Pauline](./senstheus-5.jpg)


## L’achat d’un alambic grâce à la Coopérative Oasis

« En 2019, nous avons obtenu une subvention de l’Europe de 50 000€ pour acheter la chaudière et l’alambic nécessaires à la distillation. Comme l’argent n’est versé que sur présentation des factures, la Coopérative Oasis nous a avancé la somme grâce à un prêt à taux zéro sur un an. » Jusqu’alors, le couple allait chez un distillateur à 1h15 de la ferme ; cet investissement permet donc d’ancrer l’activité dans le lieu.

En plus du prêt de 50 000€, la Coopérative propose chaque année de remboursement un accompagnement payant sur les enjeux propres au projet. En l’occurrence : une structuration du modèle économique de l’espace de formation, un soutien à la définition d’une stratégie de communication et une aide à l’aménagement du terrain. « On n’est que deux sur le projet et ça fait du bien d’avoir un point de vue extérieur et un soutien technique. La Coopérative va notamment nous aider à monter une formation en permaculture sur le site de la ferme au printemps 2020. » précise Pauline.

![Vente sur un marché](./senstheus-6.jpg)


## Nous les plantes

On a demandé à Pauline quelle était sa plante préférée… « Il y a un arbre que j’aime bien : le mélèze. Il ne pousse qu’en altitude dans les zones montagneuses, à des endroits lumineux. C’est le seul résineux qui perd ses feuilles en hiver. Il a des aiguilles mais elles sont toutes douces. L’odeur de l’huile essentielle qu’on en tire me donne l’impression d’être chez moi, de me sentir bien. Juste au-dessus de la ferme, il y a justement une forêt de mélèze où je vais souvent pour me ressourcer. »
