---
title: "Rencontré régionale oasis au Lib'Air'Terre, en Vendée"
date: "2020-11-14T06:40:32.169Z"
layout: post
type: blog
path: "/rencontrelibertaire/"
image: ./LibertaireCharlotte.jpg
categories:
  - événements
---

**Monté en 2007 par Jacques Grolleau, le [Lib'Air'Terre](https://www.libairterre.fr/) est une oasis ressources située au village de La Petite Brenonnière, en Vendée. Une association fait vivre le lieu en y faisant découvrir sa nature exceptionnelle. Le 17 octobre 2020, une cinquantaine de personnes s'y sont réunies pour parler d'oasis...**

![Libertaire A](./LibertaireCharlotte.jpg)

Quelques personnes, dont Jacques, le fondateur, habitent à temps plein sur le lieu. Charlotte, Felix et quelques voisins viennent régulièrement donner des coups de mains. Ils ont préparé le chantier participatif et la balade de la journée découverte !

![Libertaire B](./Libertairelogo.jpg)

Le Lib'Air'Terre se veut intergénérationnel et écologique. Des ateliers et des stages d’initiation y sont proposés pour transmettre des savoirs comme l'écoconstruction, la fabrication de produits d’entretien et d’hygiène, le "zéro déchets", la communication non violente...


<iframe width="560" height="315" src="https://www.youtube.com/embed/mmClIrE2zgw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

La journée a commencé par une présentation du lieu par Jacques. Antoine Jeanneret a ensuite présenté le [Rêve d'Èvre](https://www.facebook.com/revedevre/), une oasis de vie voisine, pour donner à voir la diversité des lieux du territoire vendéen. Plusieurs collectifs étaient présents : ces rencontres sont une occasion de créer du lien entre écolieux...


![Libertaire C](./LibertaireBuffet.jpg)

Après une balade au coeur des 6ha du lieu, un repas partagé a permis aux participants de se rencontrer autour d'un verre de vin et de parler de leurs envies.


![Libertaire D](./LibertaireDebat.jpg)

Par un temps de débat en cercle, le groupe a réfléchi ensemble aux différentes solutions possibles pour favoriser l'émergence de ces projets alternatifs en Vendée. Parmi les propositions, une liste de diffusion qui permettrait de mobiliser des bénévoles allant de lieux en lieux en fonction des besoins...


![Libertaire Chantier](./LibertaireChantier.jpg)

Pour passer à une dimension plus physique et faire ensemble après avoir réfléchi ensemble, toutes et tous ont participé à un chantier au jardin pour construire un mandala.

![Libertaire Mandala](./LibertaireMandala.jpg)

Le Lib'Air'Terre garde ainsi la trace du coup de main de Vendéens prêts à faire leur part pour permettre aux oasis de fleurir !
