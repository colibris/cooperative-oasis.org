---
title: "Le Domaine des Possibles : Ils ne savaient pas que c’était impossible, alors ils l’ont fait"
date: "2020-02-28T06:40:32.169Z"
layout: post
type: blog
path: "/domaine-possibles/"
image: ./domaine1.jpg
categories:
  - oasis financées
---

_Article écrit par Gabrielle Paoli (administratrice de la Coopérative) et à paraître dans KAIZEN n° 50, mai / juin 2020. Le Domaine des possibles est financé et accompagné par la Coopérative Oasis._

En une année, un collectif d’une dizaine de personnes s’est constitué, a acheté et s’est installé sur un terrain de 9ha situé à 10km de Clermont-Ferrand, sur le lieu-dit Ternant (800 habitants). Ils y ont mis en place un habitat collectif solidaire, le Domaine des Possibles, et prévoient de développer de multiples activités : maraîchage, artisanat, accueil touristique et formations… Leur plus grande fierté : avoir rendu leur rêve possible.

![Domaine des possibles](./domaine1.jpg)

## 9 foyers réunis autour d’un lieu exceptionnel

“Il y a 6 mois, tout le monde nous disait qu’on n’y arriverait jamais. Aujourd’hui, nous sommes 13 adultes et 7 enfants à vivre sur le “Domaine des Possibles”... qui ne s’appelle pas ainsi par hasard !” commente Alain, habitant du lieu. Dans le hall d’entrée, ils ont d’ailleurs affiché, comme une devise, la citation de Mark Twain : “Ils ne savaient pas que c’était impossible, alors ils l’ont fait.”

En décembre 2018, Valérie-Agathe rencontre les créateurs de La Semblada, un habitat participatif de 9 foyers à Clermont-Ferrand. Inspirée par leur expérience qui résonne avec un rêve qu’elle porte en elle depuis longtemps, elle crée un groupe facebook pour fédérer toutes les personnes souhaitant vivre en collectif dans la région. L’association locale Habiter Autrement et une première visite du lieu en février 2019 lancent la constitution du collectif. Un an jour pour jour après la première publication sur le groupe facebook, le 13 décembre 2019, le collectif de 9 foyers signe l’acte d’achat du lieu et pour certains, y posent leurs bagages le soir même. L’oasis est née.

![Domaine des possibles](./domaine2.jpg)

Situé à 10km de Clermont-Ferrand, le “Domaine des Possibles” s’étend sur 4 ha de prairie et 5ha de bois. Une maison de maître de 750m2 sur 3 niveaux déjà habitable côtoie deux anciens corps de ferme de 250m2 à rénover. “Les anciens propriétaires avaient acquis le lieu 100 ans plus tôt, en décembre 1919. raconte Valérie. Ils nous ont invités à leur soirée d’adieu au domaine et nous ont demandé de présenter notre projet à leur famille et à des  habitants du village. Tous étaient très heureux que ce domaine qu'ils aimaient devienne un lieu d'expérimentation d'un nouveau vivre ensemble plutôt que la proie d'un promoteur immobilier...” Ancrer le lieu dans l’écosystème local et le penser avec les habitants du territoire est en effet une envie fondamentale du groupe.

## La solidarité au cœur du projet

“La solidarité était une notion clef pour chacun d’entre nous. Nous l’avons donc déclinée sur plusieurs plans” explique Alain. Dans le financement d’abord : chaque foyer devait faire un apport de 130 000€. Pour permettre à tous d’intégrer le projet, la SCI a contracté un emprunt pour ceux qui n’avaient pas accès aux banques. Ces derniers se sont engagés à rembourser au fil du temps à la SCI directement. Quant aux logements, ils ont été distribués en fonction des besoins de chacun, et non des moyens : plus le foyer est nombreux, plus l’appartement est grand, avec la règle d’une chambre par enfant.

![Domaine des possibles](./domaine3.jpg)

Enfin, un loyer symbolique de 60€ est demandé chaque mois à chaque foyer. Il est prévu que le reste des charges soit, à l’avenir, payé directement par les revenus générés par l’activité d’accueil touristique du lieu, composé de 3 chambres d’hôtes et d’un gîte de 15 personnes. En échange, chacun participera bénévolement à faire tourner cette activité. L’objectif : éviter aux personnes ayant un crédit de devoir, en plus, payer de lourdes charges. La Coopérative Oasis conseille le lieu sur la structuration de ces activités et l'organisation juridique des différents projets au Domaine.

Être solidaire a été d’autant plus difficile que les banques n’ont pas souhaité accompagner le projet. “Ils n’ont pas su ni voulu comprendre notre montage collectif, qui sortait de leurs standards. Ça a finalement marché parce qu’on a fait jouer des réseaux de connaissances...” explique Alain. La Coopérative Oasis, structure de financement citoyen, a également participé à un prêt relai pour libérer les fonds dans un délai très court et elle accompagnera le groupe pendant quelques mois.

![Domaine des possibles](./domaine4.jpg)

## Autant d’activités que d’envies

Les 13 adultes membres du projet, âgés de 30 à 60 ans, ont des parcours atypiques et divers - médiatrice culturelle, ingénieur, monteuse vidéo, metteur en scène, psychologue, artisan, paysagiste… Et si certains ont décidé de garder leur emploi à l’extérieur, plusieurs ont pour projet de développer une activité économique sur le lieu. “On a voulu que le Domaine puisse permettre à chacun de réaliser ses désirs” raconte Alain. Formation en aromathérapie pour Caroline, ferme pédagogique pour Fred et Patou, maraîchage en traction animale pour Raphaël, école alternative et festival culturel pour Valérie, éducation à l'environnement et au zéro déchet pour Pauline… les projets ne manquent pas !

Valérie-Agathe, que les anciens propriétaires surnommaient “l’utopie radieuse”, conclut : ”Le lieu existe maintenant, et tout commence. Notre plus grand défi sera d’apprendre à vivre ensemble pour inventer une nouvelle réalité.”
