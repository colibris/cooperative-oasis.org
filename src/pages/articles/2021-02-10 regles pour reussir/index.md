---
title: "Les règles pour réussir ?"
date: "2021-02-10T06:40:32.169Z"
layout: post
type: blog
path: "/regles-pour-reussir/"
image: ./BelAir.jpg
categories:
  - tribunes
---

Partout en France, des collectifs citoyens créent de nouveaux lieux écologiques et solidaires appelés éco-hameaux, oasis, habitats participatifs, oasis, fermes collectives, tiers-lieux... La Coopérative Oasis accompagne toutes celles et ceux qui souhaitent se lancer !

Dans ce podcast « La Voix des Oasis », Mathieu Labonne, directeur de la Coopérative Oasis, donne à voir l’histoire et les contours du réseau des oasis. Il donne aussi quelques clefs pour réussir son projet. Même s’il n’y a pas qu’une seule façon de s’y prendre, il y a quelques bonnes pratiques à connaître !

<iframe src="https://www.podcastics.com/player/extended/978/34085/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>


## Passer du temps dans des lieux existants avant de se lancer

La carte des Oasis permet de découvrir les lieux existants. Il suffit de les contacter directement via la carte pour leur demander s’ils sont disposés à vous accueillir. La Coopérative Oasis organise également des voyages d’oasis en oasis pour des petits groupes. Pour en savoir plus, écrire à : reseau@cooperative-oasis.org

![Le Village du Bel Air, Morbihan](./BelAir.jpg)

## Bien connaître le territoire où l’on souhaite s’installer

Communiquer avec les habitants, les élus et les structures déjà existantes sur le territoire où l’on s’installe est essentiel : tant pour se faire connaître et désamorcer les craintes que pour y trouver la juste place. Une [carte des élus favorables aux lieux de vie collectifs](https://cooperative-oasis.org/wiki/?EluS) et écologiques permet d’orienter ses recherches de communes ou s’installer ou de trouver des interlocuteurs compréhensifs.

![Carte des élus favorables aux oasis](./CarteElus.png)

## Ne pas oublier le lieu qui est, en lui même, un membre du collectif

Du rêve à la réalité de l’habitat il y a souvent une différence, qui tient pour beaucoup au lieu en lui-même. Sa géographie, son climat, son énergie sont autant d’éléments constitutifs d’une oasis - il faut laisser au lieu le temps et l’espace de modeler le projet... Pour ceux qui cherchent encore une terre, nous proposons une [carte des fonciers disponibles](https://cooperative-oasis.org/wiki/?FoncierS).

 ![Grain&Sens, Ardèche](./GrainSens.jpg)

## Il n’y a que deux enjeux clefs : l’humain et le financier, sur lesquels il faut se faire accompagner !

Le juridique n’est pas un enjeu, il est une simple traduction des besoins humains et financiers partagés par le collectif : le rapport à la propriété, les usages et activités prévues, les modes de financement, la gouvernance interne, le niveau de risque pris... Dès lors que le collectif est au clair avec ces enjeux, le modèle juridique n’est plus un problème. [La Coopérative Oasis](https://cooperative-oasis.org/creer-son-oasis/) accompagne tous ceux qui vivent ou souhaitent vivre en oasis sur ces différents points !

_Article écrit par Gabrielle Paoli_

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/pepiniere-oasis/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Apprendre des fondamentaux pour développer une oasis avec la pépinière oasis !
    </a>
</div>
</div>


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Placez votre argent dans la Coopérative Oasis et soutenez la création d’oasis !
    </a>
</div>
</div>
