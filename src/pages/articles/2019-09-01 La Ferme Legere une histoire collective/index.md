---
title: "La Ferme Légère, une histoire collective tournée vers l’autonomie et la joie"
date: "2019-09-01T10:40:32.169Z"
layout: post
type: blog
path: "/ferme-legere-histoire-collective/"
image: ./fermelegere1.jpg
categories:
  -  histoires d'oasis
---

_Article écrit par Gabrielle Paoli et paru dans KAIZEN n° 45, juillet / août 2019. Crédit photo : Clément Osé / découvrez son [diaporama sonore](http://www.clementose.art/diaposons/)_

Située à Méracq, dans le Béarn, la [Ferme légère](https://fermelegere.greli.net/doku.php) est un écolieu où huit résidents vivent dans un bâtiment de 250 mètres carrés. Au cœur de ce beau projet né en 2013 : le collectif, la sobriété et l’autonomie, alimentaire comme énergétique.

![Ferme légère](./fermelegere1.jpg)

« Vivre en collectif, c’est revenir à un mode de fonctionnement naturel et efficace. Parce qu’être isolé les uns des autres est étrange et propre à une période extrêmement limitée dans l’histoire de l’humanité », soutient Marc, cofondateur de la Ferme Légère, située à Méracq, dans le Béarn. Un mode de vie qui réduit également l’impact négatif sur les écosystèmes, rappelle quant à lui Clément, photographe résident sur l’écolieu : « On revient à la notion de décroissance : on consomme dix fois moins d’électricité que la moyenne française, on partage deux véhicules à huit. On dit “ferme légère” comme “impact léger”… »

En effet, à la Ferme légère, huit personnes, ainsi qu’une quinzaine de wwoofeurs passant chaque année, partagent aujourd’hui une même maison. « Ce qui nous caractérise par rapport à d’autres lieux, c’est qu’on pousse loin la mutualisation, explique Clément. On partage le même toit, avec une centaine de mètres carrés de parties communes et des espaces privatifs : une chambre par foyer. » Les habitants voient dans ce partage d’espaces et de savoir-faire la clef de l’autonomie par la complémentarité et l’entraide. Chacun donne dix-neuf heures de son temps hebdomadaire au collectif, qu’il consacre aux réunions, à la cuisine, au jardin, au bâtiment...

![Ferme légère](./fermelegere2.jpg)

## Vers l’autonomie énergétique et alimentaire

La vieille bâtisse béarnaise en pierre, datant du XVIIIe siècle, a été intégralement rénovée par le collectif, sous la direction de Marc, le bricoleur de la bande, avec l’aide de nombreux amis et visiteurs aux compétences diverses (charpente, maçonnerie, plomberie, etc.), et parfois avec des matériaux récupération (tuiles, baies vitrées...). « En trois ans, nous sommes passés d’un grenier poussiéreux aussi plat que le Béarn à un étage lumineux, avec des baies vitrées sur les Pyrénées, un nouveau plancher, des cloisons, de l’électricité, des fenêtres, une bonne isolation d’hiver et, finalement, cinq chambres, un grand salon avec mezzanine et une passerelle », raconte Clément. La maison, rehaussée d’un étage, est aujourd’hui quasiment passive grâce à l’isolation par l’extérieur au nord et au captage solaire au sud.

![Ferme légère](./fermelegere3.jpg)

Au-delà du travail collectif sur les usages et la baisse de la consommation, l’alimentation en énergie se fait de plus en plus en autonomie. Trois panneaux thermiques chauffent l’eau, 22 mètres carrés de panneaux photovoltaïques produisent l’électricité, et le poêle Dragon (ou Rocket stove) est aujourd’hui très peu utilisé. Un puits remis en service alimente quant à lui directement la maison avec une pompe électrique. Pour compléter l’approvisionnement par ce puits, toutes les eaux de pluie sont collectées et centralisées dans une cuve de 120 mètres cubes qui sera bientôt reliée à la maison. « Du point de vue alimentaire, nous sommes autonomes en légumes deux saisons sur quatre grâce à nos 2000 mètres carrés cultivés et aux trois serres de 250 mètres carrés », précise Élodie, arrivée en mai 2019. « On n’est pas des “survivalistes”, mais la résilience, qui passe par l’autonomie, est une valeur essentielle pour nous », ajoute Marc.

![Ferme légère](./fermelegere4.jpg)

## Un coût réduit de la vie

Constitué en 2014 en SCI, à l’initiative de cinq personnes, le groupe s’est renouvelé au fil du projet. Née d’une idée abstraite, sans terre, devenue une ferme s’enracinant sur un territoire et se confrontant au réel, la Ferme légère n’existerait pas sans ceux qui sont partis comme ceux qui ont pris le train en marche. Acquis en automne 2015, l’écolieu et son terrain de 11 hectares appartiennent à une SCI qui loue l’espace à l’association Ferme Légère pour lui donner vie. Le budget total du projet, incluant l’achat du lieu et la rénovation complète, s’est élevé à 250 000 euros, soit environ 30 000 euros par personne. "« "Seul le collectif rend possible un cadre de vie magnifique et une maison rénovée autonome et confortable pour ce prix-là", résume Marc, seul résident présent depuis l’origine du projet. "Au quotidien, la mutualisation et la sobriété permettent de vivre pour moins de 400 euros par mois et par personne (incluant nourriture, eau, électricité, chauffage, logement, voitures et financement de toutes les activités collectives)."

![Ferme légère](./fermelegere5.jpg)

## Un modèle économique qui mise sur la diversité

Les ressources financières du projet varient en fonction des situations des uns et des autres. Pour assurer une source de revenus, plusieurs personnes travaillent en dehors de la ferme. Valérie est sophrologue. Clément, écrivain et photographe, travaille depuis la ferme quand il n’est pas en déplacement. Marc continue une activité de rénovation de bâtis à l’extérieur du lieu. Des activités économiques sont, en parallèle, en projet sur la ferme. Valentin prépare quant à lui une installation agricole en 2020 alliant poules pondeuses et vaches de race rustique en pâturage tournant. Élodie prépare de son côté une activité PPAM (plantes à parfum aromatiques et médicinales) et Tom du maraîchage sur sol vivant.

![Ferme légère](./fermelegere6.jpg)

Maintenant que les années d’installation se terminent, les résidents aspirent à transmettre l’esprit et les savoir-faire développés à la Ferme légère. À la suite d’un évènement Colibris, une première rencontre des écolieux de la région a ainsi été organisée pour renforcer et encourager les initiatives locales similaires. En parallèle, Valérie et Marc lancent cet été les premiers stages « Quelles résiliences face à l’effondrement ? ». Au programme : découverte de la ferme, approfondissement des actions de chaque participant et réflexion collective sur l’avenir. « Quand on a imaginé le projet, l’objectif était l’autonomie, le lien entre nous et le sens. Et ça marche... on est très heureux aujourd’hui », conclut Marc.
