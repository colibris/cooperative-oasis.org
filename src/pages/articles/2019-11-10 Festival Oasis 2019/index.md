---
title: "Festival 2019 : les oasis se réunissent pour grandir"
date: "2019-11-20T10:40:32.169Z"
layout: post
type: blog
path: "/festival2019/"
image: ./Festival2019-1.jpg
categories:
  - événements
---

_Article paru dans KAIZEN n° 47, novembre / décembre 2019._

![Festival](./Festival2019-1.jpg) _Plus de 350 personnes vivant ou souhaitant vivre en oasis ont participé au festival_

A l’intérieur du parc du château de Jambville, se sont retrouvés une centaine d'oasis venues de la France entière. Reportage sur ce grand rassemblement des écolieux et des nouveaux modes de vie...

<iframe src="https://player.vimeo.com/video/379255597" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

Avec des ateliers de qi gong et des jeux coopératifs, une grande tente pour être ensemble lors des conférences plénières, une petite ferme « pour fabriquer ses projets » et des rencontres au Château « pour s’inspirer », tout était fait pour se ressourcer dans un environnement bienveillant.

Au cœur des collines du Vexin, le Festival Oasis accueillait du 5 au 6 octobre 2019 près de 350 personnes vivant ou vivant vivre dans des écolieux, et ce, malgré la pluie ! Qu’il s’agisse de permaculture, de conception de l’habitat, de développement d'un revenu de base ou d'une monnaie citoyenne, chaque participant pouvait trouver des conseils pour mener son projet à bien.

![Festival](./Festival2019-2.jpg) _Des activités en simultané dans six espaces différents du château de Jambville_

Mais au fait, qu’est-ce qu’une oasis ? « Une oasis, c’est un lieu de vie et ou d'activité, autonome, convivial, et collectif. Certaines sont justes des lieux d’animations, des jardins partagés, des fermes collectives. Ce sont les oasis ressources. D’autres sont des lieux de vie adossés parfois à des activités, où habitent plusieurs foyers », précise Gabrielle Paoli, qui anime ce réseau d’écolieux au sein de l'association Colibris. Ce qu’elles ont en commun ? « Cinq grandes intentions, qui ne sont pas forcément des réalisations abouties : l’autonomie alimentaire et énergétique, la mutualisation du temps, des énergies et des outils, une gouvernance respectueuse, l’ouverture sur l’extérieur pour essaimer et donner envie à d’autres de faire la même chose ! »

![Festival](./Festival2019-3.jpg) _Plus de 80 oasis représentées_

Recensées sur une carte, les Oasis sont aujourd’hui plus de 900 dans l’Hexagone, en ville, à la campagne, en milieu périurbain. Certaines sont des hameaux de 30 familles dans lesquels chacun à sa maison ; d'autres, de grandes bâtisses rénovées ou vit l'ensemble du collectif, comme la Ferme légère qui abrite 15 personnes dans le Béarn. « Ce festival, confie Gabrielle, on l’a créé parce que les gens se sentaient parfois un peu seuls sur leur territoire. Ils avaient besoin de se rencontrer, de se connaitre, de créer du lien pour partager expériences et savoir-faire. On les aide à se lancer et / ou à se connecter entre eux. »

## Des laboratoires d’énergie partagées

Il y a une vraie diversité. Des moines, des militants anarchistes, des retraités qui veulent vieillir ensemble, des mères célibataires, des urbains, des ruraux.... « Aujourd’hui, il y a beaucoup de jeunes dans le réseau. Le déclic, c’est la recherche d’une qualité de vie. Les gens en ont assez d'un mode de vie urbain qui n'ont parfois plus de sens pour eux », fait remarquer Gabrielle.

![Festival](./Festival2019-4.jpg) _Joël et Béatrice de la Pommeraie Saint-Martin préparent leur programme_

Certains recherchent la bienveillance, une famille, une tribu. D’autres préfèrent se mettre au service de ceux qui en ont besoin, en créant des emplois dans un territoire défavorisé et en accueillant des personnes en situation de précarité, comme la Ressourcerie du Pont près du Vigan (Gard). Dans les maisons Ecoé, les habitants ont choisi de vieillir dignement dans un espace intergénérationnel permettant le maintien à domicile jusqu’à la fin de vie. Dans le Village de Pourgues (Ariège), il s’agit d’éduquer les enfants dans une pédagogie fondée sur la liberté et la responsabilité.

Les exemples sont multiples. Chaque collectif a sa raison d’être. Ce sont des laboratoires d’énergies partagées. Mais le parcours de la vie en collectif n'est pas chose évidente. En effet, avec l’habitat, on touche aussi à ce qu’il y a de plus intime et de plus important : les valeurs, la famille, les économies, la gestion du temps... « Il n'y a pas une seule bonne façon de faire. Chaque groupe doit trouver la forme et le fond qui lui conviennent - et qui peuvent évoluer ! La seule règle pour réussir son projet cependant, c’est de se faire accompagner et d’avoir un regard extérieur pour avancer. » analyse Gabrielle.

## A chacun sa popote !

Nicolas Pinus est venu en curieux pour découvrir d’autres projets, transmettre son expérience et rencontrer de nouvelles personnes. Il vit à l'Aérium, une oasis située dans le parc national des Cévennes, avec une vingtaine d’adultes et une demi-douzaine d’enfants. Le lieu est constitué de 3 500 mètres carrés de bâtiments et quatre hectares de terrain. Une grande colonie de vacances, perdue dans la montagne, dans un ancien sanatorium.

![Festival](./Festival2019-5.jpg) _Nicolas Pinus habite à l'Aérium (Gard)_

« J’habite sur place. Tous les autres ont emploi à mi-temps à côté, ou du télétravail. A chacun sa popote pour ramener des sous ! Au festival oasis, je suis venu suivre les ateliers « comment ne pas s’épuiser dans la durée » et « échange entre lieux déjà aboutis ». Le reste, on est béton ! », lance Nicolas. Ce qui l’enrichit le plus, c’est d’entendre les témoignages de ceux qui ont trente ans de communauté derrière eux : « cela m’aide à relativiser, à me dire qu’on est tous dans le même bateau ».

Clément Leguidcoq, lui, vient d’acheter un lieu à Saint-Omer-de-Blain au nord de Nantes, un presbytère qui a 200 ans avec trois habitations et un théâtre, pour créer une oasis ! Il est venu rechercher des informations sur la gouvernance, mais aussi pour rencontrer et trouver de futurs habitants. « 5 familles, ce serait chouette, avec des enfants qui courent partout, un lieu culturel, un jardin partagé, de la permaculture, la collecte des eaux de pluies et une phytoépuration », rêve-t-il.

![Festival](./Festival2019-6.jpg) _Le collectif de Grain&Sens (Ardèche) est venu presque au complet_

Et si une société écologique et solidaire était en train de s’inventer sous nos yeux ? Et si ces expérimentations pouvaient revivifier des territoires en friche, mais aussi essaimer ailleurs et inspirer les zones urbaines et périurbaines ?

C’est en tout cas ce que pense Mathieu Labonne, ancien directeur de Colibris et PDG de la Coopérative Oasis. Fondée en mai 2018, cette SCIC collecte de l'épargne citoyenne et la prête sans intérêts aux oasis contre de l'accompagnement payant. Le financement de lieux alternatifs et innovants comme les écolieux est en effet un enjeu majeur. La plupart des établissements bancaires traditionnels ne comprennent pas le fonctionnement de ces structures et ne les financent presque jamais. Depuis sa création, la Coopérative a prêté environ 570 000 euros  à six lieux différents en moins d'1 an.

![Festival](./Festival2019-7.jpg) _Nicolas Voisin raconte La Suite du Monde_

Dans la lignée des oasis, se trouvent aussi les "communes imaginées" financées par la Suite du Monde. Nicolas Voisin est intervenu au festival pour créer des ponts entre les oasis et les communes, toutes deux étant des « archipels de personnes essayant de se libérer de la folie de ce monde. Nous aimerions initier une cinquantaine de communes imaginées. Pour cela, on se nourrit des expériences menées et on accompagne la création de coopératives locales en SCIC, mais aussi de boulangeries, de brasseries, de conserveries, si essentielles pour transformer et donner un débouché au maraichage biologique », précise Nicolas.

## Accompagner les migrations

![Festival](./Festival2019-8.jpg) _Un bal folk est proposé le samedi soir par Terra Poetica._

Au sein de La Suite du Monde, cofondée avec Pablo Servigne, ils ont aussi mis en place une coopérative qui collecte l’épargne citoyenne et finance les installations à travers des parts sociales rémunérées à 2,5% par an, de la souscription militante et le financement de filières de production par abonnement.

![Festival](./Festival2019-9.jpg) _Les ateliers s'adressent surtout aux lieux en chemin._

Avec les oasis, le but est de faire monde avant le basculement vers une grande crise économique et écologique, et de créer des lieux de résistance et de résilience dans tous les lieux dépeuplés et préservés de France. Bref de nourrir la société, dans tous les sens du terme, et d’accélérer la transition écologique et humaniste.

 L’Agence de l’environnement et de la maîtrise de l’énergie (Ademe) qui cofinance le festival, ne s’y est d’ailleurs pas trompée. Elle s’intéresse aux oasis pour rendre compte des bénéfices de ces nouveaux modes de vie.
