---
title: "La Servantie, un échohameau sans frontière"
date: "2020-08-24T06:40:32.169Z"
layout: post
type: blog
path: "/ecohameau-servantie/"
image: ./servantie1.jpg
categories:
  - histoires d'oasis
---

_Au lieu-dit La Servantie, proche du petit village périgourdain de Beauregard-et-Bassac, existe depuis 2011 un écohameau d'une quinzaine de maisons réparties sur un peu plus de 4ha. Constructions bioclimatiques, ateliers et potager communs, liens étroits avec les paysans du coin, accueil régulier de personnes extarieurs... la joie et la bonne humeur accompagnent la construction quotidienne de l'autonomie pour ce collectif de trente personnes !_

![La Servantie](./servantie1.jpg)

Philippe, Claudette, Coline, Bernard et Catherine, Valou, Anaya et Xavier... Un collectif d'une trentaine de personnes habite aujourd'hui cet éco-hameau situé en Dordogne, au lieu-dit La Servantie. Afin de faciliter la transmission du patrimoine, chacun est propriétaire de sa parcelle et l'association « Echo-hameau de la Servantie » est propriétaire des espaces communs.

![La Servantie](./servantie2.jpg)

Andreas Schindler initie le projet dans les années 90, quand les terres où il cultive des céréales en bio deviennent constructibles. Après le désistement d'un premier collectif, quatre nouveaux foyers venus de Rhône-Alpes relancent le projet en 2011.  « On écrit "écho-hameau" et non "éco-hameau" car il y a aussi l’idée de faire passer un message, celui de développer ce genre d’initiatives », raconte Andréas Schindler, le fondateur du projet.

![La Servantie](./servantie3.jpg)

L'écohameau est construit sur une zone de 4,4ha. Les habitants bénéficient en outre du droit jouissance de 6ha de bois et de l'étang en contrebas. L'eau pluviale est récupérée grâce à deux réservoirs de 50m3, et utilisée pour le jardin et les usages domestiques. Un système de phytoépuration permet de traiter les eaux domestiques. "L’eau de javel et autres produits chimiques sont donc entièrement proscrits !" précise Philippe.

![La Servantie](./servantie4.jpg)

Sur les 17 lots de la parcelle, 10 maisons sont déjà habitées et 5 sont encore en construction. Il existe deux tranches de prix d'achat du terrain : 15€ le m2 pour la première tranche et 20€ le m2 pour la seconde tranche.

![La Servantie](./servantie5.jpg)

Chaque propriétaire finance la construction de sa maison, selon les principes bioclimatiques et avec divers matériaux locaux. On ne trouve dans ces bâtiments, souvent partiellement auto-construits, ni laine de verre, ni parpaings.

![La Servantie](./servantie6.jpg)

Ce dont les habitants sont fiers aujourd'hui : avoir surmonté des conflits interpersonnels à force de médiation et d'implication de tout le collectif. "La dimension de l'accueil nous est aussi très chère, ajoute Catherine. Ce lieu a été déjà accueilli une famille géorgienne en difficulté, une personne en convalescence, une autre en situation de handicap... nous avons réussi à ouvrir nos portes comme nous le souhaitions initialement."

![La Servantie](./servantie7.jpg)

En attendant la construction d'une maison commune, un bâtiment réunit les espaces communs : un atelier pour le stockage des outils, une salle servant de buanderie, d'atelier de vannerie et de salle de réunion. Une réunion hebdomadaire permet en effet aux habitants de gérer ensemble toutes les affaires communes. Mais la prise de décision reste encore bien souvent difficile pour les gros sujets. Et ce malgré un accompagnement extérieur régulier.

![La Servantie](./servantie8.jpg)

En contrebas, un potager géré par tout le collectif permet aux habitants de produire une bonne partie de leurs légumes et de passer du temps ensemble au jardin.

![La Servantie](./servantie9.jpg)

Pour le reste, le collectif achète des provisions aux jeunes agriculteurs voisins. Un paysan-boulanger s'est installé dans un fournil juste en face de l'écho-hameau. "Il y a aujourd'hui tellement d'habitants du village et de la région impliqués dans l'écho-hameau qu'il est difficile d'en tracer les limites... notre écho-hameau est sans frontière !" conclue Philippe.

_Article rédigé par Gabrielle Paoli / Coopérative Oasis_
