---
title: "Faites fleurir des oasis avec votre épargne !"
date: "2020-11-01T06:40:32.169Z"
layout: post
type: blog
path: "/onzeoasisfinancees"
image: ./Oasisfinancee4.jpg
categories:
  - oasis financées
---

![Oasis financees](Oasisfinancee4.jpg)

_<b>Créée en 2018, la Coopérative Oasis propose à chacun et chacune de **[mettre son épargne à disposition des oasis](https://cooperative-oasis.org/investir/)**, qui ont souvent du mal à obtenir des prêts bancaires. Presque **200 citoyens** ont décidé de donner du sens à leur argent en le plaçant dans la Coopérative. Grâce à eux, plus d’**1,3M d’euros ont déjà été prêtés 11 oasis**...</b>_  


## 200 investisseurs solidaires ont placé près d’1,5M d’euros dans la Coopérative Oasis

![Oasis financees](Oasisfinancees.jpg)

Un projet d’oasis a souvent du mal à obtenir des financements des banques traditionnelles. La Coopérative Oasis a donc décidé de financer ces projets en leur prêtant l’épargne que des citoyens ont voulu mettre à leur service. Et cela fonctionne ! Près de 200 citoyens ont déjà placé 1,5M d’euros dans la Coopérative Oasis. Ils sont régulièrement invités à passer le week-end dans les oasis soutenues, pour constater ce à quoi ils ont concrètement contribué.

![Oasis financees](Oasisfinancee2.jpg)
_Rémi, Lucette, Josiane ont par exemple eu l’occasion de se rendre à Grain&Sens, la première oasis financée par la Coopérative [© Ed Carr](https://edcarr.com/)_

##11 oasis réparties dans toute la France ont reçu des financements allant de 60 000€ à 200 000€, sur des durées allant de 6 mois à 10 ans

![Oasis financees](Oasisfinancee3.jpg)

[Le Moulinage de Chirols](https://cooperative-oasis.org/oasis/moulinagedechirols/) réhabilite une ancienne usine pour la transformer en lieu d’habitat partagé, d’activités artisanales et artistiques. La Coopérative Oasis a octroyé un prêt de 184 600 euros pour financer les travaux. En parallèle, elle accompagne le collectif sur plusieurs sujets, et notamment sur le montage juridique.

![Oasis financees](Oasisfinancee5.jpg)

Dans le Morbihan, [Demain en Main](https://cooperative-oasis.org/oasis/demain-en-main/) est avant tout un projet agricole. Huit foyers se sont installés sur une parcelle de 20 hectares. La Coopérative Oasis a prêté 100 000 euros sur 7 ans au collectif pour finaliser l’achat de la propriété sur Locoal Mendon. La Coopérative contribue également à l'élaboration du design juridique du projet.

![Oasis financees](Oasisfinancee6.jpg)

Porté par une cinquantaine d’hommes et de femmes depuis 2014, [TERA](https://cooperative-oasis.org/oasis/tera/) est un projet expérimental situé dans le Lot-et-Garonne. Une ferme, un écohameau, un centre de formation et d’autres activités économiques seront à terme répartis sur trois lieux différents. Le projet a sollicité la Coopérative Oasis pour un prêt de 180 500 euros sur 10 ans pour acheter le terrain du futur quartier rural et pour financer le centre de formation et les 11 habitats légers. La Coopérative accompagne le lieu sur son montage juridique…

![Oasis financees](Oasisfinancee7.jpg)

En 2020, quinze amis ont décidé de réhabiliter un ancien moulin et ses dépendances : 800m2 habitable sur un terrain de 13ha au bord du Loir à Saint-Jean Froidmentel, Loir-et-Cher. Le collectif du [Moulin bleu](https://cooperative-oasis.org/oasis/moulinbleu/) a fait appel à la Coopérative Oasis pour un prêt de 200 000 euros sur 7 ans, afin de réaliser l’achat du lieu et de financer des premiers travaux sur place. En parallèle, la Coopérative Oasis accompagne le projet sur sa structuration juridique, financière et humaine.

##Beaucoup d'autres oasis ont encore besoin de soutien...

De nombreux écolieux à fort impact environnemental et social ont encore besoin de soutien technique et financier. **[Chacun et chacune peut les aider !](https://cooperative-oasis.org/investir/)**
