---
title: "Le Projet Oasis désormais entièrement porté par la Coopérative Oasis"
date: "2020-06-15T06:40:32.169Z"
layout: post
type: blog
path: "/projet-oasis-envol/"
image: ./envol.jpg
categories:
  - entretien
---

_Texte paru dans le magazine de Colibris le 15 juin 2020_

Après cinq années de développement au sein de l'association Colibris, le Projet Oasis s'autonomise ! Il est désormais porté par la Coopérative Oasis, créée notamment par Colibris en 2018.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kUxn_qetL_A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Faire grandir les projets plutôt que les organisations

Le Projet Oasis est aujourd'hui mature, avec un écosystème d'acteurs riche, et autonome financièrement. Colibris souhaite qu'il devienne pleinement un bien commun, et que le réseau constitué des centaines d'oasis présentes sur le territoire puisse participer à la gouvernance du projet. Nous avons réalisé cet "essaimage", afin de "faire grandir les projets plutôt que les organisations". Agilité, souveraineté, et autonomie, sont au cœur des valeurs auxquelles nous croyons, tant pour les individus, les collectifs, que pour les structures.

Dans une logique de fonctionnement en archipel, Colibris et la Coopérative Oasis maintiennentde solides liens, et continuent de se féconder mutuellement.

![Arbre de Vie](./arbre_de_vie_1.jpg) _L'Arbre de vie, en Loire Atlantique_

Concrètement, la Coopérative Oasis prend en charge l'animation du réseau des écolieux - en particulier le temps fort que représente le Festival Oasis à l'automne - et l'organisation du financement de certains lieux : collecte de l'épargne citoyenne, audit et accompagnement des projets retenus.  

Colibris continue à soutenir les Oasis à travers :

les formations en ligne sur l'Université des colibris
la Fabrique des colibris, qui permet aux porteurs d'oasis de solliciter l'aide (humaine, matérielle, financière) de la communauté
la réalisation d'articles sur des oasis courageuses et inspirantes

![La Servantie](./servantie.jpg) _L'écohameau de la Servantie en Dordogne_

## Le témoignage de Mathieu Labonne, Président de la Coopérative Oasis et ancien directeur de Colibris

"Depuis ses débuts nous avons construit le réseau Oasis comme un réseau ouvert, un écosystème, un "commun". C'est donc logiquement que s'est posée, au fil des années, la question de lui donner une forme juridique qui intègre formellement toutes ses parties prenantes. Il s'agissait notamment de replacer les oasis au centre de la gouvernance du réseau. Il devenait crucial, à un moment donné, de formaliser la gouvernance informelle qui se vivait au sein du projet.

D'après moi, ce processus d"essaimage" est une réussite. Pendant plus d'1 an, nous avons débattu, partagé nos visions, afin d'arriver à un accord qui valorise le rôle joué par Colibris mais aussi donne les moyens à la Coopérative de réussir. Cela montre notre capacité à faire des choix au service des projets et de leur raison d'être plutôt que de chercher à "grossir". Nous pouvons être collectivement fiers d'avoir mené ce processus à bien. Nous sentons que c'est un bel exemple pour "l'archipel de la transition" que nous appelons de nos vœux !"

## L'histoire du réseau des oasis

Au départ, il y a eu le "Manifeste pour des Oasis en tous lieux", en 1997. Le contributeur principal, Pierre Rabhi, né lui-même dans une oasis algérienne, était sensible à la symbolique de ces lieux de vie, de travail, et de ressourcement, nichés au cœur des déserts. Pour répondre à "la désertification humaine, économique et morale dans nos sociétés modernes", il a initié le "Mouvement des Oasis en Tous Lieux" au début des années 2000. Ce mouvement s’est développé autour d'une dizaine de lieux, proches de Pierre Rabhi et a contribué à faire connaître le Manifeste.

En 2014, conscients qu'un nombre croissant de personnes aspirait parallèlement à des lieux et des modes de vie différents, portant des valeurs de partage, de convivialité, et d'autonomie, différents acteurs des oasis ont fait le choix de confier ce projet à Colibris, qui pouvait assurer son développement. Mathieu Labonne est alors arrivé à Colibris mi 2014 avec l’objectif de faire grandir ce réseau et de faciliter la création de centaines de lieux.

Aujourd'hui, en 2020, plus de 900 lieux sont membres du réseau et présents sur la carte nationale des oasis et habitats participatifs.
