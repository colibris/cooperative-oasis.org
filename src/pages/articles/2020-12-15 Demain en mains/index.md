---
title: "On ne s’est pas empêché de rêver… Demain en main est né"
date: "2020-12-15T06:40:32.169Z"
layout: post
type: blog
path: "/DemainenMainReve/"
image: ./Demain en Main Ludo et Julien.jpg
categories:
  - histoires d'oasis
---

**Ils ne se connaissaient pas, n’avaient ni collectif, ni terrain, mais rêvaient de quitter la ville pour vivre plus sobrement - et mettre les mains dans la terre.**

En quelques années, ils se sont rencontrés, **ont réuni leurs forces et leurs imaginaires et ont créé [Demain en main](https://demainenmain.fr/) : une oasis située sur la commune de Locoal-Mendon (Morbihan).**

Sur cet écolieu collectif, on trouve **10ha de terre agricole, 10ha de forêt, un ancien hameau de 5 bâtiments restaurés et une quinzaine de personnes qui rêvent…** et passent à l’action ensemble ! Aujourd’hui, 5 adultes et 4 enfants habitent déjà sur place. Ils seront rejoints par 5 autres foyers dans la foulée de l’achat définitif du terrain, qui aura lieu début 2021 grâce à un prêt de la Coopérative Oasis et à la campagne d'épargne citoyenne toujours active…

![Demain Ludo](./Demain en Main Ludo et Julien.jpg)

##DU RÊVE INDIVIDUEL AU RÊVE COLLECTIF

Tout commence à l’automne 2016, quand Julien et son frère Fanch dessinent leur vision d’un éco-lieu associant production agricole, habitat et accueil. Au-dessus et en grosses lettres est écrit “Agir aujourd’hui, pour vivre ensemble demain”. Diffusé sur les réseaux sociaux, cet appel touche de nombreuses personnes, qui commencent à se réunir. « _Ils se retrouvaient à 30 toutes les deux semaines, mais ça n’était jamais les mêmes,_ raconte Ludovic. _Seules 6 personnes revenaient à chaque fois… et c’est comme ça que le noyau dur s’est constitué._ »

Durant l’année 2017, le collectif se consolide grâce à des règles de gouvernance participative. Très vite, des objectifs communs se dessinent :
- Produire localement de la nourriture de qualité par une ferme régénératrice des sols
- Sensibilier à l’écologie à travers une auberge et une ferme pédagogique
- Transmettre savoirs et savoir-faire en lien avec l’autonomie et bien-être

![Demain Groupe](./Demain-en-main-groupe.jpg)

Le groupe s’étoffe progressivement jusqu’à atteindre la taille actuelle : une quinzaine d’adultes et d’enfants. Kristell a rejoint le collectif à la fin de l’année 2018. « _Je voulais du nouveau dans ma vie, j'ai demandé autour de moi l'existence de projet collectif. On m’a dit d’aller du côté de Demain en main. Je leur ai finalement écrit : « Je sais qu’il n’y a plus de logement disponible mais je suis motivée pour m’investir et apprendre avec vous… » Ça tombait au bon moment, et ça les a touchés. J’habite aujourd’hui à 10 minutes à vélo du village, en colocation avec Ludovic._»

En 2018, le groupe tombe amoureux d’un terrain de 20 hectares à Locoal-Mendon (Morbihan). Anne-Marie, la propriétaire, cède cette propriété magique avec une condition : conserver l'unité foncière du terrain et des habitats et lui redonner la vie de village d’antan ! Le groupe s’engage…

![Demain Maison](./Demain en Main Maison.jpg)

##NE PAS SE LIMITER À L’EXISTANT POUR INVENTER CE QUE L’ON VEUT VIVRE

Afin de développer les objectifs du projet, le collectif a imaginé un montage juridique unique. Plusieurs structures juridiques différentes ont progressivement été constituées : une Société Coopérative d’Intérêt Collectif (SCIC) pour acheter le lieu et gérer le patrimoine, proposer des formations en lien avec les transitions,  et garantir l’éthique du projet. Un groupement d’habitants loue les habitations à la SCIC, une SCOP agricole encadre la production de légumes, le camping à la ferme et la ferme-auberge ; enfin, une association accompagne l’émergence de l’éco-lieu par les volets communication, événements et activités de recherche.

![Demain Helene](./Demain en Main Helene.jpg)

« _Ce montage complexe n’a rien de révolutionnaire, mais il permet d’avancer sur chacun des domaines qui nous tiennent à cœur,_ explique Ludovic. _Pendant la phase de montage du projet, on n’a pas arrêté de nous dire : « c’est trop ambitieux, vous n’y arriverez pas, c’est délirant » Mais on ne s’est pas empêchés de rêver. Et on a trouvé les solutions techniques pour atteindre nos rêves._ »

Pour Kristell, là est la principale force du collectif : l’audace et la capacité à se donner la possibilité de vivre ce qu’il veut vivre, malgré les obstacles.

##LE COLLECTIF, UN DÉFI AU JOUR LE JOUR

Le collectif a des approches et des profils très divers. Certains ont des parcours scolaires d’excellence – écoles d’ingénieur, postes de cadre… - d’autres sont plus autodidactes et débrouillards, comme Gwen qui a passé 15 années sur un bateau. Cette diversité est une force mais aussi la source de confrontations et d’incompréhensions régulières. « _On a identifié cette question, et on y fait face,_ reprend Ludovic. _Ça nécessite une mobilisation permanente - dès qu’on se relâche un peu, ça peut gripper._ »

![Demain Serres](./Demain en Main Serres.jpg)

Pour faire face à ces enjeux humains, essentiels à la réussite de ces projets collectifs, Demain en main se fait accompagner par une médiatrice depuis 2017. Trois heures par mois, elle ouvre un espace où chacun peut faire une pause, accueillir les émotions, et prendre conscience et connaissance de ce que vivent les uns et les autres. « _Je repars regonflée d’amour pour les gens du groupe, des moments où l'on se rejoint dans notre humanité, ça redonne de l’élan,_ explique Kristell. _Mais tout le monde ne vient pas forcément, c’est dommage… surtout parce que 3h, ce n'est pas du temps libre pour tout le monde._ » Et il faut croire que ça marche : à ce jour, une seule personne est sortie du collectif, au bout de quatre ans !

![Demain Maison](./Demain-Kristell.jpg)

##ÊTRE AU BON ENDROIT

Entre le rêve d’une autre vie et la réalité de l’écolieu, il peut y avoir un gouffre  « _Ce que je vis à Demain en main est différent de ce à quoi je m’attendais. C’est un peu déceptif à certains endroits,_ raconte Ludovic Rocher. _Mais il y a tellement d’autres choses qui me comblent, que je n’avais pas imaginées. Je n’ai jamais autant appris sur moi-même que depuis que je vis en groupe. Et puis… ce lieu me permet d’avoir, dans ce monde délirant, un ancrage quelque part._»

![Demain Legumes](./Demain en Main Legumes.jpg)

Kristell se souvient d’un moment de prise de conscience. « _C’était un matin ensoleillé. J’étais dans les champs en train de préparer une botte de radis qui devait partir à la Biocoop. J’ai réalisé que des gens allaient se nourrir avec, que dans quelques heures ça serait dans la maison de quelqu’un… c’est bête, mais à ce moment-là je me suis dit que j’étais heureuse de contribuer à cela._ »
En janvier 2021, la SCIC sera propriétaire de ce lieu magique et le planning de l'année s'annonce déjà bien chargé pour tous les membres du collectif.


_Article écrit par Gabrielle Paoli_
