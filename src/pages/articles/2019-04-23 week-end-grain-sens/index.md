---
title: Rencontre d'investisseurs solidaires à Grain&Sens
date: "2019-04-23T22:40:32.169Z"
layout: post
type: blog
path: "/week-end-grain-sens/"
image: ./week-end-grain-sens-1.jpg
categories:
  - événements
---

_Les 13 et 14 avril 2019, une vingtaine d'investisseurs ou personnes intéressées par la Coopérative Oasis se sont retrouvés à l'oasis de Grain&Sens. L'objectif ? Découvrir de l'intérieur cet habitat collectif et écologique qu'ils ont pour certains contribué à financer._

![Partages](./week-end-grain-sens-1.jpg) _Lucette, Marie-Christine, Raphaël et Rémi, trois investisseurs et un habitant partagent leurs envies à l'ouverture du week-end (© [Ed Carr](https://edcarr.com))_

Grain&Sens est accompagné par la Coopérative Oasis depuis le début de l'année 2019. Une convention a été signée pour 7 ans. Ce partenariat convient d'un prêt à taux zéro de 100 000 euros servant à lancer les activités sur le lieu, notamment l’accueil de stages et les gîtes. Il prévoit en parallèle un accompagnement redéfini tous les ans. La première année, la Coopérative Oasis propose à l’oasis :
- un soutien à la mise en place d’un design en permaculture ;
- un soutien à la réflexion stratégique et notamment à la précision du modèle économique et du positionnement du lieu ;
- un soutien à la mise en place d’une gouvernance adaptée, notamment par un regard extérieur, la mise en place de benchmarks et l’apport d’outils.

En plaçant leur épargne dans la coopérative, les investisseurs solidaires ont permis la mise en place de cet accompagnement. Ils ont également mis un pied dans le réseau des oasis, eux qui, pour la plupart, portent un projet d'installation en habitat collectif.

![Partages](./week-end-grain-sens-2.jpg) _Le groupe se réunit au début et à la fin de chaque jour pour jouer et échanger ses réflexions, émotions et impressions (© [Ed Carr](https://edcarr.com))_

Marie-Christine, Rémi, Fabianne, Jean-Luc, Mary et les autres participants ont découvert le lieu : 15 ha de forêt, 5 ha de prairie cultivable, 2 grands gîtes de 15 personnes entièrement rénovés. Ils ont également participé à des activités proposées par les 13 habitants de l'oasis :
- la confection des pizzas dans le four à pain de Kim et Raphaël, paysans-boulangers installés sur place ;
- une balade naturaliste avec Raphaël et son fils Jonah ;
- la plantation des pommes de terre avec Romain ;
- la dégustation de repas vegan imaginés et préparés par Victoria ;
- un échange fort avec Astrid, Raphaël Kim et Romain autour des plaisirs et difficultés du vivre-ensemble ;
- des jeux divers tout au long des deux jours...

![Partage d'habitants](./week-end-grain-sens-3.jpg) _Romain, Astrid et Kim, trois habitants, ont partagé leur expérience du vivre-ensemble haute en couleurs (© [Ed Carr](https://edcarr.com))_

Chaque investisseur a donc pu voir concrètement au service de quoi était mise leur épargne. Les autres participants ont pu aussi mesurer les intérêts d'une telle coopérative citoyenne. Mais tous ont également été en mesure d'avancer sur leur propre chemin : voir que c'était possible, et appréhender de plus près les joies, les embûches et les tâtonnements d'un projet abouti...

![Elio et Séva](./week-end-grain-sens-4.jpg) _Elio et Séva, deux des quatre enfants habitant sur place_
