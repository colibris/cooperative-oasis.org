---
title: "Les sociétaires visitent le Château partagé"
date: "2019-11-08T10:40:32.169Z"
layout: post
type: blog
path: "/visite-chateau-partage/"
image: ./visite-chateau-partage-1.jpg
categories:
  - événements
---

_Article écrit par Gabrielle Paoli et paru initialement sur le Magazine de Colibris_

En avril 2019, la Coopérative Oasis a prêté 80 000€ au Château Partagé sur 7 ans. Ce montant a permis de finaliser la transformation du modèle juridique de ce lieu de vie savoyard : les anciens associés ont pu être remboursés et chacun a pu devenir locataire. Quelques mois plus tard, en septembre 2019, une vingtaine d'investisseurs de la Coopérative Oasis se sont retrouvés au Château Partagé. Ses dix-huit habitants se sont mis en quatre pour accueillir ceux dont l'épargne solidaire a contribué à la pérennisation du lieu. Récit d'un week-end passé trop vite.

![Potimarrons_et_enfants](./visite-chateau-partage-1.jpg)

Au Château Partagé, dans cette ancienne maison de maître du XVIIIsiècle et sur les terres qui l’entourent, vivent aujourd’hui 18 habitants, 5 poules, 2 chats, 1 chien, 2 canards, 4 brebis et un bélier, 4 chèvres, un bouc et deux juments. Ce qui les réunit, ce sont les valeurs communes qu’ils ont eux-même définies : équilibre entre vie personnelle, vie collective et vie professionnelle, dans le respect et l’épanouissement de chaque individu, partage, respect du milieu naturel et simplicité. En commun, ils entretiennent la bâtisse, partagent buanderie, bibliothèque, atelier, jardin, joies et peines.

![Chateaupartage](./visite-chateau-partage-2.jpg) _Le Château Partagé se situe à Dullin, commune rurale de l’avant pays savoyard sur les hauteurs du lac d’Aiguebelette_


Plusieurs habitants ont aussi choisi de travailler sur place. Le Château Partagé héberge donc un vaste panel d’activités professionnelles, qui contribue à la bio-diversité humaine et économique du lieu. Thierry a installé son activité de maraîchage biologique en 2011. Il travaille en traction équine et sa production est essentiellement vendue dans un rayon de 10km. Une boulangerie, la Fournilière, a aussi ouvert au Château le 1er janvier 2014, suite à l’auto-construction d’un four à bois. Thomas y travaille avec des farines paysannes locales sur meule de pierre. L’atelier de tournage sur bois Trois Petits Tours s’est quant à lui installé au Château à l’été 2016, juste au dessus de la Fournilière. Odeurs de bois et de pain frais s'y mélangent... Baptou fabrique essentiellement de la vaisselle et parfois des bijoux ou des objets de déco.

![Chateaupartage](./visite-chateau-partage-3.jpg) _Atelier de fabrication de pizza à la Fournilière, la boulangerie du Château ouverte par Thomas en 2014_

Près d’une centaine de citoyens ont aujourd’hui choisi de prendre des parts dans la Coopérative Oasis. Dès qu’un prêt est attribué, ils sont tous invités à venir passer un week-end dans le nouveau lieu financé. L’objectif : voir de près les alternatives respectueuses des écosystèmes qu’ils financent...

![Chateaupartage](./visite-chateau-partage-4.jpg) _Margalida, une habitante, fait la lecture aux enfants du lieu et de passage_


Ce week-end de septembre 2019 au Château Partagé, les investisseurs sont venus en famille ou entre copains. Médecins, ingénieurs, retraités, développeurs... ils ont entre 30 et 60 ans, viennent de la France entière, portent parfois un projet de vie en collectif pour eux-mêmes, et sont avant tout curieux de savoir comment on s’organise quand on vit sobrement et en collectif !

![Chateaupartage](./visite-chateau-partage-5.jpg) _Parmi les investisseurs, Alexandre, animateur de ludothèque, est venu avec ses jeux..._


Les habitants du Château ont tout préparé : visite du lieu, temps de questions-réponses, après-midi de jeux coopératifs sur la pelouse au soleil, baignade au lac, cueillette de graines d’amarante, atelier confection de pizzas au four à bois, soirée musique et jeux coopératifs (ou pas)… Deux jours pour se découvrir mutuellement et donner une dimension nouvelle à l’épargne !
