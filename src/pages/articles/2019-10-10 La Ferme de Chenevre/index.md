---
title: "L’apprentissage du faire ensemble à la Ferme de Chenêvre"
date: "2019-10-20T10:40:32.169Z"
layout: post
type: blog
path: "/ferme-chenevre/"
image: ./Chenevre-1.jpg
categories:
  -  histoires d'oasis
---

_Article écrit par Gabrielle Paoli et paru dans KAIZEN n° 46, septembre / octobre 2019. La Ferme de chenêvre est accompagnée par la Coopérative Oasis._

La ferme de Chenèvre est "un petit univers qui fait comprendre le grand monde". Située dans le Jura, cette oasis est à la fois un lieu de vie et un terrain d’expérimentation aux multiples facettes qui valorise la coopération.

![le grand prunier](./Chenevre-1.jpg) _Le grand prunier célèbre le retour du printemps._


À l’origine de cette oasis, une mobilisation autour d’un lieu unique et isolé : la ferme de Chenèvre à La Chapelle-sur-Furieuse, dans le Jura. 25 hectares, un bâtiment ancien, dont certaines parties fortifiées remontent au xiiie siècle. En 2015, lorsqu’elle apprend que la ferme est mise en vente, Manon, maraîchère, propose une réunion pour voir si d’autres personnes seraient intéressées pour l’acheter avec elle... Yorgos adhère immédiatement : « Des fermes comme ça, il n’y en a pas deux dans le coin ! » Un petit groupe de locaux, motivés, se constitue, bientôt rejoint par d’autres personnes venues de toute la France via le réseau Colibris.

![FermeChenevre](./Chenevre-2.jpg) _La Ferme de Chenèvre se trouve dans le Jura à La Chapelle sur Furieuse._


En février 2017, ce collectif de dix personnes achète la ferme grâce à un emprunt auprès de la Nef. Le projet mêle propriété collective, mixité entre habitat, activités et accueil, gouvernance partagée, transition écologique et lien social. Une SCI est créée à laquelle les résidents versent un loyer, de 150 à 500 euros par mois, en fonction de la surface occupée, de sa nature et de son usage. Une association, sociétaire de la SCI, porte les différentes activités.

## Un lieu de vie et d’activités

Deux appartements, un atelier de céramique et un fournil ont été écorénovés. Cependant, il y aura au moins dix ans de chantier, en autoconstruction participative essentiellement. Des habitats légers permettent déjà à plusieurs foyers de vivre confortablement, tandis que la rénovation et l’ensemble de l’écoconception se déploient sur du temps long.

![chenevre-portes-ouvertes](./Chenevre-3.jpg) _Premières portes ouvertes en 2017._


Mais la ferme de Chenèvre, ce n’est pas qu’une histoire de logement et de bâti. « Nous souhaitions aussi faire de la ferme un lieu d’activités professionnelles diverses. Si l’on veut être autonome et résilient, l’habitat ne suffit pas… », explique Manon. Très rapidement, elle a donc lancé son activité de maraîchage bio en traction animale pendant que Virginie installait son atelier de céramique. La première récolte de Manon a pris le chemin des Biocoop de la région, et les créations de Virginie ont été vendues dans la France entière. Terre de Pains, SCOP montée par quatre artisans-boulangers bio, s’est emparée du fournil. Morgane, vigneronne voisine, va disposer sa prochaine vendange dans les caves, renouant ainsi le fil séculaire de l’activité vinicole du lieu. De son côté, Yorgos, passionné de restauration, mais écœuré des conditions d’exercice de celle-ci en ville, relance petit à petit son entreprise. Et Maryline réfléchit au développement d’un accueil touristique.

![chenevremaraichage](./Chenevre-4.jpg) _Maraîchage en traction animale : séance de découverte lors d'un chantier participatif._


## Vivre ou faire le projet ?

Virginie, Manon, José, Pauline, Anne, Maryline, Martin… autant d’habitants réunis autour de valeurs et de rêves communs. Il leur a fallu apprendre à faire ensemble, malgré la diversité des âges, origines sociales, professions et couleurs politiques. "Le MOOC [1] Oasis de Colibris nous a permis d’avoir un langage commun, et aidés à avancer, explique Anne. Nous avons commencé par un travail de fond sur la gouvernance et le faire-ensemble du projet". Aujourd’hui, Chenèvre pratique une gouvernance proche de la sociocratie. Le groupe s’est formé à la communication non violente (CNV) et utilise des outils d’intelligence collective : gestion par consentement, élection sans candidats, système de cercles… "Ça marche plutôt bien, raconte Maryline. On est tous garants du processus.""

![chenevreceramique](./Chenevre-5.jpg) _L'atelier de céramique._


À la ferme de Chenèvre, il y a beaucoup de partage de savoir-faire. Frédéric a, par exemple, transmis son expérience d’autoconstruction. « Quand je suis arrivé, je n’avais jamais fait de travaux de construction. Maintenant, je suis référent plomberie et électricité ! », raconte Yorgos. Le collectif valorise l’expérimentation : les apprentissages valent autant que le résultat, et le droit à l’erreur existe.

Néanmoins, tous admettent que la vie communautaire n’est pas un long fleuve tranquille. Le rythme d’un week-end de travail par mois ne colle pas toujours aux envies ou aux besoins. "Cela fait presque trois ans, on commence à avoir une meilleure idée de ce que c’est que “vivre ensemble”, explique Yorgos. Ce qui nous occupe aujourd’hui, c’est de trouver des temps pour vivre le projet plutôt que le faire."

![chenevrechevrerie](./Chenevre-6.jpg) _Martin à la chèvrerie._


Dans la tradition d’ouverture du lieu à la vie du village, la ferme de Chenèvre organise régulièrement des portes ouvertes, évènements thématiques ou chantiers participatifs. Un réseau d’une cinquantaine de personnes s’est ainsi constitué alentour, apportant de l’aide, de l’amitié, du travail bénévole, mais aussi des temps d’échange à l’occasion de cafés-philo, cueillettes de plantes sauvages ou soirées guinguette. Car c’est aussi sur le territoire et à travers de multiples réseaux humains que la ferme de Chenèvre entend entretenir et valoriser la communauté dans tout ce qu’elle a de précieux...
