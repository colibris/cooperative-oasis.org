---
title: "[La voix des oasis] Demain en main "
date: "2020-10-06T06:40:32.169Z"
layout: post
type: blog
path: "/voix-des-oasis-demain-en-main/"
image: "./DemainenMain.png"
categories:
  - histoires d'oasis
---

_Vivre en oasis ne signifie pas systématiquement vivre isolé et à la bougie ! Chaque écolieu ressemble au collectif qui lui donne vie. [Kaizen Magazine](https://kaizen-magazine.com/), [Colibris](https://www.colibris-lemouvement.org/) et la Coopérative Oasis on laissé le micro à [Alexandre Sattler](https://www.facebook.com/gaiaimages) qui donne la parole aux habitants de ces lieux hors du commun. Ils nous racontent la diversité des expériences, de leurs réussites et de leurs difficultés…._

![Voix des oasis Demain en main](./DemainenMain.png)


Cinquième épisode, on vous emmène à Demain en Main, un projet collectif situé à Locoal-Mendon, dans le Morbihan. Il a pour objectif la réalisation d’un écosystème complet de village rural intégrant habitat et activités économiques.

<iframe src="https://www.podcastics.com/player/extended/978/33917/" frameborder="0" height="200" width="100%" style="border-radius: 10px;"></iframe>


## Un collectif de 8 foyers   

A l’automne 2016, Julien et son frère Fanch dessinent leur vision d’un éco-lieu associant production agricole, habitat et accueil. Leur slogan est alors “agir aujourd’hui, pour vivre ensemble demain”. Puis ils jettent la bouteille à la mer et un premier rendez-vous a lieu début 2017 à Plouhinec. Durant l’année 2017, le collectif se consolide grâce à des règles de gouvernance participative. Aujourd’hui il se compose de 8 familles de 0 à 60 ans.

![Voix des oasis Demain en main](./Demain en Main - Alexandre Sattler10.jpg)

##Un hameau qui devient une oasis agricole

Le hameau est situé sur la commune de Locoal Mendon, dans le pays d’Auray. C’est une parcelle de 20 hectares composée de 4 bâtiments - aujourd’hui des gites -, d’un hangar, d’un four à pain, de 10 hectares de terre agricole et le reste en forêt. La propriétaire a été rencontrée dès l’été 2017 et le collectif l’aide déjà dans l’entretien du lieu et l’accueil des gîtes. La confiance s’est installée et une solution a été trouvée pour un achat progressif du lieu par le collectif.

![Voix des oasis Demain en main](./Demain en Main - Alexandre Sattler13.jpg)

##En savoir plus
- [Visitez leur site](https://demainenmain.fr/)
- [Leur page facebook](https://www.facebook.com/AssoDemainEnMain/)

---

<center><h1>La Voix des Oasis : 35 podcasts pour entendre les oasis de l’intérieur</h1></center>

_Depuis des années, des centaines de lieux développent des solutions efficaces pour habiter joyeusement cette planète tout en préservant les écosystèmes. En ville ou à la campagne, tous ont en commun de remettre le collectif, l’autonomie, la sobriété et la solidarité territoriale au centre de leurs vies.	En partenariat avec [Kaizen Magazine](https://kaizen-magazine.com/), [Colibris](https://www.colibris-lemouvement.org/) et [Alexandre Sattler](https://www.facebook.com/gaiaimages), la Coopérative Oasis est allée à leur rencontre pour les interroger._

_Vivre en oasis ne signifie pas systématiquement vivre isolé et à la bougie ! Chaque écolieu ressemble au collectif qui lui donne vie. Il existe autant d’architectures humaines et habitées que de lieux ! Comment s’organise l’écovillage ? Comment est réparti le travail commun ? Comment prend-on les décisions ? Qu’en est-il des questions financières : achat de terrain, des maisons, des matériaux, des outils… ? Autant de questions auxquelles chaque lieu répond à sa façon._

_La parole est donc donnée à tous ceux qui ont fait le choix du collectif et de la sobriété. Cette série de 35 podcasts tente de dessiner un aperçu de la diversité des expériences, de leurs réussites et de leurs difficultés. Et peut-être, de donner à certains l’envie de se lancer…_
