---
title: "Vit-on mieux dans les écolieux qu’ailleurs ?"
date: "2021-02-23T06:40:32.169Z"
layout: post
type: blog
path: "/vit-on-mieux/"
image: ./Conversation1.jpg
categories:
  - tribunes
---

_Cet article a été publié sur [The Conversation France](https://theconversation.com/vit-on-mieux-dans-les-ecolieux-quailleurs-154891) le 18 février 2021_
![Logo Conversation](./logo-conversation.png)

![The Conversation](./Conversation1.jpg)

Les épisodes de confinement et le travail à distance liés à la pandémie mondiale de Covid-19 rappellent le caractère fondamental des relations humaines (familiales, amicales…) dans le bien-être individuel et collectif. Dans ce contexte, l’idée d’un retour à un mode de vie plus sobre, plus local, et donc plus résilient devient très attirante.

Les habitats partagés et les écolieux, dont c’est précisément la raison d’être, connaissent ainsi une [forte croissance de leur nombre](https://www.ademe.fr/sites/default/files/assets/documents/dossier_tour_de_france_colibris_ademe.pdf).

Ces écolieux se définissent selon [trois grandes caractéristiques](https://cooperative-oasis.org/definition-ecolieux/) : la vie collective, la recherche de sobriété et l’ouverture sur l’extérieur. On en recense [près de 1 000 en France](https://cooperative-oasis.org/les-oasis/#oasis-map), soit autant de laboratoires pour réfléchir sur les tendances et scénarios à venir dans le cadre de la transition écologique et sociale.

![The Conversation](./Conversation2.jpg)

L’impact environnemental positif de ces lieux est en outre de mieux en mieux [documenté](https://www.colibris-lemouvement.org/sites/default/files/article/etude-carbone4.pdf). En revanche, aucun outil de référence n’existe pour mesurer leur impact social.

## Le RCI, un indicateur pertinent

Depuis un an, cette question a fait l’objet d’une étude dans quatre écolieux en France, en partenariat avec la Coopérative Oasis et l’Agence de l’environnement et de la maîtrise de l’énergie (Ademe). Cette recherche s’inscrit dans la continuité des travaux du [programme CODEV de l’ESSEC](https://irene.essec.edu/rubrique-2/codev) et du [Campus de la Transition](https://campus-transition.org/) autour d’un [indicateur de capacité relationnelle](https://hal-essec.archives-ouvertes.fr/hal-00815586/document) ou RCI.

Composite, ce RCI mesure la qualité des relations sociales selon 4 dimensions :
- l’accès à des réseaux socio-économiques,
- les relations personnelles,
- la dimension civique ou politique,
- la qualité du lien au milieu de vie.

Au sein de ces dimensions, les critères et seuils retenus sont adaptés au contexte à partir d’une approche mixte combinant qualitatif et quantitatif.

L’hypothèse principale de cette recherche consiste à relier la qualité relationnelle dans les écolieux avec des modes de vie fortement soutenables. Ces deux éléments conjugués sont des caractéristiques d’une [« vie bonne, avec et pour autrui, dans des institutions justes »](https://www.seuil.com/ouvrage/soi-meme-comme-un-autre-paul-ric-ur/9782020114585) au sens du philosophe Paul Ricœur.

![The Conversation](./Conversation3.jpg)

## Dynamiques de vie positives

Les 4 premiers lieux étudiés avec « l’approche RCI » présentent des caractéristiques très différentes. Le [Campus de la Transition](https://www.lemonde.fr/campus/article/2019/10/31/en-seine-et-marne-le-chateau-pour-etudiants-en-quete-de-sens_6017646_4401467.html), en Seine-et-Marne, est à la fois écolieu et centre de recherche et de formation. Le [Centre Amma, Ferme du Plessis](https://www.etw-france.org/les-centres-en-france/la-ferme-du-plessis/), en Eure-et-Loir, est un lieu de formation mais également un centre spirituel hindouiste. Le [Château Partagé](http://www.lechateaupartage.fr/), en Isère, est un lieu de vie collaboratif et rural. Enfin, [Mascobado](https://colibris-lemouvement.org/magazine/mascobado-une-oasis-centre-montpellier) est un habitat partagé urbain au cœur de Montpellier.

Dans ces quatre lieux, des points communs sont ressortis concernant la qualité des relations sur chacune des dimensions du RCI :

__- Dimension 1, conditions socio-économiques du lien social :__
Au-delà d’un certain seuil, les relations peuvent jouer un rôle de « capital social » et ainsi remplacer d’autres formes de ressources. Un acteur du Campus de la Transition explique que : _« Le revenu n’est pas si important si on a d’autres types d’investissements, notamment les investissements sociaux. »_

__- Dimension 2, relations interpersonnelles :__
La force du collectif dépasse la somme des individus, comme en témoigne un habitant du Château Partagé : _« Que ce soit le château ou n’importe quelle structure collective, le fait de faire ensemble, permet de faire plus. Il y a une mutualisation des biens et des moyens qui ouvrent beaucoup de possibles. »_

__- Dimension 3, engagement civique :__
Les dynamiques portées par un lieu (quand celui-ci a une mission), ou simplement discutées sur place, sont un facilitateur d’engagement comme le précise un résidant du Mascobado : _« Habiter ici m’a permis de vraiment construire une pensée civique et politique, et permettre un passage à l’action sans quoi rien ne peut vraiment changer. »_

__- Dimension 4, lien à la nature :__
Les relations des personnes entre elles et avec la nature sont étroitement connectées. Un ancien occupant de la Ferme du Plessis en témoigne : _« Mon séjour ici m’a fait changer ma vision des choses. Je ne respectais pas la nature et c’est ici que j’ai appris à reconsidérer la nature, et plus généralement la vie. »_

Une cinquième dimension est apparue importante, celle du lien à soi et de sa dynamique personnelle : vivre et/ou travailler en écolieu semble contribuer fortement à une reconnexion à soi, à une réflexion forte sur les parcours de vie engagés.

![The Conversation](./Conversation4.jpg)

Pour l’un des acteurs du Centre Amma – Ferme du Plessis, son lien étroit à ce lieu lui « permet d’avoir une très grande cohérence consciente entre toutes les dimensions de sa vie, de faire une expérience d’une vie complète ! ». Dans ce contexte, la [dimension spirituelle de la personne](https://www.lemonde.fr/series-d-ete/article/2020/08/21/dans-les-ecolieux-on-reinvente-des-rituels-pour-se-retrouver_6049512_3451060.html), qui tient notamment à la quête de sens, apparaît comme structurante de cette dynamique.

## Un terreau fécond à entretenir

Des points d’attention émergent et montrent aussi les fragilités potentielles de tels modèles. Par exemple, l’accompagnement de personnes présentant des vulnérabilités extrêmes (psychiques ou économiques) ne peut se faire uniquement au sein de l’écolieu mais nécessite des partenariats avec des acteurs spécialisés. De même, la gouvernance des lieux peut s’avérer un sujet permanent de controverses.

De ces constats, il est déjà possible de pressentir la caractérisation des écolieux en fonction de leurs missions, du type de public qu’ils accueillent et de leur situation géographique. De là, il sera envisageable de réaliser une première cartographie de cet écosystème.

Dans un contexte sanitaire et social difficile, les écolieux semblent favoriser des liens combinés aux autres et à la nature, très fructueux pour le bien-vivre individuel et collectif.

Peut-on pour autant les considérer comme des modèles ou des sources d’inspiration pour promouvoir une société du bien vivre centrée sur la qualité des liens ? Cette hypothèse – encore à étayer – invite en tout cas à [mesurer autrement la qualité de vie](https://theconversation.com/comment-concevoir-collectivement-le-bien-etre-soutenable-141469) et à explorer diverses manières de nous relier aux autres et à notre milieu naturel.

Dans cette optique, nos travaux confirment l’intérêt de l’usage du RCI. Depuis 10 ans, cet indicateur a déjà été appliqué à plusieurs contextes : évaluation de [projets de RSE](http://www.theses.fr/2017LIL12010) dans des pays du Sud ; effets d’une [catastrophe naturelle](https://www.tandfonline.com/doi/abs/10.1080/13600818.2017.1328046) ; comparaisons [macroéconomiques](http://theses.fr/2018PA01E015) ; prise en compte accrue des dimensions sociales et relationnelles [dans l’entreprise](https://www.cairn.info/revue-francaise-de-gestion-2020-2-page-51.htm) ; développement de nouvelles méthodes d’évaluation d’impact social [dans l’Économie sociale et solidaire](http://www.lelabo-ess.org/aux-captifs-la-liberation-mesurer-la-relation-aux.html).

![The Conversation](./Conversation5.jpg)

Ces applications ont notamment mis en avant l’intérêt de l’utilisation du RCI dans des évaluations, notamment avec les résultats empiriques suivants :

- Les programmes sociétaux menés par les [pétroliers dans le delta du Niger](https://www.emerald.com/insight/content/doi/10.1108/14720701211267810/full/html) permettent dans l’ensemble de diminuer la pauvreté multidimensionnelle, mais aux dépens de certaines formes de cohésion sociale, dont la confiance au sein des communautés.

- Améliorer les conditions de travail des [chiffonniers mexicains](https://www.cairn.info/revue-mondes-en-developpement-2017-4-page-87.htm) n’augmente pas nécessairement leur revenu, mais favorise leur empowerment (émancipation) sociopolitique.

- Ou encore, le [classement des pays](https://www.afd.fr/en/ressources/relational-capability-index-20) selon le RCI diffère de celui que l’on obtient en comparant leur PIB ou leur IDH, avec toutefois des corrélations.

Si elle s’inscrit dans la continuité de ces travaux menés dans les pays du sud pour évaluer la qualité du lien social, l’étude en cours donne aussi une autre dimension à l’indicateur.

Jusque-là, le RCI était plutôt utilisé pour mettre en lumière des aspects du développement généralement mis de côté car moins quantifiable par les acteurs publics et privés. Le contexte de cette application aux écolieux est différent puisque l’indicateur est en complète cohérence avec les valeurs des structures évaluées : il prend une autre vocation qui pourra certainement inspirer d’autres acteurs dans leur démarche évaluative.

__Auteurs :__

_- Cécile Ezvan, Enseignante-chercheuse à Excelia, chercheuse associée à l'Université de Lyon, Excelia Group – UGEI_

_- Cécile Renouard, Enseignante-chercheuse à l'ESSEC et déléguée du Laboratoire du Campus de la Transition, ESSEC_

_- Hélène L'Huillier, Chercheuse et évaluatrice indépendante, partenaire du Campus de la Transition, ESSEC_

_- Pierre-Jean Cottalorda, Enseignant-chercheur, ESSEC_

_Ces travaux de recherche sont menés en partenariat avec la Coopérative Oasis et l'ADEME._
