---
title: "Quand Erasmus s’invite dans les écovillages..."
date: "2020-08-21T06:40:32.169Z"
layout: post
type: blog
path: "/erasmus/"
image: ./Erasmus1.jpg
categories:
  - histoires d'oasis
---

Les rayons de soleil apparaissent sur l’arrière-pays de Vintimille. À 10 km de la frontière franco-italienne, les contours de l’écovillage de Torri Superiore se dessinent. Il y a 30 ans, un groupe de jeunes utopistes s’est lancé dans la folle aventure de rénover ce hameau médiéval abandonné et en ruine. Aujourd’hui, l’utopie a laissé place à la réalité. Une vingtaine de personnes vivent sur le lieu de manière durable. Ils ouvrent leurs portes pour des séminaires et formations ainsi que pour accueillir, à travers leurs chambres d’hôtes, des personnes en quête de nature, d’écologie et de simplicité.

![Erasmus 2](./Erasmus2.jpg)

En ce mois d'octobre 2020, ça fourmille dans le village. 28 personnes sont arrivées hier soir. Elles viennent de Grèce, du Portugal, de Suède, d’Espagne, du Danemark, de France, d’Italie et d’Allemagne. Elles ont entre 20 et 35 ans. Chacune porte dans ses bagages ses rêves d’un monde plus durable. C’est d’ailleurs le thème de cette formation d’une semaine financée par l’Union Européenne à travers le programme Erasmus+ : développer des compétences de leadership pour accompagner des groupes de jeunes dans l’exploration du développement durable.

Pas à pas, le groupe se découvre, les échanges interculturels jaillissent et la diversité nous attire et nous challenge dans nos manières de penser et d’agir. Le programme nous emmène dans l’exploration de la relation à soi, aux autres et à la nature. On découvre différentes formes de leadership, différents outils de facilitation et aussi comment approcher et transformer les conflits, comment prendre des décisions en groupe, etc. C’est très riche en apprentissages !

![Erasmus 1](./Erasmus1.jpg)

Après quelques jours, c’est le moment de mettre tout ça en pratique, car sans expérience, l’apprentissage n’est pas complet. Une nouvelle mission arrive donc pour les groupes de travail qui viennent de se former : organiser un atelier d’une heure pour le grand groupe sur la thématique de votre choix ! Les idées fusent ! Le brainstorming est lancé, les groupes s’affairent à la tâche : quel sujet choisir ? Quels outils utiliser ? Comment rendre l’activité interactive, intéressante et significative ? … Travailler en groupe est un terrain d’apprentissage incroyable !

C’est le moment pour les participants de prendre le rôle de facilitateur et d’animer les différents ateliers. De la relation à nos émotions à l’exploration de nos talents en passant par la découverte du « Shadow Work » (travail de l’ombre) et l’utilisation du théâtre pour aborder le changement climatique ; les thèmes choisis sont divers, originaux et engagés, à l’image des participants. On aura même un atelier sur l’exploration du mouvement de la nature à travers le cycle menstruel.

_Article écrit par Lou Salomon_
