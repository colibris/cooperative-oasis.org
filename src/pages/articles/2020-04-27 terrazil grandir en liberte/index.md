---
title: "À Terr'Azïl, grandir en liberté dans la nature devient possible..."
date: "2020-04-27T06:40:32.169Z"
layout: post
type: blog
path: "/terrazil-grandir-liberte/"
image: ./terrazil1.jpg
categories:
  - oasis financées
---

_Au début de l'année 2020, Terr'Azïl s'est vu accorder un prêt de 120 000€ sur 10 ans par la Coopérative Oasis. Grâce à ce prêt, ils ont pu finaliser l'achat de la ferme Lessé au Mas d'Azïl. Ce prêt est associé à un accompagnement par la Coopérative sur plusieurs aspects._

Sur un plateau ariégeois entouré d'une crête, entre la grotte du Mas d’Azil et le Lac de Filheit, existe depuis janvier 2020, en partie grâce au concours de la Coopérative Oasis, l'écolieu [Terr'Azïl](https://www.terrazil.org/). Cette ferme de 74ha abrite 30ha de prairie, 44ha de forêt et 7 bâtiments à rénover. Pour le joyeux collectif constitué de 11 individus âgés de 18mois à 45 ans, l'idée est d'explorer et de redécouvrir le rapport au vivant. Leur devise : "Grandir ensemble naturellement."

![Terr'azïl](./terrazil1.jpg)

## Le soin de la liberté et de l'enfance

Terr'Azïl se définit avant tout comme un environnement "permaculturel au sens large",  explique Noémie, co-fondatrice. "Nous entendons avant toute chose trouver notre place dans le vivant. Par "nous", j'entends adultes... et enfants. Je le précise car ce sont souvent les grands oubliés de la permaculture ! Alors que chaque être, quel que soit son âge, doit pouvoir être en condition de s'épanouir, dans le respect des ses rythmes et ses spécificités."

![Terr'azïl](./terrazil2.jpg)

En effet, à Terr'Azïl comme dans les écoles dites démocratiques, les enfants ont la liberté de choisir ce qu'ils font de leur temps et de leurs journées. Les adultes n'ont envers eux aucune intention pédagogique mais sont toujours disponibles pour répondre à leurs questions et les accompagner dans une activité, quelle qu'elle soit. Il ne s'agit pas d'une école à la maison mais plutôt d'apprentissages autonomes et informels. Lesquels partent souvent de l'observation de la nature, des interactions et du jeu. Le moteur principal de cet apprentissage ? la soif des enfants de comprendre le monde et d'expérimenter. "Ici les plus jeunes sont les bienvenus dans les activités quotidiennes du lieu, et sont inclus dans les prises de décisions les concernant. Nous leur faisons confiance pour savoir ce qui est bon pour eux. Et on déconstruit par la même occasion les postures de sachant et d'apprenant. Nous grandissons tous ensemble", explique Jade, habitante du lieu, mère de trois enfants et animatrice nature.

## Du rêve au plateau ariégeois

Terr'Azïl est né de la rencontre de deux familles dans un certain "camping du bout du monde". L'une cherchait un lieu pour un accouchement à la maison, l'autre avait un petit cocon à partager. Très vite, les deux foyers se rendent compte qu'ils partagent les mêmes valeurs : minimalisme, liberté, lien à la nature... qu'ils aiment passer du temps ensemble et, qu'en plus, leurs compétences sont complémentaires !

"En un an, on a monté ce projet de dingue ! C'est allé très vite... se souvient Blaise, cofondateur et accompagnateur en gouvernance partagée. On a commencé par formuler nos envies, mettre en place une gouvernance... et puis ce terrain, au Mas d'Azil, nous est tombé dessus. C'était à priori beaucoup trop tôt pour nous, mais nous avons franchi les étapes les unes après les autres, petit à petit..."

![Terr'azïl](./terrazil3.jpg)

En janvier 2020, le collectif signe finalement l'acte d'achat du lieu pour un total de 530 000€. "Nous avions un peu de fonds propres, un prêt de la banque, des prêts de nos proches et de personnes qui nous soutiennent... mais ils nous manquait encore 120 000€ !" précise Grégory cofondateur et habitant du lieu, comptable de profession. C'est la Coopérative Oasis qui a finalement permis la réalisation du projet en permettant de franchir la dernière marche : elle a octroyé un prêt de 120 000€, grâce à l'argent d'investisseurs citoyens. Aujourd'hui, ils prévoient une nouvelle enveloppe de 120 000€ pour les travaux.

"Au-delà de notre besoin financier, faire appel à la Coopérative nous permet de faire partie plus intégrante du réseau des oasis. C'est important pour nous car nous souhaitons avant tout partager et diffuser les chemins de liberté que nous explorons..." complète Noémie, consultante en parentalité.

## Une terre en commun

Aujourd'hui, les deux familles habitent sur place. Elles s’attellent pour le moment à l'aménagement du lieu et à l'inclusion de nouveaux membres. L'objectif est de vivre sur place à 15 foyers en habitats légers individuels : constructions en terre et paille, dômes géodésiques, yourtes... Écologiques, ces structures démontables disent aussi le refus du droit de propriété privée, porté par les habitants. Juridiquement, le collectif n'est en effet pas propriétaire du terrain mais bénéficie d'un droit d'usage. "Notre passage sur cette terre ne doit pas laisser de dégradation indélébile, explique Blaise. Nous la considérons comme un bien commun et souhaitons l'ouvrir au maximum dès que nous en aurons les moyens." D'autant plus que les habitants le répètent : ils n'en seraient pas là sans le soutien, moral, matériel et financier de nombre de gens de leur réseau et du coin.

![Terr'azïl](./terrazil4.jpg)

Vivant dans la région depuis plusieurs années, Jade, Noémie, Blaise et Gregory inscrivent leur projet dans un réseau local déjà solide : petits festivals, marché de producteurs bio et artisans du coin.. Déjà proches des néo-ruraux de la région, le groupe entend également se mettre en lien avec les anciens. "Je crois que nous suscitons de la peur chez certains, et nous le comprenons bien ! Alors on essaie de se découvrir mutuellement pas à pas... Nous nous sommes liés d'amitié avec un berger qui a toujours vécu ici, c'est une belle première rencontre !" raconte Jade.

## Un chemin difficile et exigeant

La vie n'est cependant pas un long fleuve tranquille à Terr'Azïl. Si les habitants ont tous des bases de communication non violente, ils voient dans la vie quotidienne collective un défi sans cesse renouvelé. "Les autres sont comme des miroirs qui me troublent, ça active mes blessures intérieures raconte Jade. Et j'apprends à les verbaliser notamment grâce aux espaces que nous avons mis en place pour exprimer nos tensions. Cela rend créatif ! C'est aussi une force je crois, que de s'exercer à toujours se demander : en quoi cette colère parle de moi, quelle responsabilité ai-je là-dedans, comment puis-je dépasser ça...? C'est vraiment précieux pour apprendre à se comprendre et s'accepter dans nos différences."

![Terr'azïl](./terrazil5.jpg)

Pour Blaise, la difficulté vient davantage du manque de disponibilité. "Un projet comme celui-là signifie un flot d'activité, d'échéances, de sollicitations... L'enjeu pour moi est de parvenir à trouver le juste équilibre entre prendre le temps pour moi-même, mon couple, ma famille, ma tribu et le reste du monde !"

Une chose est sûre cependant, et ce malgré les difficultés : tous se sentent aujourd'hui à leur place. Et s'ils en doutaient, l'épidémie de coronavirus arrivée en France en mars 2020 le leur a confirmé. Jade le dit : ce confinement lui a permis de réaliser la valeur d'un mode de vie tourné vers l'autonomie alimentaire et la capacité à vivre en collectif.

<iframe src="https://www.podcastics.com/player/extended/978/34092/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
