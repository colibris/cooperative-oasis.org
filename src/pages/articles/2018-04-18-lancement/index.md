---
title: "L'histoire de la Coopérative"
date: "2018-05-21T22:12:03.284Z"
layout: post
type: blog
image: Mathieu.jpg
path: "/lancement-de-la-cooperative/"
categories:
  - entretien
---

*Entretien avec Mathieu Labonne, fondateur de la Coopérative, repris de [Colibris le Mag](https://www.colibris-lemouvement.org/magazine/). Découvrez ici l'histoire et la raison d'être de la coopérative Oasis.*

![Interview de Mathieu Labonne](./Mathieu.jpg)

* * *

**– En quelques mots, c'est quoi la coopérative oasis ?**

Depuis 3 ans, nous développons des outils pour faciliter l’émergence de nouveaux lieux de vie écologiques et solidaires : les oasis. On a travaillé sur les sujets de la gouvernance, de la communication, de la mise en réseau, de l’accompagnement juridique… Mais plusieurs oasis nous témoignaient qu’elles avaient aussi un soucis sur le financement. En effet les banques ne sont pas toujours ouvertes à des projets collectifs multi-activités car elles ne savent pas en évaluer le risque. On s’est mis à réfléchir pour trouver une solution. J’ai rencontré beaucoup de monde autour de la finance participative et de l’épargne citoyenne et je me suis rendu compte que ce n’était pas si inabordable que je le craignais au départ.

**– Pourquoi l'association Colibris lance-t-elle une structure de financement ? Est-ce son rôle ?**

Colibris est là pour servir ceux et celles qui veulent faire leur part et je crois que c’est vraiment notre rôle de savoir créer des outils communs qui répondent à des besoins concrets. Au lieu de laisser chaque oasis créer son propre outil de financement - au risque de drames quand le montage n’est pas d’équerre - on s’est dit qu’on pouvait créer un outil commun qui servirait à toutes. Colibris est à l’origine de ce projet et joue un rôle important dans sa gouvernance, mais cette coopérative devra aussi prendre son envol de façon plus autonome. C’est pour cela qu’il y a 6 collèges, qui représentent toutes les parties prenantes de l’écosystème Oasis (les oasis, les accompagnateurs, les partenaires…).
Quand nous avons commencé le projet, il a rapidement été clair que ce ne serait pas l’association Colibris elle-même qui pourrait le porter. Le format d’une entreprise de l’ESS (Economie Sociale et Solidaire) était primordial et on a choisi celui de la SCIC (Société coopérative d’intérêt collectif) pour le côté participatif et coopératif, en cohérence avec nos valeurs.

**– Quelle est la différence avec une banque ?**

La Coopérative n’est pas une banque. C’est une mutualisation de moyens. Nous ne faisons pas de l’argent avec de l’argent. Les oasis ne sont pas de simples clients mais des partenaires sociétaires. Pour que la structure se finance, elle facture aux projets le temps passé pour accompagner, soutenir la collecte, animer le réseau , mais elle ne demande en aucun cas des intérêts sur de l’argent. Plus qu’un prêt c’est un vrai partenariat entre l’oasis et la coopérative qui s’engage dans un véritable accompagnement, une mise en réseau, des conseils pour une bonne gestion...


**– Est-ce la seule structure de financement d'habitats alternatifs de ce type aujourd'hui ?**

Il y a quelques années, une autre SCIC avait été créée. Elle s’appelle : Cofinançons notre habitat. Dominique Schaclk et François Taconet étaient à l’origine de ce projet et nous ont beaucoup aidés à concevoir le montage juridique. Dominique est d’ailleurs au conseil d’administration de la Coopérative Oasis. Mais cet outil n’était pas adapté pour nos besoins et il leur manquait un réseau citoyen pour collecter de l’épargne. La force de la Coopérative c’est a priori aussi la capacité de Colibris de mobiliser des dizaines de milliers de citoyens prêts à faire leur part. D’autres structures existent, comme Habitat et humanisme, mais pour le compte de leurs propres projets. On peut aussi citer l’exemple de l’oasis Habiterre, aux portes du Vercors, qui a réussi par lui-même à financer sa belle maison commune grâce à l’épargne citoyenne de près de 100 personnes.

* * *

**– Quels projets peuvent prétendre au financement ? Doivent-ils être estampillés Colibris ?**

L’enjeu dans ce genre de projet est de savoir évaluer le risque et de bien comprendre le projet. Ainsi les oasis que nous connaissons déjà et dont nous avons accompagné l’émergence sont plus faciles à appréhender. On sait aussi l’importance de la dimension humaine, et plus on connaît les personnes plus on peut évaluer l’état du projet. Mais la coopérative est ouverte à toutes les initiatives d'habitat écologique et collectif, bien au-delà de celles qui sont liées à Colibris. On demande juste d’apparaître sur la carte de France des oasis et habitats participatifs que nous avons créé et animons avec la Coordin’action de l’habitat participatif dans une logique de partage d’expériences.

**– Que finance exactement la coopérative ?**

Nous finançons principalement ce que les banques financent mal. Par exemple la construction de bâtiments communs ou un emprunt pour rénover un lieu en propriété collective. On est aussi souvent confrontés à des besoins de court-terme comme des prêts relais impossibles à contracter auprès des établissements bancaires traditionnels. Prenons l’exemple d’un collectif de 8 familles qui construit une oasis avec 10 logements. Il faut un financement pour les 2 logements non encore attribués et avoir ainsi un peu de temps pour trouver 2 familles de plus. Il y a aussi des situations où une famille quitte un projet. Si c’est une SCI ou une autre forme juridique de propriété collective, il faut pouvoir la rembourser le temps d’inclure sereinement des remplaçants.

**– Qui sélectionne les projets financés ? et comptez-vous associer d'autres réseaux à la sélection des projets soumis ?**

On a la chance d’avoir déjà beaucoup de compétences dans le réseau, et notamment dans le conseil d’administration de la SCIC. Nicolas était avocat, Dominique est juriste spécialisé dans l’ESS, Ingrid accompagne des projets d’habitats participatifs… Les porteurs du projet constituent progressivement un dossier, qui inclut notamment une évaluation par un compagnon oasis. Jean-Philippe, fondateur de l’Ilot des combes, travaille à la Coopérative et accompagne les oasis qui demandent un prêt. Puis la dernière étape est le passage devant notre comité d’engagement. En plus des membres du CA, il inclut des compétences particulières en fonction des besoins. On a notamment déjà inclus Isabelle Casteras, qui dirige le pôle innovation dans une grande banque et est très ouverte à ce genre de projets. Elle apporte un regard complémentaire. Pour nous, il est essentiel d’avoir l’ouverture nécessaire pour accueillir la créativité des projets d’oasis, tout en évaluant avec conscience les risques. On est en effet est responsable du bon usage de l’argent des investisseurs citoyens.

* * *

**– Quel intérêt à mettre son argent dans la coopérative oasis ?**

Il y a un double intérêt d’après moi.
Le plus important est celui du sens et du sentiment de participer à une aventure qui peut jouer un rôle essentiel dans notre avenir. Imaginez que partant des 700 lieux créés ou en création aujourd’hui, ce qui est déjà incroyable en si peu d’années, on passe à plusieurs milliers dans les 10 ans qui viennent. Je vois à quel point cela aurait un impact sur les territoires et sur nos représentations collectives. Les oasis sont les catalyseurs d’une bascule plus globale. Notre argent peut servir à cela plutôt qu’à financer de l’armement, des industries polluantes… C’est au passage aussi ce que fait la Nef, banque éthique qui suit avec attention notre Coopérative Oasis et aimerait pouvoir bénéficier de notre expertise pour monter en compétences sur ce type de projets.
Le second intérêt peut être financier, pour ceux et celles d’entre nous qui paient des impôts. Un livret A ne rapporte plus rien, il est bloqué à 0,75%. Dans la Coopérative l’investissement en 2018 peut être déduit à au moins 18% des impôts sur le revenu, à répartir sur 7 ans. La répartition sur 7 ans veut d’ailleurs dire que même si vous ne payez pas d’impôts cette année, vous pourrez avoir cet avantage fiscal une prochaine année où vous paierez des impôts sur le revenu. La coopérative permet donc de flécher l’usage d’une partie de ses impôts.
18% sur 7 ans, cela fait une moyenne de plus de 2,5% ! Je ne sais pas aujourd’hui qui peut vous garantir un tel intérêt financier, pas une banque en tout cas ! Si vous prenez 1000 euros de parts, vous déduisez 180 euros de vos impôts et vous retrouvez vos 1000 euros dans 7 ans. Et une loi a même été votée pour passer ce taux à 25%, mais les décrets ne sont pas encore sortis.

**– Y a-t-il un montant et une durée minimum pour prêter à la coopérative ?**

On a fixé le minimum à 1000 euros pour que le travail que chaque investissement nous demande soit rentabilisé. On encourage bien sûr celles et ceux qui le peuvent à investir plus.
Il n’y a pas de durée minimum, vous pouvez revendre vos parts quand vous voulez. Pour bénéficier de la déduction fiscale, il faut cependant bloquer l’argent pendant au moins 7 ans.

**– Est-on certain de récupérer la mise de départ ?**

Par principe il y a toujours un risque dans un investissement. Toutefois, je suis vraiment confiant pour plusieurs raisons. La première est lié au fait que nous n’avons pas d’autres créanciers. L’investissement est souvent remboursé après des emprunts ou d’autres titres, là les investisseurs récupèrent facilement leur contribution.
La deuxième est que nous faisons ce qu’il faut pour choisir les bons projets, en les évaluant avec vigilance et discernement et en les suivant tout le long du financement avec des visites sur place, de l’accompagnement, en présentiel et à distance, et des points téléphoniques, ce qu’une banque ne fait jamais. Enfin une troisième raison est que l’investissement vous permet de devenir sociétaire et donc de connaître l’état de la Coopérative et participer à ses décisions.

**– Un lien entre les financeurs et les projets va-t-il être tissé ?**

Au-delà d’informations sur l’avancement du projet, il faudra le construire, oui. Avec la Coopérative on crée vraiment une nouvelle communauté autour des oasis. C’est une belle aventure collective qui commence et je suis enthousiaste à l’idée que dès cet automne des oasis puissent passer des caps grâce à nous tous.
