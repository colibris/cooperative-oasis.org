---
title: "Au Moulin Bleu, néoruraux 2.0"
date: "2020-12-03T06:40:32.169Z"
layout: post
type: blog
path: "/Moulinneoruraux/"
image: ./Moulin 1.jpg
categories:
  - histoires d'oasis
---

**Dans le Loir-et-Cher, un collectif d’écologistes s’est installé dans un vaste domaine comprenant un ancien moulin. Leur objectif : en faire un lieu d’accueil pour les collectifs climat et les luttes, ainsi qu’un espace ancré dans son territoire : [le Moulin Bleu](https://lemoulinbleu.org/le-moulin/).**

_[Article de Laury-Anne Cholez et Mathieu Génon paru le 2 décembre 2020 sur Reporterre](https://reporterre.net/Au-Moulin-Bleu-neoruraux-2-0)_

![Moulin 1](./Moulin 1.jpg)

Le confinement est parfois prétexte à de belles histoires. Sur les bords du Loir, dans le village de Saint-Jean-Froidmentel, à la frontière entre la Beauce et le Perche, un groupe d’amis est devenu propriétaire du moulin de Vernouillet, le plus ancien de la région.

L’acte de vente a été signé le 11 mars dernier, quelques jours seulement avant le début du premier confinement. _« À l’origine, seules deux personnes devaient venir vivre ici à plein temps »_, dit Anaïs, en nous faisant faire le tour du propriétaire. _« Mais à l’annonce du confinement, une quinzaine ont débarqué. Cela a donné un coup de pouce pour rendre le lieu agréable. Et plein de projets ont été lancés en même temps. »_

![Moulin 2](./Moulin 2.jpg)
_Un petit troupeau de moutons broute dans les champs à l’arrière._

L’endroit est un champ des possibles de treize hectares dans lequel les habitants veulent prendre soin « du vivant », « du patrimoine » et « des luttes ». Une vaste longère rénovée peut accueillir confortablement une vingtaine de personnes. Plusieurs dépendances abritent l’atelier vélo, le poulailler et le futur atelier de menuisier. À l’arrière, des champs où broutent un petit troupeau de moutons, un âne et un poney.

Enfin, sur une petite île à cheval sur le Loir, le fameux moulin. C’est un vaste bâtiment de cinq étages dans lequel trônent toujours d’imposantes machines en bois. Des reliques d’un temps où l’on moulait le grain à la force de l’eau. Le potentiel hydroélectrique de l’installation est d’ailleurs à l’étude. L’ancien propriétaire estimait que la production pourrait rapporter entre 30.000 et 40.000 € par an pour un investissement de 200.000 € dans de nouvelles écluses, une passe à poissons et une nouvelle turbine. Une manne financière à long terme qui aiderait la communauté à pérenniser sereinement leur projet.

Car investir un tel lieu n’est pas à la portée de toutes les bourses. Il a fallu débourser 925.000 € pour acquérir le domaine. Le tout grâce à un apport de 450.000 € et à un prêt bancaire de 500.000 €. [La coopérative Oasis](https://cooperative-oasis.org/) leur a octroyé un second prêt d’un montant de 200.000 € qui leur sert de trésorerie pour les rénovations. Chacun rembourse ensuite en fonction de ses revenus. Au quotidien, les gens de passage donnent une heure de leur salaire journalier pour couvrir les dépenses en nourriture et diverses charges.

![Moulin 3](./Moulin 3.jpg)
_Les lieux servent aussi à télétravailler._

_« Le modèle économique tient surtout parce qu’on a encore beaucoup de gens qui ont gardé leur boulot parisien avec les salaires correspondants »_, précise Édouard, qui gère la communication du [Mouvement Colibris](https://www.colibris-lemouvement.org/). Beaucoup font encore la navette sur Paris, mais tous se demandent comment mettre leurs compétences au service de l’économie locale. _« Le tissu rural français mérite de se développer. D’autant qu’avec le numérique, nous ne sommes plus obligés de vivre dans une mégalopole pour faire un travail intellectuel »_, assure Charles Adrien Louis, consultant et co-fondateur du [cabinet B&L Evolution](https://reporterre.net/Climat-rester-sous-la-barre-de-1-5-oC-impose-des-choix-radicaux-sur-la). Il n’est d’ailleurs pas tout à fait d’accord avec Édouard à propos de financement du moulin : _« Si notre groupe avait moins d’argent, nous aurions trouvé un autre lieu moins cher mais certainement plus loin de Paris. »_

Dans un des salons de la longère, un immense buffet en bois sert de stockage aux courges du jardin. Des dizaines de cucurbitacée ronds ou biscornus attendent de finir en soupe ou en gratin. À côté, la bibliothèque est déjà bien garnie et des canapés servent souvent d’espace de télétravail. Camille s’y installe, enveloppée dans un plaid, pour nous raconter la genèse du projet. Ancienne consultante dans l’aide au développement, elle a abandonné sans regrets ses voyages entre la France et l’Indonésie pour venir s’installer ici.

_« C’est le lieu le plus grand et le plus cher que nous ayons visité. Au début, cela me faisait un peu peur car je ne voulais pas avoir de pression financière. Et heureusement ce n’est pas le cas, personne ne me tient rigueur de gagner moins et d’avoir plus de temps disponible. D’ailleurs si tout le monde travaillait 40 heures par semaine, le moulin ne serait pas dans cet état. »_ Camille s’est reconvertie pour devenir masseuse et commence à avoir une petite clientèle dans le coin. Avec Margaux, qui travaille dans une pépinière de _start-up_ agricoles à Chateaudun, et Montserrat, prof au lycée agricole de Vendôme, elles ont réussi le pari de trouver un emploi dans les environs.

![Moulin 4](./Moulin 4.jpg)
_Les habitants du Moulin Bleu disent vouloir tisser des liens avec leur territoire._

_« Je suis presque soulagée de ne pas avoir eu un temps plein au lycée, car je n’aurais jamais pu me consacrer à toutes les activités que je souhaite lancer ici »_, raconte Montserrat, qui donne aussi des cours de yoga dans le village. _« Je ne veux pas que le moulin devienne un lieu de campagne pour les gens qui vivent à Paris. Je veux qu’on investisse le territoire. »_

Montserrat, Camille ou Édouard ne sont pas des inconnus. Comme la quasi-majorité des habitants du Moulin, ils ont vécu dans la Maison Bleue, une grande [éco-colocation en banlieue parisienne](https://kaizen-magazine.com/article/quand-la-generation-y-experimente-leco-colocation/). C’est en hommage à leur ancienne demeure qu’il ont rebaptisé le projet « le Moulin Bleu ». Tous sont blancs, diplômés, et travaillent souvent dans le milieu climatique. Forts conscients de leur endogamie sociale et culturelle, ils ont le désir de s’ouvrir à d’autres. Avec succès. Omar, qui a passé son enfance au village voisin Saint-Jean-Froidmentel, s’est installé au moulin pour lancer son entreprise d’aide à domicile. Jean-Baptiste, menuisier, a également rejoint l’aventure. _« Je cherchais un endroit stable pour m’affranchir du joug du salariat. J’ai entendu parler de ce lieu par hasard, alors que je déménageais des meubles dans la ville voisine de Vendôme qu’on a apportés ici. »_ Pour lancer le futur atelier de menuiserie participatif, il a lancé [un crowdfunding](https://www.kisskissbankbank.com/fr/projects/oeuvresdutemps).

Et s’il vit au moulin actuellement, il ne compte pas y loger éternellement _« Je vois des limites dans le tout-partage. Il faut être lucide : certains ont besoin de plus d’espace que d’autres. Car dans la vie communautaire, il faut être vigilant à ne pas s’oublier dans le collectif. J’ai vécu huit ans chez les Compagnons et j’aspire à être seul, à goûter l’ennui, ces moments où peut naître la créativité. Et dans un groupe de vingt, tu n’as pas beaucoup d’espace pour t’ennuyer. »_

![Moulin 5](./Moulin 5.jpg)
_Le déjeuner est préparé collectivement._

Il est midi, une cloche sonne l’heure du déjeuner pour les ventres affamés. Au menu du buffet : des courges rôties avec de la salade du jardin, des falafels maison et du gâteau au chocolat. Chacun trouve une place autour de l’immense table en chêne qui trône au milieu de la cuisine. D’autres préfèrent s’installer dehors, le visage caressé par le frais soleil d’automne qui perce à travers le feuillage doré. Les chaises sont vite toutes occupées car le moulin est plein à craquer : vingt-deux personnes sont venues se confiner ici. Si l’accueil est un pilier inconditionnel du projet, ses modalités d’application entraînent aujourd’hui quelques tensions. _« Certains sont prêts à accueillir sans conditions car ils voient le lieu comme un réservoir de possible où ils peuvent expérimenter ce qu’ils n’ont pas pu faire pendant des années. D’autres sont dans des temporalités différentes. Ils veulent accueillir bien entendu, mais préfèrent renforcer le collectif avant »_, dit Camille.

Déjà cet été, plusieurs groupes sont venus planter leur tente dans le vaste champ à l’arrière de la longère et ont travaillé dans les salles de réunion rénovées du moulin. Accueillir des collectifs écolos, féministes, et autres associations de luttes sociales pour diverses rencontres et séminaires est un point central du projet. Mais il reste encore du travail pour remettre en état l’intérieur du bâtiment où les pigeons s’en donnent à cœur joie. _« On pourrait faire des résidences artistiques, un coworking ou des ateliers. Rien n’est encore figé, tout reste possible »_, rêve Édouard.

Mais pour l’instant, les habitants préfèrent faire profil bas et écouter avec humilité les gens du coin qui viennent à leur rencontre. _« On ne va pas commencer à les saouler avec l’écologie ou avec nos grands principes. Et puis, nous n’avons pas encore de projet fixe, on se laisse le temps de voir en fonction des rencontres »_, poursuit Édouard.

![Moulin 6](./Moulin 6.jpg)
_Les récoltes remplissent meubles et placards._

Leur première préoccupation est de tisser des liens localement, d’abord avec les 550 habitants du village. Ils ont déjà organisé avec succès deux fêtes des voisins et comptaient fêter Halloween avec l’association de parents d’élèves de l’école. Le confinement aura eu raison du projet, mais l’idée demeure : le Moulin Bleu se veut un lieu de rencontres, d’échanges et de partage.

Bien qu’elle soit encore toute jeune dans l’écosystème des « tiers-lieux », l’équipe du Moulin Bleu reçoit chaque semaine des dizaines de courriels et de demandes de conseils. Il faut dire qu’elle maîtrise à merveille les codes de la communication, avec un [site internet](https://lemoulinbleu.org/le-moulin/) attractif, de très belles photos et déjà quelques articles de presse. _« Même si nous avons bien défriché le sujet, nous sommes encore loin d’être prêts à donner des conseils »_, estime pourtant Charles Adrien Louis. Camille en revanche comprend l’engouement du grand public : _« Nous sommes une sorte de passerelle entre les gens mainstream qui s’identifient à nous plus qu’à un élevage collectif de chèvres dans le Larzac, par exemple. »_ Dans un monde où beaucoup d’urbains s’interrogent sur leur mode de vie et cherchent des alternatives, le Moulin Bleu tente d’apporter un début de réponse.

![Moulin 7](./Moulin 7.jpg)

Source : Laury-Anne Cholez pour Reporterre

Photos : © Mathieu Génon / Reporterre
