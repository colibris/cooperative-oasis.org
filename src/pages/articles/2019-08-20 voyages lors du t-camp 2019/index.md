---
title: La Coopérative Oasis anime les voyages du T-Camp
date: "2019-08-20T10:40:32.169Z"
layout: post
type: blog
path: "/voyages-t-camp/"
image: ./tcamp-totem.jpg
categories:
  - événements
---

_Lancé en 2019 par l'oasis du Campus de la transition et le mouvement Colibris, le T-Camp est une formation certifiante de 2 mois destinée à des jeunes diplômés sur la transition écologique et sociale. Elle inclut des cours de permaculture, de gouvernance partagée, d'économie, sur les communs, d'éthique... et se fonde sur le tryptique terre-corps-coeur. Une large place est donné à l'expérience concrète, avec la vie collective sur le domaine de Forges, siège du campus de la Transition, mais aussi 2 voyages dans des oasis qui ont été organisés par la Coopérative Oasis._

![VisiteTotem](./tcamp-totem.jpg) _Lors du second voyage les T-campeurs ont visité l'espace Totem, cofondé par Marc de la Ménardière._


Colibris et le Campus de la Transition ont choisi de travailler avec la Coopérative Oasis pour l'organisation des 2 voyages lors du T-camp 2019, première édition d'une longue série de formations certifiantes à venir.

Le premier voyage a eu lieu en avril après 3 semaines de formation théorique et pratique (constats globaux, permaculture, gouvernance partagée…) au sein du domaine de Forges, situé près de Fontainebleau,. Les étudiants ont pris la route en direction de l'Isère, la Drôme et l'Ardèche  afin d' appréhender l’échelle locale de la transition à travers des oasis qui innovent en agriculture permaculturelle, en écoconstruction et explorent la vie en collectif... Ils étaient notamment accompagnés de Gabrielle Paoli, chargée de mission à Colibris et membre du CA de la Coopérative Oasis.

Ils ont visité l'[Arche de Saint Antoine](https://www.arche-de-st-antoine.com/), [Ecoravie](http://ecoravie.org/), [les Amanins](http://www.lesamanins.com/), [Changement de Cap](http://www.changedecap.fr/), le [Hameau des Buis](https://hameaudesbuis.com/), [Ardelaine](https://www.ardelaine.fr/) et le [Viel Audon](https://www.levielaudon.org/), lors d'une semaine intense en rires, joies et témoignages. La diversité des projets rencontrés a inspiré leur créativité et a permis de mieux comprendre différents enjeux inter-reliés de la nécessaire transition.

![VielAudon](./tcamp-vielaudon.jpg) _Echange avec Yann Sourbier, un des piliers du Viel Audon en Ardèche._


Le financement du second voyage a été assuré grâce à une subvention accordée par l'[ADEME](https://www.ademe.fr/) à la Coopérative Oasis et a permis au T-Camp d'équilibrer ses finances. La Coopérative Oasis a mandaté Maïté Gayet, compagnon oasis, pour accompagner le groupe lors du deuxième voyage, celui-ci au Pays Basque et dans le Sud-Ouest. Maïté a pu notamment partager sa grande expérience en gouvernance et en animation de collectif.

Le groupe a d'abord découvert le projet d'[Arterra](https://arterrabizimodu.org/) au Pays Basque, siège du Global Ecovillage Network -Europe, pendant 3 jours. Ils ont pu expérimenter la vie collective de ce lieu et ses nombreuses réalisations écologiques. Ils ont ensuite découvert plusieurs projets dans le Sud-Ouest : le village de Lakabé (Espagne), la communauté Emmaus de Lescar Pau, l'[écohameau de Verfeil](http://verfeil-eco.over-blog.org/), le [village de Pourgues](http://www.villagedepourgues.coop/) et le projet [TERA](http://www.tera.coop/).

![ArcheSaintAntoine](./tcamp-arche-angel.jpg) _L'Arche de Saint Antoine est une oasis historique, dans une ancienne abbaye à Saint Antoine l'Abbaye en Isère._


Ces voyages ont été un grand succès et ont complété la formation des T-Campeurs. Ce sont des expériences sensibles et tangibles que les étudiants ont pu vivre à la rencontre de ces lieux innovants et pionners. Ils seront marqué à vie par les témoignages des porteurs de projet.

Mi 2019, la coopérative Oasis intègre le comité de pilotage du T-Camp  pour l'organisation du second T-Camp en avril et mai 2020, conçu pour 35 étudiants pour une durée de 2 mois. Le soutien de l'ADEME permettra à nouveau d'organiser des visites d'oasis et de faire vivre à ces jeunes une expérimentation sensible des nouveaux modes de vie à inventer et accompagner pour l'avenir. Le réseau qui se tisse grâce au T-Camp porte aussi de nombreux fruits car plusieurs étudiants ont commencé à s'engager dans différents projets rencontrés, voire à créer leur propre oasis !

Découvrez le T-Camp en vidéo [en cliquant ici](https://vimeo.com/347482896).
<iframe src="https://player.vimeo.com/video/347482896" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/347482896">T-Camp 2019 : retour en images</a> from <a href="https://vimeo.com/colibris">Mouvement Colibris</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
