---
title: "Film : Vivre dans un hameau autonome"
date: "2020-01-15T06:40:32.169Z"
layout: post
type: blog
path: "/film-vivre-hameau-autonome/"
image: ./film-vivre-hameau-autonome.jpg
categories:
  - histoires d'oasis
---

Tout proche de la ville de Chateaubriand, se trouve le petit ["Hameau du Ruisseau"](http://heol2.org/lecohameau-du-ruisseau/). Initié par un couple que l’on annonce volontiers comme étant parmi les pionniers de l’autonomie, ce collectif intergénérationnel abrite aujourd’hui plusieurs familles, réunies par un même objectif : celui de ne pas servir un système dans lequel ils ne se reconnaissent plus, et d’inventer ensemble une vie en cohérence avec leurs convictions. Convaincus que le bonheur passe par la sobriété, adultes comme enfants ont ici fait de leur vie un laboratoire de l’autonomie.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vZ8FF1DKI6A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ce documentaire fait partie de la série "Un Autre chemin". Dans un monde en perte de repères, des hommes et des femmes ont choisi de quitter les autoroutes surchargées de notre société sclérosée, pour inventer une vie qui qui leur ressemble, une vie proche de la nature et respectueuse de toutes formes du vivant.Entre modernité et pratiques anciennes, seuls, en famille ou regroupés en collectifs, ils ont fait de leurs vies de véritables laboratoires de créativité, toujours plus en accord avec leurs convictions. Respectueux à la fois de l’homme et de son environnement, leur quête de vie s’appelle Autonomie...
