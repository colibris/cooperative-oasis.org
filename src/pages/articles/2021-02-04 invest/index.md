---
title: "Devenez plus que des investisseurs solidaires"
date: "2021-02-04T06:40:32.169Z"
layout: post
type: blog
path: "/investisseur-solidaire/"
image: ./photo1.jpg
categories:
  - évènements
---

__Les 13 et 14 avril 2019, cette heureuse époque où nous pouvions nous rassembler, une vingtaine d’investisseurs de la Coopérative Oasis se sont retrouvés à l’oasis de Grain&Sens. L’objectif ? Découvrir de l’intérieur cet habitat collectif et écologique qu’ils ont contribué à financer.__

![Photo 1](./photo1.jpg)
_Lucette, Marie-Christine, Raphaël et Rémi, trois investisseurs et un habitant partagent leurs envies à l’ouverture du week-end ©Ed Carr_

En effet, Grain&Sens est accompagné par la Coopérative Oasis depuis le début de l’année 2019. Celle-ci accompagne le lieu en lui prêtant à taux zéro 100 000 euros sur 7 ans. Ce montant, que Grain&Sens n’a pas réussi à emprunter auprès des banques, lui a permis de lancer les activités sur le lieu, notamment l’accueil de stages et les gîtes. En parallèle, la Coopérative accompagne le collectif sur ses besoins durant toute la durée du prêt.

__La première année, la Coopérative Oasis propose à l’oasis :__
 - un soutien à la mise en place d’un design en permaculture ;
 - un soutien à la réflexion stratégique et notamment à la précision du modèle économique et du positionnement du lieu ;
 - un soutien à la mise en place d’une gouvernance adaptée, notamment par un regard extérieur, la mise en place de benchmarks et l’apport d’outils.

 ![Photo 2](./photo2.jpg)
 _Le groupe se réunit au début et à la fin de chaque jour pour jouer et échanger ses réflexions, émotions et impressions ©Ed Carr_

En plaçant leur épargne dans la coopérative, les investisseurs solidaires ont permis la mise en place de cet accompagnement. Ils ont également mis un pied dans le réseau des oasis, eux qui, pour la plupart, portent un projet d’installation en habitat collectif.

![Photo 4](./photo4.jpg)
_Rémi, Lucette, Josiane au jeu du “Soleil bille” ©Ed Carr_

Marie-Christine, Rémi, Fabianne, Jean-Luc, Mary et les autres participants ont découvert le lieu : 15 ha de forêt, 5 ha de prairie cultivable, 2 grands gîtes de 15 personnes entièrement rénovés. Ils ont également participé à des activités proposées par les 13 habitants de l’oasis :
 - la confection des pizzas dans le four à pain de Kim et Raphaël, paysans-boulangers installés sur place ;
 - une balade naturaliste avec Raphaël et son fils Jonah ;
 - la plantation des pommes de terre avec Romain ;
 - la dégustation de repas vegan imaginés et préparés par Victoria ;
 - un échange fort avec Astrid, Raphaël Kim et Romain autour des plaisirs et difficultés du vivre-ensemble ;
 - des jeux divers tout au long des deux jours…

 ![Photo 5](./photo5.jpg)
 _Romain, Astrid et Kim, trois habitants, ont partagé leur expérience du vivre-ensemble haute en couleurs ©Ed Carr_

Chaque investisseur a donc pu voir concrètement au service de quoi était mise leur épargne. Mais ils ont également été en mesure d’avancer sur leur propre chemin : voir que c’était possible, et appréhender de plus près les joies, les embûches et les tâtonnements d’un projet abouti…

 ![Photo 6](./photo6.jpg)
_Soirée confection collective de pizza, avec la pâte au levain préparée par Raphaël et Kim, paysans-boulangers installés sur place ©Victoria_

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en plaçant votre épargne dans la Coopérative !
    </a>
</div>
</div>

_Article écrit par Gabrielle Paoli_
