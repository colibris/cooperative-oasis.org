---
title: "Paillettes et grelinette #7 : Arche de Saint Antoine"
date: "2020-09-12T06:40:32.169Z"
layout: post
type: blog
path: "/Paillettes-et-grelinette-arche/"
image: ./Paillettes-et-grelinette-arche.jpg
categories:
  - histoires d'oasis
---

_Paillettes et Grelinette est une série de vidéos tournées en immersion dans des oasis du réseau de la Coopérative Oasis. Elle donne a voir les deux grandes facettes de la centaine d'écolieux qui jalonnent la France aujourd'hui : les paillettes - la joie, la fête, la convivialité, le partage, le glamour - et la grelinette - le lien à la terre et à la nature._

## Épisode 7 : Arche de Saint Antoine

<iframe width="560" height="315" src="https://www.youtube.com/embed/LHcwbxyfXFw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

La Communauté de l’[Arche de Saint Antoine](https://www.arche-de-st-antoine.com/) (Isère) a été fondée en 1987 dans un bâtiment de l'ancienne Abbaye de Saint-Antoine. Elle réunit une quarantaine de personnes unies autour de la non-violence, outil de transformation de soi et de transformation de la société. Toutes et tous expérimentent une vie communautaire faite de travail, de simplification des besoins, de conciliation et de réconciliation, de prière et méditation, ainsi que de chants, de danses et de fêtes.

La [Fève](http://www.feve-nv.com/) est un projet de la communauté de l’Arche de Saint-Antoine créé en 2010. Sa finalité est de transmettre, partager et encourager les pratiques et expérimentations du vivre-ensemble  qui favorisent une transition vers une société résiliente, respectueuse et bienveillante dans la relation humaine comme dans notre rapport au vivant, dans un esprit de non-violence.

Guillem a grandi à l'Arche, qu'il a quittée pour s'y réinstaller des années plus tard avec  sa famille. Magatli a rejoint l'Arche il y a quelques années, et y a rencontré son compagnon. Ils nous racontent dans cet entretien le fonctionnement, les contraintes et les joie de la vie quotidienne de l'Arche. Qu'est-ce que la non-violence ? En quoi l'habitat collectif et écologique est-il politique ? Quel cheminement intérieur cela implique-t-il ?


<iframe width="560" height="315" src="https://www.youtube.com/embed/asTSt0dtrLg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
