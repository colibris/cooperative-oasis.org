---
title: "Cet été, découvrez la vie en oasis !"
date: "2021-04-27T06:40:32.169Z"
layout: post
type: blog
path: "/voyages-oasis-2021/"
image: ./voyages1.jpg
categories:
  - événements
---

![Voyages en Oasis](./voyages1.jpg)

Pour répondre à l’intérêt grandissant du public pour les écolieux, la Coopérative Oasis a décidé d’organiser en 2021 **une série de voyages pour découvrir la beauté des oasis et les vivre de l’intérieur** ! Que l'on soit curieux de la vie en collectif, que l’on ait envie de vacances sensationnelles ou que l'on aspire soi-même à vivre dans un écolieu, les voyages oasis sont une formidable façon de profiter de son été…

Pendant **une semaine complète**, vous logerez dans au moins deux oasis différentes et **vivrez en immersion** dans le collectif. Plusieurs **excursions** vous permettront de découvrir d’autres écolieux, le temps d’un pique-nique, d’une **visite** ou d’un **chantier participatif**...

La [carte des oasis](https://cooperative-oasis.org/les-oasis/#oasis-map) comprend aujourd’hui plus de 1 000 lieux, tous très différents les uns des autres, tant en termes de taille que de niveau d'avancement, de territoire, ou d'activités… Difficile donc de s'y retrouver ! Ces **parcours** ont été pensés pour donner à voir une grande variété de lieux. Vous découvrirez à travers eux **la richesse et la diversité** de ce que recouvre le terme d'oasis. Et pourrez peut-être à votre tour imaginer la vôtre…

Quatre voyages, repas et hébergement compris, sont donc proposés cet été !

|        Voyages               |         Dates             |         Tarif         |
| ---------------------------- |: -----------------------: | --------------------: |
| Deux-Sèvres et Charente      | 21 - 27 juin 2021         | 480 € avant le 31 mai |
| Saône-et-Loire et Jura       | 19 - 25 juillet 2021      | 480 € avant le 31 mai |
| Drôme et Ardèche             | 19 - 25 juillet 2021      | 480 € avant le 31 mai |
| Grand-Est                    | 26 juillet - 1er août     | 480 € avant le 31 mai |

* * *

![La Kambrousse](./voyages3.jpg)

## Voyage Oasis "Vers les Oasis ressources" du 21 au 27 juin 2021 en Charente et Deux-Sèvres

**Où ?** En itinérance dans 4 oasis :
- [La Kambrousse](https://www.lakambrousse.org/) (trois nuits)
- [La Tour d’Auzay](https://www.latourdauzay.fr/)
- [La Forge du Vallon](https://www.laforgeduvallon.fr/page-viergece4b00f2) (trois nuits)
- [L’écohameau de Froidevon](http://ecohameau-interval.overblog.com/)

**Quand ?** Arrivée le lundi 21 juin après le déjeuner à La Kambrousse (Deux-Sèvres) et départ le dimanche 27 juin après le déjeuner de La Forge du Vallon (Charente)		

**À quel prix ?** 480€ TTC avant le 31 mai puis 500€ TTC par personne tout compris : hébergement (6 nuits en dur), nourriture, visites et animations sur place. Les trajets pour arriver et repartir sont à la charge des participants.
En cas d’annulation liée au covid nous assurons un remboursement à 100%.

![trajet Vendée](./trajetVendee.jpg)

**Repas et couchages** : Les repas sont bio, végétariens et préparés sur place avec les récoltes du jardin... Les couchages sont proposés dans des chambres à partager de 2 à 5 personnes. Nous adapterons la répartition des personnes en fonction des demandes !

**Transport** : Voitures partagées. Si vous venez avec votre voiture, vous bénéficierez du remboursement sur la base de l'indemnité kilométrique.

**Au programme** : Immersion dans les lieux : visites, participation à la vie collective (cuisine, jardin, chantiers), partages et jeux !

**Le groupe** : 15 personnes maximum. Couples et enfants sont les bienvenus !

**Questions et inscriptions** : gabrielle.paoli@cooperative-oasis.org

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="mailto:gabrielle.paoli@cooperative-oasis.org" class="btn btn-block btn-primary px-5">
      Je m’inscris
    </a>
</div>
</div>

* * *

![Ilot des combes](./voyages4.jpg)

## Voyage Oasis "Permaculture et changement de vie" du 19 au 25 juillet 2021 en Saône-et-Loire et Jura

**Où ?** En itinérance dans six oasis :
- [Le Domaine de Chardenoux](https://www.association-a-ciel-ouvert.org/lieux.aspx?lieu=147) (quatre nuits)
- [Solenvie](http://solenvie-oasis.org/)
- [Le Maquis de Safre](https://www.basededonnees-habitatparticipatif-oasis.fr/?LeMaquisDeSafre)
- [La Ferme de Chenèvre](https://www.colibris-lafabrique.org/les-projets/la-ferme-de-chenevre)
- [L’Îlot des Combes](https://www.lilotdescombes.fr/) (deux nuits)
- [Alôsnys](https://www.alosnys.com/)

**Quand ?** : Arrivée le lundi 19 juillet après le déjeuner au Domaine de Chardenoux (Saône-et-Loire) et départ le dimanche 25 juillet après le déjeuner de l’Îlot des Combes (Saône-et-Loire)		

**À quel prix ?** 480€ TTC avant le 31 mai puis 500€ TTC par personne tout compris : hébergement (6 nuits en dur), nourriture, visites et animations sur place. Les trajets pour arriver et repartir sont à la charge des participants.
En cas d’annulation liée au covid nous assurons un remboursement à 100%.

![trajet Bourgogne](./trajetbourgogne.jpg)

**Repas et couchages** : Les repas sont bio, végétariens et préparés sur place avec les récoltes du jardin... Les couchages sont proposés dans des chambres à partager de 2 à 5 personnes. Nous adapterons la répartition des personnes en fonction des demandes !

**Transport** : Voitures partagées. Si vous venez avec votre voiture, vous bénéficierez d’une indemnité kilométrique.

**Au programme** : Immersion dans les lieux : visites, participation à la vie collective (cuisine, jardin, chantiers), partages et jeux !

**Le groupe** : 20 personnes maximum. Couples et enfants sont les bienvenus !

**Questions et inscriptions** : gabrielle.paoli@cooperative-oasis.org

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="mailto:gabrielle.paoli@cooperative-oasis.org" class="btn btn-block btn-primary px-5">
      Je m’inscris
    </a>
</div>
</div>

* * *

![Viel Audon](./voyages5.jpg)

## Voyage Oasis "Autonomie et écoconstruction" du 19 au 25 juillet 2021 en Drôme et Ardèche

**Où ?** En itinérance dans 4 oasis, dans un rayon de 50km au Sud de Valence.
- [Le Viel Audon](https://levielaudon.org/)
- [Château Pergaud](https://www.chateaupergaud.fr/)
- [Ecoravie](http://www.ecoravie.org/)
- [Les Amanins](https://www.lesamanins.com/)

**Quand ?** : Arrivée le lundi 19/07 après-midi à Balazuc (07). Départ le dimanche 25/05 à Allex (26) en fin de matinée.

**À quel prix ?** : 480€ TTC avant le 31 mai puis 500€ TTC par personne tout compris : hébergement (6 nuits en dur), nourriture, visites et animations sur place. Les trajets pour arriver et repartir sont à la charge des participants.
En cas d’annulation liée au covid nous assurons un remboursement à 100%.

![trajet Drôme-Ardèche](./trajetdromeardeche.PNG)

**Repas et couchages** : Les repas sont bio, végétariens et préparés sur place avec des produits frais et locaux, voire du jardin... Les couchages sont proposés dans des chambres à partager. Nous adapterons la répartition des personnes en fonction des demandes !

**Transport** : Voitures partagées. Si vous venez avec votre voiture, vous bénéficierez d’une indemnité kilométrique.

**Au programme** : Immersion dans les lieux : visites, participation à la vie collective (cuisine, jardin, chantiers), partages et jeux !

**Le groupe** : 18 personnes maximum. Couples et enfants sont les bienvenus !

**Questions et inscriptions** : camille.bonneville@cooperative-oasis.org

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="mailto:camille.bonneville@cooperative-oasis.org" class="btn btn-block btn-primary px-5">
      Je m’inscris
    </a>
</div>
</div>

* * *

![Langenberg](./voyages6.jpeg)

## Voyage Oasis "Vivre mieux et contribuer à la vie locale" du 26 juillet au 01 août 2021 dans le Grand Est

**Où ?** En itinérance dans 5 oasis, dans un rayon de 50km sur l’axe Strasbourg-Nancy.
- [Ecolieu de Langenberg](https://www.ecolieu-langenberg.eu/)
- [Oasis Multikulti](https://www.oasismultikulti.org/)
- [Domaine de la Chouette](http://domainedelachouette.org/)
- [Ecolline](https://www.basededonnees-habitatparticipatif-oasis.fr/?EcollinE)
- [Château des Avettes](https://colibris-lemouvement.org/magazine/web-serie-oasis-chateau-avettes)

**Quand ?** Arrivée le lundi 26/07 après-midi à Lembach (67), départ le dimanche 01/08 à Réméréville (54) en fin de matinée.


**À quel prix ?** 480€ TTC avant le 31 mai puis 500€ TTC par personne tout compris : hébergement (6 nuits en dur), nourriture, visites et animations sur place. Les trajets pour arriver et repartir sont à la charge des participants.
En cas d’annulation liée au covid nous assurons un remboursement à 100%.

![trajet Grand Est](./trajetgrandest.PNG)

**Repas et couchages** : Les repas sont bio, végétariens et préparés sur place avec des produits frais et locaux, voire du jardin... Les couchages sont proposés dans des chambres collectives. Nous adapterons la répartition des personnes en fonction des demandes !

**Transport** : Voitures partagées. Si vous venez avec votre voiture, vous bénéficierez d’une indemnité kilométrique.

**Au programme** : Immersion dans les lieux : visites, participation à la vie collective (cuisine, jardin, chantiers), partages et jeux !

**Le groupe** : 10 personnes maximum. Couples et enfants sont les bienvenus !

**Questions et inscriptions** : camille.bonneville@cooperative-oasis.org

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="mailto:camille.bonneville@cooperative-oasis.org" class="btn btn-block btn-primary px-5">
      Je m’inscris
    </a>
</div>
</div>

## Conditions d'annulation
Pour tous les voyages, les conditions d'annulation sont les suivantes :
 - remboursement de la totalité jusqu'à un mois avant et de la moitié jusqu'à deux semaines avant.
 - toute annulation au cours des quinze jours précédant le voyage ne pourra conduire à aucun remboursement.
 - en cas d'annulation pour cause de covid, le remboursement sera effectif à hauteur de 100%, quelle que soit la date de l'annulation
