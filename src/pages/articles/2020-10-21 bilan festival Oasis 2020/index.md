---
title: "Revivre le festival oasis 2020 !"
date: "2020-10-21T06:40:32.169Z"
layout: post
type: blog
path: "/revivrefestivaloasis2020/"
image: ./Festival20200.jpg
categories:
  - événements
---

**Le festival oasis 2020 s'est déroulé du 28 septembre au 4 octobre au château de Jambville (Yvelines). 500 participants venus de toute la France, 80 lieux représentés et l'énergie antique qui rejaillit quand reviennent les temps difficiles. Certains disent même que la grâce s'est invitée...**.


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.colibris-outilslibres.org/videos/embed/24afdcde-2aa4-411c-b37b-0fe1769983f9" frameborder="0" allowfullscreen></iframe>

## En chiffres

**Le Festival Oasis 2020** s'est déroulé au Château de Jambville (Yvelines) du 2 au 4 octobre 2019 avec :

- **500 participants** ;
- **80 oasis** et habitats participatifs représentés ;
- 48 présentations d'écolieux ;
- 40 formations et partages
- des dizaines de bénévoles extraordinaires ;
- 13 fûts de bière, 2 fûts de kombucha, 1 fût de limonade amenés par Gus et Agathe (Roue Libre) ainsi que 12 caisses de bière belge, la Bardaf de Catherine et Jonas ;
- 5 cercles samoan avec 5 intervenants : Marie Laroche (Le Moulin Bleu), Pierre Lévy (Les Colibres), Laurent Marseault, Anne-Laure Nicolas (Le Bois du Barde), Lydia Pizzoglio et Laurent Van Ditzhuyzen (UDN), Margalida Reus (Arche de Saint-Antoine) ;
- 3 sessions de jeux de confiance et de coopération avec Jonas et Brigitte ;
- 1 auberge de la parole animée par Élise ;
- 1 bal folk animé par la lumineuse Brigitte.

<br/>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/wiki/?QuestionnaireFestival2020" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Remplir le questionnaire de satisfaction du Festival Oasis
    </a>
</div>
</div>



## En photos

_Cliquer sur la photo pour accéder à l'album complet_

<a data-flickr-embed="true" href="https://www.flickr.com/photos/190728599@N06/albums/72157716539460973" title="Festival Oasis 2020"><img src="https://live.staticflickr.com/65535/50508227668_b5b614473e_z.jpg" width="640" height="480" alt="Festival Oasis 2020"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Les photos ont été prises par [Clément Osé](http://www.clementose.art/). Elles sont soumises à la licence CC by SA, vous pouvez les réutiliser en mentionnant le crédit : **Coopérative Oasis / Clément Osé**.
