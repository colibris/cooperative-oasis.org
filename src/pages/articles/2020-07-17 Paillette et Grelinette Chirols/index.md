---
title: "Paillettes et grelinette #5 : Le Moulinage de Chirols"
date: "2020-07-17T06:40:32.169Z"
layout: post
type: blog
path: "/Paillettes-et-grelinette-chirols/"
image: ./Paillettes-et-grelinette-chirols.jpg
categories:
  - histoires d'oasis
---

_Paillettes et Grelinette est une série de vidéos tournées en immersion dans des oasis du réseau de la Coopérative Oasis. Elle donne a voir les deux grandes facettes de la centaine d'écolieux qui jalonnent la France aujourd'hui : les paillettes - la joie, la fête, la convivialité, le partage, le glamour - et la grelinette - le lien à la terre et à la nature._

## Épisode 5 : Le Moulinage de Chirols

<iframe width="560" height="315" src="https://www.youtube.com/embed/59wTi_ksvqQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Né en 2015, le collectif du Moulinage de Chirols (Ardèche) a décidé de réhabiliter une ancienne usine ardéchoise de fil de soie. L’objectif ? En faire un lieu ouvert regroupant activités artistiques et artisanales, habitat participatif et espaces communs publics. Intérêt général, refus de la spéculation, préservation des écosystèmes et engagement citoyen sont les maîtres-mots des 23 personnes qui ont signé l’acte d’achat au printemps 2019… et on bénéficié d'un prêt de la Coopérative Oasis pour engager les travaux !


Sylvain, urbaniste cofondateur du projet et Felix, en stage au Moulinage depuis plusieurs mois, nous racontent dans ce podcast ce qui les a conduits au Moulinage. Ils nous racontent ce qu'ils y vivent, les difficultés qu'ils rencontrent tous les jours, les joies... Comment change-t-on de vie, passant de la capitale à un village d'une centaine d'habitants ? Comment vit-on en collectif dans un chantier quotidien ? Est-ce un geste politique, artistique ou juste une recherche de bien-être?


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.colibris-outilslibres.org/videos/embed/b735b9d4-250e-4428-bc35-7e225e1cd970" frameborder="0" allowfullscreen></iframe>
