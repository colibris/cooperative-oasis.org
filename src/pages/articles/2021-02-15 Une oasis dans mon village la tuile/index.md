---
title: "Une oasis dans mon village : la tuile !"
date: "2021-02-15T06:40:32.169Z"
layout: post
type: blog
path: "/oasis-village-tuile/"
image: ./LaurentMarseault.jpg
categories:
  - tribunes
---

__[Laurent Marseault](https://cocotier.xyz/?PagePrincipale) aide les groupes à mettre en œuvre des projets coopératifs avec des outils libres et ouverts. Depuis des années, il soutient le réseau des oasis en posant sur lui un regard critique sans complaisance qui le fait grandir… et beaucoup rire ! À l’occasion du [Festival Oasis 2020](https://cooperative-oasis.org/revivrefestivaloasis2020/), Laurent a interpellé les collectifs sur leur capacité à s’intégrer aux territoires sur lesquels ils s’installent… Une bonne occasion d’explorer les enjeux d’une coopération territoriale sincère !__

<iframe width="560" height="315" src="https://www.youtube.com/embed/iDTCPQwqBtk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Travailler en réseau localement

En s’installant sur un territoire, un collectif peut se demander : y sommes-nous les seuls à mener une action écologique ? La réponse est forcément non, bien entendu. Le travail doit donc toujours être fait de recenser au préalable toutes les autres structures qui s’engagent autour d’une même cause, bien identifiée. Alors seulement, il est possible d’envisager ce qu’on appelle communément le travail en réseau.

Le réseau, s’il est bien animé et bien géré, permet d’avoir des actions concertées et cohérentes permettant d’être plus efficaces. Mais en quoi cela consiste-t-il exactement, le travail en réseau ?

![Les Usines](./LesUsines.jpg)
_Les Usines_


Le réseau n’est pas une entité plane horizontale ; il se tisse à différentes échelles. La collaboration s’effectue à chacune de ces échelles et de façon différente. Si l’on prend l’exemple des oasis, travailler en réseau, c’est travailler ensemble :
 - au sein même d’une oasis entre les membres du collectif ;
 - au sein du réseau des oasis, entre toutes les oasis du territoire ;
 - au sein du réseau de tous les autres acteurs de la transition qui s’engagent autour de la notion d’habiter autrement le territoire (quartiers en gestion citoyenne, tiers-lieux…).

 ![La Maison forte](./LaMaisonForte.jpg)
 _La Maison forte_

## La porosité ou le désert

Un réseau peut être fermé et fonctionner en circuit clos si sa raison d’être le justifie. C’est ce qu’ont pu faire les réseaux de résistance pendant la guerre, par exemple. Si l’on veut que le réseau soit ouvert, si rien ne justifie de travailler caché, il faut veiller à bien faire circuler les flux sortants et entrants, au risque sinon, de finir par faire un désert autour de soi.
À toutes les échelles précédemment identifiées, il s’agit donc d’abord de donner de la visibilité à ce que l’on fait et produit. Non pas par de la matière brute et indigeste mais au travers d’une synthèse exploitable. Ce flux sortant permet de créer du commun et de transmettre des compétences aux autres acteurs du réseau.
À toutes les échelles, il s’agit également de reconnaître, de recevoir et de se laisser inspirer par ce que font et produisent les autres structures. Pour cela, pas de mystère, il suffit de pratiquer l’écoute apprenante. Qu’est-ce à dire ? Prenons l’exemple d’une oasis juste à côté de la mienne qui fait de l’accueil gratuit alors que les stages que je propose sont payants. Plutôt que de se dire qu’elle plombe la cause en faisant du dumping, le travail en réseau consiste à partager avec elle ce qui marche chez moi et à voir si je ne peux pas appliquer ce qui fonctionne chez elle. En reconnaissant que les deux structures œuvrent différemment mais de façon complémentaire…

![Ecohameau du Plessis](./EcohameauPlessis.jpg)
_Ecohameau du Plessis_


Nous sommes là au cœur de la notion de porosité qu’implique un réseau. L’enjeu est d’accepter qu’il y a des choses que l’on fait en partie pareil et des choses que l’on fait en partie différemment. Cette posture légitime la place de l’autre et permet de se mettre en respiration avec ceux qui sont autour de soi. Cela requiert une bonne dose d’assurance… et d’humilité.

## L’archipel pour horizon

Bien sûr, quand mon réseau fonctionne bien (une fois encore, à toutes les échelles), j’ai l’impression que je sais tout faire et que si tout le monde fait comme moi, les problèmes seront réglés. Cela s’entend pour chaque oasis, qui peut parfois donner l’impression, en arrivant sur un territoire, de prendre toute la place et de ne pas considérer ce que font ou ont fait avant lui les autres structures.

![Arche de Saint Antoine](./ArcheStAntoine.jpg)
_Arche de Saint Antoine_


L’idée de l’archipel, à l’inverse, consiste à dire : « Voilà ce qu’on fait et ce qu’on fait très bien (méthodo, pédagogie…). En revanche, on peut avoir intérêt à aller voir tel ou acteur, qui est en train de travailler à fond sur les nouvelles formes de démocratie. Nous on y connaît rien pour l’instant, venez nous expliquer...  »
Cette démarche renvoie à ce qu’Édouard Glissant appelle l’identité racine et l’identité réseau. L’identité racine est la mienne, celle qui a présidé à ma création et qui fait que je suis singulier. L’identité réseau, ce sont les valeurs partagées, ce qu’il y a de convergent entre toutes les identités des structures qui veulent travailler ensemble. Disons, par exemple, les valeurs humanistes.  

![Ressourcerie du Pont](./RessourcerieDupont.jpg)
_Ressourcerie du Pont_

Ainsi, dans un réseau comme dans un archipel, il y a des îles très proches, avec lesquelles la collaboration est intense et la communauté de valeurs très forte. Et il y a des îles beaucoup plus lointaines, avec lesquelles on travaille plus ponctuellement, mais qui restent dans notre réseau via un socle commun minimal de valeurs. Dans chaque réseau, il y a des passeurs, des personnes qui vont régulièrement alerter en disant : « Là, vous parlez du nouveau rapport aux élus, mais il y a déjà 30 groupes constitués qui bossent sur le sujet ». C’est lui, le chieur bien souvent, qui permet la perméabilité et l’archipel. À bon entendeur...

_Cette tribune écrite est issue d’une intervention donnée par Laurent Marseault à l’occasion du Festival Oasis 2017 au Domaine de Chardenoux, sur le pourquoi et le comment du travail en réseau._
