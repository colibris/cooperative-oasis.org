---
title: "La loi augmente la rémunération de l’épargne solidaire !"
date: "2020-09-01T06:40:32.169Z"
layout: post
type: blog
path: "/loi-epargne-solidaire/"
image: ./finance-solidaire.jpg
categories:
  - tribunes
---

_Bonne nouvelle pour toutes les entreprises de l'économie sociale et solidaire, dont fait partie la Coopérative Oasis ! Entre le 10 août et le 31 décembre 2020, chaque placement effectué au sein de la Coopérative Oasis pourra en effet bénéficier d’une déduction de l’impôt sur le revenu représentant 25% du capital placé. Et ce en lieu et place des 18% appliqués jusqu’alors. Le capital étant bloqué 7 ans, cette souscription représente une « rémunération » de plus de 3,5% par an… pour un placement vertueux et contributif !_

![Finance solidaire](./finance-solidaire.jpg)
_Image par Nattanan Kanchanaprat de Pixabay_


## Mettre son épargne au service d’un territoire

Il est désormais bel et bien possible de permettre à son épargne d’être utile pour la société. La Coopérative Oasis propose en effet à chacune et chacun de placer une partie de ses économies en souscrivant des parts sociales. Les fonds collectés sont prêtés à des collectifs portant des lieux écologiques et solidaires ayant fait le choix de la sobriété, de la protection de la biodiversité, de la revitalisation des campagnes et de l’activité économique locale et respectueuse…

<iframe width="560" height="315" src="https://www.youtube.com/embed/AQy0HO4Qn8c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

La Coopérative a une action nationale : elle mutualise les fonds, priorise les prêts en fonction des besoins et de la répartition géographique. Chaque associé contribue donc au financement de tous les lieux. Cependant, chacun peut choisir de « flécher » son épargne, en indiquant le lieu ou la région qu’il souhaite symboliquement soutenir.

## Un investissement sûr et intéressant

En plus des garanties diverses (cautions solidaires, hypothèques, accompagnement des lieux sur toute la durée du prêt) et des agréments et labels (Agrément Entreprise Solidaire d’Utilité Sociale, adhésion à Finansol), la Coopérative Oasis permet à chaque associé de bénéficier d’une déduction fiscale. L’article 137 de la loi de finance 2020 entré en vigueur le 10 août permet de déduire 25% de la somme placée dans la Coopérative, de son impôt sur le revenu.

![Finance solidaire](./schema-cooperative1.jpg)

Jusqu’ici fixé à 18%, le taux de réduction d’impôt est donc passé à 25% pour les investissements des particuliers réalisés entre le 10 août et le 31 décembre. Ainsi, pour un placement de 1 000€ (montant d’investissement minimal) bloqué 7 ans, un souscripteur peut déduire 250€ de son impôt sur le revenu.  La limite annuelle d’investissement reste de 50 000€ pour une personne célibataire et 100 000€ pour un couple.

Il n’est néanmoins pas obligatoire de bloquer son épargne 7 ans dans la Coopérative Oasis. Un souscripteur peut demander à tout moment à la Coopérative de racheter ses parts (compter environ un mois) mais doit alors renoncer à la déduction fiscale.

## Déjà 11 lieux financés grâce à 180 souscripteurs ayant placé plus d’1,5 million d’euros

Anne-Marie a entendu parler de la Coopérative Oasis par sa fille. En juin 2019, elle a décidé de placer 2 000€ pendant 7 ans dans la structure. Ça lui a permis de déduire 360€ de son impôt sur les revenus 2019, soit 18% de 2 000€. Ayant entendu parlé de la nouvelle réglementation, Anne-Marie a décidé de placer 3 000€ de plus en septembre 2020, et de bénéficier ainsi de 750€ de déduction de son impôt sur les revenus 2020, soit 25% de 3 000€.

![Finance solidaire](./schema-cooperative2.jpg)

Grâce aux 180 souscripteurs similaires à Anne-Marie, la Coopérative Oasis a récolté plus d’1,5M d’euros. Avec ces fonds, elle a déjà pu prêter à 11 lieux, répartis dans toute la France. N’ayant pas accès aux prêts bancaires, ces collectifs ont pu acheter leur terrain comme Demain en Main dans le Morbihan ou TERA dans le Lot, financer leurs travaux comme le Moulinage de Chirols en Ardèche, modifier leur modèle économique comme le Château Partagé en Savoie ou encore lancer leurs activités comme le Moulin Bleu dans le Loir-et-Cher.

Et Anne-Marie a pu voir concrètement à quoi servait son épargne dont elle ne se servait pas elle-même en participant à un week-end organisé pour les associés dans les lieux financés par la Coopérative…

_Article rédigé par Gabrielle Paoli / Coopérative Oasis_
