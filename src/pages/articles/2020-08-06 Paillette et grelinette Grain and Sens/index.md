---
title: "Paillettes et grelinette #6 : Grain&Sens"
date: "2020-08-05T06:40:32.169Z"
layout: post
type: blog
path: "/Paillettes-et-grelinette-grainandsens/"
image: ./Paillettes-et-grelinette-grainandsens.jpg
categories:
  - histoires d'oasis
---

_Paillettes et Grelinette est une série de vidéos tournées en immersion dans des oasis du réseau de la Coopérative Oasis. Elle donne a voir les deux grandes facettes de la centaine d'écolieux qui jalonnent la France aujourd'hui : les paillettes - la joie, la fête, la convivialité, le partage, le glamour - et la grelinette - le lien à la terre et à la nature._

## Épisode 6 : Grain & Sens

<iframe width="560" height="315" src="https://www.youtube.com/embed/w0vgzOp3yec" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Prononcé avec "et" ou "and", [GRAIN&SENS](https://www.grainandsens.com/) est un lieu de vie et de partage fondé en 2019. La volonté commune est de trouver une manière plus juste d'habiter, de cultiver la terre, d'éduquer, de se réaliser au sein d'un projet collectif. C'est sur le domaine de Lavenant, typiquement ardéchois, comprenant 15ha de prés et de bois, dont une majestueuse hêtraie, que les 7 adultes et 4 enfants on décidé d'établir leur rêve...



Romain est formateur en agroécologie et cofondateur de Grain&Sens. Margarita est comédienne et a rejoint le lieu une année après sa fondation. Ensemble, ils nous [racontent dans cette vidéo](https://www.youtube.com/watch?v=m7YbgjRCwbU&feature=emb_logo) ce qui les a conduits à Grain&Sens. Ils nous racontent ce qu'ils y vivent, les difficultés qu'ils rencontrent tous les jours, les joies... Comment change-t-on de vie, passant de la capitale à un bout de forêt en Ardèche ? Comment vit-on en collectif, le couple et la famille en même temps ? Est-ce un geste politique, artistique ou juste une recherche de bien-être ?

<iframe width="560" height="315" src="https://www.youtube.com/embed/m7YbgjRCwbU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
