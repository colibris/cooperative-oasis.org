---
title: "Rejoignez le groupe facebook dédié aux oasis !"
date: "2021-04-21T06:40:32.169Z"
layout: post
type: blog
path: "/groupe_fb_oasis/"
image: ./FBcouverture.jpg
categories:
  - tribunes
---

**Annonce de (bonnes) nouvelles, recrutement de nouveaux habitants pour une oasis en fonctionnement, recherche d’un lieu où se ressourcer quelques semaines, partage de ressources en ligne, de contacts, formations et événements… Toutes celles et ceux que les oasis intéressent ont leur [groupe facebook](https://www.facebook.com/groups/reseaudesoasis), un lieu de partage d’informations et de pépites !**

![Groupe Facebook](./FBcouverture.jpg)

_“Recherchons familles pour la création de l’écolieu « Art’Nature », à la croisée des départements 25, 70, 90.”_

_“Deconfinez vous au jardin ! Semaine de bénévolat à l'Oasis des  ges, en Corrèze, du 3 au 7 mai 2021.”_

_“Bonjour, nous cherchons un juriste pour la rédaction des statuts de notre SAS d'abord, puis de notre SCI. Avez-vous un contact à nous partager ?”_

_“Appel à résidence créative....”_

_“Un nouvel Oasis est monté, youpiii ... 4 hectares de forêt, une maison terre-paille, des yourtes, des caravanes, des roulottes, un petit paradis.”_

_“Avis de recherche. Bientôt 4 ans que nous sommes nomades. Nous souhaitons poser nos bagages entre l’ Aude et l'Ariège, terre nature et grands espaces...”_

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://www.facebook.com/groups/reseaudesoasis" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Découvrez le groupe du réseau national des oasis
    </a>
</div>
</div>

![Groupe Facebook](./FB1.jpg)

“[Le réseau national des oasis](https://www.facebook.com/groups/reseaudesoasis)” est le groupe facebook dédié aux écolieux collectifs. Habitants d’oasis, porteurs de projet ou personne s’intéressant à la question de près ou de loin ont toute leur place dans ce groupe. L'objectif de ce lieu numérique est de :
 - Partager ses nouvelles, son expérience, son quotidien
 - Partager ses bonnes pratiques, bons contacts, bons conseils
 - Demander de l'aide, proposer des missions de volontariat, des chantiers, des conseils...
 - Inviter la communauté à participer à des événements ou des formations en lien avec les Oasis
 - Proposer des terrains, des places dans son oasis, des maisons à vendre...
La particularité de ce groupe est qu’il n'est pas fait pour partager ses réflexions personnelles, sa vision de la vie ou ses combats politiques ; c'est avant tout un espace de partage d'infos pratiques !

![Groupe Facebook](./FB2.jpg)

Un groupe facebook permet aux membres de se rassembler autour d’un sujet afin d’organiser des événements, d’énoncer des objectifs, de publier des photos et de partager du contenu à ce sujet. [La Coopérative Oasis](https://cooperative-oasis.org/) a donc créé le groupe “réseau national des oasis” pour permettre à toutes celles et ceux intéressés par la question de pouvoir échanger en ligne. C’est elle qui modère cet espace au quotidien. Cette action s’inscrit dans un travail plus global d’animation du réseau des oasis - modération de la [carte des oasis](https://cooperative-oasis.org/les-oasis/#oasis-map), organisation de [rencontres](https://cooperative-oasis.org/revivrefestivaloasis2020/), [documentation des expériences](https://cooperative-oasis.org/wiki/?PagePrincipale)...

![Groupe Facebook](./FB3.jpg)

“Le réseau national des oasis” comprend aujourd’hui plus de 4 000 membres ; plus nous y serons nombreux, plus les partages apporteront des solutions alors…

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://www.facebook.com/groups/reseaudesoasis" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Rejoignez le groupe et partagez-le !
    </a>
</div>
</div>
