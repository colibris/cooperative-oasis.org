---
title: "Rd’Évolution, un écosystème de lieux et de personnes au service d’une écologie solidaire"
date: "2019-12-20T10:40:32.169Z"
layout: post
type: blog
path: "/rd-evolution-ecosysteme/"
image: ./ressourcerie-pont-7.jpg
categories:
  - histoires d'oasis
---

_Article écrit par Gabrielle Paoli et paru dans KAIZEN n° 47, novembre / décembre 2019. Ces lieux construisent un partenariat avec la Coopérative Oasis._

Depuis sa création en 2004 à Lyon, l’[association Rd’Évolution](http://www.rdevolution.org/) s’appuie sur le collectif pour préserver les écosystèmes et soutenir les personnes dans le besoin. Aujourd’hui située dans les Cévennes, l’association pratique la sensibilisation, la transmission de savoir-faire, l’accueil dans un lieu de vie collectif, le réemploi grâce à une ressourcerie, la [Ressourcerie du Pont](http://www.rdevolution.org/-La-Ressourcerie-du-Pont-), et bientôt l’achat d’une [“terre de convergence”](https://terre-de-convergence.org/?PagePrincipale) avec l’association “Le Village du Possible”… L’objectif ? mêler création d’emploi, sensibilisation à l’environnement et mise réseau à l’échelle du territoire.

![Ressourcerie du pont 1](./ressourcerie-pont-1.jpg)

## Au Vigan, la Ressourcerie du Pont allie activité économique et réduction des déchets

Dès 2004, l’association Rd’Évolution entreprend de nombreuses actions de sensibilisation à l’environnement. Grande marche de trois mois des Pyrénées à Paris en 2006, festival “Le souffle du Rêve” réunissant 15 000 personnes en 2013… En 2014, le collectif d’artistes activistes cofondé par Violette, Élise, Uto et Antoine investit une usine de 3 500m2 située sur la commune du Vigan, dans le Gard. Ils y ouvrent rapidement une ressourcerie : un lieu de collecte d’objet, de réparation, revalorisation, revente et sensibilisation à la réduction des déchets. “Quand on est arrivés, les habitants ont tout de suite pris l’habitude de nous apporter leurs objets. Et quand ils ont vu que ça générait des emplois, d’autres formes de soutien sont apparus, le bénévolat par exemple...” explique Uto. Aujourd’hui, la ressourcerie donne une seconde vie à 90% de la centaine de tonne d’objets qu’elle collecte tous les ans, dont 55% de réemploi direct. Le reste est réutilisé par des artisans, envoyé au recyclage ou à la déchetterie (6%).

![Ressourcerie du pont 2](./ressourcerie-pont-2.jpg)

Membre actif du “Réseau National des Ressourceries”, le lieu est animé par une équipe d’une vingtaine de bénévoles et de six salariés en CDI ou contrat aidé. “Bertrand, le deuxième salarié, a d’abord été bénévole pendant quelques mois. Âgé de 56 ans et habitant du coin, il a été formé dans sa jeunesse par un compagnon en ébénisterie, marqueterie et menuiserie. Aujourd’hui, c’est un valoriste hors pair qui transmet son savoir-faire” raconte Elise. La ressourcerie met en effet également des ateliers et outils à disposition d’artistes et d’artisans pour de la couture, menuiserie, soudure, vannerie, vidéo, musique, peinture… Ceux-ci peuvent aussi travailler à partir d’objets et de matières issues du réemploi.

Grâce à son activité, à des subventions, à des emprunts auprès de particuliers (apports avec droit de reprise) et à des dons, l’équipe a réussi à réunir en 2018 les 300 000€ nécessaires à l’achat de la ressourcerie dont elle est maintenant propriétaire.

![Ressourcerie du pont 3](./ressourcerie-pont-3.jpg)

## Dans les hauteurs du Vigan, Artimbal est un lieu de vie collectif, écologique et artistique qui accueille une partie de l’équipe de la Ressourcerie du Pont

Un partie du groupe qui assure l’activité de la ressourcerie habite à Artimbal, un écolieu dans les hauteurs du Vigan. Les espaces et outils communs permettent à la douzaine d’habitants de vivre dans la sobriété et la nature ; un bois entoure les deux anciennes bâtisses où cohabitent les habitants. Tout le monde participe au loyer et aux charges, entre 80€ et 150€ par mois et une cagnotte collective permet l’achat de nourriture. Le potager fournit au collectif une partie de ses légumes ; compost, toilettes sèches et produits ménagers faits maisons sont le quotidien de la vie d’Artimbal.

![Ressourcerie du pont 4](./ressourcerie-pont-4.jpg)

La gouvernance prend soin de tous les membres du collectif. Toutes les décisions y sont prises au consensus, et, si nécessaire, au consentement dans un deuxième temps. “Cependant, nous préférons la communication vivante à la “communication non-violente”. Comme le ciel, nous laissons éclater les orages et les ouragans, c’est naturel, les tensions se déchargent et le soleil finit toujours par revenir…” précise Uto.

![Ressourcerie du pont 5](./ressourcerie-pont-5.jpg)


“On accueille tout ceux qui veulent découvrir, mais on demande une participation aux chantiers collectifs et/ou financière.” explique Antoine. “On est très attachés à la dimension solidaire du projet. On facilite aussi régulièrement l’arrivée de personne souhaitant changer de vie et s’installer sur le territoire.” L’écolieu permet aussi à Antoine, Élise, Uto et leurs compagnons d’ajouter la dimension artistique qui leur est chère en accueillant des résidences d’artistes et en cultivant une identité de troubadours. “On fait beaucoup de chanson française et de musique tribale. La vibration est un levier extraordinaire. On a toujours utilisé l’art pour bâtir des ponts entre nous et les gens.” raconte Uto.

## Vers une “Terre de Convergence”

La Ressourcerie du Pont constitue en réalité l'acte numéro un d'un projet de transition active en trois actes. Le deuxième de ces actes consiste à acheter une “terre de convergence”, un lieu permettant la formation et la sensibilisation au travers d’éco-événements.

![Ressourcerie du pont 6](./ressourcerie-pont-6.jpg)

“En août 2019, nous avons organisé le festival “Terre de Convergence” qui a permis de mettre en lumière la pertinence d’une terre pérenne. Elle serait le lieu de libération et de construction de l’expertise citoyenne pour impacter les politiques locales et accélérer la transition territoriale.” raconte Élise. Le collectif continue donc son chemin avec la recherche d’une centaine d’hectares entre Nîmes, Montpellier et le sud des Cévennes. Une fois le lieu sanctuarisé, il restera une dernière étape : les “Gardiens de la Terre”, un soutien à l’accès à la terre pour des projets locaux de transition soutenant une tribu du monde ou une espèce en voie de disparition.

![Ressourcerie du pont 7](./ressourcerie-pont-7.jpg)
