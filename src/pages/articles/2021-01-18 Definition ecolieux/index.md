---
title: "Écohameaux, Oasis, Habitats participatifs, Tiers-lieux… quels points communs ?"
date: "2021-01-18T06:40:32.169Z"
layout: post
type: blog
path: "/definition-ecolieux/"
image: ./TiersLieux2.jpg
categories:
  - tribunes
---

**Vivre de façon écologique et collective est une pratique qui a le vent en poupe… Aujourd’hui, [une seule carte](https://cooperative-oasis.org/les-oasis/) réunit Oasis, habitats participatifs et quelques tiers-lieux. Elle rassemble près de 1000 lieux et projets…  qui se donnent volontiers les noms les plus divers ! Écohameau, écohabitat, Oasis, habitat participatif, habitat groupé, tiers-lieu écologique, ferme collective… La différence est-elle purement sémantique ou ces lieux sont-ils vraiment distincts les uns des autres ? Qu’est-ce qui différencie réellement une Oasis d’un habitat participatif ? Un tiers-lieu écologique d’un habitat groupé ? Tour d’horizon de l’habitat collectif et écologique en France !**

![Volume / Grain & Sens](./TiersLieux2.jpg)

## Petit lexique
- **Habitat Participatif** : des personnes se regroupent pour imaginer et réaliser un habitat collectif combinant espaces privés et espaces communs, avec un projet collectif de solidarité et mutualisation.
- **Habitat partagé, groupé** :  les noms précédemment utilisés, aujourd’hui réunis par le terme habitat participatif, consacré par la loi instituant ces formes alternatives d’habitat.
- **Oasis** : des lieux de vie et/ou d’activité, collectifs et écologiques ; orientés vers l’autonomie énergétique et alimentaire, ils associent habitat et activités économiques, agricoles et/ou artisanales pour la plupart d’entre eux.
- **Tiers-lieu** : un espace de travail partagé et collaboratif, un lieu intermédiaire de rencontres et d’échanges informels ; ils peuvent être des Oasis et sont quelquefois intégrés à des habitats participatifs.

## L’habitat participatif : un réseau issu des années 70, en forte expansion depuis les années 2010

Depuis les années 70, on assiste à une recrudescence des habitats collectifs auxquels s’ajoutent une forte dimension écologique. Il existe aujourd’hui trois grands réseaux en France : **l’habitat participatif**, **les Oasis** et les **tiers-lieux**.

Au fil du temps, les expériences d’habitat collectif autogéré, en autopromotion et/ou en autoconstruction, ou encore portées par des maitrises d’ouvrage associant citoyens et bailleurs sociaux, se sont multipliées. Résurgences des utopies collectives du XIXe siècle, elles ont été nourries par les mouvements [des Castors](https://les-castors.fr/) de l’après- guerre, puis par les mouvements autogestionnaires des années 70.

Le Mouvement pour l’Habitat Groupé Autogéré (MHGA) né dans les années 70 fédère des groupes de citoyens désireux de concevoir, créer et gérer leur habitat collectivement ; combinant espaces privatifs et espaces communs, en cohérence avec leurs moyens et leurs aspirations, ils développent des pratiques de solidarités, de mutualisation entre eux et ouvertes sur leur environnement. En 2006 les coopératives d’habitants se structurent en fédération et viennent compléter les formes juridiques possibles de constitution de ces projets, en mettant l’accent sur la non spéculation et l’accès pour tous à ces formes plus collectives d’habitat. Progressivement ces mouvements se rapprochent et s’organisent au sein d’une coordination nationale qui contribue  à l’élaboration de la Loi Alur, qui  en fixe les deux formes principales : les Sociétés d’Attribution et d’Autopromotion, et les Coopératives d’Habitants.

![©Alexandre Sattler](./MasCobado.JPG)

A partir des années 2010, les projets se multiplient avec une forte aspiration à générer des projets  ayant une qualité environnementale (en neuf comme en rénovation) et à se structurer sur le développement d’usages durables en sus d’être porteurs de solidarités et mutualisation. Ils se réalisent  en milieu urbain et péri urbain, dans les grandes agglomérations comme dans les petites villes et  en milieu rural, et peuvent accueillir dans leurs espaces communs des activités associatives extérieures, ou offrir des hébergements d’accueil. Ils peuvent être conçus comme des projets intergénérationnels, regrouper des retraités ou s’inscrire dans une dimension inclusive, se développer en locatif social ou dans des programmes mixtes. Petits collectifs de 5 logements ou plus grands ensembles de 20 à 30 logements, ils sont réunis par un désir « d’habiter autrement » avec leurs voisins et leurs territoires et d’être acteurs engagés des dynamiques locales.

En 2019 le mouvement et l’ensemble des acteurs qui y contribuent deviennent « _Habitat Participatif France_ ».

![©PatrikLazic](./patricklazic.jpg)


En 2014, la loi ALUR a défini l’habitat participatif comme « _une démarche citoyenne qui permet à des personnes physiques de s'associer, le cas échéant avec des personnes morales, afin de participer à la définition et à la conception de leurs logements et des espaces destinés à un usage commun, de construire ou d'acquérir un ou plusieurs immeubles destinés à leur habitation et, le cas échéant, d'assurer la gestion ultérieure des immeubles construits ou acquis._ »

![©PatrikLazic](./©patricklazic2.jpg)

## À partir ses années 2000, les Oasis se multiplient, davantage tournées vers la terre et l’autonomie

Aujourd'hui accompagnées et mis en réseau par la [Coopérative Oasis](https://cooperative-oasis.org/), les Oasis sont des lieux de vie et d’activité collectifs et écologiques. Majoritairement installés au cœur des territoires ruraux, ces habitats collectifs reprennent les principes des habitats participatifs en  ajoutant souvent des activités économiques, agricoles, artisanales et ou d'accueil. Ils tendent souvent vers l’autonomie.

![©PatrikLazic](./patricklazic9.jpg)

Impulsé par Pierre Rabhi avant les années 2000, puis réellement développé au sein du [Mouvement Colibris](https://www.colibris-lemouvement.org/) après 2014, le terme d’Oasis n’est pas un label mais une appellation très large définie par cinq grandes intentions qui forment un socle cohérent :
 - La **souveraineté alimentaire** : les Oasis souhaitent travailler la terre et créer des espaces naturels. Elles essaient de produire une partie de leur consommation grâce à des méthodes de cultures respectueuses du vivant, comme l’agroécologie, ou de cultiver des circuits courts.
 - La **sobriété énergétique** : les Oasis cherchent à minimiser l'impact de leur activité, que ce soit par l'écoconstruction, la performance énergétique, des actions de sobriété ou des choix de vie simple.
 - La **mutualisation** : les Oasis mettent en commun des ressources pour limiter leur impact sur l'écosystème et gagner en confort de vie. Il s'agit principalement de bâtiments et jardins partagés, éventuellement ouverts sur l'extérieur.
 - La **gouvernance partagée** : les Oasis cherchent des modes d'organisation collective qui respectent les individus et leurs besoins.
 - L’**ouverture vers l’extérieur** : les Oasis participent à la vie de leur territoire et partagent des activités et des événements avec l'extérieur. Sans faire nécessairement de plaidoyer pour promouvoir leur démarche, les lieux restent ouverts pour diffuser leurs pratiques et témoigner de leurs expériences.

La définition des Oasis est donc très large pour intégrer les multiples manières de vivre de façon collective et écologique qui sont adaptées à différents types de personnes. La caractéristique principale du réseau des Oasis est donc sa diversité : cela va du monastère à la communauté anarchiste, du lotissement familial en milieu rural à l'habitat groupé urbain, d’un lieu d’une seule bâtisse à d’autres avec plusieurs maisons ou des habitats légers (tiny houses, yourtes…). Ce très large réseau rassemble par l'expérience des collectifs aux origines idéologiques ou spirituelles très variées, plus ou moins engagés politiquement, qui partagent le souhait de faire d'une vie plus collective le levier d'un mode de vie plus joyeux et respectueux des humains et du Vivant.

Ainsi, au-delà d'être des lieux, Oasis est un mode de vie qui se base sur la relation humaine, la solidarité locale et le partage pour construire des lieux écologiques et résilients et pour contribuer activement à une transition de leur territoire.

## Les tiers-lieux dans le sillage de l’Economie Sociale et Solidaire

Pour ce qui est des tiers-lieux, la plupart sont nés de l’impulsion de travailleurs indépendants du secteur de l’Économie Sociale et Solidaire (ESS) désireux de se regrouper. Initialement, c’était pour partager un espace de travail (ou coworking, qui est avant tout une modalité d’organisation du travail), soit un bureau, un atelier, un fablab. Puis cela s’est étoffé avec d’autres types d’activités : culturelles, domiciliation d’entreprises, café associatif, librairie, jardin partagé, boutique partagée, galerie, salle de réception, etc. Formellement, il s’agit donc d’un espace de travail partagé et collaboratif, d’un lieu intermédiaire de rencontres et d’échanges informels, d’un espace de sociabilité mis en œuvre par un collectif, au service d’un territoire.

![©PatrikLazic](./grenoble1.jpg)

D’un point de vue historique les tiers-lieux s’inscrivent dans le mouvement d’idées du socialisme utopique : réconcilier les temps de vie, produire des communs, favoriser l’épanouissement et l’émancipation des individus, selon des principes démocratiques et solidaires. Conceptualisé dans les années 1980 par le sociologue urbain américain Ray Oldenburg, un tiers-lieu est censé être ouvert à tous, abordable et flexible avec une large prise en compte de l’individu, de sa capacité à adhérer, à s’adapter et à créer son projet au sein d’une communauté ouverte et porteuse d’une forte culture du collectif.

L’accueil inconditionnel du public est un principe fondateur pour de nombreux tiers-lieux, surtout dans les territoires ruraux où ces lieux sont des espaces pour organiser de nombreuses activités dont l’accès est plus aisé qu’en allant à la ville. Ils permettent de croiser des mondes qui ne se seraient pas rencontrés par ailleurs et de favoriser des échanges.

## Une seule carte pour tous les lieux !

L’intention de vivre en collectif et de prendre soin des hommes et des écosystèmes est un socle commun très fort à tous ces types de lieux. Ils prennent donc des formes très variées : allant de l’éco-village en milieu rural, aux habitats participatifs intergénérationnels, aux tiers-lieux ruraux ne proposant pas d’habitat. Ces lieux se donnent les noms qu’ils veulent se donner et ont souvent des identités multiples. Il n’est donc pas rare qu’ils s’inscrivent dans un ou plusieurs réseaux en même temps et se donnent des noms divers.

![Carte](./Carte.png)

Aujourd'hui, [une seule carte réunit Oasis, habitats participatifs et quelques tiers-lieux](https://cooperative-oasis.org/les-oasis/#oasis-map). Co-portée par [Habitat Participatif France](https://www.habitatparticipatif-france.fr/?AccueilHPF) et la Coopérative Oasis, elle réunit plus de 1000 lieux et projets… Au-delà des noms différents, ils manifestent ensemble une autre façon d'habiter et de vivre.

_Co-écrit par la Coopérative Oasis et les membres d’Habitat Participatif France_
