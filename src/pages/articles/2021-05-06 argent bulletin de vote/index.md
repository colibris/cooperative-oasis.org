---
title: "« L’argent est un formidable bulletin de vote »"
date: "2021-05-06T06:40:32.169Z"
layout: post
type: blog
path: "/argent-bulletin-vite/"
image: ./Argentvote1.jpg
categories:
  - tribunes
---

**La Coopérative Oasis permet à toutes celles et ceux qui le souhaitent de [mettre leur épargne au service de lieux de vie écologiques, les oasis](https://cooperative-oasis.org/investir/). Spécialiste de la relation à l’argent, Christian Junod revient pour nous sur ce qu’une telle démarche veut dire pour chaque investisseur solidaire. Dans cet entretien, il rappelle surtout qu’investir dans des projets vertueux permet de faire advenir une société à laquelle on aspire tout en dépassant ses peurs…**

![Argent vote](./Argentvote2.jpg)

« J’ai travaillé pendant dix-sept ans dans une grande banque Suisse en tant que conseiller en placement financier. J’ai été licencié en 2009 suite à la crise des subprimes … Heureusement, j’avais rencontré Peter Koenig 3 ans auparavant et avais pu grandement pacifier ma relation à l’argent, particulièrement ma peur de manquer.  Après avoir organisé des ateliers pour Peter, avec son soutien, j’ai eu envie  de me consacrer pleinement à cette passionnante relation à l’argent.

Aujourd’hui, j’aide les gens à comprendre ce que leur comportement avec l’argent révèle d’eux-mêmes et à travailler dessus. Mon objectif est aussi de permettre à l’argent d’être ce qu’il est seulement : un contributeur et un accélérateur.


## Le pouvoir que l’on donne à l’argent est le pouvoir dont on se prive
« En vérité, on donne à l’argent un pouvoir, positif et/ou négatif, qu’il ne devrait pas avoir. Soit il nous sécurise, nous rend libre, heureux… soit il génère des guerres, de l’inégalité, de la destruction.
Le pouvoir que l’on donne à l’argent est une partie de notre propre pouvoir dont on se défait par la même occasion. On devient alors incapable de se rendre soi-même heureux, de se sécuriser ou d’être libre. L’argent se transforme en une béquille sensée compenser quelque chose et on est totalement dépendant. L’argent n’a pas toujours le pouvoir qu’on veut bien lui attribuer ! J’ai connu des clients multimillionnaires qui avaient toujours cruellement peur de manquer....

![Argent vote](./Argentvote3.jpg)

## L’argent est un formidable bulletin de vote !

« Je trouve qu’une chose n’est pas assez dite aujourd’hui en France : l’argent est un formidable bulletin de vote. À chaque fois que j’utilise mon argent - achat, investissement, prêt… - je vote pour un certain monde. Je vote pour une organisation et des pratiques en lesquelles je crois, que je veux voir advenir, je donne une direction à ma communauté. Et ce puissant bulletin je le mets dans l’urne une, deux, dix… vingt fois par jour !

Dans cette logique bien entendu, plus on a d’argent, plus on peut orienter les contours du monde. Gagner, faire circuler beaucoup d’argent n’est donc pas forcément un mal, ça peut être une façon noble et efficace de changer le monde.

![Argent vote](./Argentvote4.jpg)

## « Faire l’amour avec l’argent »
« Pete Koenig parle souvent de “Faire l’amour avec l’argent“. Plutôt que de projeter des peurs inconscientes sur l’argent, il nous invite ainsi à avoir des projections positives et conscientes. Par exemple, donner de l’argent à une personne dans la rue en projetant de l’amour sur cet argent, une intention positive. Faire en sorte que l’argent soit un vecteur de lumière et non un vecteur d’ombre. Le don, la réception et la valeur de cet argent en sont ainsi profondément transformés et en décuplent la portée.

Avec ma femme, nous avons par exemple placé l’argent de notre retraite dans un projet d’habitat écologique au Québec. En plaçant cet argent, nous n’avons pas laissé de place à la peur de manquer et nous avons mis toute l’espérance du monde dans ce qu’il pouvait contribuer à réaliser. Ce que ce projet porte de soin et de réparation au vivant nous enrichit plus qu’il ne nous fait peut-être perdre. C’est probablement ce que se disent toutes celles et ceux qui placent dans la [Coopérative Oasis](https://cooperative-oasis.org/qui-sommes-nous/). Les investissements vertueux nous permettent à notre façon de faire advenir une société à laquelle nous croyons tout en dépassant nos peurs…»

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Je place mon argent dans la Coopérative Oasis
    </a>
</div>
</div>

**Pour aller plus loin** :
Le site de Christian Junod : [cjunodconseil.com](https://cjunodconseil.com/)
