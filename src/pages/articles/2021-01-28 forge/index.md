---
title: "Monter une oasis était une question de survie"
date: "2021-01-28T06:40:32.169Z"
layout: post
type: blog
path: "/forge-du-vallon-survie/"
image: ./ForgeduVallon1.jpg
categories:
  - histoires d'oasis
---

__Depuis quelques mois existe au cœur de la Charente une bâtisse entourée de forêt où règnent la nature et la liberté : la Forge du Vallon. Chacun peut y venir, avec les moyens qui sont les siens, pour créer, se ressourcer, être lui-même. Mirabelle et Alexandre, à l’origine de cette oasis de vie bilingue dédiée à la créativité et au bien-être, nous racontent le chemin qui les a menés de la banlieue parisienne à Brigueuil…!__

![Forge du Vallon](./ForgeduVallon1.jpg)

## Libre d'être soi-même

_« Ici, je peux être moi-même pour la première fois »_ Il n’est pas rare d’entendre cette phrase quand on passe un peu de temps au coin du feu de la Forge du Vallon. Depuis quelques mois, un collectif de 3 foyers accueille en effet tous ceux qui le souhaitent dans cette oasis enfouie en pleine nature.

_« La liberté d’être soi-même… Ça n’est pas exactement ce que nous avions écrit dans notre raison d’être, mais c’est ce qui existe avec le plus de puissance aujourd’hui »_, explique Alexandre, cofondateur. « Ce lieu magique s’offre à tous ceux qui ont des passions, des talents et qui veulent les concrétiser, même si c’est pour la première fois. » Quand les tiers-lieux des environs proposent du coworking ou des formations aux locaux, la Forge du Vallon leur propose aussi de transmettre eux-mêmes ce qu’ils portent en eux. On a ainsi pu voir un peintre en bâtiment renouer avec une vieille passion pour donner des cours de théâtre d’improvisation…

![Forge du Vallon](./ForgeduVallon2.jpg)

## Un nécessaire et radical changement de vie

Être soi-même. C’est aussi pour cela qu’Alexandre et Mirabelle ont cheminé depuis 10 ans, de difficultés en obstacles. Et ces voisins de palier en banlieue parisienne n’ont jamais renoncé à leur rêve. « On était 16 au début du projet, raconte Mirabelle, cofondatrice. Au fur et à mesure, tout le monde est parti… jusqu’à ce qu’il ne reste que nous. On s’est regardés _« Tu envisages la vie autrement ? - Non. - Moi non plus » Alors on a continué. »_

Née en Charente, Mirabelle a grandi aux Etat-Unis jusqu’à 20 ans, âge auquel elle est revenue en France pour y devenir comédienne et scénariste en vue. _« Dans mon métier comme dans ma vie personnelle, je souffrais d’une dissonance grandissante entre mon quotidien et mes aspirations intérieures. L’argent, le renoncement à mes valeurs et la consommation étaient mon quotidien quand je rêvais de sobriété, de joie, de créativité libre. Une image revenait sans cesse : j’étais sur l’eau, chaque pied sur une bûche s’éloignant l’une de l’autre. Je devais en choisir une des deux, ou je me noyais. J’ai choisi celle d’une vie nouvelle, dans un lieu en accord avec moi-même, que j’aurais créé. Malgré les deuils que “changer de vie” implique, je ne regrette rien, au contraire. Je me sens tellement en sécurité ici… »_

![Forge du Vallon](./ForgeduVallon3.jpg)

Alexandre a quant à lui été routier pendant 33 ans. Marié et père de deux enfants, il a peu à peu commencé à ne plus supporter d’être le maillon d’une économie dont il voyait chaque jour les effets destructeurs et les absurdités. _« J’ai pris conscience de plein de choses quand j’ai commencé à pratiquer la danse libre, je me suis reconnecté à mes rêves d’enfant et j’ai fini par ne plus me sentir à ma place. Si bien qu’en septembre 2019, j’ai subi ce qu’on appelle couramment un “burn out”. Monter une oasis était une question de survie. Après 33 années passées à se lever à 4h du matin et à être chronométré à chaque instant, c’est difficile de réapprendre son propre rythme. Mais que c’est bon… Ce matin, j’étais dehors en train de faire un abri pour les moutons. Le soleil s’est levé et m’a réchauffé. Je me suis dit « bah voilà c’est ça, c’est simple et ça me fait un bien fou. »_

Au bout de quelques semaines, Mirabelle et Alexandre se sont rendu compte qu’ils avaient un nouveau métier : être Mirabelle et Alexandre.

![Forge du Vallon](./ForgeduVallon4.jpg)

## Un lieu pour créer et soigner

Une SCIC qui a acheté ce terrain d’1,5ha entouré de 20ha de forêt dont les habitants ont le droit d’usage. Potager, verger, poulailler, jardins entourent une bâtisse principale de 400m2. A terme, 5 foyers d’artistes et de soignants habiteront de petites maisons nomades en bois de 30m2 chacune, construites à un bout du terrain. La moitié d’entre eux au moins travailleront à plein temps sur place. L’accueil à prix libre et la location d’espace constituent aujourd’hui les revenus principaux du groupe, dont les besoins restent simples.

Soucieux de vivre sobrement, les membres actuels du collectif Jodie, Mirabelle, Minh, Pierre-Jean et Alexandre développent petit à petit un réseau local. Des paysans bio des alentours fournissent les légumes en direct, une association antigaspillage livre les invendus de grande surface chaque samedi…

Des travaux de rénovation commenceront bientôt : transformation d’une grange de 250m2 en salle de danse, théâtre, art du cirque, rénovation d’une salle de 150m2 en atelier d’art, une maison de l’audiovisuel et une maison des enfants permettant aux artistes visiteurs de venir accompagnés de leur progéniture.

![Forge du Vallon](./ForgeduVallon5.jpg)

## La Charente pour horizon

Le collectif a vite compris qu’un des enjeux de la Charente était la présence d’une population anglaise peu en lien avec les habitants du coin. Aux cours de danse libre prodigués par Alexandre dans le salon, Français et Anglais se sont vite retrouvés par hasard, ce qui arrive peu dans la région. _« Alors on a décidé de se définir comme un lieu franco-anglais », raconte Mirabelle. « Ça nous permettait de répondre à un besoin du territoire et, pour moi, d’exprimer les deux parties moi-même ! »_ Tous les jeux de société du salon se jouent désormais dans les deux langues !

## Un prêt de la Coopérative Oasis

La Coopérative Oasis accompagne celles et ceux qui vivent ou souhaitent vivre dans des écolieux collectifs : les oasis. Depuis décembre 2020, la Coopérative Oasis accompagne la Forge du Vallon. Elle lui a fait un prêt de 150 000 euros sur 10 ans, pour l’achat de la propriété, en complément des capitaux propres des habitants. Elle les soutient en parallèle sur leur structuration juridique et financière. Pour continuer ce travail de prêt et d’accompagnement d’oasis partout en France dans le besoin, nous cherchons plus d’investisseurs !

_Article écrit par Gabrielle Paoli_

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Placez votre argent dans la Coopérative Oasis et soutenez la création d’oasis !
    </a>
</div>
</div>
