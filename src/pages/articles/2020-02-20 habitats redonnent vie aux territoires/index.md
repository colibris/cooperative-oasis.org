---
title: "Des habitats qui donnent vie aux territoires (vidéo)"
date: "2020-02-15T06:40:32.169Z"
layout: post
type: blog
path: "/habitat-vie-territoire/"
image: ./vie-territoire.jpg
categories:
  - histoires d'oasis
---


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.colibris-outilslibres.org/videos/embed/c63d231d-30a9-4f60-a92c-3eaa910e8202" frameborder="0" allowfullscreen></iframe>

A l'occasion des municipales, la Coopérative Oasis, [Habitat Participatif France](https://www.habitatparticipatif-france.fr/?AccueilHPF), et [le  Mouvement Colibris](https://colibris-lemouvement.org/) se sont associés pour réaliser cette vidéo avec le soutien de [l'Ademe](https://www.ademe.fr/) (qui finance la Coopérative Oasis). La vidéo a été réalisé par Cécile Avril.


Cette vidéo présente quelques uns des atouts que ces lieux écologiques et collectifs ont pour les territoires :
- densification de l'habitat à l'heure où les terres agricoles doivent être préservées ;
- création d'activités économiques locales vertueuses - agriculture biologique, artisanat, tourisme... ;  
- implantation de populations nouvelles, souvent jeune et disposée à s'impliquer dans la vie locale citoyenne ;
- ouverture de lieux d'accueil divers faisant souvent office de service publique - école, ressourcerie, centre d'accueil social, centre de formation, salle de spectacle... ;
- animation du territoire grâce à des guinguettes, festivals culturels, café associatifs... ;
- mise en place d'espaces favorables à la biodiversité, à la régénérescence des sols et à la préservation de la flore ;
- relocalisation de la production et de la vente, participant à la résilience d'un territoire ;
- réduction de l'impact négatif des habitants sur leur environnement (baisse de l'émission des gaz à effet de serre, traitement des eaux usées sur place, compost..) qui participe à la bonne santé d'un territoire ;
- réhabilitation et entretien de bâtiments patrimoniaux surdimensionnés pour des familles seules ou des petites collectivités.

Ces écolieux dynamisent les territoires à de nombreux niveaux. Pour se développer, ils ont besoin de coopérer étroitement avec les élus locaux. Transmission d'informations régulière et réciproque, réservation de terrains, autorisations diverses, soutien aux actions communales... autant de démarches que peuvent mettre en place les groupes d'habitants et leurs élus !

Cette vidéo a été réalisée pour aider les citoyens et citoyennes à promouvoir les oasis auprès de leurs élus. n'hésitez pas à la relayer largement.
