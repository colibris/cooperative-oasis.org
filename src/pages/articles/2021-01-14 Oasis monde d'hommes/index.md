---
title: "Les Oasis, un monde d'hommes ?"
date: "2021-01-14T06:40:32.169Z"
layout: post
type: blog
path: "/Oasis-monde-dhomme/"
image: ./AnneLaure.jpg
categories:
  - tribunes
---

**Anne-Laure Nicolas a fondé [Le bois du barde](https://www.leboisdubarde.bzh), une oasis de vie dans le centre Bretagne. Depuis 13 ans, elle y expérimente avec les autres habitants une vie quotidienne collective et sobre où chacune tente de prendre une place équivalente…**

<iframe width="560" height="315" src="https://www.youtube.com/embed/tcOMcy8cNGE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_Intervention / table-ronde sur les enjeux et défis des oasis_


## Les figures de la transition sont essentiellement masculines

Les figures de la transition sont essentiellement masculines.
Depuis des années, les voix critiquant le manque de représentativité et de responsabilité des femmes dans tous les milieux se font de plus en plus entendre. Qu’en est-il des milieux alternatifs en général, et écologique en particulier ? Quelle place les femmes prennent-elles  réellement ?
Pour ce qui est du plaidoyer à l’échelle nationale, les figures de proue de la transition sont, la plupart du temps, des hommes. Cyril Dion, Maxime de Rostolan, Pablo Servigne, Arthur Keller, Nicolas Hulot, Pierre Rabhi, Paul Watson, Marc de la Ménardière… la liste est longue.


![Le Bois du Barde](./groupe1.jpg)

_Le Bois du Barde_

## Les Oasis ne sont pas épargnées par la domination

Les oasis ont pour ambition d’être des lieux d’expérimentation, de transformation intérieure et sociétale et de soin de tous les vivants. Je crois pouvoir dire que la majorité des fondateurs et habitants de ces écolieux collectifs ont à coeur d’être vigilent sur la place de la femme au quotidien. Beaucoup d’ailleurs pratiquent l'écriture inclusive, le partage équitable des tâches quotidiennes, l’éducation “non genrée”...

Cependant, force est de constater que les vieux réflexes de notre société judéo-chrétienne patriarcale reviennent souvent au galop. Rapports de force, verticalité, compétition, et domination masculine sont souvent aussi monnaie courante dans nos lieux. Nous ne sommes pas exempts de ces mécanismes, conscients ou inconscients. Le travail consiste aussi à les regarder, les analyser, et tenter sans cesse de les contrecarrer.


![Le Bois du Barde](./couple.jpg)
_Le Bois du Barde_

## Reste humble et construire ensemble

De ce que je comprends de l'éco féminisme et d’un certain féminisme, c'est la force du « contre » : « contre le patriarcat », « contre la société de consommation » « contre l'appauvrissement de la planète ». De mon point de vue, prendre sa place en temps que femme n'est pas un combat, c'est une danse. Une danse « pour ».
J'ai grandi à l’ombre de femmes fortes qui ont construit davantage qu’elles ont combattu. Ma mère, cadre supérieure dans une entreprise exclusivement masculine, a géré sa vie professionnelle et de mère de famille d’une main de maître, ne laissant rien au hasard. Ma grand-mère a accueilli nombre de personnes en danger lors de la seconde guerre mondiale. J’ai moi-même tenté d’aborder les épreuves de la vie de façon positive : la rupture d'anévrisme et le handicap du père de mes enfants, un burn-out, la création du Bois du Barde que j’ai tenu des années à bout de bras, avec la volonté de réaliser ces rêves, d'avancer toujours avancer.

Les oasis doivent montrer le chemin d’une société plus juste pour les femmes. Nous devons pour cela ouvrir les yeux sur les mécanismes traditionnels qui sont encore à l'œuvre dans nos propres maisons, rester humbles, et continuer à construire ensemble.


![Le Bois du Barde](./tracteur.jpg)
_Le Bois du Barde_
