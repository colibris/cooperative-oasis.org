---
title: "« Oasis » : une marque collective pour protéger sans privatiser !"
date: "2021-04-07T06:40:32.169Z"
layout: post
type: blog
path: "/marque_collective/"
image: ./marquecollective1.jpg
categories:
  - tribunes
---

**Oui “oasis” désigne une boisson (très) sucrée et le groupe de rock du (très) sexy Liam Gallagher. Mais “oasis” est aussi une marque collective caractérisant des lieux de vie collectifs et écologiques… Comment fonctionne cette marque collective et quel pouvoir donne-t-elle au réseau des oasis ?**

![Marque collective](./marquecollective1.jpg)
Crédit : [Pexels](https://www.pexels.com/fr-fr/)

## Un cadre légal pour le terme “oasis”

“ _Avec le développement rapide du réseau des oasis, nous avons éprouvé le besoin de protéger le terme d’”oasis_ ”, explique Mathieu Labonne, directeur de la [Coopérative Oasis](https://cooperative-oasis.org/). _Le risque existait en effet que des projets de construction utilisent le terme “oasis” pour se parer de notre image d’une pratique écologique exigeante et audacieuse, sans en avoir la réalité. Par exemple des lotissements lambda ou des constructions sans vraie démarche participative._”

Le réseau avait donc besoin d’un outil juridique pour garantir un usage du terme “oasis” en accord avec ses valeurs. Il s’agissait également d’éviter que d’autres acteurs ne privatisent le terme après qu’il a été utilisé et développé pendant des années au sein de la Coopérative Oasis, la privant ainsi _a posteriori_ de sa propre appellation.

![Marque collective](./marquecollective2.jpg)
Crédit : [Pexels](https://www.pexels.com/fr-fr/)

## Protéger sans privatiser

L’enjeu était cependant de protéger la valeur du terme “oasis” sans pour autant le privatiser ni rendre son usage extrêmement compliqué pour les collectifs. Dans le cas d’un dépôt de marque traditionnel, tout usage du même terme que celui de la marque dans un domaine similaire doit faire l’objet d’une demande _a priori_. Déposer la marque “oasis” de façon “classique” aurait donc consisté à demander à tous les groupes souhaitant s’appeler “oasis” de faire la demande au préalable à la Coopérative Oasis. Cela revenait à privatiser un mot qui appartient avant tout aux groupes eux-mêmes !

La solution a donc vite pris la forme de la “marque collective”, le modèle développé par Wikipedia. Le dépôt de la marque collective s’accompagne d’un règlement de marque. Dès lors qu’une personne physique ou morale souhaite utiliser le terme d’oasis dans certaines catégories d’activité - en l’occurrence la construction et le bâtiment, les publications… - elle doit se référer à ce règlement de marque. Celui-ci liste ce qu’il est possible de faire d’office, ce qu’il est possible de faire à condition de demander l’accord explicite à la Coopérative Oasis et ce qu’il est interdit de faire.

 - **[Télécharger le règlement de la marque “oasis”](./ReglementdemarqueOasis.pdf).**

 “_Avec la “marque collective” qui a été déposée fin 2017, le terme d’oasis peut être_ a priori _utilisé collectivement,_ explique Mathieu Labonne. _En outre, c’est la Coopérative Oasis qui rédige le règlement de marque, elle-même organisée en société coopérative où tous les membres du réseau associés ont voix au chapitre. Nous avons donc réussi à recréer une gouvernance collective au sein d’une marque protégée. Cela permet de s’approcher du principe de licence libre, mais appliqué au droit des marques._”

![Marque collective](./marquecollective4.jpg)
Crédit : [Alexandre Sattler / Cooperative Oasis](https://cooperative-oasis.org)

## Un contrôle limité

En cas d’utilisation abusive du terme “oasis”, il s’agit pour la Coopérative Oasis d’être proactive dans le contrôle et de contacter la personne en cause. Tout dépend alors de sa bonne volonté, encline ou non à discuter et évoluer. Le procès est la dernière étape qui permet de faire respecter le règlement de marque, et il s’agit de procédures longues et coûteuses dont l’opportunité mérite d’être évaluée avec soin.

“_Nous avons engagé un dialogue dans le cadre d'un gros projet immobilier, qui n'était ni vraiment collectif, ni écologique et qui était appelé "l'Oasis"_, raconte Mathieu Labonne. _Mais ça n’a rien donné. Le problème de l’application de ce droit des marques est qu’il dépend beaucoup du rapport de force et de notre capacité à aller au procès si besoin. Cela pose systématiquement la question d'assumer ou non les coûts d’une procédure juridique fastidieuse._”

Cependant, malgré une capacité de contrôle limité dans certains cas, il s’agit d’un service mutualisé de protection du terme “oasis” proposé au réseau, qui saura toujours jouer son rôle en cas de problème grave.

![Marque collective](./marquecollective3.jpg)
Crédit : [Pexels](https://www.pexels.com/fr-fr/)

## Glossaire

_**Le droit des marques** est une des composantes du droit de la propriété intellectuelle. Il regroupe l’ensemble des règles régissant les marques et leur protection juridique par l’[Institut National de la Propriété Intellectuelle (INPI)](https://www.inpi.fr/fr). Il confère à une entreprise, une association ou un particulier le monopole d’exploitation de la marque pour le type de produits ou services qu'elle propose._

_La **marque** est un signe permettant à un acteur économique ou social de distinguer les produits ou services qu'il distribue des produits ou services identiques ou similaires de ses concurrents. Elle représente l’image d’une entreprise et est garante, aux yeux du public, d’une certaine constance de qualité._

_La **marque collective** a pour fonction d’identifier l’origine de produits et de services émanant d’un groupement d’acteurs (association, groupement de fabricants, de producteurs ou de commerçants, personne morale de droit public) autorisé à l’utiliser en vertu d’un règlement d’usage. “Wikipedia” est l’exemple le plus connu de marque collective._
