---
title: "Déjà 300 investisseurs solidaires dans la Coopérative Oasis… et vous ?"
date: "2021-03-30T06:40:32.169Z"
layout: post
type: blog
path: "/deja-300-investisseurs/"
image: ./epargne.jpg
categories:
  - tribunes
---

**Mettre son épargne au service des écolieux ? C’est possible ! La Coopérative Oasis a prêté 750 000 € à quatre nouvelles oasis depuis janvier 2021... [aidez-la à faire plus !](https://cooperative-oasis.org/investir/)**

![Investisseurs](./epargne.jpg)

Créée en 2018, la Coopérative Oasis propose à chacun et chacune de mettre son épargne à disposition des oasis qui ont souvent du mal à obtenir des prêts bancaires. Et ça marche ! Près de 300 citoyens ont déjà décidé de donner du sens à leur argent en le plaçant dans la coopérative. Grâce à eux, **près de 2 millions d’euros ont été prêtés à quinze oasis en 2019 et 2020** et 750 000 € vont l'être dans les prochains semaines à quatre nouveaux projets validés depuis janvier 2021. Ces citoyens pionniers permettent à ces écolieux bénéfiques aux territoires de voir le jour. Et vous ?

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Je place mon épargne dans la Coopérative Oasis
    </a>
</div>
</div>

<br>
<br>
<br>

## Transformer un ancien élevage de volaille industrielle en grande ferme collective agroécologique bretonne (200 000 € sur dix ans)
![Caringa](./caringa-vue.jpg)
Près de Redon dans le Morbihan, treize adultes et sept enfants vont bientôt s’installer dans leur oasis : la **coopérative Caringa**. Militants de nombreuses associations, notamment de Greenpeace, Alternatiba, Combat Monsanto et des Incroyables Comestibles, la joyeuse bande a décidé de créer une “base résiliente en milieu rural”, avec en son cœur une activité agricole diversifiée. Une préemption de la SAFER leur permet de transformer cet ancien élevage de volaille sur 20 hectares en lieu de vie et d’accueil et en terrain d’alternatives et d’activités économiques connectées à la nature. Les seize hangars et les deux maisons du hameau, les bois, les fours à pain et le puits seront bientôt le décor d’un lieu de vie et d’activité collectif.  Les 20 hectares accueilleront en effet prochainement dix habitants et quatre enfants, deux troupeaux de chèvres laitières et Mohair, des poules pondeuses, d'autres animaux de la ferme et de la production maraîchère diversifiée, le tout en bio !
La Coopérative a prêté 200 000 € à Caringa sur dix ans pour finaliser l’achat du lieu, le financement des premiers travaux et l’installation de panneaux solaires sur les toits.

## Créer une ferme collective et intergénérationnelle en Alsace (200 000 € sur dix ans)
![Pfeifferberg](./Pfeifferberg2.jpg)
Gwladys et Bertrand, un jeune couple formé à l’agriculture biodynamique, cherchait à s’installer dans les environs de Colmar lorsqu’ils sont tombés sur l’annonce de la vente de la **Ferme du Pfeifferberg**. Située à Metzeral dans la vallée de Munster dans les Vosges, elle a été animée pendant 37 ans par Richard Martin et Françoise Marchand qui y accueillaient les marcheurs et visiteurs de passage. Deux hectares, une grande bâtisse, un chalet et des bâtiments agricoles… tout pour réaliser un lieu de vie écologique et intergénérationnel ! Une association nommée “La Montagne Qui Gazouille” s'est constituée autour de Gwladys et Bertrand pour porter le projet collectif. Les neuf membres ont remporté ensemble un appel à projets de Terre de Liens pour la reprise de la ferme.
La Coopérative Oasis a permis au projet de finalement se réaliser en prêtant 200 000 € sur dix ans pour financer l’achat du lieu, en complément d’un emprunt bancaire et des apports des membres du collectif.

## Développer un écovillage en Centre Bretagne (200 000 € sur dix ans)
![BelAir](./BelAir.jpg)
Ils sont juristes, employés d'une ressourcerie, surveillants au collège, étudiants, coachs en développement personnel ou encore médecins... Depuis 2019, une dizaine de trentenaires  ont décidé de vivre autrement en créant un écovillage au cœur de la Bretagne. Aujourd’hui, le **Village du Bel Air** c’est :
 - huit habitants humains, une vingtaine d'habitants non humains augmentés d’une centaine d'animaux sauvage ;
 - un terrain d'une quinzaine d'hectares, sur lequel se trouvent les ruines d'un château datant du XVème siècle ;
 - un lac accessible par un petit chemin dans la forêt ;
 - deux maisons communes ; des véhicules et outils mutualisés ;
 - deux yourtes, un dôme géodésique, une cabane et des caravanes pour que chaque habitant ou couple dispose d'un lieu à lui ;
 - une société civile immobilière (SCI), liée à deux associations, le tout en gouvernance partagée organique.
En 2021, le Bel Air a sollicité un prêt de 200 000 € à la Coopérative Oasis sur dix ans pour financer les premiers travaux de rénovation des deux longères et renforcer le démarrage des activités d'accueil.

## Monter un écolieu touristique et collectif dans le Jura, avec très peu d’apports (150 000 € sur dix ans)
![Maison du haut](./MaisonduHaut.jpg)
En 2021, cinq amis dont deux sœurs ont décidé de changer de vie pour s’installer ensemble à la montagne et monter un projet de vie qui ait du sens. Le collectif a croisé la route de **La Maison du Haut**, un ancien gîte situé à Saint-Lothain, dans le Jura, qui cherchait des repreneurs. Les futurs habitants ont alors décidé de le racheter pour en faire un lieu de vie pour le territoire grâce à la prolongation de l’activité de gîte et d’auberge, la mise en place d’un relais équestre et d’une zone de camping écologique. Pour boucler le plan de financement de l’achat du lieu et les travaux d’installation, la Coopérative Oasis a prêté 150 000 € sur dix ans à ce collectif ayant très peu d’apports mais beaucoup d’idées, de courage et d’audace

De nombreux écolieux à fort impact environnemental et social ont encore besoin de soutien technique et financier. Chacun et chacune peut les aider !

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Je place mon épargne dans la Coopérative Oasis
    </a>
</div>
</div>

_Article écrit par Gabrielle Paoli / Coopérative Oasis_
