---
title: "TERA : l'autonomie dans un rayon de 30 km"
date: "2020-08-03T06:40:32.169Z"
layout: post
type: blog
path: "/tera-autonomie/"
image: ./tera7.jpg
categories:
  - oasis financées
---

_Porté par une cinquantaine d’hommes et de femmes depuis 2014, [TERA](https://www.tera.coop/) est un projet expérimental situé dans le Lot-et-Garonne. Une ferme, un écohameau, un centre de formation et d’autres activités économiques seront à terme répartis sur trois lieux différents, dont un a été acheté grâce à un prêt de la Coopérative Oasis. L’objectif ? l’autonomie et la résilience d’un territoire sur un rayon de 30km. Tour d’horizon d’un projet unique en France par son approche globale._

![Tera](./tera1.jpg)

## Réunir les toutes les solutions alternatives citoyennes dans une seule oasis

"_On n’était pas satisfaits de notre vie à Marseille, raconte Armelle, membre du collectif de TERA. Avec le travail, mon mari et moi ne nous voyions presque plus, ni les enfants ; on était tout le temps fatigués et on n’appréciait même plus les rares moments passés ensemble.

Je me souviens avoir eu une sorte de déclic : je marchais dans la rue avec ma petite fille qui me parlait, et je n’arrivais pas à l’entendre, à cause de bruit… Je me suis dit que ça n’était plus possible, qu’il fallait que nous changions d’environnement. À TERA, nous avons finalement trouvé ce que nous cherchions._"

![Tera](./tera2.jpg)

En effet, si TERA constitue la promesse d’une vie plus sobre et plus heureuse pour beaucoup de ses membres, le projet ne se limite pas à cela. Il s’agit aussi et avant tout d’une expérimentation - en témoigne le conseil scientifique d’une dizaine de spécialistes (en économie, géographie, philosophie…) qui accompagne et documente la recherche.

Car TERA entend trouver le moyen de réunir en un seul lieu toutes les solutions déjà mises en œuvre par la société civile pour répondre aux enjeux écologiques de notre temps. Agro-écologie, monnaie locale et revenu de base, écoconstruction, énergie renouvelable, gouvernance partagée, mobilité en partage… toutes ces initiatives, que l’on trouve aujourd’hui disséminées, ont vocation à être rassemblées dans le Lot-et-Garonne pour bâtir un écosystème coopératif résilient.

![Tera](./tera3.jpg)

## D’un tour de France à vélo en solitaire à l’emménagement d’une cinquantaine de personnes à Tournon d’Agenais

Tout commence par un tour de France en tricycle. En 2013, Frédéric Bosqué décide de sillonner le pays pour faire connaître le revenu de base et découvrir les alternatives existantes. « _En revenant, je me suis dit qu’en fait, la révolution avait eu lieu. Il fallait juste aider tous ceux qui la faisaient déjà jour après jour._ » Trois autres tours de France à vélo plus tard, Frédéric et une fine équipe s’installent dans une ferme qu’un donateur leur propose à Masquières, dans une zone rurale à revitaliser.

![Tera](./tera4.jpg)

Aujourd’hui, 50 personnes sont installées dans la région pour participer à TERA, dont 25 sont très investies dans le quotidien du projet. « _Nous habitons tous à Tournon d’Agenais, siège social de l’association. Dans ce village de 750 habitants, on fait déjà l’expérience d’une vie de voisinage très forte, on ne peut pas sortir sans croiser quelqu’un de TERA._ » explique Armelle, future habitante.

![Tera](./tera5.jpg)

Deux lieux commencent à être aménagés par le groupe : la ferme de Lartel à Masquières où se mettent en place le maraîchage en permaculture, la forêt jardin, la future boulangerie et la maison qui sert de gîte aux volontaires et gens de passage. Sur un deuxième terrain, à Trentels, les travaux du centre de formation à l’écoconstruction ne vont pas tarder à démarrer…

## Quand autonomie ne rime pas avec autarcie

Derrière le terme “d’écosystème coopératif résilient” se cache une ambition : relocaliser toutes les réponses aux besoins vitaux dans un rayon de 30km autour des trois lieux.

« _Attention, on distingue les notions d’autonomie et d’autarcie ! explique Delphine, membre du projet. On souhaite aller le plus loin possible vers l’autonomie alimentaire et énergétique, c’est vrai. Mais il ne s’agit surtout pas de se fermer au reste du monde. TERA s’inscrit pleinement dans la République et la citoyenneté !_ »

![Tera](./tera6.jpg)

TERA est en effet en lien avec de nombreux représentants politiques, de la commune à l’Europe, de même qu’avec l’administration et les habitants du coin. Marie-Hélène précise : « _C’est avec le l’échelon local que c’est le plus difficile. Il faut du temps pour établir de la confiance…_ »

Frédéric renchérit : « _Je crois que ma plus grande satisfaction, a été de voir le voisin enlever sa pancarte « Non à l’écohameau » 3 ans après l’avoir accrochée. À force de discussion et d’écoute, les gens ont fini par nous croire quand on leur expliquait qu’on n’était pas venus ici pour les emmerder_ » raconte Frédéric.

![Tera](./tera7.jpg)

L’intégration de TERA entend aussi être économique. La coopérative d’achat “Garde-manger” implique déjà des producteurs extérieurs à la ferme de Lartel, installée sur le premier terrain du TERA, à Masquières. En sens inverse, toute la production de la ferme de Lartel n’est pas destinée qu’aux seuls habitants de TERA. « _En fait, avec TERA, il s’agit moins de créer un mode de vie communautaire que d’inventer une façon de vivre ensemble localement 2.0_ » résume Frédéric.

Et tout l’enjeu est là : animer une coopération entre les acteurs d’un territoire pour construire tous ensemble des solutions locales, qui amènent des investissements, de la production, des services...

![Tera](./tera8.jpg)

## Les difficultés du travail en collectif

Cependant, un projet collectif d’une telle ampleur ne se déroule pas sans difficultés majeures. L’organisation du travail est aujourd’hui un véritable défi pour le groupe. Il s’agit d’une part de parvenir à faire porter la responsabilité du projet sur plusieurs membres en même temps, afin que sa pérennité ne repose pas sur les épaules d’une seule personne. D’autre part, les futurs habitants du collectif font le constat d’une tendance à travailler les uns à côté des autres mais en silo, sans vraiment coopérer ni faire circuler suffisamment l’information.

Liée à cette organisation du travail compliquée, la gouvernance du groupe est une question majeure. En perpétuel mouvement et redéfinition, elle reste un défi colossal pour ceux qui veulent vivre et travailler de concert. « _On a connu pas mal de situations où des personnes se sont retrouvés en souffrance dans le projet. À chaque fois, le collectif s’est mobilisé pour apporter son soutien indéfectible. Malgré toutes les difficultés, c’est pour moi, déjà, la plus grande réussite de TERA_" conclut Delphine.

![Tera](./tera9.jpg)

_Article rédigé par Gabrielle Paoli / Coopérative Oasis_
