---
title: "« J’ai transformé la ferme abandonnée de mes grands-parents en oasis ressources »"
date: "2021-03-16T06:40:32.169Z"
layout: post
type: blog
path: "/Multikuti-ferme-grands-parents/"
image: ./Multikulti1.jpg
categories:
  - histoires d'oasis
---

_"En 2018, j’ai décidé de reprendre la ferme de mes grands-parents pour en faire un lieu créatif, festif et agroécologique"_ raconte Stéphanie, fondatrice de [l’Oasis Multikulti](https://www.oasismultikulti.org/) en Alsace. Fêtes, entraide, jardinage, ateliers culturels… retour sur l’histoire un peu magique de ce lieu qui enchante désormais la vie du village de Mietesheim.

![Oasis Multikulti](./Multikulti1.jpg)

## “Je suis revenue aux racines agricoles de ma famille par la permaculture”
Stéphanie est fille et petite fille d’agriculteurs alsaciens. Après des études de beaux-arts, elle s’installe à Strasbourg pour y travailler dans une association culturelle. _"J’étais sûre de quitter le monde agricole,_ raconte-t-elle. _J’en avais une très mauvaise image : celle d’un père absent parce qu’il travaille sans relâche. Contre toute attente, c’est la permaculture qui m’a donné envie d’y retourner."_ Lasse aussi de passer ses journées derrière un ordinateur malgré un travail qui lui plaît, Stéphanie décide en 2018 de tout quitter pour reprendre la ferme de ses grands-parents, à l’abandon depuis 5 ans.

![Oasis Multikulti](./Multikulti2.jpg)

_"Tout le monde me trouvait dingue de quitter un CDI pour un projet aussi incertain_, se souvient-elle. _Bien que sceptiques au départ, ses parents finissent par se prendre au jeu. « Quand mon père m’a vue me lancer dans la permaculture, il a décidé de relever le défi à son tour. À l’âge de 60 ans, au moment où sa fille faisait ses premières buttes, il est passé en bio sur ses 100ha maïs, blé, colza luzerne et mélanges fourragers. Nous avons tous les deux appris un nouveau métier. Ça a été et c’est toujours très dur…"_

Aujourd’hui, Stéphanie fait son pain avec le blé bio de son père pour le vendre aux gens qui passent à la ferme. _"Vous imaginez bien que mes parents sont mes meilleurs clients…!"_ conclut-elle en souriant.

![Oasis Multikulti](./Multikulti3.jpg)

## "Mon ancien moniteur de colo est mon associé, ma maîtresse d’école nous aide au jardin, et j’ai intégré le conseil municipal !"
Mietesheim est un village de moins de 700 habitants situé dans le nord de l’Alsace, à proximité du Parc National des Vosges du Nord. Depuis que la boulangerie a fermé, il y a dix ans, le restaurant y fait office de dernier commerce ouvert. Alors quand Stéphanie va voir le maire du village pour lui parler des oasis et de son projet, il est très enthousiaste. De même que les habitants auxquels elle raconte ses envies lors du marché de Noël du village en décembre 2017, où elle tient un stand avec son associé et ancien moniteur de colonie de vacances, Fabrice.

![Oasis Multikulti](./Multikulti4.jpg)

Après presque un an de woofing pour se former et confirmer son goût pour le travail de la terre, Stéphanie revient et s’installe à l’étage de la grande maison familiale de 200m2. Pour lancer son activité, Stéphanie et son associé proposent une journée portes ouvertes. _"Une centaine d’habitants du village et des environs sont venus, beaucoup voulaient revoir la ferme qu’ils avaient connue dans leur enfance… Mon père racontait comment ça se passait à l’époque, c’était très émouvant"_ explique Stéphanie.

Aujourd’hui, l’association compte une cinquantaine d’adhérents dont la grande majorité habitent le village._"Je viens d’ici, tout le monde me connaît déjà, moi ou mes parents, ça facilite les choses pour se faire accepter !_ explique Stéphanie. _Ma maîtresse d’école vient souvent m’aider par exemple ! Il reste encore une grosse partie du village qui ne sait trop quoi en penser, de l’oasis Multikulti... on essaie de les attirer. C’est pour ça que j’ai intégré le conseil municipal. J’ai beaucoup hésité mais je me suis dit que ça faisait partie du projet."_

![Oasis Multikulti](./Multikulti5.jpg)

## "Tout ce qu’on fait, c’est un prétexte pour créer du lien !"
Les activités ne manquent pas dans cette ancienne ferme. En décembre 2018, le festikulti est le premier événement culturel organisé, avec un petit concert, une conteuse, des danses… Les ateliers culturels ont suivi, proposés par des gens venus d’eux-mêmes : un atelier de céramique par une strasbourgeoise retraitée qui a donné tout son matériel à Multikulti ; un atelier d’écriture, de couture, de sophrologie…

![Oasis Multikulti](./Multikulti6.jpg)

Le jardin est venu un peu plus tard, avec la création d’une mare, d’un jardin en forme de mandala, la rénovation du poulailler pour l'installation de l'atelier d'une artiste peintre... _"Cet été, on prévoit un chantier participatif pour isoler le bâtiment en roseau, terre et paille,_ reprend Stéphanie. _Les chantiers ramènent toutes les générations, j’adore ces moments."_

En 2021, un café culturel devrait voir le jour  pour permettre aux gens du coin de se retrouver. En attendant, Stéphanie distribue toutes les semaines son pain et les 25 paniers de légumes de deux jeunes maraîchers en biodynamie du village d’à côté. _"Au final, tout ce qu’on fait, c’est un prétexte pour créer du lien !"_ explique Dom, un artiste engagé dans le projet depuis juin 2020.

![Oasis Multikulti](./Multikulti7.jpg)

## "À l’été 2020, je me suis réveillée épuisée, avec l’impression de tout porter seule malgré le monde autour de moi"

Salariée depuis janvier 2021, Stéphanie a réuni sept personnes d’horizons très différents pour siéger au comité décisionnaire de l’association qui l’emploie et qu’elle a créée. Fabrice un musicien habitant du village, Fabienne directrice de la maison des associations, Noémie  référente instruction en famille, Olivier spécialiste de permaculture, Elsa habituée des réseaux écologistes… Chacun apporte son réseau et son expertise.

![Oasis Multikulti](./Multikulti8.jpg)

Cette organisation n’a cependant pas empêché Stéphanie d’être progressivement gagnée par la fatigue et le découragement. _"J’étais tellement enthousiaste au début, et il y avait tant de choses à faire, que je n’ai pas compté mon temps. À l’été 2020, je me suis tout d’un coup réveillée épuisée, avec l’impression de tout porter toute seule malgré le monde autour de moi. J’ai été à deux doigts d’abandonner"_ se raconte la fondatrice du lieu.

Gestionnaire d’un collectif d’artistes Strasbourgeois, Dom est arrivé en résidence à Multikulti à peu près au même moment et a apporté son expérience de la gouvernance collective. _"Tous les soirs, on faisait des dîners partagés pour parler de notre organisation. On a créé des équipages : des binômes avec des rôles bien définis, qui permettent de clarifier les périmètres tout en ne faisant pas reposer la responsabilité d’un domaine sur une seule personne. Tous les ans, tout le monde change d’équipage, ce qui permet à chacun de toucher à tout et à l’association de gagner en résilience…"_  

![Oasis Multikulti](./Multikulti9.jpg)

Cet apport de Dom a permis au groupe de rééquilibrer les contributions de chacun et à Stéphanie de s’appuyer davantage sur les personnes autour d’elle. _"Le collectif, ça n’est pas juste une idée cool,_ explique Dom, _c’est un réel outil pour réaliser des projets ambitieux… mais il faut apprendre à bien l’utiliser, sinon ça peut être contre-productif."_

## "À Multikulti, on se demande ce qui est vraiment important pour soi, et on s’y consacre pleinement"  

L’Oasis Multikulti est aussi l’histoire du cheminement personnel de Stéphanie, qui a fait le choix de revenir dans le village familial, chez ses grands-parents, pour s’enraciner là où elle se sent, au final, le plus chez elle. _"Je veux avant tout que ce lieu soit ouvert au public et que tout le monde y trouve sa place. Je n’ai jamais trop parlé de ma démarche personnelle parce que j’ai peur que les gens se sentent chez moi et n’osent pas s’approprier le lieu ! Mais petit à petit, on trouve un équilibre tous ensemble. Mon espace est à l’étage, l’association est installée au rez-de-chaussée… et les gens commencent à faire des choses dans le lieu quand je ne suis pas là, c’est une sacrée victoire !"_

![Oasis Multikulti](./Multikulti10.jpg)

L’activité est donc revenue dans ce lieu longtemps animé par une vie paysanne traditionnelle disparue. Et avec elle, chacun a réappris à s’épanouir. _"Toutes les personnes impliquées dans Multikulti ont repris des activités qui leur tenait à cœur - musique, art, jardinage - ou ont pu développer celles qui les faisaient vibrer. Ici on se demande ce qui est vraiment important pour soi, et on s’y consacre pleinement."_


_Article écrit par Gabrielle Paoli / Coopérative Oasis_

_Crédits photos : [P-Mod](http://www.p-mod.com/), sauf les photos 4 et 7 qui sont d'Alice Blot._
