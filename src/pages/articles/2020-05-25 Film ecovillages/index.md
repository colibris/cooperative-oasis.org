---
title: "Un film à découvrir : Écovillages, en quête d'autonomie"
date: "2020-05-23T06:40:32.169Z"
layout: post
type: blog
path: "/film-ecovillages/"
image: ./film-ecovillages.jpg
categories:
  - histoires d'oasis
---

Pour apprendre à être plus résilient face aux effondrements, Demos Kratos a rencontré les habitants de trois écovillages du réseau de la Coopérative Oasis, dont un, TERA, a été financé par cette dernière. Dans ce documentaire, TERA, le Village de Pourgues et l'écohameau de Busseix  nous livrent leur expérience et leur conception de l'autonomie.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wy_jvp5wn8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
