---
title: "Climat, lien social, travail : des études montrent que les oasis profitent à tous !"
date: "2021-07-22T06:40:32.169Z"
layout: post
type: blog
path: "/etudes-recherche-oasis/"
image: ./recherche1.jpg
categories:
  - tribunes
---

**Il existe aujourd’hui en France plus de mille collectifs de vie et de travail tournés vers l’écologie. Au fil des années, nous avons constaté les effets vertueux de ces projets sur les territoires : ils créent du lien social, répondent aux enjeux écologiques locaux, rénovent le patrimoine, s’impliquent dans la vie citoyenne locale… Tout l’enjeu désormais est de mesurer scientifiquement cet impact social et environnemental déjà constaté empiriquement.**

**La Coopérative Oasis a pour cela initié des travaux de recherche, qui viennent s’ajouter aux quelques études déjà réalisées. Tour d’horizon de ce que l’on sait et de ce que l’on veut savoir de l’impact social et environnemental des oasis !**

![Recherche](./recherche1.jpg)
_Crédit : Alena Koval_

## Un bilan carbone divisé par deux quand on vit en oasis
En 2016, Colibris et Carbone 4 ont réalisé une [étude sur l’empreinte carbone d’un habitant d’Oasis](./Etude_Carbone4_2016.pdf) en passant au crible les données de 6 oasis situées partout en France. Le résultat était clair : **un habitant d’oasis émettait alors deux fois moins de gaz à effet de serre qu’un Français moyen**. La construction écologique des habitations, la mutualisation des espaces et des outils, ou encore un mode de vie plus sobre étaient les causes de ces émissions réduites. En 2021, [BL Evolution](https://www.bl-evolution.com/) réalisera pour la Coopérative Oasis un bilan carbone approfondi d'une dizaine d'écolieux. Les résultats prévus pour la fin de l’année viendront affiner ceux de 2016.

![Recherche](./recherche2.jpg)

## Une participation active à la vie citoyenne locale
Des entretiens menés en 2021 par les étudiants chercheurs du sociologue Guillaume Faburel ont permis d’enquêter sur les [relations entre écolieux et collectivités locales](./Etude_Faburel.pdf). Si des difficultés ont été constatées dans certains cas du fait de l’indifférence voire parfois de la méfiance des habitants historiques de la commune, **de nombreux projets ont pu insuffler une dynamique citoyenne et écologique dans leur commune**. A l’image d’Ecoravie, installée à Dieulefit, ville dont le Maire s’est réjouit de l’intégration des habitants de l’éco-lieu dans un tissu associatif,  ayant contribué à le dynamiser. Ces derniers ont même finalement pris part à l’élaboration d’une liste citoyenne élue aux dernières municipales.

## Une baisse significative de la consommation d’énergie grâce à des constructions bioclimatiques
Des chercheurs associés à l’Ademe ont tenté d’évaluer, entre autres, les [bénéfices environnementaux de l’habitat participatif « Mascobado »](./Etude_Mascobado_ADEME.pdf), situé à Montpellier. Il résulte de leur étude que la conception bioclimatique des bâtiments (orientation sud, inertie, protections solaires et logements traversants) et le choix de matériaux naturels par les habitants (briques de terres cuites, bois, etc.) ont permis une **baisse significative de la consommation d’énergie tout en maintenant un niveau de confort très satisfaisant**. Les consommations d’eau, d’électricité et de chauffage sont notamment inférieures aux moyennes habituelles - la consommation d’eau des habitants est par exemple en moyenne de 67 m³/ménage/an, en dessous de la médiane du département établie à 90 m³.

![Recherche](./recherche3.jpg)

## Un cadre de travail qui invente de nouvelles pratiques
En 2019, la Fondation de France a permis de passer [l’écolieu Jeanot](https://jeanot.fr/) au crible d’une [recherche-action d’un an](./Etude-Jeanot.pdf). La façon d’y travailler a notamment été observée. À l’écolieu Jeanot, cinq personnes sont salariées à 35h/semaine, en CDD ou en CDI et gagnent un peu plus du SMIC (autour de 1450€ net/mois). Tous reconnaissent un cadre de travail agréable et sécurisant où chacun est libre de proposer et d'organiser des projets qui l’intéressent. « En effet, chacun estime son collègue comme "capable" et une grande marge de manœuvre est laissée aux salariés. Ces derniers sont donc responsables de faire remonter les informations qu'ils estiment importantes de transmettre au collectif pour en discuter.(...) La veille mutuelle portée sur les activités de chacun ne va pas dans le sens d'un "flicage". » L’emploi du temps est établi par chacun en fonction de ses besoins selon un fort principe de confiance.

![Recherche](./recherche4.jpg)

## Aller plus loin dans la mesure....
Nous avons décidé de missionner des chercheurs pour qu’ils approfondissent ces bribes d’étude d’impact des oasis sur leurs territoires. En plus du bilan carbone mené par BL Evolution, quatre études vont donc être enclenchées en 2021 par la Coopérative Oasis qui contribue financièrement et/ou humainement à chacune de ces études :

- L’**impact social des oasis** sera mesuré par l’équipe de chercheurs du [Campus de la Transition](https://campus-transition.org/) avec le soutien de l’Ademe et de Fonds de Solidarité Européen. Une [étude préliminaire](https://theconversation.com/vit-on-mieux-dans-les-ecolieux-quailleurs-154891) présentait déjà les enjeux de cette mesure inédite.

- La [Bergerie de Villarceaux](http://www.bergerie-villarceaux.org/) mènera une **étude comparative de l’impact social et environnemental** d'une dizaine d'écolieux français.

- La [Coop des Communs](https://coopdescommuns.org/fr/association/) mènera un travail autour de l'**impact des oasis sur la création de communs en France**, ainsi que sur les innovations en termes de gouvernance de communs.

- La Bergerie de Villarceaux crée un indicateur pour mesurer l'**impact des oasis sur la préservation de la biodiversité menacée** aujourd’hui en France.

Le philosophe et théologien Michel de Certeau parlait en son temps des « tactiques de résistance par lesquelles on peut détourner les objets et les codes, se réapproprier l’espace et l’usage ». Il englobait ces tactiques alternatives émancipatrices sous l’appellation « arts de faire ». À bien des égards, les oasis sont aujourd’hui des « arts de faire » qui bénéficient déjà à l’ensemble de la société et ont pour vocation de la guider vers des pratiques plus vertueuses. Le premier pas est donc d’objectiver ces pratiques et leur portée : il s’agit là d’un véritable projet politique.

## Pour en savoir plus
1. _Empreinte carbone habitant d’un Oasis_, Carbone 4 / Colibris, 2016
2. _Enquête sur l’installation d’éco-lieux, l’insertion dans la vie locale, les relations aux autorités locales_, sous la direction de Guillaume Faburel, Université Lyon 2 / Coopérative Oasis / Ademe, 2021.
3. _Analyse et suivi des bénéfices environnementaux, économiques et sociaux de l’habitat participatif « Mascobado »_, rapport après deux ans de vie de la résidence, Ademe / Adret / Cerfise, Janvier 2019.
4. _La valeur et la durabilité technico-économique, sociale et écologique de l'écolieu Jeanot_, recherche-action d’Eva Timone-Martinez, Fondation de France / Écolieu Jeanot, Octobre 2019.

 _Article écrit par Gabrielle Paoli / Coopérative Oasis_
