---
title: Le Château Partagé, soutenu par la Coopérative Oasis !
date: "2019-06-08T10:40:32.169Z"
layout: post
type: blog
path: "/soutien-chateau-partage/"
image: ./rireschateau.jpg
categories:
  - oasis financées
---

_Le [Château Partagé](http://www.lechateaupartage.fr/) est un lieu de vie et d’activités situé sur la commune de Dullin, en Savoie. Installés dans une maison de maître datant du XVIIIe siècle, les 18 habitants expérimentent une vie collective alternative. Certains ont même décidé d'avoir leurs activités professionnelles sur le lieu - maraîchage biologique, boulangerie, ostéopathie, accompagnement... En avril 2019, la Coopérative Oasis a prêté 80 000€ sur 7 ans au collectif afin de lui permettre de finaliser le remboursement d'anciens associés, pour que chacun devienne locataire du Château. La Coopérative accompagnera en outre le château autour des questions de structure juridique et de gouvernance, de gestion des conflits..._

![Rires au château](./rireschateau.jpg)


* * *
**D'où venez vous ?**

_**Thomas, Mathieu, Annick**_ : Du nord de la France, de Saigon, de la Loire, de la Loire Atlantique, de Haute-Savoie et même de Vesoul... bref, un vrai melting pot !

_**Gautier, 9 ans**_ : De Dullin.

_**Florence**_ : De la banlieue parisienne, mâtinée de Corrèze, de Bretagne et d'Hongrie, transvasée à Lyon, puis Chambéry, puis...


**Un moment précis, une phrase, une vision ont-ils opéré comme un déclic, et vous ont poussé à véritablement vous lancer dans l'aventure ?**

_**Thomas et Annick**_ : Notre expérience professionnelle en Centrafrique. Nous en sommes revenus avec l'envie de vivre autant de richesse sociale que là-bas mais chez nous.

_**Claire**_ : J'ai fait un stage au Château Partagé et j'ai ensuite gardé un œil sur le projet, pendant deux ans. Le projet m'attirait. Je ne me sentais pas de monter un collectif depuis le début, et pour autant, prendre une maison individuelle ne faisait pas sens. Se greffer à un collectif existant était juste ce qu'il nous fallait !

_**Florence**_ : Le paysage, qui parle profondément à mon âme de hobbit ! en France.


**Pourquoi vivre en collectif ?**

_**Thomas**_ : Pour la richesse des échanges et l'apprentissage de la compréhension de l'autre.

_**Vincent**_ : Parce qu'au-delà des dynamiques collectives qui me portent, je trouve que mutualiser est pertinent . Ici, la solidarité est forte.

_**Gautier, 9 ans**_ : J'ai pas décidé de vivre en collectif mais j'aime ça.

_**Baptou**_ : Les gens c'est chiant, mais c'est aussi très très chouette ! J'aime partager, rendre et recevoir des services, échanger quelques mots, un sourire, savoir qu'on est entouré.

_**Claire**_ : Je trouve que ça nous tire vers le haut. Pour ma part, ça m'incite (la plupart de temps !!) à sortir le meilleur de moi-même, à choisir d'être plutot agréable que morose. Et puis, l'énergie des autres peut être inspirante. Quand elle est dérangeante, ça peut aussi être l'occasion de réfléchir sur soi-même et de se connaître mieux.

_**Florence**_ : J'ai toujours vécu avec d'autres personnes, et jusque là je n'ai pas encore eu envie d'arrêter !


**Comment vous êtes-vous rencontrés ?**

_**Annick**_ : Le noyau de départ était constitué de personnes bénévoles dans des associations chambériennes ou nationales (Les Amis de la Terre, Vélobricolade), de collègues de travail et également un lien familial.

_**Vincent**_ : J’avais travaillé sur la création d’un habitat partagé avec d’autres personnes dès 2015, mais il a été abandonné. Le château partagé était alors un lieu emblématique et quand j'ai su qu'ils cherchaient un nouveau voisin, je les ai contactés.

![Groupe saute](./groupe-saute.jpg)

**Un premier souvenir vécu au Château, qui vous fait vous dire que vous êtes au bon endroit ?**

_**Thomas**_ : La réalisation de mon four à bois. Cette construction était à la fois un défi personnel, une reconversion professionnelle, un dépassement de soi.... Le lieu et le collectif ont été un tremplin essentiel pour me permettre d'aller au bout.

_**Gautier**_ : J'en ai tellement...

_**Claire**_ : Des sourires impromptus échangés entres voisins et les rires des enfants jouant ensemble dehors.

_**Annick**_ : Au début, lorsqu'on a acheté la bâtisse, je me souviens d'une personne qui avait dit : les utopies d’aujourd’hui sont les réalités de demain. Dix ans plus tard (déjà !), quand je regarde le chemin accompli, je me dis que nous sommes devenus réalité.

_**Florence**_ : Le mois après la naissance d'Alix, que j'ai à la fois vécu intimement et tranquillement, tout en me sentant entourée de plein de précieuses attentions.


**Ce que vous faites en ce moment et dont vous êtes fiers ?**

_**Annick**_ : Je vis en cohérence avec mes valeurs.

_**Annick**_ : Je suis fier (et un peu las) d'être premier adjoint de la commune sur laquelle je vis, Dullin, et que j'apprécie.

_**Florence**_ : Je participe à offrir un environnement respectueux et épanouissant pour les enfants qui vivent au sein du Château Partagé.

![château de face](./chateau-face.jpg)

**Un problème rencontré, déjà réglé ou non ?**

_**Baptou**_ : Les conflits entre personnalités, des façons de faire qui ne sont pas les mêmes. Ça n'est pas toujours facile à régler...

_**Thomas**_ : Il y a eu des départs, pour plein de raisons différentes : à cause de la charte, des séparations de couples... On peut désormais considérer qu'ils sont derrière nous.

_**Claire**_ : Les inégalités d'affinité entres habitants.


**Quelle nature est autour de vous ?**

_**Thomas**_ : Le lac d'Aiguebelette, les montagnes de Chartreuse et directement autour les champs... mais aussi un peu l'autoroute !

![Château de loin](./chateau-loin.jpg)


**Une réussite en terme d'organisation, de fonctionnement de votre collectif ?**

_**Claire**_ : J'aime bien nos réunions hebdomadaires, fondées sur les principes de la gestion par consentement, sur lit de Communication Non Violente. Je trouve que ça roule bien.

_**Annick**_ : J'aime beaucoup les élections sans candidats. Cela nourrit bien la dynamique du groupe ensuite.

_**Florence**_ : J'ajoute un travail d'équilibriste permanent entre formel et informel, une reconnaissance de la nécessité des deux, et que cet équilibre soit toujours vivant.


**Qu'est-ce qui vous relie ?**

_**Claire**_ : Le fait de vivre ensemble. On se rend souvent compte, par comparaison parfois, de la chance que l'on a à vivre avec des gens qui tendent à la bienveillance, à l'entre-aide, à la communication... Nous prenons soin les uns des autres.


**Qu'est-ce qui fait votre diversité ?**

_**Thomas**_ : Nos origines différentes, nos compétences différentes, nos motivations diverses.

![Arc en ciel au château](./arcenciel.jpg)

**Comment caractériseriez-vous le processus engagé auprès de la coopérative oasis, comment l'avez-vous vécu ?**

_**Vincent**_ : C’est un processus soutenant, bienveillant et responsabilisant.

_**Baptou**_ : Je me sens très distant de ce processus, je fais confiance aux habitants qui s'en chargent.

_**Claire**_ : Les échanges pour la mise en place du prêt sont constructifs et bienveillants mais le processus est long. Maintenant que le prêt est fait, j'attends la mise en place concrète des échanges, soutiens, conseils, et espère que l'on arrivera à ce que ça soit juste de part et d'autres. C'est une petite inquiétude que j'ai.


**À quoi va servir l'argent prêté par la coopérative ? Et quel sera l'accompagnement prodigué par cette coopérative ?**

_**Thomas**_ : Cela nous a servi à rembourser un associé parti. Nous avons fait appel à la Coopérative Oasis pour le soutien et l'aide qu'elle apporte (analyse comptable, accompagnement humain...). Et puis nous avions déjà fait appel à une banque, il était difficile de renouveler la demande.

_**Annick**_ : Faire appel à la Coopérative c'est tout de même le sentiment précieux de se sentir soutenu et accueilli autant sur le plan humain que juridique, fiscale et comptable. Ce qui est rare à ces derniers niveaux.


**Et demain ?**

_**Thomas**_ : Un dispositif local d'accompagnement (DLA) est en cours, notre activité d'accueil de groupe est à consolider, et il reste un toit à refaire, d'autres associés à rembourser, des formations sur le consentement à réaliser collectivement et encore plein de bons moments à passer à nous tous, les 18, ensemble... des anniversaires, des décès, des voyages, des rires, des jeux, des films... un bon bout de vie encore pour demain !
