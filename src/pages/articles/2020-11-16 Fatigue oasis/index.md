---
title: "La fatigue dans les oasis, parlons-en !"
date: "2020-11-17T06:40:32.169Z"
layout: post
type: blog
path: "/fatigueoasis/"
image: ./Moulinbois.jpg
categories:
  - tribunes
---

**Marie Laroche a cofondé le [Moulin Bleu](https://lemoulinbleu.org/) en 2019, en partie financé par la Coopérative Oasis. Cette joyeuse communauté d’une vingtaine d’amis s’est installée dans le Loir-et-Cher car toutes et tous voulaient changer de vie, se connecter à la nature, « ralentir ». Deux ans plus tard, à l’occasion du [Festival Oasis 2020](https://cooperative-oasis.org/revivrefestivaloasis2020/), Marie fait le constat d’un épuisement physique et moral généralisé au sein du groupe. Comment en sont-ils arrivés là ? Pourquoi est-ce si commun dans ces écolieux ? Comment faire face à ce phénomène ?**

![Moulin Marie](./MoulinMarie.png)

## Monter un lieu, c’est tout sauf « ralentir »

"Moi la première, Marie, je mettais la priorité sur le fait de quitter un Paris oppressant de pollution et de surpopulation, de larguer un boulot prenant et pas très bien payé, et de ne plus vivre à 2 dans un 40m² qui me coûtait la moitié de mon salaire. Je ne passerai plus 2h par jour dans les transports, je n’irai plus au supermarché tous les deux jours car je ne peux pas stocker chez moi par manque de place, les tâches du quotidien seront diluées entre tous les membres du collectif…

"Bref, il serait alors possible de vivre davantage en adéquation avec le modèle de société que je défends, de pouvoir transformer mon quotidien, et ainsi de ralentir… enfin c’est ce que je croyais.

<iframe width="560" height="315" src="https://www.youtube.com/embed/heRDHMQzDv0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
_Intervention de Marie Laroche eu festival oasis 2020_

"Aujourd’hui, le Moulin Bleu a clairement répondu à plusieurs de mes quêtes : de l’espace, l’horizon, du temps pour faire autre chose que mon travail rémunérateur… mais, je crois qu’au chapitre « envie de ralentir », on n’y est pas encore.

"Depuis notre installation en mars 2020, nous avons enchaîné petits et gros travaux, ménages, aménagements, accueil de proches, accueil de groupes, chantiers collectifs, rencontres avec le voisinage, potager et récoltes… et tout cela entremêlé de réunions quotidiennes en petits et grands groupes – soirs et week-ends, et ponctué d’un espace de discussion en ligne constitué de plus de 50 canaux différents aux sujets plus ou moins passionnants.

![Moulin Bleu repos](./Moulinbleulieu.jpg)

## Alerte : fatigue générale !

"Quelques mois après l’installation, au milieu de l’été, nous avons été plusieurs à tirer la sonnette d’alarme : l’épuisement physique et moral était proche.  Ce n’est clairement pas la vie que j’étais venue chercher. Ce lieu où je me sens bien, où l’on crée du commun, où l’on se soutient… avait pris les allures d’une entreprise multiactivités en mouvement permanent.

![Moulin Bleu repos](./moulinrepos.jpg)

"Je me suis alors demandé ce qui nous avait mené jusqu’ici. Au départ, je croyais qu’il s’agissait d’une sorte d’euphorie des débuts, l’envie d’aller vite pour voir se concrétiser un projet fou. Ensuite j’ai pensé que nous avions une forme d’exigence collective qui nous poussait à nous lancer vite et à nous mettre une pression folle pour réussir à tous prix à modeler le lieu à notre image.

"Peut-être que cette suractivité révélait notre besoin d’appréhender ce nouveau lieu, de le comprendre, pour mieux l’organiser, et qu’elle passerait avec le temps ? Est-ce que cette cadence infernale n’était pas liée au rythme des saisons ? Ou bien cela était-il simplement le fait de la vie d’un groupe composé d’individus qui se mobilisent à différents niveaux sur différents moments pour trouver son rythme avec une ambition énorme en toile de fond ? Ou peut-être que c’était simplement moi qui me trompais sur mon envie de ralentir.

![Moulin Bleu bois](./Moulinbois.jpg)

## Dans presque tous les lieux, ce phénomène d’épuisement

"Pendant le Festival Oasis, en octobre 2020, j’ai présenté ce sujet et animé un atelier d’échanges pour savoir si ce ressenti était partagé par d’autres lieux collectifs. Les premiers retours étaient formels : le « burn out » du militant est aussi courant dans les écolieux que dans le monde du travail en entreprise ou en association.

"Stéphanie m’a raconté comment son lieu, parfaite incarnation de ses valeurs et de son idéal, était devenu toute sa vie… jusqu’au jour où elle s’est effondrée de fatigue. Autour d’elle personne n’a compris comment la dépression pouvait survenir dans une démarce qui répondait à sa quête de sens. Pierre nous a raconté que, lui, ne s’autorisait pas de pauses car il culpabilisait de ne rien faire quand ses cohabitants couraient partout.

![Moulin Bleu toit 2](./moulintoit2.jpg)

"C’est Samia qui nous a amenés vers une forme de solution. Elle nous a expliqué qu’au départ, elle avait besoin de tout savoir et se tenait informée sur tous les sujets en cours dans son oasis. Elle portait du même coup une énorme charge mentale… jusqu’au jour où elle a compris que ce besoin de tout savoir révélait un manque de confiance envers le groupe. Toutes et tons ont travaillé ensemble sur ce point précis et chacun a trouvé sa place et son rythme, tout en conservant des espaces d’information réguliers sur la vie du lieu.

## Une louche de confiance et un zeste de planification

"On m’a beaucoup répété qu’il fallait trouver l’équilibre entre le « je », le « nous deux » (lorsque l’on vit en couple) et le « nous du collectif » pour ne pas s’oublier et ne vivre qu’au service du collectif. Ces échanges et réflexions m’ont permis de comprendre que je ne manquais pas de confiance envers le groupe que nous sommes, mais que je m’étais peut-être trompée sur l’un de mes objectifs de départ. Je ne cherche pas à ralentir en vivant ici.

![Moulin Bleu toit](./moulintoit.jpg)

"Concrétiser ce lieu, le faire exister en tant qu’espace d’accueil et d’activités multiples prendra du temps et de l’énergie (et de l’argent aussi, mais ça on en parlera une autre fois). Et si on veut y arriver, sans que ce soit dans 15 ans, on ne va pas ralentir vraiment. En revanche, nos vies s’organisent déjà différemment et c’est ça que je suis venue chercher en premier lieu.La vie est plus douce ici malgré des périodes de fatigue et l’angoisse qui peut arriver lorsque je listes les choses à faire. Je me sens plus libre de mes choix, de mes activités, car je sais que le collectif porte ce projet.

"Il nous reste encore à caler nos rythmes bien sûr, à planifier pour visualiser les contours du chemin que nous prenons. Visualiser où l’on va, dans les grandes lignes, sera, pour moi un moyen essentiel de ne pas nous épuiser trop vite et de tenir la distance."



_Texte écrit par Marie Laroche à retrouver [sur le site du Moulin Bleu](https://lemoulinbleu.org/a-t-on-vraiment-envie-de-ralentir/)_
