---
title: "[La voix des oasis] le moulin bleu"
date: "2020-09-08T06:40:32.169Z"
layout: post
type: blog
path: "/voix-des-oasis-moulin-bleu/"
image: ./Voix-Moulin-bleu-1.jpg
categories:
  - histoires d'oasis
---

_Vivre en oasis ne signifie pas systématiquement vivre isolé et à la bougie ! Chaque écolieu ressemble au collectif qui lui donne vie. En partenariat avec [Kaizen Magazine](https://kaizen-magazine.com/), [Colibris](https://www.colibris-lemouvement.org/) et [Alexandre Sattler](https://www.facebook.com/gaiaimages), la Coopérative Oasis donne la parole aux habitants de ces lieux hors du commun. Ils nous racontent la diversité des expériences, de leurs réussites et de leurs difficultés…._

![Voix des oasis Moulin bleu](./Voix-Moulin-bleu-1.jpg)


Pour ce premier épisode, direction le Loir-et-Cher, dans un ancien moulin de Saint-Jean-Froidmentel où une quinzaine d’amis se sont installés le 11 mars 2020. Montse et Camille nous racontent tout de leur aventure !


<iframe src="https://www.podcastics.com/player/extended/978/34046/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>


Née en 2015 avec la création de la Maison Bleue, un laboratoire de vie écologique et communautaire à Bourg-la-Reine, la communauté compte aujourd’hui des membres âgés de 25 à 35 ans. Tous ont des professions variées : ingénieur, chargé de projets, indépendant, consultant, professeur, masseur, étudiant, responsable associatif…

![Voix des oasis Moulin bleu](./Voix-Moulin-bleu-2.jpg)

Le lieu est un ancien moulin et ses dépendances : 800m2 habitable sur un terrain de 13ha au bord du Loir. Cette immense bâtisse de 1500 m² a passé les années et est aujourd’hui prête à vivre une nouvelle aventure.
Les futures activités pour ce Moulin sont multiples : production agricole, production d’électricité, espace bien-être, brasserie locale, atelier de sérigraphie participative, restaurant bio… Mais le plus important pour le collectif est de contribuer à impulser une dynamique locale de transition. Alors avant de se mettre au service, ils observent le territoire et ses besoins.

![Voix des oasis Moulin bleu](./Voix-Moulin-bleu-3.jpg)

Le collectif du Moulin bleu a fait appel à la Coopérative Oasis pour un prêt de 200 000 euros sur 7 ans, afin de réaliser l’achat du Moulin et de financer des premiers travaux sur place. En parallèle, la Coopérative Oasis accompagne le projet sur sa structuration juridique, financière et humaine.


---

_**La Voix des Oasis : 35 podcasts pour entendre les oasis de l’intérieur**_

_Depuis des années, des centaines de lieux développent des solutions efficaces pour habiter joyeusement cette planète tout en préservant les écosystèmes. En ville ou à la campagne, tous ont en commun de remettre le collectif, l’autonomie, la sobriété et la solidarité territoriale au centre de leurs vies.	En partenariat avec [Kaizen Magazine](https://kaizen-magazine.com/), [Colibris](https://www.colibris-lemouvement.org/) et [Alexandre Sattler](https://www.facebook.com/gaiaimages), la Coopérative Oasis est allée à leur rencontre pour les interroger._

_Vivre en oasis ne signifie pas systématiquement vivre isolé et à la bougie ! Chaque écolieu ressemble au collectif qui lui donne vie. Il existe autant d’architectures humaines et habitées que de lieux ! Comment s’organise l’écovillage ? Comment est réparti le travail commun ? Comment prend-on les décisions ? Qu’en est-il des questions financières : achat de terrain, des maisons, des matériaux, des outils… ? Autant de questions auxquelles chaque lieu répond à sa façon._

_La parole est donc donnée à tous ceux qui ont fait le choix du collectif et de la sobriété. Cette série de 35 podcasts tente de dessiner un aperçu de la diversité des expériences, de leurs réussites et de leurs difficultés. Et peut-être, de donner à certains l’envie de se lancer…_
