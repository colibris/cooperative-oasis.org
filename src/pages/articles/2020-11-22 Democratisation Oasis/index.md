---
title: "Comment rendre les oasis accessibles à tous ?"
date: "2020-11-22T06:40:32.169Z"
layout: post
type: blog
path: "/democratisationoasis/"
image: ./ColibrisDrone.jpg
categories:
  - tribunes
---

**Pierre Lévy a cofondé [les Colibres](https://lescolibres.jimdofree.com/), un habitat groupé écologique d'une trentaine de personnes situé à Forcalquier (Alpes-de-Haute-Provence). Il a également fondé [Regain](http://regain-hg.org/), une structure qui favorise la réalisation d'habitats participatifs depuis plus de 10 ans en Provence-Alpes-Côte-d'Azur. À l’occasion du [Festival Oasis 2020](https://cooperative-oasis.org/revivrefestivaloasis2020/), Pierre a partagé ses interrogations sur le changement d'échelle de ces projets. Comment résoudre la tension entre la difficulté de réaliser des oasis et l'exigence de leur multiplication ? Comment rendre ces lieux accessibles à toutes et tous ?**

![Colibres A](./ColibresPierre.jpg)

## Le logement n’est pas un produit, l’habitant n’est pas un consommateur

Oasis et habitats participatifs sont des opportunités d’habiter la terre sans la détruire et de faire société en essayant de s’affranchir des logiques capitalistes qui prévalent dans la façon de produire le logement dans notre monde moderne.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bhWRIPXM1Q0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ces logiques capitalises tendent à considérer le logement comme un simple produit, conçu pour être ‘bien vendu’ ou ‘bien loué’ d’avantage que pour être ‘bien habité’. Toutes ces boites superposées qui s’empilent dans nos villes sont autant de supports économiques, dont le rôle premier, plutôt que de créer de la valeur d’usage, est à la fois de stocker de la valeur financière et de faciliter les transactions à visées spéculatives. Pour cela, il faut créer un marché fluide et prévisible, en standardisant les biens qui y circulent… et les acheteurs susceptibles de les acquérir.

![Colibres F](./ColibrisDrone.jpg)

Le système capitaliste réduit ainsi l’habitant au rôle de consommateur passif, aux besoins uniformisés et déterminés par des études marketing déterminant des profils types, des segments de marchés… dont le principal déterminant est le niveau de pouvoir d’achat plutôt que le souhait d’habiter de telle ou telle manière.


![Colibres B](./ColibresEnfant.jpg)

## La prise collective de risque, prix à payer pour l'autonomie

Pour tenter de s’affranchir de ces logiques qui séparent et segmentent, les Oasis et habitats participatifs visent à se donner un maximum d’autonomie par rapport au modèle dominant, en s’appuyant sur des stratégies collectives visant à rechercher des formes de propriété où la capacité à faire société prime sur l’enjeu patrimonial, et qui impliquent de remplacer les promoteurs qui standardisent par une autopromotion citoyenne créative guidée par le désir de vivre ensemble.

Cette recherche d’autonomie implique de transformer le consommateur habitué à être protégé en acteur collectif, capable de prendre et d’assumer des risques. Mais ces risques, quels sont-ils ?

![Colibres C](./ColibreFacade.jpg)

Quand des citoyens s’organisent pour imaginer, monter et réaliser leur habitat (en neuf ou en réhabilitation), ils prennent des risques liés à une opération immobilière de parfois plusieurs millions d’euros - l’immobilier est l’une des activités économiques aujourd’hui les plus risquées – grâce à des formes de propriété et de financement encore pionnières dans le marché de la propriété privée. Ils sont des non spécialistes qui prennent sur leur temps de loisir, en expérimentant en même temps des formes autogestionnaires dont on commence à connaître l’extrême difficulté... Bref, les risques sont élevés et un fort désir de faire advenir, s’il est essentiel, ne suffit pas toujours pour les surmonter.

## Aujourd’hui, monter une oasis relève de la virtuosité

L’ensemble des qualités humaines, le niveau d’expertise citoyenne et l’énorme disponibilité qu’il faut concrètement mobiliser pour parvenir au but tendent ainsi à rendre ces projets accessibles seulement à quelques virtuoses.

![Colibres D](./ColibreArbre.jpg)

Comment dans ces conditions tenir la promesse de multiplier le nombre des Oasis par 10, par 100, par 1000 ?… Comment faire nombre, condition nécessaire si l’on est sérieux dans notre projet politique de contribuer un peu à l’amélioration de la condition humaine ?

Comment résoudre la tension entre la recherche d’une autonomie à laquelle est attachée le sens de nos démarches mais dont les conditions de faisabilité en font un projet élitiste, et l’exigence de démocratisation des Oasis dont dépendra toute leur puissance ?

![Colibres E](./ColibrsRdc.jpg)

L’accompagnement de projet peut-il suffire à résoudre le tiraillement entre ces deux exigences opposées ? La construction d’une expertise citoyenne en réseau telle que la développe aujourd’hui la [Coopérative Oasis](https://cooperative-oasis.org/) ou [Habitat Participatif France](https://www.habitatparticipatif-france.fr/?AccueilHPF) de manière militante est-elle à la hauteur des enjeux ? Est-il possible (pertinent, faisable et éthique) d’imaginer la constitution de promoteurs citoyens spécialisés dans la création d’Oasis ?

![Colibres F](./ColibreEventail.jpg)

Doit-on souhaiter la venue des gros promoteurs qui s’intéressent de plus en plus à ce nouveau segment de marché ? Mais comment alors conserver sa posture d’acteur dans des solutions de marché où l’on redevient client ?

##La question du changement d’échelle des oasis se joue maintenant.

La résignation n’est pas une option. Des solutions doivent être imaginées maintenant car c’est en ce moment que la question du changement d’échelle de notre mouvement se joue. La manière dont les mouvements citoyens résoudront cette tension entre autonomie et démocratisation, ou dont elle sera résolue par des professionnels plus ou moins proches de ces mouvements, fournira très certainement la couleur des Oasis de demain.
Je vous invite à venir discuter des pigments qui composeront cette couleur…


_Toutes les photos ont été prises aux Colibres par Alexandre Sattler_

<br/>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="https://www.cooperative-oasis.org/investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Participez à la multiplication des oasis !
    </a>
</div>
</div>

<br/>
