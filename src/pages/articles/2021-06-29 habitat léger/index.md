---
title: "L’habitat léger, une solution pour la vie collective et écologique"
date: "2021-06-29T06:40:32.169Z"
layout: post
type: blog
path: "/habitat-leger-solution/"
image: ./Habitatleger1.jpg
categories:
  - tribunes
---

**Moins cher, écologique et mobile, l’habitat léger s’adapte parfaitement aux besoins des oasis ! Il permet aux personnes de tester la vie collective sans s’engager ad vitam eternam, il offre une solution économique et rapide, il préserve les sols et le vivant tout permettant de vivre au plus prêt de la nature. Malheureusement, installer un habitat léger en toute légalité peut devenir un véritable casse-tête. L’association Hameaux Légers prend le temps de donner toutes les explications et astuces à celles et ceux qui veulent tenter l’aventure...**

<iframe width="560" height="315" src="https://www.youtube.com/embed/RWcUlFkYoLY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Qu’est-ce que l’habitat léger ?
La Loi ALUR de 2014 définit l’habitat réversible comme les “**résidences démontables constituant l’habitat permanent de leurs utilisateurs**” :
 - _“occupées au moins 8 mois par an”_
 - _“sans fondations”_
 - _“disposant d'équipements intérieurs ou extérieurs”_
 - _“facilement et rapidement démontables”_
Elle précise que ces installations peuvent être autonomes vis-à-vis des réseaux (eau, électricité, assainissement).

Il existe **quatre familles d’habitats légers ou réversibles**, lesquels ont tous des fondations démontables (pierres sèches, pneus, vis, pieux ou plots de fondations) :
 - **mobiles**, montés sur roue (tiny house, roulotte, caravane)
 - **transportables à l’aide d’une remorque** (mobile home, Beauhabitat, conteneur) ;
 - **démontables**, pouvant être désassemblés, déplacés et réassemblés (yourte, dôme, maison nomade) ;
 - **biodégradables**, conçus à partir de matériaux naturels, à même de se décomposer naturellement une fois détruits (kerterre, maison terre-paille).

 ![Habitat léger](./Habitatleger1.jpg)
_Le Village du Bel Air, crédit : Alexandre Sattler_

 Pour Hameaux Légers, l’habitat réversible est une réponse à des enjeux à la fois écologiques et sociaux car ils sont beaucoup plus accessibles financièrement, ils ont une empreinte carbone réduite ou nulle pour les habitants comme pour la construction, et ils préservent à long terme la vie des sols.

## L’essentiel soutien des élus locaux

L'urbanisme encadre l’aménagement du territoire ; il faut donc bien connaître ces règles qui conditionnent l’installation en habitat léger. Dans la mesure où il est interdit d’installer un habitat léger sur des zones non constructibles, un tel projet doit faire l’objet d’une **demande d’installation auprès de différents acteurs**.

Les élus sont les premières personnes vers lesquelles se tourner (communauté de commune, communauté d’agglo, communauté urbaine, métropole) car leur soutien est essentiel pour obtenir les permis. Il existe des **astuces pour être en bons termes avec ses élus** : privilégier l’oral à l’écrit, mettre son enfant à l’école publique, exercer une activité sur la commune, être originaire du coin, s’investir dans une association locale ou au conseil municipal, avoir un projet de construction ou de rénovation en parallèle, proposer de payer des impôts locaux en faisant un don à la Mairie… Il existe aussi de **mauvaises pratiques qui peuvent desservir le lien aux élus** : s’installer sur un terrain dont le propriétaire est en conflit avec eux, rendre l’habitat visible depuis une voie passante, choisir une zone naturelle protégée, une zone inondable ou une zone où le foncier est très cher.

![Habitat léger](./Habitatleger2.jpg)
_Crédit photo : Pexels_

Les techniciens des collectivités peuvent aussi être d’une grande aide et sont souvent assez ouverts aux initiatives alternatives du type habitat léger ! L’administration - à l’échelle locale, la direction départementale des territoires (DDT) - est aussi à prendre en compte, plutôt dans un deuxième temps. Enfin, les conseils d’architecture, d’urbanisme et de l’environnement (CAUE) sont des associations qui peuvent conseiller gratuitement sur les projets d’habitat léger et s’avérer être très utiles.

## Bien connaître le PLU (plan local d’urbanisme)

Ces acteurs s’appuient sur différents **documents pour encadrer l’installation en habitat léger**, notamment le Plan Local d’Urbanisme (PLU) qui définit les zones constructibles. À noter qu’il est plus facile de demander des modifications de ce document en début de mandature…

![Habitat léger](./Habitatleger3.jpg)
_Crédit photo : Pexels_

Les “pastilles STECAL” sont des dérogations d’urbanisme qui permettent de s’installer en zones non constructibles (zone agricole ou naturelle). On entend beaucoup parler des STECAL mais il faut rappeler que ces dérogations sont accordées à titre tout à fait exceptionnelles, et sont très difficiles à obtenir quand il n’y a pas déjà des pastilles STECAL existantes. Il ne faut donc pas compter dessus quand on imagine son projet.

L’association Hameaux Légers recommande vivement de ne pas installer illégalement d’habitat réversible car cela installe ce type d’habitat dans l’illégalité et les habitants dans une peur constante d’être délogés. Elle propose donc des formations et des accompagnements à tous ceux qui rencontreraient des difficultés dans leur installation !

## Pour en savoir plus
 - [Tous les enjeux de l'habitat léger par les Hameaux Légers (PDF 5.2MB)](https://cooperative-oasis.org/wiki/?BaseDonneesJuridiques/download&file=Webinaire_Cooperative_Oasis_2021_Hameaux_Lgers.pdf)
 - [Le site de l’association Hameaux Légers](https://hameaux-legers.org/)
 - Livre : Guide juridique pour habitats alternatifs de Joris Danthon
