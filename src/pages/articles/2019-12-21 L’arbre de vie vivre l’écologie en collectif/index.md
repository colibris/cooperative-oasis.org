---
title: "L’arbre de vie : vivre l’écologie en collectif et entre amis"
date: "2019-12-21T06:40:32.169Z"
layout: post
type: blog
path: "/larbre-vie-vivre-lecologie/"
image: ./arbre-vie-6.jpg
categories:
  -  histoires d'oasis
---

_Par Gaëlle Richardeau et Loick Kalioudjoglou. Article initialement paru sur le Magazine de Colibris_

Gaëlle Richardeau est graphiste et illustratrice. Loïck Kalioudjoglou est ingénieur-docteur en énergie. Ensemble, ils ont décidé de parcourir les routes des France en 2019 pour découvrir des Oasis qui répondent concrètement aux enjeux écologiques et sociétaux du 21ème siècle. C’est avec leur camion aménagé qu’ils se déplacent et livrent leur regard sur les lieux visités.

![Arbre de Vie 1](./arbre-vie-1.jpg)

Dans le petit village de Maumusson (Loire Atlantique), les huit habitants de l’Arbre de Vie expérimentent mille et une manières de vivre autrement dans le respect de l’homme et l’environnement. Et la dynamique de ces trentenaires motivés crée la curiosité dans le pays d’Ancenis...

C’est en 2015 que naît l’Arbre de Vie, lorsqu'Aurélien et Alex achètent un ancien corps de ferme et un terrain de près de 2 hectares pour près de 150 000€. L’ambition initiale de ce couple et de leur bande d’amis : créer un jardin pédagogique cultivé en permaculture. Conscients des enjeux environnementaux, c’est donc par conviction que ces amis décident de construire un lieu de partage en s’appuyant sur le MOOC Oasis des Colibris.

![Arbre de Vie 2](./arbre-vie-2.jpg)

Aujourd’hui, les 8 membres de l'Arbre de Vie souhaitent développer leurs activités tout en conservant une entraide et une mutualisation des espaces et des outils. Cette diversité se retrouve au point de vente présent à l’entrée de la ferme où sont proposées toutes les productions de la ferme : fruits et légumes du jardin, jeux en bois produits à l’atelier, savon et shampooing artisanaux, confiseries et gâteaux de la cuisine. Les habitués salivent d’avance en imaginant le fromage des brebis qui viennent d’arriver sur le lieu. Ici tout est à prix libre. Autrement dit, la caisse est gérée par les clients : chacun renseigne sur un carnet ses achats, paie le montant qu’il veut et prend ses produits !

![Arbre de Vie 3](./arbre-vie-3.jpg)

La ferme est également un lieu de rencontre où de nombreux bénévoles et amis de l’association partagent les activités et les repas. Le lundi matin, un temps est pris pour faire la « météo » du groupe - un rituel d’échange et d’organisation du collectif. Chacun y exprime son état d’esprit du moment qui peut être «ensoleillé», «nuageux» ou encore «arc-en ciel» ! Ce moment permet également de répartir les tâches collectives et d’organiser les chantiers de la semaine.

![Arbre de Vie 4](./arbre-vie-4.jpg)

Actuellement, 5 habitants partagent la maison principale qui a été auto-rénovée. Une chambre supplémentaire et une cave, qui permettra de stocker les productions du collectif, sont également en cours de construction. Les méthodes d’éco-construction tels que l’utilisation de mélange terre-paille ou la construction en pierre sont utilisées par les habitants pour ces chantiers. Au jardin, l’autonomie est le maître-mot : des graines au terreau en passant par les outils de travail, des solutions pour l’atteindre sont imaginées par les habitants pour les cultures.

![Arbre de Vie 5](./arbre-vie-5.jpg)

À l’occasion de ses portes ouvertes 2019, ce n’est pas moins de 1 100 personnes qui sont venues à l’Arbre de Vie découvrir comment ses habitants essaient de faire différemment dans le respect de l’humain et de l’environnement. Les curieux ont pu y voir une démonstration de forge où l’on travaille le métal, entendre un conte sous la structure du “zome”, un losange en bois, se promener dans le jardin Picasso et découvrir des applications concrètes des principes de la permaculture. Les allées de ce jardin pédagogique invitent à la balade et éclairent les interactions entre le compost de cuisine, le poulailler et les buttes de cultures.

![Arbre de Vie 6](./arbre-vie-6.jpg)

Cette année, c’est une équipe de près de 20 personnes qui a permis d’organiser ce temps fort de l’association. Vivre ici quelques jours suffit pour sentir l’effervescence et l’envie de transmettre aux autres les pratiques écologiques. En 2019, le lieu s’est ouvert à la culture avec une résidence artistique organisée par Jasmine et le tournage d’un film documentaire sur la vie du collectif. Son réalisateur Antoine Trichet s’intéresse aux parcours des habitants de cet oasis et souhaite mettre en lumière les pratiques mises en place pour répondre aux enjeux sociétaux et écologiques.

![Arbre de Vie 7](./arbre-vie-7.jpg)
