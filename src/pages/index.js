import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import LazyLoad from 'react-lazyload'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import CardPost from '../components/CardPost'
import CardOasis from '../components/CardOasis'
import OasisSlider from '../components/OasisSlider'

class HomeIndex extends React.Component {
  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const posts = get(this, 'props.data.remark.posts')
    const oasis = get(this, 'props.data.oasis.posts')
    const sliderOasis = get(this, 'props.data.sliderOasis.nodes')

    // on recupere tous les oasis libres
    const tabCardOasis = []
    oasis.forEach((data, i) => {
      tabCardOasis.push(
        <LazyLoad height={1} offset={500} once={true} key={i}>
          <CardOasis data={data.post} site={site} isIndex={true} key={i} />
        </LazyLoad>
      )
    })

    // on recupere les derniers articles pour mettre en une
    const cardPosts = []
    posts.forEach((data, i) => {
      const layout = get(data, 'post.frontmatter.layout')
      const path = get(data, 'post.path')
      if (layout === 'post' && path !== '/404/') {
        cardPosts.push(
          <LazyLoad height={1} offset={500} once={true} key={i}>
            <CardPost data={data.post} site={site} isIndex={true} key={i} />
          </LazyLoad>
        )
      }
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={get(site, 'title')}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            { property: 'og:title', content: get(site, 'title') },
            { property: 'og:type', content: 'website' },
            { property: 'og:description', content: get(site, 'description') },
            { property: 'og:url', content: get(site, 'url') },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/logo.png`,
            },
          ]}
        />
        <div className="container">
          <div className="row">
            <div className="col-md-4 text-right">
              <h1 className="underlined">
                Bienvenue
                <br />
                sur le site de la
                <br />
                coopérative <strong>Oasis</strong>
              </h1>
              <br />
              Partout en France, des collectifs citoyens inventent de nouveaux lieux de vie écologiques et solidaires : les oasis.
              <br />
              La Coopérative Oasis est le réseau de ces centaines d'écolieux et existe pour les aider à se développer.
              <br />
              Elle diffuse ce mode de vie auprès du grand public et soutient les oasis en les outillant, en les accompagnant et en les finançant.
              <br />
              Chaque citoyen peut s'associer à la Coopérative et y <Link to="/investir">placer son épargne</Link> pour contribuer au développement des oasis.
              <br />
              Ensemble, construisons le monde de demain.
            </div>
            <div className="col-md-8">
              <OasisSlider items={sliderOasis} />
            </div>
          </div>
        </div>
        <div className="oasis">
          <div className="container">
            <div className="row">
              <div className="col-md-4 text-right">
                <h1 className="underlined">Des oasis à découvrir</h1>
              </div>
              <div className="col-md-8"></div>
            </div>
            <div className="grid">{tabCardOasis}</div>
            <br />
            <div className="row">
              <div className="col-md-4"></div>
              <div className="col-md-4">
                <Link
                  to="/les-oasis/"
                  className="btn btn-block btn-primary px-5"
                >
                  <i className="fa fa-plus"></i> Voir toutes les oasis
                </Link>
              </div>
              <div className="col-md-4"></div>
            </div>
          </div>
        </div>
        <div className="articles">
          <div className="container">
            <div className="row">
              <div className="col-md-3 text-right">
                <h1 className="underlined">Dernières actus</h1>
              </div>
              <div className="col-md-9"></div>
            </div>
            {cardPosts}
            <div className="row">
              <div className="col-md-3"></div>
              <div className="col-md-6">
                <Link to="/actus/" className="btn btn-block btn-primary px-5">
                  <i className="fa fa-plus"></i> Voir toutes les actualités
                </Link>
              </div>
              <div className="col-md-3"></div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default HomeIndex

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    remark: allMarkdownRemark(
      limit: 4
      filter: { frontmatter: { type: { eq: "blog" } } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            categories
            date(formatString: "DD.MM.YYYY")
            type
            image {
              childImageSharp {
                resize(width: 600) {
                  src
                }
              }
            }
            description
          }
        }
      }
    }
    oasis: allMarkdownRemark(
      limit: 6
      filter: {
        frontmatter: { type: { eq: "oasis" }, highlighted: { eq: true } }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            avancement
            montant
            departement
            image {
              childImageSharp {
                resize(width: 600) {
                  src
                }
              }
            }
            description
          }
        }
      }
    }
    sliderOasis: allSlideshowYaml {
      nodes {
        id
        type
        link
        legende
        image {
          childImageSharp {
            id
            resize(width: 750, height: 500) {
              src
            }
          }
        }
      }
    }
  }
`
