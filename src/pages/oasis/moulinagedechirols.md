---
title: "Le Moulinage de Chirols"
date: "2018-11-20T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/moulinagedechirols/"
avancement: "financé"
montant: "184 600 euros"
departement: "Ardèche"
image: ./img/moulinagedechirols-paysage.jpg
website: https://lemoulinagedechirols.org/
highlighted: true
description: "Issus des domaines la construction, de la culture, de l’associatif et de l’artisanat, une vingtaine de citoyens se mobilisent pour faire revivre une usine du XIXème siècle dans le Parc naturel des monts d'Ardèche et d'y créer un lieu de vie et d'activités."
---

Né en 2015, le collectif du [Moulinage de Chirols](https://lemoulinagedechirols.org) a décidé de réhabiliter une ancienne usine ardéchoise de fil de soie. L’objectif ? En faire un lieu ouvert regroupant activités artistiques et artisanales, habitat participatif et espaces communs publics. Intérêt général, refus de la spéculation, préservation des écosystèmes et engagement citoyen sont les maîtres-mots des 23 personnes qui ont signé l’acte d’achat au printemps 2019…

![moulinage de Chirols](./img/chirols1.jpg)

## Un projet mêlant habitats et des activités économiques
La vingtaine de personnes du collectif sont déjà presque toutes installées dans les environs ; une coopérative d’habitants a acheté le lieu pour 200 000€ au printemps 2019 ; des espaces communs sont déjà réhabilités et occupés : dortoir et sanitaires, salle des fêtes, foyer, espace d’art graphique, menuiserie, bureaux temporaires… le tout pour continuer à accueillir les premiers chantiers participatifs.

<iframe width="560" height="315" src="https://www.youtube.com/embed/59wTi_ksvqQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Le projet final prévoit un __espace commun d’activités de 2400m²__ à usage collectif et ouvert au public comprenant  :
- un pôle culturel avec une salle de spectacle de 200 places pour des résidences d’artistes et des formations
- un espace de « coworking »
- une cantine participative associative privilégiant les produits bio et locaux
- un espace d’hébergement temporaire pouvant accueillir une trentaine de résidents (artistes, étudiants…) ou de bénévoles

Un __espace d’habitat participatif de 1900m²__ sera dédié à 30 résidents permanents, privilégiant la mixité sociale, avec des espaces domestiques partagés (buanderie, épicerie participative, atelier).

Une __ruche d’activités__ intégrant des artisans locaux (menuiserie, sculpture, transformation de fruits et légumes), un atelier participatif associatif, et des activités audiovisuelles et numériques est également prévue.
Un terrain commun d’environ 2000 m² en “faysses” (cultures en terrasses traditionnelles) est déjà repris en main, pour créer des potagers bio en permaculture et des poulaillers.

<iframe src="https://www.podcastics.com/player/extended/978/34088/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

## Un joyeux collectif en mouvement
Le Moulinage ne se réduit pas aux 23 porteurs de projet du « premier cercle ». Très vite, s’est constitué autour de l’ancienne usine un réseau de partenaires, au premier rang desquels figure l’équipe municipale très engagée. _« Il y a aussi des dizaines de bénévoles qui viennent nous aider sur le chantier. C’est très précieux. Désormais et pour toujours, ils sont ici chez eux »_ explique Juliette.
Architecte, ethnologue, comédienne, communicante, paysagiste, menuisier… Si bon nombre d’horizons différents convergent au Moulinage, une pratique les rassemble tous : la fête ! Chants, danses, improvisations, spectacles, soirée… la bonne humeur et la joie semblent envelopper le travail au quotidien.

## Prendre soin du patrimoine local
Cette ancienne usine ardéchoise constitue un patrimoine cher à la région. À la pointe de la technologie, le moulinage et ses terrasses cultivées en vignes et en fruitiers ont fait vivre pendant près d’un siècle une bonne partie de la vallée. Et ce, jusqu’à leur fermeture dans les années 2000. _« Bien plus que réhabiliter des murs anciens, nous souhaitons prolonger la lignée de tous ceux qui ont construit, travaillé et vécu ce lieu pendant des années. Inscrivant nos pas dans les leurs, nous essayons d’être à la pointe en montrant la capacité des territoires ruraux à se réinventer avec des solutions écologiques et sociales vertueuses »_ raconte Alexandre, paysagiste, qui a rejoint le groupe en 2018.

![moulinage de Chirols](./img/chirols2.jpg)

## Le financement de la Coopérative Oasis
La Coopérative Oasis et le Moulinage de Chirols ont signé une convention de 4 ans avec notamment un prêt de 184 600 euros qui permet de financer les travaux, dans l’attente du versement de financements européens Leader.
La Coopérative Oasis accompagne le projet sur plusieurs sujets, et notamment sur le montage juridique et l’articulation des sous-projets (coopérative d’habitants, coworking, salles de spectacles…).

<iframe width="560" height="315" src="https://www.youtube.com/embed/DINWNaTZCho" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<div class="row">
<div class="col-md-12">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
