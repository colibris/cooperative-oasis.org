---
title: "Les sens de Théus"
date: "2019-01-04T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/sensdetheus/"
avancement: "financé"
montant: "50 000 euros"
departement: "Hautes Alpes"
website: https://www.lessensdetheus.fr/
image: ./img/Sens de Theus couple.jpg
description: "Une ferme bio dans les Alpes qui produit des plantes aromatiques, médicinales et à parfum, transmet ses savoirs et développe un gîte « accueil paysan » avec une dimension sociale
"
---

[Les Sens de Theus](https://www.lessensdetheus.fr) est ferme en agriculture biologique située dans la Hautes-Alpes. Pauline et Jean-Pascal, paysans herboristes, y produisent et transforment ensemble des plantes à parfums, aromatiques et médicinales. Le lieu est également un lieu de vie, d’échange, et de formation pour tous les passionnés de plantes.

![Sens de Theus couple](./img/Sens de Theus couple.jpg)

## Du ski de fond aux herbes aromatiques

Pauline est ingénieure agronome, Jean-Pascal plombier de formation. Tous deux pratiquent le ski de fond de haut niveau. En 2015, un peu lassés de leurs métiers respectifs et aspirant à une vie plus sobre et proche de la montagne qu’ils connaissent bien et aiment intensément, Pauline et Jean-Pascal décident de changer de vie. Elle, quitte son poste de chargée de projet faune et flore à la SNCF ; lui, arrête de travailler comme plombier pendant la période estivale mais garde son métier de technicien d’une équipe de ski américaine.

![Sens de Theus fleurs](./img/Sens de Theus fleurs.jpg)

"_Les plantes nous ont toujours beaucoup attirés, mais on ne pensait pas qu’une ferme de plantes aromatiques et médicinales était viable. On s’est donc d'abord orientés vers le maraichage. Cependant, au fil de nos visites et de nos rencontres, nous avons changé d'avis et décidé de tenter le coup. En plus des formations que nous avons faites, nous sommes donc allés travailler 6 mois dans une ferme de plantes médicinales avant de nous lancer_ » explique Pauline. Très vite, ils se rendent compte que ce domaine correspond pleinement à leurs attentes tant en terme de lien à la nature que de stimulation intellectuelle.

Un peu désarmé par les prix exorbitants des terres en Haute-Savoie, le couple y cherche pourtant un lieu pendant 2 ans. Saisis par l’opportunité d’un terrain de 3,5ha avec une petite maison, mis en vente juste à côté de la maison de famille de Pauline dans les Hautes-Alpes, ils décident finalement de changer de région et de l’acquérir.

Fin 2017, le couple s’installe sur son nouveau lieu de vie et d’activité qu’il appelle « Les Sens de Theus ».

![Sens de Theus Pauline](./img/Sens de Theus pauline.jpg)

## Le soin des gens et de leur intimité

Aujourd’hui, la ferme « Les sens de Théus » produit et transforme des plantes aromatiques et médicinales. Le cœur de l’activité de ces « paysans herboristes » est la « phytothérapie » - le soin par les plantes. Celle-ci consistent en huiles essentielles, eaux florales, teintures mères (préparations traditionnelles, base de la pharmacopée traditionnelles) et gemmothérapie (le soin par les bourgeons de plantes). Pauline et Jean-Pascal produisent aussi des tisanes et des cosmétiques.

![Sens de Theus JP](./img/Sens de Theus jeanPascal.jpg)

Certaines plantes sont cueillies dans la nature, d’autres sont cultivées en agriculture biologique. Pour obtenir des plantes ayant des propriétés puissantes, chaque étape est fondamentale et nécessite une maîtrise d’un processus propre à chaque variété - la culture, la cueillette, la transformation, le stockage, le transport et la diffusion…

« _Mais l'herboristerie ne se résume pas à la biologie pure. Émotions, énergies, philosophie… les plantes parlent des hommes » raconte Pauline. « En vendant ces produits, on entre dans l’intimité des gens. Ils viennent nous voir avec leurs enjeux physiques et émotionnels du moment_ », renchérit Jean-Pascal.

Pour cette raison, les « Sens de Theus » ne se limite pas à être un lieu de production ; formations et accueil de gîte prendront bientôt place à la ferme.

![Sens de Theus marche](./img/Sens de Theus marche.jpg)

## Herboriste : un métier interdit et un savoir traditionnel perdu

Depuis 1941, la loi interdit de parler de plantes à tous ceux qui ne sont pas détenteurs d’un diplôme de pharmacien. Devenue illégale pour beaucoup de paysans, la transmission de ce savoir-faire, traditionnellement orale, s’est donc largement perdue en deux générations. La figure du docteur éduqué venu de la ville s’est imposée face à celle de la femme paysanne dotée d’un savoir populaire.

« _Nous sommes officiellement_ « exploitant agricole, producteur et transformateur de plantes à parfum, aromatiques et médicinales ». _Et même si, dans notre cœur, nous sommes_ « paysan herboriste », _nous n’avons officiellement pas le droit de vendre certaines plantes ni celui de parler des propriétés des plantes_ » explique Jean-Pascal.

![Sens de Theus pochette](./img/Sens de Theus pochette.jpg)

Comme il n’existe pas de cursus clair pour apprendre la connaissance médicinale des plantes, Jean-Pascal et Pauline sont donc allés à la rencontre de ceux qui font encore ce travail en France pour apprendre « sur le tas ». Afin d’obtenir une forme de reconnaissance de son métier, et malgré le fait qu'il ne soit pas officiellement reconnu, Pauline a également décidé de passer le diplôme d’herbaliste à l’Ecole Lyonnaise de Plantes Médicinales. Aujourd'hui, le couple s’entoure d’un réseau de professionnels naturopathes et docteurs en phytothérapie pour échanger sur leur métier. « _On n’a jamais fini d’apprendre. On travaille avec à peu près 90 plantes différentes… !_ » s’exclame Pauline.

Dans ce podcast, Pauline et Jean-Pascal nous parle d’agriculture, d’accueil, de wwoofing, de transmission de savoirs au travers de différents stages et ateliers.

<iframe src="https://www.podcastics.com/player/extended/978/34055/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

## Un financement de la Coopérative pour financer un alambic

Une convention entre la Coopérative Oasis et les Sens de Theus a été signée à la fin du printemps 2019, pour permettre de financer l'achat d'un alambic afin de mettre en place l’atelier de distillation d'huiles essentielles.

Cette opération est rattachée à une subvention européenne Leader et le financement de la coopérative Oasis permettra en particulier de faciliter l'achat car les subventions européennes sont remboursées bien après l'achat.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
