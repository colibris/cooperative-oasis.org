---
title: "L'îlot des Combes"
date: "2018-12-23T21:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/ilotdescombes/"
avancement: "en demande de prêt"
montant: ""
departement: "Saône et Loire"
image: ./img/ilotdescombes-drone.jpg
website: http://www.lilotdescombes.fr/
description: "Situé au Creusot, en Bourgogne, l'îlot des Combes est un lieu de production, de ressourcement et d'initiation autour de l'agroécologie. Il comprend notamment une micro-ferme en permaculture et un espace d'accueil avec cuisine pédagogique, salles d'atelier et des chambres pour vingt personnes."
---

Situé en Bourgogne, au Creusot, l’îlot des Combes est une oasis ressources créée en 2013. En plus des bâtiments d'accueil, deux hectares sont consacrés à la production fondée sur les principes de la permaculture – verger, plantes aromatiques, ruches… Initier des rencontres entre les personnes de tous les âges et de tous les horizons culturels et religieux est au coeur de la raison d'être de ce lieu unique.

![L'ilot des combes vue de haut](./img/ilotdescombes-drone.jpg)

## Un lieu pour se relier à la nature
L'îlot des Combes est née du cheminement d'un groupe d'amis partageant des intentions de bienveillance, d'émerveillement, de contentement et de partage. Il leur est apparu indispensable de créer un lieu propice aux temps de pause dans nos quotidiens frénétiques, un lieu pour vivre au rythme de la nature et pour apprendre à faire par soi-même.

## Un centre agroécologique
L'îlot des Combes est aussi un lieu de production et d'initiation autour de l'agroécologie, situé au Creusot, en Bourgogne. Il intègre une micro-ferme en permaculture et un espace d'accueil avec cuisine pédagogique, salles d'atelier et de quoi loger une vingtaine de personnes.

![La ferme de l'ilot des combes](./img/ilotdescombes-ferme.jpg)

## Un lieu d'accueil pour des publics très variés
Une des richesses de l'Îlot des combes tient à sa capacité d'accueillir des publics variés, en particulier des jeunes urbains qui connaissent mal ou peu la nature. De nombreux bénévoles viennent du Creusot comme de la banlieue parisienne pour accompagner le développement du lieu.

![groupe à l'îlot des combes](./img/Ilotdescombes-groupe.jpg)

Dans ce podcast "La Voix des Oasis", nous partons en balade dans les jardins en compagnie de Joachim, avant de retrouver Jean-Philippe et Karima, qui nous présentent l’histoire du lieu et les activités en cours et à venir :
<iframe src="https://www.podcastics.com/player/extended/978/34039/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
