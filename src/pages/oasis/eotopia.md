---
title: "Eotopia"
date: "2020-09-29T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/eotopia/"
avancement: ""
montant: ""
departement: "Saône-et-Loire"
image: ./img/EotopiaGroupe.jpg
website: http://www.eotopia.org/wordpress/fr/home/
description: "Eotopia est une communauté végane fondée en 2016 par une dizaine d’alternatifs venus bâtir le monde de demain."
---

**Eotopia est une communauté végane fondée par une poignée d’alternatifs venus bâtir le monde de demain. Plusieurs mois passent pendant lesquels les faux espoirs se succèdent jusqu'à ce que l'équipe décide de réunir ses économies pour acheter un terrain en Saône-et-Loire. Tout le monde s’y installe début juillet 2016.**

![Ecolectif](./img/EotopiaGroupe.jpg)

"Eo" s’apparente au grec ancien εἶμι (« aller »), au sanscrit एति (« il va »), au latin eō (« aller, avancer, avoir lieu, passer, s’écouler »). "Utopia" est un terme inventé en 1516 par Thomas More dans un livre éponyme. Il s’apparente au grec τόπος (« lieu ») et au préfixe privatif latin u. "Eotopia" pourrait donc signifier « un lieu où l’on avance » !

##Un tour du monde qui mène en Saône-et-Loire

Eotopia débute avec un voyage sans argent des Pays-Bas jusqu’au Mexique. Lors de ce voyage, les protagonistes Raphael, Benjamin et Nicola apprennent à vivre sans un sou en poche et découvrent le bonheur de la simplicité. Ils deviennent véganes et se convainquent qu’une économie basée sur le don est la seule économie juste et solidaire pour tous.

![Eotopia Ciel](./img/EotopiaCiel.jpg)

Eotopia reste un rêve jusqu’au début de l’année 2013. Raphael s’est marié avec Nieves ; Benjamin revient du Mexique avec Yazmin. A quatre, ils et elles décident de concrétiser le rêve, créent un site internet pour présenter le projet et organisent une réunion à Freiburg dans un école Steiner à la fin du mois d’octobre 2013. Plus de trente personnes s’y réunissent, certains pour s’informer, d’autres pour s’impliquer plus durablement, de 8 nationalités différentes !

![Eotopia Benjamin](./img/EotopiaMec.jpg)

Benjamin et Yazmin retournent alors en France et commencent sérieusement à chercher un lieu. Plusieurs mois passent pendant lesquels les faux espoirs se succèdent jusqu'à ce que l'équipe décide de réunir ses économies pour acheter un lieu et en avril 2016 un compromis de vente est signé pour un terrain en Saône-et-Loire. Tout le monde s’y installe début juillet 2016.

![Eotopia Chemin](./img/EotopiaChemin.jpg)

##Manger végan, local sain et de saison

À Eotopia, les habitants ne consomment que ce qui est nécessaire à leur bien-être en essayant au maximum de préserver les ressources environnantes. Tout en acceptant la mort et la souffrance comme parties inhérentes de notre monde, le collectif rêve d’une société où le respect et l’empathie s’étendent à tous les êtres vivants et où le spécisme n’a plus lieu. Manger végan, local, sain et de saison est le quotidien d'Eotopia. Tous ont pris la décision de ne pas avoir des produits d’origine animal sur le lieu, même récupérés. La nourriture des chats est encore achetée malgré le souhait de ne pas cautionner l’industrie de la viande...

![Eotopia Chat](./img/EotopiaChat.jpg)

##La confiance et le don comme boussoles

L'épanouissement est en grande partie possible grâce à la capacité à évoluer de manière individuelle et collective. À Eotopia, chacun fait au mieux pour prendre soin de soi et des autres. Comme une grande famille, les résidents et visiteurs sont invités à s’exprimer avec transparence, à écouter avec empathie, et à répondre avec calme et présence. De cette manière, le groupe apprend la vie ensemble et transforme les conflits en opportunité de se rendre plus fort et les relations, plus durables.

Les habitants donnent leur énergie, leur temps et leurs savoir-faire aux autres, au monde. Ainsi sont organisées des événements publics sans conditions ; de l'hébergement, des repas, des services et du temps sans contrepartie ; de la cueillette sauvage et de l'échange de services...

![Eotopia Moutarde](./img/EotopiaMoutarde.jpg)

La pratique de l’économie du don se distingue de la gratuité ou encore d’un système basé sur des prix libre. Ici, le groupe expérimente avec l’idée que chacun donne ce qu’il veut quand il le souhaite, sans conditions. C’est une philosophie de confiance, d’engagement, de responsabilité et d’auto-gestion débouchant sur la création d’un système économique transparent et collaboratif.

##S'inspirer de la nature, apprendre librement et vivre l'art

À Eotopia, le groupe observe les systèmes naturels et s'en inspire. Une méthode permaculturelle qui permet la mise en œuvre d'une forêt nourricière, d'un jardin potager, de systèmes énergétiques autonomes, d'habitats auto-construits et écologiques... Ces actions permettent aux habitants de tendre vers l’autonomie alimentaire et énergétique.

![Eotopia Yourte](./img/EotopiaYourte.jpg)

A Eotopia, l'objectif est de tisser des liens durables avec d’autres projets et d'accompagner la création de nouvelles initiatives.

La diversité d’âges et d’expériences est importante à Eotopia car tous ses habitants pensent que l’on peut apprendre les uns des autres sans distinction. Apprendre et désapprendre ensemble est devenu une activité quotidienne grâce à des partages de savoir, des espaces d’expérimentations accessibles à tous, des jeux inclusifs en groupe, et le choix de l’éducation en famille...

L'art tient une place essentielle dans la vie de l'écolieu : écriture, jam musicale, peinture, jeux de rôle, festival d'écologie artistique sont autant d'activités joyeuses pour vivre l’harmonie au quotidien.

![Eotopia Panneaux](./img/EotopiaPanneaux.jpg)

Dans ce podcast, Pierre et Benjamin partagent avec nous leur vision du lieu. Ils posent une réflexion sur l’économie du don, sur le vivre ensemble, sur les tâches communes et les évolutions du projet.

<iframe src="https://www.podcastics.com/player/extended/978/34034/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
