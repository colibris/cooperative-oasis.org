---
title: "Le Château partagé"
date: "2019-03-23T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/chateaupartage/"
avancement: "financé"
montant: "80 000 euros"
departement: "Savoie"
image: ./img/Chateaupartage-concert.jpg
website: http://www.lechateaupartage.fr
description: "Ancienne maison de maître du XVIIIe siècle, le Château partagé est depuis 2009 le lieu de vie de 17 personnes et accueille plusieurs activités : boulangerie bio, maraîcher bio, tourneur sur bois, accueil de groupes. Aidons-les à continuer leur développement !"
---

## Le collectif s'installe dans le château en 2009
Le 1er août 2009, jour de la fête du village, les fondateurs du Château partagé emménageaient à Dullin, commune rurale de l’avant pays savoyard sur les hauteurs du lac d’Aiguebelette. Cette ancienne maison de maître du XVIIIe siècle, ayant subi des modifications pour devenir une colonie de vacances après guerre, faisait parfaitement l’affaire pour leur projet collectif.
Bien modifiée, aux normes des années 1980, ils ont tout de suite commencé les travaux et aménagé plusieurs appartements individuels.

![Concert au Château partagé](./img/Chateaupartage-paysage2.jpg)

## Une trentaine d'habitants...
20 habitants, une dizaine de poules, 2 chats, 3 chiens, 2 canards, 4 brebis, deux juments... vivent dans ce beau lieu autour de valeurs communes : équilibre entre vie personnelle, vie collective et vie professionnelle, dans le respect et l’épanouissement de chaque individu, partage respect du milieu naturel, simplicité.

![Habitants du Château partagé](./img/Chateaupartage-groupe.jpg)

En commun, ils entretiennent la bâtisse, partagent buanderie, bibliothèque, atelier, jardin, joies et peines.

Plusieurs habitants ont aussi choisi de travailler sur place, le Château Partagé héberge donc un vaste panel d’activités professionnelles, qui contribuent à la bio-diversité humaine et économique du lieu.

![Champs Château partagé](./img/Chateaupartage-champs.jpg)

Thomas, le boulanger, vend son pain bio dans les épiceries du coin ou en AMAP. Thierry est maraîcher bio en traction animale et régale les papilles des villageois. Magdalina gère les chambres d'accueil et la location des salles. Baptou est tourneur sur bois... Une des richesses du Château partagé tient à la diversité des métiers qu'il héberge.

![Serre Château partagé](./img/Chateaupartage-serre.jpg)

Thomas, l’un des fondateurs, parle dans cette émission de la création du projet, de l’évolution au fil des années, du processus d’inclusion et de la constitution d’une charte. Il nous rappelle quelques pièges à éviter pour bien vivre ensemble...

<iframe src="https://www.podcastics.com/player/extended/978/34045/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

## Le prêt de la Coopérative Oasis

Le partenariat avec la Coopérative Oasis a été signé début avril 2019. Il a permis de finaliser la transformation du modèle juridique du Château. Les anciens associés ont progressivement été remboursés pour que chacun devienne avant tout locataire du Château. Cela a notamment donné une sécurité au projet en cas de départ d'un foyer. Au total, le prêt de 80 000 euros a permis de soutenir ce lieu exemplaire et ce collectif courageux.

Lors des 7 années du partenariat, l’accompagnement se compose notamment des actions suivantes :
- une animation d’un temps consacré aux liens humains entre les habitants
- l'accompagnement d’un DLA en cours sur les structures juridiques de l’oasis
- l'accompagnement juridique sur l’évolution des baux d’habitation et d’activités
- l'animation d’un atelier consacré aux différentes formes de gouvernance partagée.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
