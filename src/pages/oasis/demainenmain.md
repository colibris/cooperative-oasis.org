---
title: "Demain en main"
date: "2019-03-01T18:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/demain-en-main/"
avancement: "financé"
montant: "100 000 euros"
departement: "Morbihan"
image: ./img/demainenmain-groupe.jpg
website: https://demainenmain.fr/
highlighted: true
description: "Demain en main est un collectif de 8 familles, structuré aujourd’hui en association, qui inclut notamment 3 paysans qui souhaitent porter un projet agricole. Le projet est en cours d'installation dans le Pays d'Auray en Bretagne."
---

![Bandeau demain en main](./img/demainenmain-bandeau.jpg)

## Un collectif de 8 foyers
A l'automne 2016, Julien et son frère Fanch dessinent leur vision d'un éco-lieu associant production agricole, habitat et accueil. Leur slogan est alors "agir aujourd'hui, pour vivre ensemble demain". Puis ils jettent la bouteille à la mer et un premier rendez-vous a lieu début 2017 à Plouhinec. Durant l'année 2017, le collectif se consolide grâce à des règles de gouvernance participative. Aujourd'hui il se compose de 8 familles de 0 à 60 ans.

![Collectif demain en main](./img/demainenmain-groupe2.jpg)

## Un  hameau qui devient une oasis agricole
Le hameau est situé sur la commune de Locoal Mendon, dans le pays d'Auray. C'est une parcelle de 20 hectares composée de 4 bâtiments - aujourd'hui des gites -, d'un hangar, d'un four à pain, de 10 hectares de terre agricole et le reste en forêt.
La propriétaire a été rencontrée dès l'été 2017 et le collectif l'aide déjà dans l'entretien du lieu et l'accueil des gîtes. La confiance s'est installée et une solution a été trouvée pour un achat progressif du lieu par le collectif.

Découvrez le podcast “la Voix des oasis” sur Demain en main :

<iframe src="https://www.podcastics.com/player/extended/978/33917/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>


![Collectif demain en main](./img/demainenmain-batiments.jpg)

## Le besoin de financement
Le projet a sollicité la Coopérative Oasis pour un prêt de 100 000 euros sur 7 ans, qui a été versé en juillet 2020. Il a servi à l'achat de la propriété sur Locoal Mendon, en complément aux capitaux propres des habitants.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
