---
title: "Oasis Multikulti"
date: "2020-06-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/oasismultikulti/"
avancement: ""
montant: ""
departement: "Bas-Rhin"
website: https://www.oasismultikulti.org/
image: ./img/Multikulti0.jpg
description: "Fêtes, entraide, jardinage, ateliers culturels… l’Oasis Multikulti enchante depuis 2018 la vie du village de Mietesheim."
---

**_"En 2018, j’ai décidé de reprendre la ferme de mes grands-parents pour en faire un lieu créatif, festif et agroécologique”_ raconte Stéphanie, fondatrice de l’Oasis Multikulti en Alsace. Fêtes, entraide, jardinage, ateliers culturels… retour sur l’histoire un peu magique de ce lieu qui enchante désormais la vie du village de Mietesheim.**

![Oasis Multikulti](./img/Multikulti1.jpg)

## “Je suis revenue aux racines agricoles de ma famille par la permaculture”
Stéphanie est fille et petite fille d’agriculteurs alsaciens. Après des études de beaux-arts, elle s’installe à Strasbourg pour y travailler dans une association culturelle. _"J’étais sûre de quitter le monde agricole,_ raconte-t-elle. _J’en avais une très mauvaise image : celle d’un père absent parce qu’il travaille sans relâche. Contre toute attente, c’est la permaculture qui m’a donné envie d’y retourner."_ Lasse aussi de passer ses journées derrière un ordinateur malgré un travail qui lui plaît, Stéphanie décide en 2018 de tout quitter pour reprendre la ferme de ses grands-parents, à l’abandon depuis 5 ans.

![Oasis Multikulti](./img/Multikulti2.jpg)

## “Mon ancien moniteur de colo est mon associé, ma maîtresse d’école nous aide au jardin, et j’ai intégré le conseil municipal !”
Après presque un an de woofing pour se former et confirmer son goût pour le travail de la terre, Stéphanie revient et s’installe à l’étage de la grande maison familiale de 200m2. Pour lancer son activité, Stéphanie et son associé proposent une journée portes ouvertes. _"Une centaine d’habitants du village et des environs sont venus, beaucoup voulaient revoir la ferme qu’ils avaient connue dans leur enfance… Mon père racontait comment ça se passait à l’époque, c’était très émouvant"_ explique Stéphanie.

Aujourd’hui, l’association compte une cinquantaine d’adhérents dont la grande majorité habitent le village._"Je viens d’ici, tout le monde me connaît déjà, moi ou mes parents, ça facilite les choses pour se faire accepter !_ explique Stéphanie. _Ma maîtresse d’école vient souvent m’aider par exemple ! Il reste encore une grosse partie du village qui ne sait trop quoi en penser, de l’oasis Multikulti... on essaie de les attirer. C’est pour ça que j’ai intégré le conseil municipal. J’ai beaucoup hésité mais je me suis dit que ça faisait partie du projet."_

![Oasis Multikulti](./img/Multikulti3.jpg)

## "Tout ce qu’on fait, c’est un prétexte pour créer du lien !"
Les activités ne manquent pas dans cette ancienne ferme. En décembre 2018, le festikulti est le premier événement culturel organisé, avec un petit concert, une conteuse, des danses… Les ateliers culturels ont suivi, proposés par des gens venus d’eux-mêmes : un atelier de céramique par une strasbourgeoise retraitée qui a donné tout son matériel à Multikulti ; un atelier d’écriture, de couture, de sophrologie…

![Oasis Multikulti](./img/Multikulti4.jpg)

Le jardin est venu un peu plus tard, avec la création d’une mare, d’un jardin en forme de mandala, la rénovation du poulailler pour l'installation de l'atelier d'une artiste peintre... _"Cet été, on prévoit un chantier participatif pour isoler le bâtiment en roseau, terre et paille,_ reprend Stéphanie. _Les chantiers ramènent toutes les générations, j’adore ces moments."_

En 2021, un café culturel devrait voir le jour  pour permettre aux gens du coin de se retrouver. En attendant, Stéphanie distribue toutes les semaines son pain et les 25 paniers de légumes de deux jeunes maraîchers en biodynamie du village d’à côté. _"Au final, tout ce qu’on fait, c’est un prétexte pour créer du lien !"_ explique Dom, un artiste engagé dans le projet depuis juin 2020.

![Oasis Multikulti](./img/Multikulti5.jpg)

L’activité est donc revenue dans ce lieu longtemps animé par une vie paysanne traditionnelle disparue. Et avec elle, chacun a réappris à s’épanouir. _"Toutes les personnes impliquées dans Multikulti ont repris des activités qui leur tenait à cœur - musique, art, jardinage - ou ont pu développer celles qui les faisaient vibrer. Ici on se demande ce qui est vraiment important pour soi, et on s’y consacre pleinement."_  

_Crédit photo : [P-Mod](http://www.p-mod.com/)_

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
