---
title: "L'écohameau de Verfeil"
date: "2020-10-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/ecohameauverfeil/"
avancement: "financé"
montant: "92 400 euros"
departement: "Tarn et Garonne"
image: ./img/Verfeil0.jpg
website: http://verfeil-eco.over-blog.org/
description: "15 adultes et 12 enfants vivent aujourd’hui dans un écohameau de maisons bioclimatiques situé à Verfeil-sur-Seye"
---

**15 adultes et 12 enfants vivent aujourd’hui dans un écohameau situé à [Verfeil-sur-Seye](http://verfeil-eco.over-blog.org/), une commune de 400 habitants dans le Tarn-et-Garonne. Les maisons bioclimatiques ont toutes été en partie construites par les membres de ce collectif passionné d'éco-construction et de bœufs musicaux.**

![Verfeil](./img/Verfeil0.jpg)
_Crédit photo Mathieu Hoffmann_

## Un collectif en mouvement

Il ne reste aujourd’hui personne du groupe d'éco-constructeurs d’origine, qui avait conçu un projet d’écovillage en zone rurale et l’avait soumis à la commune de Verfeil, enthousiaste. En 2010, un deuxième collectif avait finalement acheté le terrain avec ses fonds propres ; seuls deux foyers de ce deuxième groupe habitent aujourd’hui sur place.

Parmi les 15 adultes habitant à Verfeil, on trouve des métiers aussi différents les uns des autres que professeur de piano, chant et français, traductrice, créatrice de bijoux, cadre en Mairie, puéricultrice, écoconstructeur, restaurateur d’objets d’art… La plupart d’entre eux ont en effet gardé un métier à l’extérieur, l’exercent parfois en temps partiel, afin d’avoir du temps à consacrer aux activités du lieu.

Ce collectif bigarré a fait le choix d’une gouvernance collective exigeante et intégrée : la totalité des décisions liées au lieu sont prises au consentement au cours du cercle hebdomadaire. _« Nous n’avons jamais eu besoin d’utiliser la majorité au 2/3 prévue en cas de blocage, ça fonctionne bien »_ explique Sarah. Cette gestion intégrée des parties communes est contrebalancée par une grande liberté laissée à chaque foyer dans la construction et la gestion de sa maison individuelle, grâce au modèle juridique traditionnel de copropriété choisi : l’ASL.

![Verfeil](./img/Verfeil1.png)

## L’écoconstruction en commun

Libre donc à chaque foyer de choisir son mode de construction et son esthétique, mais toujours dans le cadre d’une charte constructive écologique commune : ossature bois, isolation paille et enduit terre, matériaux biosourcés et locaux. « L’écoconstruction était vraiment à la base du projet » explique Stéphan, constructeur membre du [Réseau français de la construction terre paille](https://www.rfcp.fr/). Il est le seul de l’écohameau à avoir construit sa maison seul, de A à Z. Les huit autres maisons ont été soit réalisées en « autoconstruction accompagnée », mixant autoconstruction et artisans, soit complètement construites par des artisans.

Avoir une maison au sein de l’écohameau de Verfeil demande un investissement pour l’achat du terrain (35 000€ avec les frais de notaire) et la construction de la maison (entre 300€ et 1000€ du m2 pour une maison bioclimatique). Une fois cet investissement de départ effectué, les foyers vivent souvent avec peu de dépenses du fait des faibles consommations énergétiques des bâtiments bioclimatiques. Deux stères de bois grand maximum sont nécessaires pour le chauffage d’un foyer, soit moins de 150€ par an pour un grand confort d’habitat. À Verfeil, la sobriété énergétique va de pair avec les économies !

Le défi principal de l’écohameau n’est pas technique mais bien relationnel. _« On a traversé des conflits,_ raconte Elsa. _On n’en est pas toujours sortis indemnes, il y a eu de la casse individuelle et collective, il faut le dire. Mais le groupe a beaucoup évolué grâce à ces ruptures. À titre personnel, ça m’a appris à gérer des situations que j’avais tendance à éviter, c’est aussi très enrichissant. »_

![Verfeil](./img/Verfeil4.png)

## Recréer la vie de village

Mère de deux enfants, Sarah a pris la décision de s’installer en collectif suite au décès de son mari. _« Au début, j’ai essayé de continuer comme avant – même travail, même maison, même train de vie… mais je n’ai pas tenu longtemps. J’avais besoin de changer et le collectif me donnait l’impression de pouvoir être épaulée tout en donnant en échange. Et puis j’avais besoin d’un projet fou dans lequel mettre mon énergie. Alors j’ai tapé « écovillage France » sur internet et je suis tombée sur le blog de Verfeil. Une semaine plus tard, je participais à la journée d’accueil. »_

Les enfants de Sarah ont donc rejoint la dizaine d’autres enfants habitant déjà sur place.
Tous vont à l’école ou au collège à l’extérieur et partagent leur quotidien dans l’écohameau. _“Les enfants ont une vie de village : ils explorent le lieu, passent de maison en maison, grandissent avec bien plus d’enfants que leurs frères et soeurs, sont entourés de nombreux adultes… et tout cela dans une sécurité totale”_ explique Elsa. Bien sûr, chaque parent éduque un peu différemment, sur la question des écrans par exemple. _“On a appris à en discuter entre nous et à trouver des solutions, raconte Sarah. Aujourd’hui, j’ai une confiance totale quand mes enfants sont chez d’autres adultes qui font à leur façon. C’est aussi ça la vie de village, la découverte de la diversité…”_

![Verfeil](./img/Verfeil3.png)

## Un financement par la Coopérative Oasis

En novembre 2020, la Coopérative Oasis a commencé  à accompagner l’écohameau de Verfeil. Elle lui a fait un **prêt de 90 000 euros sur 3 ans, pour la rénovation de la halle commune, en attente d’une subvention régionale**.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
