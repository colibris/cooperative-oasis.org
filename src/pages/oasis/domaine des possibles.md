---
title: "Domaine des possibles"
date: "2018-01-10T21:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/domainedespossibles/"
avancement: "financé"
montant: "60 000 euros"
departement: "Puy-de-dôme"
image: ./img/domainedespossibles.jpg
description: "Un habitat participatif et intergénérationnel de 9 familles dans un ancien château près de Clermont-Ferrand qui vise à y créer aussi une ferme, une école et un tiers-lieu."
---

## L'histoire du projet
Le domaine des possibles est un collectif composé de 9 foyers qui a acheté en décembre 2019 un beau domaine de 9 hectares situé à dix kilomètres de Clermont-Ferrand, pour y créer un habitat partagé et y implanter une ferme et un jardin pédagogique, un potager en permaculture, une école alternative, un tiers-lieu, un atelier fer/bois, des chambres d'hôtes.

Le projet sera porté par une SCI dans laquelle tous les habitants seront actionnaires. La
forme de la SCI n’est pas encore définie. Puis une SCIC sera créé pour gérer les activités communes au collectif. Celle-ci deviendra du coup actionnaires de la SCI pour éviter la spéculation.

![domaine des possibles](./img/domainedespossibles.jpg)

## Un domaine au grand potentiel
Le domaine se trouve à 15 minutes de Clermont Ferrand, ce qui en fait un emplacement très intéressant pour attirer différents types de public. Le lieu comporte actuellement une très grande maison, deux dépendances, un pigeonnier, un garage et 9 hectares de terrain.
Le collectif prévoit notamment de :
 - Rénover la bâtisse principale pour réaliser 8 logements à l’étage ainsi que 2 chambres d’hôtes, et les espaces communs privatifs au rez-de-chaussée
 - Rénover la première dépendance pour faire au rez-de-chaussée une salle commune ouverte au public et à l’étage un gîte de groupe (15 places) et un neuvième logement
 - Transformer le pigeonnier en gîte
 - Rénover la seconde dépendance pour faire une salle de pratique et un atelier

![domaine des possibles](./img/domainedespossibles2.jpg)

## Besoin de financement
Le projet a sollicité la Coopérative Oasis pour un prêt de 60 000 euros sur 6 mois comme prêt relai permettant l'achat du lieu dans l'attente de la revente de biens immobiliers par 2 membres du projet. L'accompagnement de 6 mois va permettre d'accompagner le collectif dans la programmation précise du projet et permettra d'initier un partenariat plus long terme.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
