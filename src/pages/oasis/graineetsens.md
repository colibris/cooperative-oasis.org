---
title: "Grain&Sens"
date: "2020-03-31T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/graineetsens/"
avancement: "financé"
montant: "100 000 euros"
departement: "Ardèche"
image: ./img/graineetsens-boulange.jpg
website: https://www.grainandsens.com/
description: "Un collectif de 14 adultes et enfants multilingues sur un lieu exceptionnel en Ardèche autour de l'accueil dans la nature, la boulangerie bio, le maraîchage et des éco-camps d'anglais."
---

Créé en 2019, [GRAIN&SENS](https://www.grainandsens.com/) est un **lieu de vie et d'activités** dont la volonté commune est de trouver une manière plus juste d'habiter, de cultiver la terre, d'éduquer, de se réaliser au sein d'un projet collectif. C'est en **Ardèche**, sur le domaine de Lavenant, **comprenant 15ha de prés et de bois**, dont une majestueuse hêtraie, que les **10 adultes et 4 enfants** on décidé d'établir leur rêve...

## Un collectif multilingue
En 2019, 3 familles mixtes anglophones se sont réunies autour d’une vision commune : “créer un lieu de vie et de partage nourrissant les êtres et les consciences”. Aujourd'hui 10 adultes et 4 enfants habitent ensemble sur un domaine Ardéchois et développent plusieurs projets :
- de l’accueil en gite et en nature ;
- une boulangerie paysanne bio ;
- du maraîchage en permaculture ;
- des éco-camp d’immersion en anglais.

<iframe width="560" height="315" src="https://www.youtube.com/embed/w0vgzOp3yec" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ainsi est né le projet Grain&Sens. Littéralement, le Grain et la Graine jouent des rôles essentiels dans la production d’une nourriture saine. Symboliquement, le grain représente l'ambition pédagogique du lieu : des graines d’idées et d’attitude sont semées en ceux qui viennent rendre visite. Le Sens est leur aspiration à vivre des convictions fortes, suivre le chemin pour donner du sens à leur vies, vivre avec “bon sens”, dans le respect du vivant et la bienveillance.

![collectif de Graine et Sens](./img/graineetsens-groupe2.jpg)

## Un domaine Ardéchois permettant l'accueil et la culture du sol
Après un an de recherche, le collectif a trouvé le lieu idéal pour s'installer : le domaine de Lavenant à Boffres (07440). Le lieu et le mobilier ont été été achetés tout début janvier 2019, avec le soutien de la Coopérative.

Le lieu, typiquement Ardéchois dans sa topographie, offre de nombreuses possibilités avec ses 15 ha de forêt, 5 ha de prairie cultivable, 2 grands gîtes de 15 personnes entièrement rénovés, une habitation et de multiples dépendances.


![Lieu Grain et Sens](./img/graineetsens-lieu.jpg)

Dans ce podcast "La Voix des Oasis", Romain et Astrid présentent les spécificités de ce lieu, où l’on vient apprendre une nouvelle langue mais aussi le language de la nature. On y parle du montage financier et de la manière de présenter son projet aux banques pour pouvoir lever les fonds nécessaires à l’acquisition d’un lieu :
<iframe src="https://www.podcastics.com/player/extended/978/34038/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>


## Le besoin de financement
La coopérative a signé un partenariat de 7 ans avec Grain&Sens. Le prêt de 100 000 euros sert à lancer les activités sur le lieu, notamment l'accueil de stages et les gîtes.
L'accompagnement de la coopérative Oasis est redéfini tous les ans. La première année, la coopérative Oasis propose à l'oasis :
 - un soutien à la mise en place d'un design en permaculture et notamment l'organisation d'un certificat de conception en permaculture sur le lieu
 - un soutien à la réflexion stratégique et notamment à la précision du modèle économique et du positionnement du lieu
 - un soutien à la mise en place d'une gouvernance adaptée, notamment par un regard extérieur, la mise en place de benchmarks et l'apport d'outils.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez Grain&sens en participant à la Coopérative.
    </a>
</div>
</div>
