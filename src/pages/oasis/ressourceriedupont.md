---
title: "La Ressourcerie du Pont"
date: "2018-09-25T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/resssouceriedupont/"
avancement: ""
montant: ""
departement: "Gard"
website: http://www.rdevolution.org/
image: ./img/RessourceriePont2bis.jpg
description: "Une ressourcerie historique des Cévennes, un lieu de vie collectif, écologique et artistique et bientôt une Terre de Convergence"
---

## Un projet multiforme
Depuis sa création en 2004 à Lyon, l’association Rd’Évolution s’appuie sur le collectif pour préserver les écosystèmes et soutenir les personnes dans le besoin. Aujourd’hui située dans les Cévennes, l’association pratique :
- la transmission de savoir-faire et l’accueil dans un lieu de vie collectif, Artimbal
- le réemploi grâce à une ressourcerie située au Vigan, la Ressourcerie du Pont
- bientôt l’achat d’une “terre de convergence” avec l’association “Le Village du Possible”…
L’objectif ? mêler création d’emploi, sensibilisation à l’environnement et mise réseau à l’échelle du territoire.

![La ressourcerie du pont](./img/RessourceriePont1.JPG)

## Au Vigan, la Ressourcerie du Pont allie activité économique et réduction des déchets
En 2014, le collectif d’artistes activistes cofondé par Violette, Élise, Uto et Antoine investit une usine de 3 500m2 située sur la commune du Vigan, dans le Gard. Ils y ouvrent rapidement une Ressourcerie : un lieu de collecte d’objet, de réparation, revalorisation, revente et sensibilisation à la réduction des déchets. Aujourd’hui, la ressourcerie donne une seconde vie à 90% de la centaine de tonne d’objets qu’elle collecte tous les ans, dont 55% de réemploi direct. Le reste est réutilisé par des artisans, envoyé au recyclage ou à la déchetterie (6%).

Membre actif du “Réseau National des Ressourceries”, le lieu est animé par une équipe d’une vingtaine de bénévoles et de six salariés en CDI ou contrat aidé. Plus qu’une ressourcerie, l’ancienne usine met des ateliers et outils à disposition d’artistes et d’artisans pour de la couture, menuiserie, soudure, vannerie, vidéo, musique, peinture…

Grâce à son activité, à des subventions, à des emprunts auprès de particuliers (apports avec droit de reprise) et à des dons, l’équipe a réussi à réunir en 2018 les 300 000€ nécessaires à l’achat de la ressourcerie dont elle est maintenant propriétaire.

![La ressourcerie du pont](./img/RessourceriePont2.JPG)

## Dans les hauteurs du Vigan, Artimbal est un lieu de vie collectif, écologique et artistique qui accueille une partie de l’équipe de la Ressourcerie du Pont
Un partie du groupe qui assure l’activité de la ressourcerie habite à Artimbal, un écolieu dans les hauteurs du Vigan. Les espaces et outils communs permettent à la douzaine d’habitants de vivre dans la sobriété et la nature ; un bois entoure les deux anciennes bâtisses où cohabitent les habitants. Tout le monde participe au loyer et aux charges, entre 80€ et 150€ par mois et une cagnotte collective permet l’achat de nourriture. Le potager fournit au collectif une partie de ses légumes ; compost, toilettes sèches et produits ménagers faits maisons sont le quotidien de la vie d’Artimbal.

![Artimbal](./img/RessourceriePont3.jpg)


## Le besoin de financement : vers une “Terre de Convergence”
La Ressourcerie du Pont constitue en réalité l'acte numéro un d'un projet de transition active en trois actes. Le deuxième de ces actes consiste à acheter une “terre de convergence”, un lieu permettant la formation et la sensibilisation au travers d’éco-événements.
Le projet a sollicité la Coopérative Oasis pour un prêt de 100 000 euros sur 5 ans. Il servira à l’achat de la terre d’une centaine d’hectares entre Nîmes, Montpellier et le sud des Cévennes.


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez la Ressourcerie du Pont en participant à la Coopérative
    </a>
</div>
</div>
