---
title: "Ecolectif"
date: "2020-01-29T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/ecolectif/"
avancement: ""
montant: ""
departement: "Haute-Garonne"
image: ./img/Ecolectif1.jpg
website: https://ecolectif.jimdofree.com/
description: "Fondé en 2012, Ecolectif est un écohameau implanté sur un domaine agricole de 46ha où vivent un 20e d'adultes et une 20e d'enfants. "
---

Fondé en 2009, Écolectif est un éco-hameau implanté sur un domaine agricole de 46ha situé en Haute-Garonne, entre Toulouse et Tarbes. Une vingtaine d’adultes et une vingtaine d’enfants y vivent et s’y activent. Ensemble, en cultivant une “écologie relationnelle” : prendre soin de l’individu, de la famille, du collectif et de la terre.

![Ecolectif](./img/Ecolectif1.jpg)

 _"À Ecolectif, l’intelligence est émotionnelle. Si je me laissais aller à hurler de joie ou pleurer toutes les larmes de mon corps dans ma yourte, je savais que si quelqu’un passait, il ferait preuve d’une empathie inconditionnelle. Dans beaucoup de milieux, il y a tout un tas d’émotions qu’on ne sait pas gérer : la colère est minimisée, la tristesse réprimée, les élans de gratitude et d’amour moqués. J’ai découvert autre chose et ça m’a offert une immense sécurité intérieure"_ témoigne Guillaume Mouton, ancien habitant d'Écolectif.

## Un collectif d'habitats sobres alliant espaces communs et parties privées
 Ecolectif tend vers des habitats auto-construits, des bâtiments écologiques et des économies d’énergie. Pour cela, l’auto-écoconstruction a été envisagée d’abord dans un état d’esprit d’entraide, de créativité, permettant l’utilisation des matériaux locaux et le recyclage de matériaux usagés : terre, pierres, bois, paille et autres végétaux produits localement, matériaux recyclables ou réutilisables (bâches, bouteilles en verre, textiles divers).

 ![Ecolectif](./img/Ecolectif2.jpg)

  L’auto-construction a occasionné la pratique de techniques traditionnelles (bois rond, murs en terre-paille, enduits terre), l’accès à des réseaux d’artistes et d’artisans constructeurs et l’organisation de chantiers participatifs, formateurs et festifs.

  Le collectif s’engage vers l’autonomie énergétique grâce à une production d’électricité par photovoltaïque, une douche solaire thermique, bientôt une production d’eau chaude solaire. La vie est organisée autour de la sobriété : toilettes sèches, compost, récupération d’eau de pluie pour le ménage, le jardin, les cultures maraîchères, gestion autonome des eaux grises, mutualisation des transports.

 ![Ecolectif](./img/Ecolectif3.jpg)

 Les ressources et espaces sont mutualisés lorsque c’est possible et souhaité : cuisines, salle polyvalente, bureau partagé, salles de bain, buanderie équipée, chambres d’amis, bibliothèque, ludothèque, ateliers divers, épicerie, salle de musique, ressourcerie, une voiture, et de nombreux outils et matériaux… sont ainsi mis en commun.

## Un collectif d'alimentation : production maraîchère et commandes groupées
 Écolectif et ses membres ont développé des activités agricoles : 3 000m2 de maraîchage alimentant les habitants et visiteurs ; des vergers et une pépinière ; une activité de boulangerie grâce à la construction d’un four à bois ; plusieurs poulaillers et quelques ruches, un élevage d’alpagas, quelques chèvres et quelques brebis. Toute la production se fait en minimisant la consommation d’énergie fossile (peu de transports, autoproduction ou récupération des fertilisants chez les proches voisins).

![Ecolectif](./img/Ecolectif4.jpg)

 Le collectif et ses habitants bénéficient de plusieurs groupements d’achats pour compléter sa production alimentaire grâce à des partenariats avec d’autres groupements ou producteurs, les plus proches possibles éthiquement et géographiquement.

##  Un collectif d'activités et d'apprentissage intergénérationnel
 En plus du maraîchage, les habitants d’Écolectif exercent certaines de leurs activités professionnelles sur le lieu. Des activités artisanales - atelier de menuiserie, fabrique familiale d’objets en laine, céramique, sculpture, confection - des activités de formation et de soin – ostéopathie, réflexologie, coaching - et un groupe de musique « Freedams »…

![Ecolectif](./img/Ecolectif5.jpg)

 Sur le lieu, toutes les générations se mélangent lors d’évènements variés, souvent ouverts aux voisins ou à tous les publics. L’idée est d’apprendre ensemble, dans l’ouverture aux richesses intérieures de tous, quels que soient les statuts, âges, genres, origines, parcours de vie, qualifications… au gré des lieux, des visiteurs et des moyens à disposition.

 ![Ecolectif](./img/Ecolectif6.jpg)

 Dans ce podcast Isabel, l’une des fondatrices d’Écolectif nous présente la manière dont cet éco-hameau envisage le collectif pour faire avancer les projets individuels. On y parlera de communication bienveillante, de gouvernance, de relations intergénérationnelles, l’un des objectifs premiers du projet :

<iframe src="https://www.podcastics.com/player/extended/978/33918/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
