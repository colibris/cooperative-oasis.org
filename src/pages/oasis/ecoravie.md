---
title: "Ecoravie"
date: "2021-01-02T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/Ecoravie/"
avancement: ""
montant: ""
departement: "Drôme"
website: http://www.ecoravie.org//
image: ./img/Ecoravie-Claire-Florine.jpg
highlighted: true
description: "Créée en mars 2009 à Dieulefit dans la Drôme, Ecoravie est un habitat collectif, écologique et solidaire emblématique composé d’une quarantaine de personnes..."
---

**Créée en mars 2009 à Dieulefit dans la Drôme, [Ecoravie](http://www.ecoravie.org/) est un habitat collectif, écologique et solidaire emblématique. Composé d’une quarantaine de personnes, le collectif s’est notamment illustré dans sa capacité à construire de grands bâtiments d’habitation de façon bioclimatique. Trois bâtiments à énergie positive sont déjà sortis de terre, et bientôt une maison commune…**

![Ecoravie Claire Florine](./img/Ecoravie-Claire-Florine.jpg)

## De tous les âges et de tous les horizons

Les « écoravissants », habitants et membres actifs du projet, sont au 1 mai 2020 au nombre de 29 adultes et 16 enfants de quelques mois à plus de 80 ans. Tous viennent d’horizons différents : quelques drômois ont été rejoints par des alsaciens, lyonnais et stéphanois se côtoient, des parisiens, une savoyarde...

Certains écoravissants sont en pleine activité : informaticien, enseignant, maître d’œuvre, ouvrier polyvalent, intermittents du spectacle, formateur… D’autres membres du groupe sont retraités, de métiers aussi divers qu’éducatrice, documentaliste, professeur d’art, chef d’entreprise, infirmière, psycho...

![Ecoravie batiments](./img/Ecoravie-batiments.jpg)

Certains sont dans le projet depuis ses débuts (2007), d’autres l’ont rejoint au fil des ans. Quelques-uns sont partis car le projet était trop long à se concrétiser, d’autres ont préféré lancer un projet ailleurs: ce groupe vivant s’est consolidé peu à peu avec le temps et un fort engagement !

## Des constructions écologiques sur 8 000m2 de Drôme

Le terrain de 8 000m2 sur lequel s’est installé Écoravie fait partie d’une zone de 3ha urbanisable au plan local d'urbanisme (PLU). Cette zone est située sur les coteaux des Reymonds, à 5 minutes à pied du centre-ville de Dieulefit, dans la Drôme.

![Ecoravie Potager](./img/Ecoravie-potager.jpg)

Le projet consiste à créer un éco-lieu composé de 3 bâtiments de 5 à 7 logements chacun ainsi qu’une maison commune à l’usage des habitants et ouverte sur le village. Le choix d’un habitat concentré permet de préserver au maximum l’espace naturel. Les logements vont d’environ 50 à 110m2 et du T2 au T5/6. Dans chacun des 3 bâtiments, une buanderie et des espaces de stockage seront mutualisés.

Les 3 bâtiments d’habitation sont construits avec des matériaux biosourcés naturels et sains (bois, paille, terre), des techniques locales tournées vers un développement durable. _« Pour améliorer notre expérience de la construction, les bâtiments sont construits les uns après les autres. Cela nous permet également d’optimiser notre « main d’œuvre ultra-locale » : des habitants ou futurs habitants ! »_ explique Claire.

![Ecoravie batiment 2](./img/Ecoravie-batiment-profil.jpg)

Il n’y a pas de chauffage à Écoravie ! Grâce à l’orientation des bâtiments et la disposition intérieure des pièces, c’est le soleil qui chauffe l’intégralité des appartements notamment grâce à des serres bioclimatiques. Les 3 bâtiments sont équipés de toilettes sèches, les fèces vont au lombricompostage. Après,2 ans, c’est un excellent engrais pour les cultures. Récupération des eaux de pluie : 60.000 litres pour l’arrosage !


## Des apports personnels allant de 6 200€ à 220 000€

Le budget total dEcoravie s’élève à 4,5 M€. Ce coût inclut principalement l’achat du terrain (12 000 m2), la construction des 3 bâtiments (18 logements) ainsi que de la maison commune (180 m2) et le paiement des honoraires des prestataires auxquels le groupe a fait appel : architectes, assistant à maîtrise d’ouvrage, juristes, etc.

![Ecoravie Profil](./img/Ecoravie-rousse.jpg)

A Ecoravie, personne n’est propriétaire de son logement. C’est la coopérative d’habitants qui possède le lieu de vie. Les habitants sont à la fois locataires et associés de la coopérative. Ils prennent les décisions relatives à la conception et à la gestion de leur lieu de vie parce qu’ils y habitent, et non parce qu’ils en ont la propriété.

Pour financer la construction, les futurs habitants du groupe ont apporté, selon leurs moyens, de 6 200 à 220 000 euros. Cette solidarité interne entre les habitants permet de couvrir environ la moitié du budget du projet !
Les  banques refusant de prêter ,l’appel à des prêteurs solidaires a permis la construction. Une aide importante est venue de 2 caisses de retraite.

![Ecoravie Toilette](./img/Ecoravie-toilettes.jpg)

Les mensualités payées par les habitants sont calculées pour rembourser l’emprunt collectif extérieur en une vingtaine d’années. Mais impossible de donner le prix au mètre carré : la mensualité ne dépend pas de la surface du logement mais des moyens des habitants et de la composition du foyer.

## La gouvernance : un travail au long court !

_« En 2013, des tensions relationnelles réapparaissent et la fatigue règne,_ raconte Claire. _Après la communication, c’est notre “faire ensemble” qui est à améliorer. Nous nous tournons alors vers l’Université du Nous, qui nous dote d’outils précieux au fil des années. »_

![Ecoravie Interieur](./img/Ecoravie-interieur.jpg)

Aujourd’hui, le travail sur la gouvernance novatrice se poursuit avec les enfants : météo personnelle en début et fin de journée, utilisation de bâtons-de-paroles fabriqués eux-mêmes, partage des tâches  domestiques*** La sociocratie entre dans leur vie en douceur.

_« A ce jour, nous constatons que notre groupe a réellement progressé dans la démocratie participative. Nous restons très attentifs et attachés à notre mode de gouvernance, puisqu’il est la manifestation quotidienne de nos valeurs fondamentales. Et nous sommes fiers d’être les partenaires d’une société en transition. »_

Dans ce podcast, Claire et Florine nous présentent le fonctionnement de ce lieu inspirant :

<iframe src="https://www.podcastics.com/player/extended/978/33920/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
