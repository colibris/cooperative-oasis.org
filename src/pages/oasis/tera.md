---
title: "Tera"
date: "2019-03-31T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/tera/"
avancement: "financé"
montant: "180 500 euros"
departement: "Lot-et-Garonne"
image: ./img/tera2.jpg
website: https://www.tera.coop/
description: "Tera est un écosystème coopératif dans le Lot-et-Garonne qui vise notamment à relocaliser à 85% la production vitale à ses habitants et valoriser cette production en monnaie citoyenne locale."
---

## À l'origine du projet
L'association loi 1901 Tera est créée le 21 juin 2014. Au cours des étés 2014 et 2015, deux tours de France en van et vélos à assistance électrique, ont permis d'aller à la rencontre de collectifs citoyens, d'écolieux, d'habitants de territoires ruraux (maires de petites communes, agriculteurs...) et d'organiser des réunions publiques: des diagnostics, propositions, retours d'expérience ont été collectés et ont fait avancer le design du projet.  Tera s'installe le 15 octobre 2015, sur le domaine du Tilleul, un domaine de 12ha situé à Masquières (Lot-et-Garonne).

Pendant ses premières années, elle a attiré plus de quarante nouveaux habitants, venus contribuer au projet et/ou développer leurs propres activités (entre autres, des micro-entreprises ont été créées, et un réseau de producteurs en circuits courts est en cours de développement), et a noué des relations avec des parties prenantes locales et plus lointaines. Elle a été lauréat de l'innovation sociale de Nouvelle Aquitaine en 2016; elle est et a été soutenue
pour ses diverses actions par la Région Nouvelle Aquitaine, le département du Lot-et-Garonne, l'Europe (programme Leader), l'ADEME, plusieurs fondations, et des centaines de particuliers qui la suivent sur les réseaux sociaux ou y contribuent financièrement.

![Tera](./img/tera1.jpg)

Aujourd'hui en 2020, une trentaine de permanents travaillent sur le projet qui se déploie sur 3 communes: Tournon d’Agenais, où l'association a installé ses bureaux et déplacé son siège social, Masquières, où se développent les activités autour de l'alimenter (maraîchage, forêt-jardin, compost...), et Trentels où un quartier rural est en train de naître, en partenariat avec la commune. L’association compte 400 adhérents et plus de 500 donateurs. Les "Terians"  participent aux différentes activités, de façon professionnelle ou bénévole: activités de production, organisation des événements associatifs, gouvernance et renforcement de la coopération... Le travail ne manque pas ! Une nouvelle association, "Les Amis de Lustrac" est en cours de structuration depuis 2019 pour porter plus spécifiquement le projet de quartier rural.

![Tera](./img/tera2.jpg)

## Un écosystème coopératif qui se donne 10 ans pour créer un territoire résilient
La raison d'être de TERA est de créer les conditions matérielles et immatérielles pour que chacun puisse expérimenter le chemin de son propre bonheur dans le respect des humains et de la nature. Il s'agit concrètement de construire un écosystème coopératif s'articulant autour d'axes fondamentaux : la démocratie, le vivre-ensemble, la relocalisation de 85% de la production vitale aux habitants, l'habitat durable, la mutualisation des ressources et le partage des connaissances.

Afin de contribuer à la revitalisation et à la résilience de son territoire rural d'adoption, le projet TERA entend valoriser la production locale en monnaie citoyenne locale, émise via un revenu d'autonomie d'un euro supérieur au seuil de pauvreté pour chacun de ses habitants. Il est accompagné par un conseil scientifique qui se réunit chaque année et apporte éclairages, avis, points de vigilance, méthodes, ouvertures.

![Tera](./img/tera3.jpg)

## Le besoin de financement

Le projet a sollicité la Coopérative Oasis pour un prêt de 180 500 euros sur 10 ans et la convention a été signée en février 2020. Cette somme soutient le développement d'un quartier rural sur la commune de Trentels : ce quartier rural associera des habitations écologiques, un centre de ressources et de formation à l'écoconstruction, un espace d'accueil en habitats légers, une zone de production agricole, et d'autres services répondant aux besoins de la commune et de ses habitants. Elle permet pour une partie d'acheter rapidement le terrain et pour l'autre partie de financer le centre de formation et les 11 habitats légers.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
