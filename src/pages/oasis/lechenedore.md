---
title: "Le chêne doré"
date: "2020-07-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/lechenedore/"
avancement: ""
montant: ""
departement: "Aude"
image: ./img/Chenedore1.jpg
description: "17 adultes et 8 enfants vivent dans un domaine de 16 hectares pour tester et vivre ensemble « l’Éthicratie. »."
---

**17 adultes et 8 enfants vivent aujourd’hui au Chêne Doré, une oasis créée en 2018 dans l’Aude. Sur ce domaine de 16 hectares comprenant 2 000 m2 de bâti, chaque foyer vit dans son habitat indépendant mais tous sont là pour tester et vivre ensemble « l’Éthicratie. » Une éthique de l’accomplissement de l’individu, du collectif et de la nature.**

![Le Chêne doré](./img/Chenedore1.jpg)

## « On est tellement heureux d’être ensemble »
« _J’ai l’impression de vivre au paradis, quel pied ! raconte Pierre. À deux reprises, j’ai été à l’origine de grosses tensions. J’ai eu peur, je me suis dit « Pierre, t’es bête, tu vas tout détruire » mais la réaction du groupe a été fabuleuse, et on est toujours là…_ »

En plus d’une maison en colocation, chacun des dix foyers habite dans une maison indépendante avec salle-de-bain, cuisine et terrasse privée. Et tous partagent une salle commune, une cuisine, une buanderie, un atelier, le potager et mutualisent la garde des enfants quand c’est nécessaire.

Tous les samedi matin, c’est la « Moussada » : un temps de travail collectif suivi d’un repas partagé. Il y a également un pickup et une voiture électrique collectifs, de la production d’énergie solaire, des chambres d’ami et un lieu de camping. Beaucoup de choses restent en effet à faire encore au niveau écologique : réduire les déchets, récupérer l’eau de pluie, installer des panneaux solaires…

![Le Chêne doré](./img/Chenedore2.jpg)

Le [Chêne Doré](https://www.facebook.com/groups/271930356697895) est avant tout un lieu d’habitat mais des séminaires d’auto construction, de permaculture, de développement personnel et spirituel et bien sûr d’Éthicratie seront bientôt proposés sur place.

## Un collectif de personnes qui ne se connaissaient pas avant
En 2005, quand Pierre Piront, producteur de métier, commence à s’intéresser aux questions de gouvernance, il ne sait pas encore que ça le mènera à monter un écolieu habité par 30 personnes. Il commence par se former à la sociocratie, à l’holacratie, puis à la communication non-violente, à la philosophie de Ken Wilber et travaille avec Marshall Rosenberg et Thomas d’Ansembourg dont il devient le producteur.  Il décide finalement de mettre en pratique tout le savoir qu’il a accumulé en créant une oasis.

“ _Après une première tentative inaboutie en 2010, ma femme Patricia et moi avons finalement acheté le domaine du Chêne Doré en nous disant que le collectif viendrait après_” raconte Pierre. Ils ont monté une SCI qui a financé l’ensemble du domaine ainsi que les travaux, le tout pour une valeur d’un peu plus d’un million d’euros, sans emprunt. Celle-ci, dont les enfants de Pierre sont aujourd’hui actionnaires, a fait un bail long terme à une association pour un montant de 2 000€ de loyer par mois.

L’association dont tous les habitants sont membres à parts égales perçoit l’ensemble des loyers (plus ou moins 5 000€ /mois) dont les bénéfices servent à financer l’entretien, les charges et de nouveaux projets. C’est grâce à ce modèle que l’autonomie financière a été atteinte. Les habitants sont tout à la fois locataire et bailleur. Le montant des loyers est calculé par consentement mutuel en tenant compte de la superficie des logements.

![Le Chêne doré](./img/Chenedore3.jpg)

Pierre s’est d’abord concentré sur la rénovation des logements. Et comme il l’avait pressenti, les gens sont arrivés naturellement, petit à petit. « _Serge est la première personne à s’être installée sur place, se souvient Pierre. Je l’ai rencontré dans un bistrot à Limoux. Dès que je l’ai vu, mon cœur a vibré. « Comment on fait pour rejoindre le collectif ? » a-t-il demandé. J’ai répondu : « C’est fait ». C’était un russe de 81 ans. On a vécu trois mois merveilleux et il est tombé amoureux… alors il est parti s’installer avec sa dulcinée_ ».

Le processus d’inclusion est simple : après la première rencontre, si le courant passe, le collectif propose à l’arrivant de passer une semaine sur place puis, si c’est positif, 6 mois. Au bout des 6 mois, la décision est prise d’inclure définitivement le nouveau ou pas. « _On a traversé un gros conflit avec deux personnes. Violences, procédure judiciaire et exclusion… On a beaucoup appris, notamment de ne plus privilégier les compétences des personnes par rapport à la qualité de la relation._ »

![Le Chêne doré](./img/Chenedore4.jpg)

Quentin est arrivé à 24 ans avec son enfant d’un an et demi et sa compagne. Il s’est pris de passion pour le jardinage et s’occupe aujourd’hui à du potager avec Thierry et Piêro pour construire l’autonomie alimentaire du collectif.

« On est une famille de quatre personnes, raconte Solène. On est arrivés dans l’Aude en janvier 2021. Mon conjoint a trouvé une formation en écoconstruction par ici. On cherchait un collectif parce qu’on vient de Mayotte où les gens sont très en lien les uns avec les autres. Je ne me voyais pas louer un appartement en centre-ville, surtout avec les enfants. C’est à ce moment-là qu’on a vu l’annonce sur Le Bon Coin… »

## L’Éthicratie comme manuel de conduite
La raison d’être du Chêne Doré est de « Vivre la joie à travers la Démocratie Éthique dans un lieu paradisiaque autonome financièrement, alimentairement et énergétiquement. » « C’est plus facile à dire qu’à faire, explique Pierre. C’est une volonté permanente et une discipline quotidienne. On a le droit de s’engueuler bien sûr ! À condition de vouloir continuer à s’aimer. »

![Le Chêne doré](./img/Chenedore5.jpg)

Le collectif crée des règles uniquement quand c’est nécessaire. Il y en a quelques-uns aujourd’hui : nettoyer la maison commune, entretenir le domaine, remettre les outils à leur place après utilisation… Mais tout s’organise autour des principes de l’Éthicratie. Proche de la gouvernance partagée, cette philosophie a été développée au sein du Chêne Dorée et s’organise en piliers. Parmi eux, on retrouve par exemple la notion de distance sacrée. Le collectif n’a ainsi pas à faire d’intrusion dans la vie privée de l’individu, à quelque niveau que ce soit : spiritualité, croyance, alimentation….

Au Chêne Doré, véganes, végétariens, omnivores et crudivores mangent à la même table ! L’Éthicratie prône également une « communication vivante » alliant bienveillance et sincérité, et un leadership au sein du groupe. « _Il y a des leaders dans chaque domaine (potager, gouvernance, bâti…) mais le vrai patron, c’est la raison d’être_ » conclut Pierre.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Placez votre épargne dans la Coopérative pour soutenir les oasis    </a>
</div>
</div>
