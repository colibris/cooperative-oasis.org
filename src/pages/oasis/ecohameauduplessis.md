---
title: "Ecohameau du Plessis"
date: "2020-01-22T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/ecohameauduplessis/"
avancement: "financé"
montant: "190 000 euros"
departement: "Eure-et-Loir"
image: ./img/EcohameauPlessis1.jpg
website: https://ecohameauduplessis.fr/
highlighted: true
description: "L'écohameau du Plessis est un écohameau conçu pour accueillir 28 familles. Situé en Eure-et-Loir à 200 m du Centre Amma - Ferme du Plessis, il permet un mode de vie très écologique."
---

## L'histoire du projet
A l’origine du projet, on trouve le Centre Amma - Ferme du Plessis. Depuis 2002, cette grande ferme fortifiée du Moyen Âge est devenu un centre spirituel, fondé et inspiré par Amma, une grande figure spirituelle et humanitaire indienne. De nombreuses personnes sont venues vivre au fil des années dans ses environs pour participer au fonctionnement du lieu et aux activités proposées.

Plusieurs de ces « voisins » ont partagé leur aspiration à un mode de vie en lien avec leurs valeurs (écologie, partage d’équipements, économie locale, solidarité…). Un groupe, initialement d’une petite dizaine de personnes, a depuis 2014 réfléchi à la création d’un écohameau sur une parcelle qui se situe à côté de la Ferme du Plessis.

Depuis 2017, l'écohameau du Plessis sort progressivement de terre. il se veut comme un soutien de leur intention de partage, de sobriété, de simplicité, d’autonomie, et de respect de la Nature et des cycles de la Vie. Les habitants cherchent à cultiver des valeurs de bienveillance et de responsabilité, afin de prendre soin du lieu, de la relation humaine, et de le rendre vivant. Ils visent un équilibre entre l’intimité et la souveraineté individuelle d’une part, et une vie collective qui facilite la réussite des intentions du projet. L’écohameau s’intègre dans un ensemble plus large, qui comprend le Centre Amma - Ferme du Plessis mais aussi un projet maraîcher "les Jardins du Plessis" et une maison séniors «les aînés du Plessis» qui sera construite prochainement.

![VueEcohameau](./img/EcohameauPlessis2.jpg)

## Un collectif de 28 familles structuré en ASL
Le projet prévoit d'accueillir un total de 28 familles. 26 foyers étaient déjà engagés mi 2020. Les profils des participants sont très divers, ce qui traduit le souhait d’un projet de mixité sociale et générationnelle. 1/3 des propriétaires ont entre 20 et 40 ans, 1/3 entre 40 et 60 ans et 1/3 aura plus de 60 ans. Les plénières régulières et l’engagement de certains membres dans des projets associatifs communs, notamment à la Ferme du Plessis, ont créé un collectif solidaire. La force du collectif tient à ce cheminement et l’expérience concrète d’avoir été de longue date impliqué dans la gestion et les projets écologiques de la Ferme du Plessis.

Le projet est porté par une association syndicale libre ou ASL, qui sert notamment à gérer les biens communs, notamment plusieurs bâtiments communs. Ce fonctionnement est plus simple que celui lié à des propriétés collectives (SCI, coopératives d’habitants…) et plus souple qu’une copropriété classique. Chaque foyer possède une petite parcelle privée, soumise à un règlement de construction qui contraint les choix individuels. Ce choix permet donc d’assurer une haute qualité écologique, tout en laissant chacun responsable des travaux sur sa maison. Cela facilite la diversité des projets (autoconstruction ou non, modes constructifs différents…).

![Inauguration](./img/EcohameauPlessis3.jpg)

## La dimension écologique
Les habitants de l’écohameau sont très attachés à la dimension écologique de leur projet et à une recherche d’autonomie, avant tout alimentaire, mais aussi énergétique et en eau.

Le règlement de construction assure que ce sont des maisons en matériaux biosourcés, quasi passives, chauffées sans énergie fossile, avec toilettes sèches, eau chaude solaire ou chauffe-eau thermo-dynamique… Chaque maison trouve ses propres solutions pour suivre ces contraintes et le projet constitue ainsi un véritable laboratoire de l’éco-construction.

Le collectif a fait le choix d’un assainissement écologique en installant 3 phytoépurations. Ainsi les eaux grises sont recyclées et réintègrent le cycle naturel de l’eau. Cela participe au design global de la question hydrique, avec des noues, mares, bassins… car le climat local est humide en hiver mais très sec l’été.

Le plan général des 4,3 hectares a été réalisé grâce à un design en permaculture, pour faciliter l’entretien. Plus de 100 arbres ont déjà été plantés, dont une majorité de fruitiers. Une forêt nourricière a été implantée. Ce design permet d’aller vers la production progressive d’une partie de la nourriture des habitants, avec des potagers collectifs au coeur des habitations.

Enfin, la mutualisation grâce à des bâtiments communs permet de réduire l’empreinte écologique des habitants. Ainsi des buanderies collectives ou des chambres d’amis dans la grande maison commune permettent de construire des maisons moins grandes, sans machine à laver.

## La maison commune financée par la Coopérative Oasis
En décembre 2020, la Coopérative Oasis a prêté 190 000 euros sur 10 à l'ASL Ecohameau du Plessis pour financer la grande maison commune qui se construira en 2021 au centre de l'écohameau. Elle comportera une grande salle d'activités, 5 chambres d'amis avec salle de bains, une des 3 buanderies collectives et un espace de coworking.
L'Ecohameau du Plessis avait sollicité plusieurs banques mais le statut d'ASL ne rend pas facile l'accès à un prêt bancaire. La Coopérative Oasis permet ainsi de compléter les apports des habitants et boucle le plan de financement de tous les espaces communs de ce grand écohameau.

Découvrez le podcast “la Voix des oasis” sur l'Ecohameau du Plessis :

<iframe src="https://www.podcastics.com/player/extended/978/34086/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
