---
title: "Le Village de Pourgues"
date: "2020-09-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/villagedepourgues/"
avancement: ""
montant: ""
departement: "Ariège"
website: http://www.villagedepourgues.coop/
image: ./img/Pourgues0.jpg
description: "Un vingtaine d’adultes et d’enfants habitent 50 ha en Ariège et y célèbrent la nature, la démocratie, la liberté et la joie de vivre."
---

**Depuis 2017, en Ariège, le collectif du [Village de Pourgues](http://www.villagedepourgues.coop/) fait le pari de la liberté. Cet écolieu qui s'étend sur près de 50ha est habité par une vingtaine d'adultes et d'enfants. On y célèbre la nature, la démocratie et la joie de vivre.**

![Village de Pourgues](./img/Pourgues0.jpg)

## De l’école parisienne au village ariégeois
Un an après avoir créé, en 2015, l'École Dynamique à Paris, deux des six membres fondateurs éprouvent l’envie forte de passer de l’échelle de l’école à celle du village. Ils décident donc de monter un lieu de vie collectif où s’appliquera la philosophie et la gouvernance de leur école parisienne.
En quelques mois, 25 personnes dont 8 enfants se réunissent autour du projet d’emménager et fabriquer ensemble une petite utopie. Le premier lieu visité est Pourgues, et c’est le coup de foudre immédiat. Le groupe n’en visitera pas d’autres. C’est en Ariège, sur un terrain de 50 hectares, avec 1000m² de bâti et 5000m² à construire, que le collectif des six fondateurs rejoints par une dizaine d’autres personnes s’installe donc dès 2017 en coopérative d’habitants.

![Village de Pourgues](./img/Pourgues1.jpg)

Pourgues, c’est une grande bâtisse du XIXème siècle et deux habitats légers où résident les habitants. C’est aussi six tentes safari, et, suite à l’acceptation d’un permis d’aménager début 2020, bientôt quatorze autres habitats légers pour accroître la capacité d’accueil de visiteurs.

## Un collectif stable et épris de liberté
Il y avait vingt-cinq personnes au commencement, en 2017 : ce recensement est le même trois années plus tard, en 2020, après cinq départs et cinq arrivées causées par les élans, les amours, les évolutions des uns et des autres....

![Village de Pourgues](./img/Pourgues2.jpg)

Au Village de Pourgues, chaque adulte et chaque enfant est considéré comme un individu à la fois libre et responsable de ses actes. Mais toujours dans le cadre du règlement intérieur, lui-même sans cesse redéfini par le collectif. Ainsi, chacun peut faire ce qui l’enthousiasme ; s’occuper du jardin en permaculture, monter un atelier, étudier l’histoire, organiser l’accueil des visiteurs… « _C’est une véritable exploration de la liberté qui invite l’individu à la résilience :  il avance de manière plus libre, efficace et apprend à se connaître_ » résume Jérôme, ancien habitant du village.

## Grandir au fil des expérimentations
Chaque année a vu la mise en place de fondations successives. En 2017 et 2018 : vivre- et faire-ensemble ! Ces deux premières années sont marquées par la mise en place d’un fonctionnement autogéré avec un Conseil de Village, un Comité d’Enquête et d’Arbitrage, des Cercles Restauratifs, la décision par sollicitation d’avis, des réunions de Gestions par Tensions opérationnelles… bref, une panoplie d’outils pour vivre et faire ensemble dans l’horizontalité.

![Village de Pourgues](./img/Pourgues3.jpg)

En 2019, l’énergie est surtout mise au service de l’autonomie économique. Après quelques semaines d’accueil expérimentées en 2018, le collectif se lance dans l’organisation de séjours immersifs et de formations, qui occupent intensément la communauté sur la belle saison, et lui permet d’atteindre 115 000€ de chiffre d’affaires, couvrant l’ensemble des charges. Après le facteur humain, c’est le deuxième principal défi d’une communauté de vie et de travail purement basée sur le volontariat et la libre contribution de chacun.
En 2020 sonne l’heure de la résilience. En plein confinement, cette année est celle d’un bond significatif en matière de prise d’autonomie et de résilience locale, avec l’auto-construction d’habitats écologiques, la multiplication par quatre du volume de l’activité de maraîchage et de transformation, et la diversification de l’activité économique par le lancement de formations en ligne.

<iframe width="560" height="315" src="https://www.youtube.com/embed/W1y7jvKnkJ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dans ce podcast de “La Voix des Oasis”, Alexandre Sattler donne la parole à Ramin, l’un des fondateurs du Village de Pourgues. Ils y parlent de la création du collectif, du volet éducatif, des prises de décision dans le village, des formations proposées qui sont aujourd’hui l’un des piliers du modèle économique de cet éco-hameau.

<iframe src="https://www.podcastics.com/player/extended/978/34095/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
