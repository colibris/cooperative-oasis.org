---
title: "Le Moulin bleu"
date: "2020-03-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/moulinbleu/"
avancement: "financé"
montant: "195 000 euros"
departement: "Loir-et-Cher"
image: ./img/Moulinbleu1.jpg
website: https://lemoulinbleu.org
highlighted: true
description: "Le Moulin Bleu est une communauté d'une quinzaine de jeunes en quête d’un quotidien simple et ouvert à tous. Après une expérience de collocation en région parisienne, ils se sont installés dans un ancien moulin dans le Loir-et-Cher et y développent plein d'activités."
---

## Un projet politique
Le Moulin Bleu est une communauté d’amis, amoureux de la vie, défenseurs d’une société juste et durable et en quête d’un quotidien simple et ouvert à tous. Installée dans un ancien moulin de Saint-Jean-Froidementel depuis le 11 mars 2020, la quinzaine d’amis construit pas à pas un lieu de vie, d’expérimentation, de transmission, de développement d’activités, et de lutte.

"Un autre modèle est nécessaire ; ce lieu existe pour montrer qu’il est possible et enviable."
Le Moulin Bleu souhaite mettre en pratique un mode de vie résilient et solidaire, qui participe au dynamisme et à l’autonomie du territoire sur lequel il s’implante. Pour ses les habitants, le projet est donc politique : il ne s’arrête pas aux portes du moulin.

![Le collectif](./img/Moulinbleu1.jpg)

## Une quinzaine de jeunes habitants
Cette communauté débute en 2015 avec la création de la Maison Bleue, un laboratoire de vie écologique et communautaire à Bourg-la-Reine, dans lequel une grande partie du groupe s’est rencontré et à vécu.  gés de 25 à 35 ans, les quinzaine de colocataires a des professions variées : ingénieur, chargé de projets, indépendant, consultant, professeur, masseur, étudiant, responsable associatif...

Le groupe se consolide à travers la confiance et la bienveillance qui s’installent au fur et à mesure de l’expérience de vie commune. Toujours à l’écoute des besoins et des envies des personnes, ils ont progressivement établi un mode de gouvernance basé sur les principes de la démocratie participative qui permet à tous de s’exprimer librement.

![Vue ancienne du Moulin](./img/Moulinbleu2.jpg)

## Un ancien moulin pour un nouveau monde
Le lieu est un ancien moulin et ses dépendances, 800m2 habitable sur un terrain de 13ha au bord du Loir (Saint-Jean Froidmentel, Loir-et-Cher). Cette immense bâtisse de 1500 m² a passé les années et est aujourd’hui prête à vivre une nouvelle aventure.

Les futures activités pour ce Moulin sont multiples : production agricole, production d'électricité, espace bien-être, brasserie locale, atelier de sérigraphie participative, restaurant bio... Mais le plus important pour le collectif est de contribuer à impulser une dynamique locale de transition. Alors avant de se mettre au service, ils observent le territoire et ses besoins.

<iframe src="https://www.podcastics.com/player/extended/978/34046/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

![Télétravail au Moulin bleu](./img/Moulinbleu3.jpg)

## Le besoin de financement
Le collectif du Moulin bleu a fait appel à la Coopérative Oasis pour un prêt de 200 000 euros sur 7 ans, dont 5 000 euros en parts sociales et 195 000 euros en compte courant d'associés, afin de réaliser l'achat du Moulin et de financer des premiers travaux sur place. En parallèle, la Coopérative Oasis accompagne le projet sur sa structuration juridique, financière et humaine.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
