---
title: "L'écovillage Sainte Camelle"
date: "2020-08-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/ecovillagesaintecamelle/"
avancement: ""
montant: ""
departement: "Ariège"
website: https://ecovillagestecamelle.fr/
image: ./img/SainteCamelle1.jpg
description: "Créé en 2011 sur une parcelle de 10ha, ce lieu de vie et de travail intergénérationnel mêle décroissance matérielle à croissance intérieure..."
---

**Pour la vingtaine d’habitants de l’écovillage de Sainte-Camelle, vivre en collectif est avant tout un engagement au jour le jour. _« Nous avons tous choisi cette forme de vie parce que nous croyons que le rassemblement de différentes identités, expériences et savoir-faire est une réponse viable à certains problèmes de notre société. »_ Créé en 2011 en Ariège sur une parcelle de 10ha, ce lieu de vie et de travail intergénérationnel n’en finit pas, il est vrai, de changer le monde au quotidien…**

![Sainte Camelle](./img/SainteCamelle3.jpg)

## Un collectif pour être pleinement soi-même
À Sainte-Camelle, on préfère parler de collectif plutôt que de communauté. En effet, chacun a son propre appartement autonome, et tous ont en commun des valeurs d’écologie et de solidarité, des espaces (cuisine, jardin, potager, salles communes, buanderie, ateliers, espace enfants…) et des occupations : 100% de l’activité de chaque habitant est pratiquée au sein de l’écovillage.
_« Chacun peut exprimer totalement son potentiel, être pleinement lui-même mais toujours au service de notre mission commune. Nous cultivons la solidarité, l’entraide, le respect de soi, de l’autre et de l’environnement, la joie du faire-ensemble et la joie d’être… »_
L’engagement de ce collectif intergénérationnel en matière d’écologie est globale : il s’applique aussi bien à l’extérieur, sur la nature proche, qu’à l’intérieur, en prenant soin de chacun et du collectif.

## Sobriété et autosubsistance
Les bâtiments, à l’origine en pierre et en terre crue, ont été rénovés avec des matériaux écologiques et locaux – brique, chanvre, laine de mouton, paille, carton, terre… Des toilettes sèches et des douches solaires, des bassins de récupération d’eau de pluie et une phyto-épuration allègent les consommations de ressources et d’énergie. En 2020, une partie des bâtiments a été équipée d’un nouveau chauffage fonctionnant avec un mix panneaux solaires et chaudière bois.

Seuls des produits bio sont consommés dans l’écovillage, et une nourriture végétarienne compose l’essentiel de la nourriture des habitants. Un verger de variétés rustiques et une zone de maraichage en permaculture on également été mis en culture pour l’autosubsistance du collectif.

Quant aux prairies et forêts, elles n’ont jamais subi de traitements chimiques et sont laissées autant que possible dans leur état naturel.

![Sainte Camelle](./img/SainteCamelle4.jpg)

## Décroissance matérielle et croissance intérieure
_« A la différence des enjeux environnementaux qui nous guident vers la décroissance, l’écologie intérieure et relationnelle nous appelle à la croissance ! »_ Cet aspect fait l’objet d’un grand soin chez les habitants de Sainte-Camelle, car chacun a conscience que les bonnes relations sont essentielles au bon fonctionnement de l’écovillage.

_« Nous mettons beaucoup de coeur à préserver un relationnel harmonieux. Bien sûr, cela demande un réel travail sur soi, ses forces, ses blessures. Mais pour transformer le monde, il faut commencer par nous transformer nous-mêmes, alors... »_ Cette philosophie ne s’arrête pas aux frontières de l’écovillage : créer des relations de bon voisinage avec les habitants proches est une règle d’or !

![Sainte Camelle](./img/SainteCamelle5.jpg)

## Résilience économique
Afin de ne pas être un écovillage dortoir, le collectif expérimente depuis des années la création d’une économie viable et locale. Chacun a choisi de travailler sur le lieu, qui est devenu une pépinière de projets dont certains sont collectifs et d’autres personnels - stages et animations, cabanes dans les arbres, cabinet de Shiatsu…
L’écovillage témoigne de son expérience grâce à des visites de groupes, des stages, des temps en immersion… _«Nous avons fait le choix d’être heureux et de partager notre bonheur !»_

![Sainte Camelle](./img/SainteCamelle2.jpg)

Ce podcast donne la parole à Alain, Dane et Agathe. Ensemble, ils abordent le vivre ensemble en parlant d‘écologie intérieure plutôt que de développement personnel. Ils évoquent également les incontournables pour réussir la vie collective !

<iframe src="https://www.podcastics.com/player/extended/978/34091/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
