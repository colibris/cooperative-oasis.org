---
title: "La Nourrice"
date: "2019-12-22T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/nourrice/"
avancement: ""
montant: ""
departement: "Bouches-du-Rhône"
image: ./img/nourricecollectif.jpg
website: https://www.facebook.com/oasisdelanourrice/
description: "Située en Provence sur un domaine de 2ha, l’oasis de la Nourrice a été fondée par Christel en 2016. Trois habitants sont entourés d'une équipe dynamique pour cultiver fruits et légumes distribués en vente directe à la ferme..."
---

Située **en Provence sur un domaine de 2ha**, l’oasis de la Nourrice a été **fondée par Christel en 2016**. Cette ancienne ferme en polyculture abrite deux maisons d'habitation et des dépendances agricoles. **Trois habitants permanents** sont entourés d'une équipe dynamique pour cultiver **fruits et légumes distribués en vente directe** à la ferme. La vie associative et les événements et ateliers en tout genre rythment aussi le quotidien de ce lieu unique dans cette partie de la France.

![Nourrice 1](./img/nourricecollectif.jpg)

A l'origine de ce projet, la conviction profonde que changer de vie et choisir ce qui a du sens pour soi est possible. Christel a eu la chance d'accéder à cette terre provençale de 2ha en décembre 2016. En décembre 2018, Nathalie et Romain la rejoignent dans l'aventure et deviennent ses cocréateurs avec l'équipe de bénévoles de l'association Oasis de la Nourrice.

 ![Nourrice Face](./img/NourriceFace.jpg)

L'ensemble des outils et du matériels ont été mutualisés : l'espace buanderie, la production d'eau chaude avec l'installation d'un chauffe-eau thermodynamique commun. Les résidents oeuvrent ensemble dans les jardins et vergers vivriers, et développent ensemble les volets professionnels agricole et agrotouristique du lieu.

 ![Nourrice Christel](./img/NourriceChristel.jpg)

La Nourrice développe une production de fruits et légumes, oeufs et miel biologiques pour les fuvelains (habitants de Fuveau), en vente directe et cueillette à la ferme. Cela permet aux habitants du coin de découvrir la permaculture et de se former à ses méthodes respectueuses de l'environnement.

![Nourrice Champs](./img/Nourrice-Choux.jpg)

"Des ateliers de sensibilisation citoyenne sur les grands enjeux de la transition écologique sont proposés régulièrement, de même que des stages de développement personnel, et toute sorte d'ateliers permaculturels et festifs.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ALajvuqQES8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

En 2021, la Nourrice s'apprête à vous offrir le gîte (chambres ou camping) ainsi que le couvert à la table de l'auberge où les produits de la ferme et des amis producteurs locaux seront transformés pour le délice de toutes et tous !

![Nourrice Soiree](./img/NourriceSoiree.jpg)

Assis sur une botte de paille, Nathalie, Romain et Christel racontent leur aventure...

<iframe src="https://www.podcastics.com/player/extended/978/34042/" style="border-radius: 10px;" width="100%" height="200" frameborder="0"><em><strong></iframe>
