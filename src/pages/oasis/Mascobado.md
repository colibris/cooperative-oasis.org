---
title: "Mascobado"
date: "2021-02-04T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/Mascobado/"
avancement: ""
montant: ""
departement: "Hérault"
website: https:/www.mascobado.org/
image: ./img/MasCobado2.jpg
highlighted: true
description: "Mascobado est un habitat participatif de 23 familles habité depuis 2016 au coeur de Montpellier. Bâtiments bioclimatiques, logements sociaux et convivialité sont le quotidien de ce collectif précurseur..."
---

**[Mascobado](http://www.mascobado.org/) est un habitat participatif de 23 familles habité depuis 2016 au coeur de Montpellier. Bâtiments bioclimatiques, logements sociaux et convivialité sont le quotidien de ce collectif précurseur.**

![Mascobado](./img/Mas Cobado5.jpg)

## Les habitants aux manettes, du début à la fin
Frédéric Jozon a grandi dans les Cévennes. Près de chez lui, tout le monde se connaissait et s’entraidait. Cet esprit de village, c’est ce dont il rêvait pour lui et sa famille. Et c’est ce qu’il a finalement réussi à réaliser avec ses voisins au sein de MasCobado, un habitat participatif à Montpellier, mêlant personnes de tout âge et de toutes origines sociales…

À l’origine, un groupe de futurs habitants s’est rassemblé autour d’un terrain à bâtir pour définir son projet d’habitat : plans, financement, construction et fonctionnement futur. Le résultat relève d’une programmation toute professionnelle… pourtant menée par des « amateurs ». Vingt-trois logements personnalisés et déclinés selon une grande diversité sont aujourd’hui répartis au sein de 2 bâtiments bioclimatiques neufs.
On ne s’étonne donc pas que MASCOBADO soit l’acronyme de « Maison coopérative bâtie avec douceur ». C’est aussi le nom d’un sucre de canne non raffiné qui peut évoquer aussi celui de la rue où est implantée la résidence (rue de la réglisse).

![Mascobado](./img/Mas Cobador1.jpg)

## Fiche technique
**Les personnes** : 23 foyers composés de 1 à 5 personnes

**Localisation:** Quartier des Grisettes – Montpellier

**Maîtrise d’ouvrage:** Association Mascobado + bailleurs social Promologis
23 logements répartis sur 2 bâtiments . Il n’y a pas de distinction de bâtiment entre les logements en location et les logements en accession, ils sont répartis dans les deux bâtiments.

**Les espaces communs:**
- 27 places de parking souterrain
- 3 chambres d’amis
- 4 espaces de buanderie
- 3 locaux vélos/poussettes
-1 local ordures ménagères
- Les circulations et terrasses communes (au Nord, les terrasses Sud étant privatives)
- Le jardin (espace collectif dont une bande de 5 mètres le long du bâtiment est à usage privatif) et un potager partagé à l’angle Nord de la parcelle

**Montant des travaux:** 3 millions d’euros HT

**Durée des travaux:** De janvier 2015 à juin 2016, soit 18 mois.


![Mascobado](./img/Mas Cobado6.jpg)

##Un habitat moins cher et plus agréable
Un rapport de l’Ademe datant de novembre 2018 tente d’évaluer, entre autre, l’impact économique de Mascobado sur la vie de ses habitants, après 2 années d’existence.  _« Nous observons que les habitants ont réalisé d’importantes économies. La première lors de l’acquisition du logement : environ 9% d’économie par rapport à un achat en accession libre. Deuxièmement, les charges de copropriété et d’énergies sont plus faibles que dans les opérations classiques. Juste un chiffre : 89€, c’est ce que paye un ménage dans un T3 de 60m2 à Mascobado pour l’ensemble de ses charges mensuelles (électricité, eau, chauffage, eau chaude et charges de copropriété). Ce chiffre résulte des choix de conception bioclimatique, d’un syndic bénévole, d’un entretien de la résidence par les habitants, mais surtout d’une démarche proactive des habitants sur la maîtrise de leurs consommations et leur implication dans la copropriété. Cerise sur le gâteau, la mutualisation d’objets et le prêt d’automobiles entre voisins, génère encore de nouvelles économies sur le budget des habitants. Certains ont ressenti une amélioration de leur niveau de vie. »_

![Mascobado](./img/Mas Cobado2.jpg)

##La maison au service du vivant
Ce même rapport analyse les effets de Mascobado sur l’environnement. La conception de la résidence respecte le « bioclimatisme » : orientation Sud, inertie, protections solaires et logements traversants. Les habitants ont affiché dès le départ une volonté forte quant au choix de matériaux naturels (briques de terres cuites, bois, etc.).  L’opération s’est bâtie sur une approche constructive simple : pas de climatiseurs, pas d’ascenseurs et pas de VMC. Cette approche « low-tech » a aujourd’hui un impact positif sur les économies de ressources et de carbone.  Enfin, les consommations d’eau, d’électricité et de chauffage sont inférieures aux moyennes habituelles, signe de comportements vertueux.
Cette économie de ressources se poursuit au quotidien, à travers la mutualisation d’objets de consommation courante. Les habitants disent avoir modifié leurs habitudes, en favorisant désormais les achats locaux. Les espaces verts sont particulièrement valorisés et gérés dans un souci d’accueil de la biodiversité. Les habitants en tirent aussi d’autres avantages que l’agrément visuel : potager, compost, espaces de jeux pour les enfants, lutte contre les îlots de chaleur urbain…

<center>

##[Placez votre dans la Coopérative Oasis et soutenez la création d’Oasis !](https://cooperative-oasis.org/investir/)

</center>

Dans ce podcast, Frédéric revient sur l’historique du projet et décortique avec nous le montage juridique et le montage financier nécessaire à la création et au fonctionnement de Mascobado, un habitat participatif exemplaire.

<iframe src="https://www.podcastics.com/player/extended/978/34064/" frameborder="0" height="200" width="100%" style="border-radius: 10px; »></iframe>




<br>


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i>
    </a>
</div>
</div>
