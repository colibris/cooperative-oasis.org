---
title: "Le Village du Bel air"
date: "2020-12-23T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/villagedubelair/"
avancement: "financé"
montant: "200 000 euros"
departement: "Morbihan"
image: ./img/Villagedubelair1.jpg
website: https://villagedubelair.org/
highlighted: true
description: "Le Village du Bel Air est un lieu de vie collectif, créé en 2018, en plein cœur de la Bretagne, tourné autour de l’écologie, la démocratie et l’épanouissement personnel."
---

**Situé dans le Morbihan, à Priziac, le Village du Bel-Air est un lieu de vie ouvert sur le monde fondé par six amis en 2018. Remettre la nature au cœur de leur quotidien, telle est l'ambition de ce collectif aux valeurs fortes : souveraineté alimentaire et énergétique, respect de la nature, interdépendance, économie symbiotique et bien-être !**

![Village du Bel Air](./img/Villagedubelair1.jpg)

## Un collectif qui expérimente l’épanouissement et l’harmonie
Ils sont juristes, employés d'une ressourcerie, surveillants au collège, étudiants, coachs en développement personnel ou encore médecins... En 2018, ils ont décidé de vivre autrement en s’installant au bord du lac de Priziac, en Centre-Bretagne. À six, ils ont donc créé une Société Civile Immobilière (SCI) et acheté le lieu. Une association s'occupe de toute la vie des habitants sur place et une autre s’occupe des différentes activités qu’ils proposent.

![Village du Bel Air](./img/Villagedubelair2.jpg)

## Une vie de château en pleine nature
Le terrain d'une quinzaine d'hectares est situé sur les ruines d'un château entouré de douves datant du XVIème siècle. Le lac est accessible par un petit chemin dans la forêt, bordé par la mousse et les myrtilles sauvages. Une maison commune est partagée pour les repas et les moments à plusieurs. Des yourtes, des dômes, des caravanes ou des cabanes : chaque habitant ou couple dispose d'un lieu à soi pour se retrouver, s'isoler, se ressourcer. Le but n'est pas de se laisser absorber par le collectif, mais de laisser à chacun la liberté d'être seul s'il le souhaite. Vivre ensemble, mais pas tout le temps.

En mars 2021, la Coopérative Oasis a débuté un accompagnement du lieu : en plus d’un soutien annuel sur les aspects humain, juridique et financier, 200 000€ ont été prêtés au collectif pour financer les premiers travaux de rénovation des deux longères et les premiers habitats individuels sur le site.

![Village du Bel Air](./img/Villagedubelair4.jpg)

## Des valeurs fortes et incarnées
Cet éco-village concentre des valeurs comme le vivre ensemble avec la mutualisation des espaces et des services pour créer un mode de vie basé sur la coopération, le partage et la solidarité. Ce lieu se veut un lieu expérimental avec une gouvernance partagée organique qui respecte autant les besoins du collectif que la souveraineté de chacun. Le collectif met également en commun les ressources financières, pour repenser leur rapport à l’argent et au travail, inspiré des modèles d’économie symbiotique.
Le respect de la nature est bien sûr une valeur phare, qui leur permet d’envisager un lieu de vie abondant et résilient, où la place de l’humain dans la nature peut se trouver plus justement. Cette démarche est permise par la conception des espaces de vie en harmonie avec le paysage, avec des outils de permaculture, en utilisant des ressources locales et en favorisant l’auto-construction autant que possible.

![Village du Bel Air](./img/Villagedubelair3.jpg)

La sobriété heureuse est un guide de conduite, pour récupérer, recycler, emprunter ou réparer avant d’acheter de nouveaux produits. L’autosuffisance est un objectif, comme un cap à suivre afin d’assurer la pérennité du projet. Pour ce faire, le collectif avance sur le chemin de la souveraineté alimentaire (potager, forêt-jardin, etc.) et énergétique (récup’ de panneaux solaires, rénovation de l’habitat, etc.) sans jamais faire de ces valeurs des dogmes enfermant et que le bien-être individuel et collectif reste au coeur du projet.

![Village du Bel Air](./img/Villagedubelair5.jpg)

## Une oasis qui se réinvente
L’interdépendance est un pilier du projet. Ce collectif souhaite faire bénéficier leurs expériences au reste de la société en proposant des formations, des ateliers, des conférences, des écrits et autres médias sur de nombreuses thématiques. Cette ouverture sur le monde va bien au-delà de l’éco-village et le désir de s’inclure dans la dynamique du territoire les emmène à la création dans le village d’une épicerie associative, de salon de thé, d’espaces de rencontre, et même d’un pôle de santé holistique, écologique, démocratique et social.

## Pour en savoir plus
Dans ce podcast “La Voix des Oasis”, Jesse se prête volontiers au jeu du micro pour partager sa vision du lieu, les projets en cours et à venir. Seront abordés ici le sensible sujet du facteur humain, le montage juridique et financier, mais surtout la vie, sous un angle résolument positif. Selon Jesse, le collectif est un miroir pour nous faire avancer :

<iframe src="https://www.podcastics.com/player/extended/978/34043/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
