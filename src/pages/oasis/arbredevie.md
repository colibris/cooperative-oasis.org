---
title: "L'Arbre de vie"
date: "2020-10-06T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/arbredevie/"
avancement: ""
montant: ""
departement: "Loire Atlantique"
image: ./img/arbredevie1.jpg
website: http://larbredevie44.wixsite.com/larbredevie44/
description: "L'Arbe de vie est un lieu collectif alliant habitat, maraîchage, construction écologique et transformation. Né en 2015 à Maumusson (44), ce lieu essaie de faire autrement dans le respect de l'humain et de l’environnement."
---

## Du jardin pédagogique au lieu de vie collectif éco-construit
C’est en 2015 que naît l’Arbre de Vie à Maumusson, en Loire-Atlantique (44), lorsqu'Aurélien et Alex achètent un ancien corps de ferme et un terrain de près de 600m2 pour près de 45 000€. Ils décident rapidement de louer 8 000m2 à un voisin et voient un second voisin mettre à leur disposition gratuite 1,2ha. L’ambition initiale de ce couple et de leur bande d’amis : créer un jardin pédagogique cultivé en permaculture.

![Maison autonome](./img/arbredevie1.jpg)

Rapidement, le projet évolue et prend de l'ampleur. Les deux initiateurs du projet croisent le chemin de quatre autres personnes partageant les mêmes valeurs. et décident d'habiter ensemble dans la maison principale, auto-rénovée. Des méthodes d’éco-construction sont utilisées, telles que l’utilisation de mélange terre-paille ou la construction en pierre. Pour mener à bien les chantiers, une équipe composée d'un électricien, d'une peintre en bâtiment et de deux éco-constructeurs de formation. Ajouté à ces professionnels, le reste des habitants se forment sur le tas et est assistée par les nombreux bénévoles de passage...

![Maison autonome](./img/arbredevie2.jpg)

## Des légumes à prix libre pour le village
Depuis 2016, le collectif travaille un jardin vivrier sur les terres mises à disposition de l'association : 2500m2 de cultures légumières, environ 110 arbres fruitiers ainsi que beaucoup de fleurs aromatiques dispersés partout dans le jardin. La production permet ainsi de nourrir le collectif au quotidien ainsi qu'une vingtaine de familles par semaine.

La production est vendue en cohérence avec les valeurs du collectif : en tarif libre pour tous les adhérents. Depuis septembre 2017 la vente des légumes permet de générer un salaire ! Le début de  l'avancée vers l'autonomie financière.  Beaucoup d'améliorations sont encore à prévoir pour palier au changement climatique et rendre le jardin de plus en plus résilient et autonome.

![Maison autonome](./img/arbredevie3.jpg)

Une activité de transformation s'est aussi développée au fil du temps. La réalisation d'un séchoir solaire dans l'une des serres permet de faire sécher de belles quantités de plantes, graines, fleurs et autres végétaux pour réaliser tisanes et infusions. Le collectif transforme également les surplus de production légumière en confitures et bocaux en suivant des recettes gourmandes et saisonnières.

## Élevage ovin et savonnerie...
Les 8 premières brebis de Julie sont arrivées le 5 mai 2019. Une fois l'atelier de confection fini, Julie fera la traite du matin à la main, amènera le lait à la fromagerie pour ensuite confectionner des fromages et des yaourts vendus sur place.

![Maison autonome](./img/arbredevie4.jpg)

En parallèle, Jessica développer des savons naturels, réalisés localement et selon la saisonnalité des plantes...

## Un lieu en mouvement : partage et évolutions

L'objectif pour l'association, au travers de toutes ces activités, est de pouvoir ancrer le lieu dans la durée et ainsi permettre la mise en place de nouvelles activités artisanales par l'intégration de nouveaux membres au sein de l'association. Qu'elle soit alimentaire, culturelle, pédagogique, liée au bien être ou à un savoir faire traditionnel, toute activité a potentiellement sa place dans ce projet, pour peu qu'elle en partage les valeurs !

![Maison autonome](./img/arbredevie5.jpg)

L'une des vocations communes reste de partager et d'échanger ces connaissances. Pour se faire, la dizaine d'habitants organise régulièrement des visites pédagogiques et des événements liés à la permaculture...

Dans ce podcast Jessica et Oliver, nous parlent de wwoofing, de bénévolat, de modèle économique, de l’importance de son voisinage et de la joie du vivre ensemble...

<iframe src="https://www.podcastics.com/player/extended/978/34041/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
