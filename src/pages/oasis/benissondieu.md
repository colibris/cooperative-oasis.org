---
title: "La Bénisson-Dieu"
date: "2020-06-01T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/benissondieu/"
avancement: ""
montant: ""
departement: "Loire"
image: ./img/BenissonDieu2.jpg
website: https://www.facebook.com/Eco-hameau-de-La-B%C3%A9nisson-Dieu-910877545749469/
description: "Créé à l’été 2016 avec le soutien du diocèse de Lyon l’éco-hameau de la Bénisson-Dieu accueille à ce jour 18 personnes sur 2 hectares."
---

**Créé à l’été 2016 avec le soutien du diocèse de Lyon et de la fondation Saint-Irénée, [l’éco-hameau de la Bénisson-Dieu](https://www.facebook.com/Eco-hameau-de-La-B%C3%A9nisson-Dieu-910877545749469/) situé près de Roanne (Loire) accueille à ce jour 7 foyers, soit 14 adultes et une trentaine d'enfants. Ces familles participent à la rénovation écologique des bâtiments du diocèse, pour qu’ils servent ensuite de lieu d’accueil, notamment pour des personnes en difficulté.**

Dans le podcast de « La Voix des Oasis », la parole est donnée à Gaultier et Marianne ; ensemble ils parlent d’écologie intégrale, de la foi chrétienne, mais aussi de vivre ensemble, d’éducation, de cette envie d’être en lien et de partager une direction commune.
<iframe src="https://www.podcastics.com/player/extended/978/34044/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

_Extraits issu de l'[article de l'Homme nouveau "Reportage : À La Bénisson-Dieu, la tentative d’un éco-hameau"](https://www.hommenouveau.fr/2547/politique-societe/reportage---a-la-benisson-dieu--brla-tentative-d-un-eco-hameau.htm)_

## Un collectif inspiré par l’encyclique Laudato Si

Dans ce petit village de 400 âmes, fort bien nommé « La ­Bénisson-Dieu », à quelques kilomètres de Roanne, Blandine et François Nollé sont à l’initiative, avec deux autres familles, d’un projet d’éco-hameau chrétien. Leur idée ? Retrouver un mode de vie sain et vivre concrètement l’écologie intégrale, pivot de [l'encyclique "Laudato Si" du Pape François](http://www.vatican.va/content/francesco/fr/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html) et qu'ils comprennent comme :
 - un mode de vie plus équilibré et plus écologique,
 - une vie conviviale et amicale au quotidien,
 - un environnement sain pour les enfants,
 - une foi vécue en pleine cohérence dans le quotidien.

Tout a commencé en 2015 quand Blandine, François, Antoine, Odile, Pierre-Alban et Stéphanie ont rédigé la charte de leur projet pour l’envoyer aux évêques de France et construire leur écolieu en lien étroit avec l’Église. C’est finalement Mgr Barbarin, évêque de Lyon, qui leur a proposé des bâtiments que possédait le diocèse à La Bénisson-Dieu. En août 2016, ce sont trois familles, rejointes aujourd’hui par quatre autres foyers, qui ont posé leurs valises dans le village. Ils ont acheté ou louent les maisons, selon leur situation financière, et vont rénover les bâtiments du diocèse pour en faire des lieux communautaires et d’accueil, notamment une ferme pédagogique pour les jeunes en difficulté. Mais l’accueil fait déjà partie du quotidien à La Bénisson-Dieu puisque cette drôle de communauté reçoit déjà régulièrement des visiteurs, des gens qui veulent voir de plus près cet éco-hameau, par curiosité, pour s’y installer aussi… ou pour trouver l’inspiration avant d’aller fonder un autre hameau ailleurs.

![Bénisson Dieu](./img/BenissonDieu1.jpg)

## Recréer une vie de hameau

Chaque famille est autonome financièrement et a sa propre maison mais elles sont suffisamment proches pour permettre de se rendre à pied chez les uns et les autres. Tous sont convaincus de la nécessité d’un retour à un mode de vie plus rural, moins individualiste, et plus respectueux de la nature. La vaisselle se fait au savon de Marseille, les vêtements achetés d’occasion, la nourriture est si possible locale quand elle ne vient pas du potager. Plus largement, c’est aussi le rapport au travail et au lien social qui est repensé. Ici, le repos dominical est sacré et le travail doit servir l’homme et non l’inverse. Et si la famille est évidemment pensée comme la première des communautés, elle n’exclut pas celle, plus large, du village. Ce qu’explique ainsi François : _"C’est parce que ma famille passe en premier que je suis ici, parce que je suis convaincu, pour le bien de mon couple et de mes enfants, de l’importance de cette vie de hameau, faite d’entraide au quotidien et de vie spirituelle en commun"_.

Les membres de l’éco-hameau se retrouvent donc régulièrement pour le « conseil du village », qui permet de prendre les décisions nécessaires au bon fonctionnement du groupe et de faire le point sur d’éventuels incompréhensions ou conflits. Ils se retrouvent également quotidiennement pour les laudes qu’ils disent dans l’abbatiale du village, ainsi que le dimanche pour les vêpres. C’est dans ces murs qu’ils voudraient faire résonner le chant grégorien qu’ils apprennent auprès du chantre Damien Poisblaud. À n’en pas douter, la vie spirituelle est bien le ciment de l’éco-hameau et ses membres ne cachent pas leur foi, ce qui ne les empêche pas de s’entendre avec les autres habitants du village, bien au contraire !

![Bénisson Dieu](./img/BenissonDieu2.jpg)

## Imiter la nature et non la perfectionner

« Mais de manière plus concrète, tout a commencé quand, lors de sa première grossesse, Blandine a été atteinte d’une pathologie. Avec son mari, ils ont couru de spécialiste en spécialiste mais aucun n’était capable de déterminer la cause du mal. Blandine et François se sont tournés, en désespoir de cause, vers des médecines dites alternatives, grâce auxquelles ils ont découvert la cause de la pathologie. C’est alors un monde qui s’est ouvert à eux, celui d’une conception moins technique du corps et, de fil en aiguille, ils ont découvert des voies alternatives dans bien d’autres domaines que celui de la médecine. _"Le paradigme moderne repose sur l’idée que la nature est mal faite et qu’il faut la perfectionner, la dominer. Nous pensons au contraire que nous ne pouvons rien faire de mieux qu’imiter la nature. L’organisme humain n’est pas une machine défaillante ! Bien sûr, une ferme en permaculture n’existe pas à l’état sauvage. Mais l’intelligence humaine doit permettre à la nature d’exprimer toutes ses potentialités. Notre regard doit être d’abord contemplatif. Et pour revenir à l’exemple de la permaculture, l’homme ne cherche par là qu’à favoriser la biodiversité incroyable dont est capable la nature"_, commente encore François."

![Bénisson Dieu](./img/BenissonDieu3.jpg)


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
