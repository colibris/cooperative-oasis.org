---
title: "La Forge du Vallon"
date: "2020-03-23T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/laforgeduvallon/"
avancement: "financé"
montant: "150 000 euros"
departement: "Charente"
image: ./img/ForgeduVallon1.jpg
website: https://www.laforgeduvallon.fr/
description: "Artistes et soignants ouvrent un magnifique lieu bilingue franco-anglais, plongé en pleine nature, à tous ceux qui le veulent"
---

Depuis l’été 2020 existe au cœur de la Charente une bâtisse entourée de forêt où règnent la nature et la liberté : la Forge du Vallon. Chacun peut y venir, avec les moyens qui sont les siens, pour créer, se ressourcer, être lui-même. Mirabelle et Alexandre, à l’origine de cette oasis de vie bilingue dédiée à la créativité et au bien-être, nous racontent le chemin qui les a menés de la banlieue parisienne à Brigueuil…!

## Un lieu pour créer et soigner
Une SCIC a acheté ce terrain d’1,5ha entouré de 20ha de forêt dont les habitants ont le droit d’usage. Potager, verger, poulailler, jardins entourent une bâtisse principale de 400m2. A terme, 5 foyers d’artistes et de soignants habiteront de petites maisons nomades en bois de 30m2 chacune, construites à un bout du terrain. La moitié d’entre eux au moins travailleront à plein temps sur place. L’accueil à prix libre et la location d’espace constituent aujourd’hui les revenus principaux du groupe, dont les besoins restent simples.
Soucieux de vivre sobrement, les membres actuels du collectif Jodie, Mirabelle, Minh, Pierre-Jean et Alexandre développent petit à petit un réseau local. Des paysans bio des alentours fournissent les légumes en direct, une association antigaspillage livre les invendus de grande surface chaque samedi…
Des travaux de rénovation commenceront bientôt : transformation d’une grange de 250m2 en salle de danse, théâtre, art du cirque, rénovation d’une salle de 150m2 en atelier d’art, une maison de l’audiovisuel et une maison des enfants permettant aux artistes visiteurs de venir accompagnés de leur progéniture.

![Forge du Vallon](./img/ForgeduVallon2.jpg)

## Libre d’être soi-même
_« Ici, je peux être moi-même pour la première fois »_ Il n’est pas rare d’entendre cette phrase quand on passe un peu de temps au coin du feu de la Forge du Vallon. Depuis quelques mois, un collectif de 3 foyers accueille en effet tous ceux qui le souhaitent dans cette oasis enfouie en pleine nature.
_« La liberté d’être soi-même… Ça n’est pas exactement ce que nous avions écrit dans notre raison d’être, mais c’est ce qui existe avec le plus de puissance aujourd’hui »_, explique Alexandre, cofondateur. _« Ce lieu magique s’offre à tous ceux qui ont des passions, des talents et qui veulent les concrétiser, même si c’est pour la première fois. »_ Quand les tiers-lieux des environs proposent du coworking ou des formations aux locaux, la Forge du Vallon leur propose aussi de transmettre eux-mêmes ce qu’ils portent en eux. On a ainsi pu voir un peintre en bâtiment renouer avec une vieille passion pour donner des cours de théâtre d’improvisation…

![Forge du Vallon](./img/ForgeduVallon3.jpg)

## Un nécessaire et radical changement de vie

Être soi-même. C’est aussi pour cela qu’Alexandre et Mirabelle ont cheminé depuis 10 ans, de difficultés en obstacles. Et ces voisins de palier en banlieue parisienne n’ont jamais renoncé à leur rêve. _« On était 16 au début du projet, raconte Mirabelle, cofondatrice. Au fur et à mesure, tout le monde est parti… jusqu’à ce qu’il ne reste que nous. On s’est regardés « Tu envisages la vie autrement ? - Non. - Moi non plus » Alors on a continué. »_

Née en Charente, Mirabelle a grandi aux Etat-Unis jusqu’à 20 ans, âge auquel elle est revenue en France pour y devenir comédienne et scénariste en vue. _« Dans mon métier comme dans ma vie personnelle, je souffrais d’une dissonance grandissante entre mon quotidien et mes aspirations intérieures. L’argent, le renoncement à mes valeurs et la consommation étaient mon quotidien quand je rêvais de sobriété, de joie, de créativité libre. Malgré les deuils que “changer de vie” implique, je ne regrette rien, au contraire. Je me sens tellement en sécurité ici… »_
Alexandre a quant à lui été routier pendant 33 ans. Marié et père de deux enfants, il a peu à peu commencé à ne plus supporter d’être le maillon d’une économie dont il voyait chaque jour les effets destructeurs et les absurdités. _« J’ai pris conscience de plein de choses quand j’ai commencé à pratiquer la danse libre, je me suis reconnecté à mes rêves d’enfant et j’ai fini par ne plus me sentir à ma place. Si bien qu’en septembre 2019, j’ai subi ce qu’on appelle couramment un “burn out”. Monter une oasis était une question de survie. »_
Au bout de quelques semaines, Mirabelle et Alexandre se sont rendu compte qu’ils avaient un nouveau métier : être Mirabelle et Alexandre.

## Un prêt de la Coopérative Oasis

Depuis décembre 2020, la Coopérative Oasis accompagne la Forge du Vallon. Elle lui a fait un prêt de 150 000 euros sur 10 ans, pour l’achat de la propriété, en complément des capitaux propres des habitants. Elle les soutient en parallèle sur leur structuration juridique et financière. Pour continuer ce travail de prêt et d’accompagnement d’oasis partout en France dans le besoin, nous cherchons plus d’investisseurs !
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
