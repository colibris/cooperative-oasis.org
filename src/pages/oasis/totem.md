---
title: "Totem"
date: "2019-02-25T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/totem/"
avancement: "financé"
montant: "100 000 euros"
departement: "Deux-Sèvres"
website: https://www.espacetotem.fr/
image: ./img/Totem1.jpg
highlighted: true
description: "L’Espace Totem est le fruit d’une heureuse rencontre entre deux êtres aux parcours de vie atypiques et inspirants. Le lieu permet de vivre des séjours transformateurs dans un bel écrin de nature."
---

## Le fruit d’une rencontre
L’Espace Totem est né de la rencontre entre deux êtres aux parcours de vie atypiques et inspirants.
D’un côté Marc de La Ménardière : entrepreneur, globe-trotter et réalisateur "en quête de sens", investi dans la transition sociétale. De l’autre Malory Malmasson : auteure, youtubeuse et psycho-energéticienne aux talents uniques et dons bien particuliers. Ils ont voulu créer un lieu inspirant pour réconcilier connaissance de soi et transition sociétale.

Ensemble, ils ont monté et ouvert une oasis ressources : l’Espace Totem. Situé en gâtine, dans les Deux- sèvres, Totem est niché dans un écrin de verdure, adossé à une forêt de 500 hectares. Le terrain de près de 2ha comprend deux bâtiments d’habitation, un potager en permaculture, une grange transformée en salle d’activité, un espace bien-être.

![Les fondateurs de Totem](./img/Totem1.jpg)

## Un espace de ressourcement pour s’inspirer, s’aligner et se réaliser
Marc et Malory ont mis en commun leurs intuitions, leurs compétences et les fruits de leurs expériences transformatrices pour créer une boîte à outils innovante et faire vivre des séjours véritablement initiatiques.
Révolution intérieure, révolution des habitudes et révolution sociétale. L'association Totem souhaite assister et soutenir tous ceux qui souhaitent vivre de façon plus harmonieuse avec eux-mêmes et les écosystèmes qui les entourent.

![Espace Totem](./img/Totem2.jpeg)

## Le besoin de financement
Le projet a sollicité la Coopérative Oasis pour un prêt de 100 000 euros sur 7 ans et la convention a été signée en décembre 2019. Cette somme permet de rembourser plusieurs prêteurs ayant contribué au début du projet et étale la charge financière sur un plus grand nombre d'années pour faciliter le développement du lieu.


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
