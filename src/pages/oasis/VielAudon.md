---
title: "Le Viel Audon"
date: "2020-06-02T22:12:03.284Z"
layout: post
type: oasis
path: "/vielaudon/"
avancement: ""
montant: ""
departement: "Ardèche"
website: https://levielaudon.org/
image: ./img/VielAudon1.jpg
description: "Sur les bords de l’Ardèche, le hameau abandonné du Viel Audon a été réhabilité par à des chantiers internationaux de jeunes. Depuis 50 ans, il fonctionne comme un véritable village coopératif et écologique."
---

**Niché au flanc d’une falaise des Gorges de l’Ardèche, à proximité de Balazuc, le hameau du Viel Audon est déserté par sa population au début du XIXe siècle et disparaît peu à peu sous la végétation galopante. À partir de 1970, de jeunes utopistes le découvrent et décident de le réhabiliter grâce à des chantiers internationaux de jeunes. Ils donnent jour à un véritable village coopératif et écologique où l’on accueille et où l’on vit "autrement" depuis 50 ans.**

![Viel Audon](./img/VielAudon1.jpg)
_Crédit : Patrick Lazic_

## Reconstruire un hameau abandonné
Après des siècles d’occupation d’une population vivant d’agriculture vivrière, le Hameau du Viel Audon est brusquement abandonné par ses habitants, suite à l’arrêt subit de l’industrie de la soie. Durant des décennies, le hameau disparaît progressivement sous la végétation généreuse des bords de l’Ardèche. Il aurait sans doute été définitivement rayé de la carte si quatre jeunes utopistes n’avaient, dans les années 70, conçu le projet fou de le faire revivre, en le reconstruisant pierre à pierre.

![Viel Audon](./img/VielAudon2.jpg)
_Crédit : Patrick Lazic_

## « Se construire en construisant »
L'une des premières activités du lieu a été d'accueillir des jeunes en difficulté afin qu’ils participent aux différents chantiers. Pendant 10 ans, le lieu se reconstruit, la production agricole s'améliore et les chantiers pour les jeunes se diversifient. Cela entraîne la création de différentes structures : l’Association des Jeunes de Chantier (AJC) porte l’idée de  « se construire en construisant », et organise les chantiers de jeunes à dimension internationale en période estivale. Chaque année, ce sont 200 à 250 jeunes qui se retrouvent sur le site pour vivre et faire ensemble ; plus de 10 000 jeunes ont apporté leur pierre à l’édifice en 50 ans.

![Viel Audon](./img/VielAudon3.jpg)
_Crédit : Patrick Lazic_

L’association le Bateleur porte la dynamique « Les Bocaux Locaux » dont les bénévoles ont remis en culture les terrasses environnantes, développent la production en circuits-courts et proposent des animations sur l’agriculture biologique.

## Élevage, agriculture et animations
Les salariés de la ferme, portée par la Société civile d’exploitation agricole du Viel Audon, s’occupent de l’élevage (chèvres, vaches, cochons, poules), de la production de lait et de fromage et de la culture des oliviers et des aromatiques. Ils en assurant la transformation (produit fini ou restauration) et la commercialisation dans la boutique du hameau.

![Viel Audon](./img/VielAudon4.jpg)
_Crédit : Alexandre Sattler_

L’association le Mat, la plus ancienne, est un centre d’éducation à l’environnement, au développement durable et à la coopération. Elle assure la gestion de l’hébergement de groupe (50 places), proposent des animations à l’attention des scolaires et des particuliers (éco-jardinage, gastronomie de la garrigue, etc.) et également des formations à l’usage des animateurs ou encadrants de structures associatives, MJC, mouvement d’éducation populaire…

_"Le hameau est un corps vivant_, explique Yann Sourbier, ancien directeur de l’association Le Mat. _Il est composé de différentes structures, de différents métiers, qui ont chacun leurs fonctions propres, comme les organes dun corps. Malgré leurs différences de fonctions, ces structures et leurs acteurs concourent tous au même objectif, celui de transmettre des valeurs et des savoir-faire liés à l’identité du territoire ardéchois et à un certain art de vivre en paix, ensemble et avec notre environnement."_

Dans ce podcast, Alexandre Sattler rencontre Aurore, Eléonore et Floriane qui lui permettent de mieux comprendre la dynamique de cet écolieu. 

<iframe src="https://www.podcastics.com/player/extended/978/34093/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
