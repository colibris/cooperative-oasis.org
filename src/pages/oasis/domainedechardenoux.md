---
title: "Le domaine de Chardenoux"
date: "2020-08-23T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/domainedechardenoux/"
avancement: ""
montant: ""
departement: "Saône et Loire"
image: ./img/Chardenoux1.JPG
website: https://www.association-a-ciel-ouvert.org/lieux.aspx?lieu=147
description: "En pleine Bresse bourguignonne, le Domaine de Chardenoux est un centre écologique d’étude et de pratique spirituelle."
---

**En pleine Bresse bourguignonne (Saône-et-Loire), le Domaine de Chardenoux est le centre d’étude et de pratique spirituelle oecuménique coordonné par l’association À Ciel Ouvert. Créé en 1999 avec un fort souci écologique, il accueille des groupes toute l’année et permet des temps de solitude et de contemplation dans son parc de 28 hectares composé pour moitié de bois, pour moitié de prés et champs….**

![Chardenoux](./img/Chardenoux2.JPG)

## Diffuser l’étude et la pratique des spiritualités
Lorsqu’Alain Chevillat crée l’association Terre du Ciel en 1987, il entend diffuser en France l’étude et la pratique des spiritualités du monde entier. Une revue, des événements et des formations sont progressivement mises en place partout sur le territoire. « _Nous en avons eu assez de toujours chercher de nouveaux espaces pour nos événements_, raconte Alain. _Nous avons donc décidé de créer un lieu à nous, où nous pourrions développer toutes nos activités._ »

![Chardenoux](./img/Chardenoux3.JPG)
_Alain Chevillat_

## Un domaine écologique entre la terre et le ciel
En 1999, Terre du Ciel acquiert donc le domaine de Chardenoux, une demeure bâtie par le comte de Poligny au milieu du XVIIIe siècle, ainsi que les 28 hectares de forêt et de prairie l’entourant. De gros travaux d’aménagement des bâtis permettent d’accueillir aujourd’hui jusqu’à 120 personnes pour des événements et des stages en lien avec la spiritualité. Une dizaine de personnes habitent sur place toute l’année.

![Chardenoux](./img/Chardenoux1.JPG)

Deux vergers, une serre tropicale et un jardin sauvage permettent en partie de nourrir habitants et stagiaires toute l’année. Manu s’occupe de ces espaces : « _Ma pratique du yoga incite à travailler en offrande, sans attente des fruits de l’action. L’agriculture est un très bon exercice dans ce domaine… le but n’est pas de produire avant tout, mais de servir le vivant._ » Le jardin est en effet aussi un espace de ressourcement et un décor pour des cérémonies.

![Chardenoux](./img/Chardenoux4.JPG)

## « Je viens pour apprendre. Je repars pour servir »
En 2009, Alain Chevillat lance « Les Fleurs du Vivant ». Le programme permet à douze jeunes en manque de cadre et de perspectives, âgés entre 18 et 35 ans, de passer un an en immersion dans le Domaine de Chardenoux. Hébergés dans un hameau écologique proche du château, ils y apprennent à faire la cuisine, cultiver le jardin, entretenir un lieu et se forment à divers métiers – massage, ayurveda, plantes médicinales. Plusieurs voyages dans le désert de Mauritanie et dans des ashrams en Inde complètent ce cheminement intérieur. « _Quand ils quittent Chardenoux, la plupart ont une idée précise de ce qu’ils veulent faire de leur vie_, explique Christine, directrice de Chardenoux. _Et parfois ils reviennent, comme Manu qui gère aujourd’hui le jardin_ ! »

![Chardenoux](./img/Chardenoux6.JPG)

## Une oasis qui se réinvente
Chardenoux occupe une place spéciale dans le réseau oasis. C’est au domaine que le premier Festival Oasis a eu lieu en 2017, réunissant 150 personnes autour des valeurs de l’écologie et de la vie collective solidaire. Aujourd’hui, le Hameau des Fleurs et les 3ha de terrain autour (dont 1,7ha constructibles) sont en vente pour accueillir un collectif susceptible de monter un petit écovillage en lien avec le château. Début 2021, la [Coopérative Oasis](https://cooperative-oasis.org/) accompagne le domaine de Chardenoux dans le lancement d'une nouvelle étape, au travers d'une restructuration du projet.

![Chardenoux](./img/Chardenoux5.JPG)

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
