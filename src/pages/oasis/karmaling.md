---
title: "Karma Ling - écosite d'Avallon"
date: "2020-05-23T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/karmaling/"
avancement: ""
montant: ""
departement: "Savoie"
image: ./img/KL0.jpg
website: https://www.institutdharma.net/lieux-de-pratique/shangpa-karma-ling-domaine-davallon/
description: "Un écosite sacré bouddhiste fondé en 1979 en Savoie et dédié à une vision universelle et éthique de la spiritualité"
---

**Le domaine d’Avallon est situé au cœur du massif de Belledonne à 800 m d’altitude sur les bords du torrent du Bens à une heure et demi de Genève et de Lyon. Haut lieu historique et légendaire, il abrite l’ancienne chartreuse de Saint-Hugon depuis 1173.**

![Karma Ling](./img/KL0.jpg)

## Aperçu des origines à nos jours
En 1980, une association des étudiants de [Vajradhara Kalu](https://www.shangpafoundation.org/library/masters-page/kyabje-kalu-rinpoche/), l’un des plus éminents yogis de la tradition tibétaine, acquiert le site en ruines et entreprend progressivement sa réhabilitation. Il est aujourd’hui constitué d’un domaine de 60 hectares de forêt et d’un ensemble de d’habitations. A la fois lieu de retraite et lieu d’accueil ouvert sur le monde, le domaine d’Avallon a gardé sa vocation d’écosite sacré ; il est devenu le campus résidentiel de la Buddha University. Suite au grand incendie qui ravage la chartreuse en décembre 2017, le domaine et son habitat est en cours de rénovation.

Parrainée par Edgar Morin et Pierre Rabhi, Karma Ling est un espace dont l’esprit est dédié à la convergence des valeurs humanistes et écologistes d’Orient d’Occident, inspiré par la sagesse multimillénaire du Bouddha. Elle s'inscrit dans le réseau de la [Coopérative Oasis](https://cooperative-oasis.org/).

![Karma Ling](./img/KL1.jpg)

## La Buddha University
La [Buddha University](https://www.buddha.university/) offre à toute personne la possibilité d’étudier et de pratiquer les sciences contemplatives de sagesse et de compassion du Bouddha dans leur expression contemporaine.

La transmission, l’étude et la pratique s’opèrent, en ligne et en présentiel, au sein de trois instituts :
- L’[institut Pleine Présence](https://www.buddha.university/instituts/institut-pleine-presence/) transmet la méditation de présence ouverte et altruiste dans une approche humaniste naturelle et non confessionnelle accessible à tous.
- L’[institut Dharma](https://www.buddha.university/instituts/institut-dharma/) transmets l’étude et la pratique de la voie universelle du Bouddha, sa discipline éthique écologiste, ses méthodes de contemplation profonde et son intelligence fondamentale de l’interdépendance et de la connaissance de soi.
- L’[école de la lignée](https://www.buddha.university/instituts/ecole-de-la-lignee/) transmet les enseignements et pratiques les plus essentiels, « Mahâmudrâ-Dzogchen », associés aux méthodes du Vajrayâna.

![Karma Ling](./img/KL2.JPG)

## Activités
Le domaine d’Avallon abrite le centre Shangpa Rimay Ling (anciennement Karma Ling) et les centres de retraite de Garuda Ling ainsi que la plupart des activités « en présentiel » de la Buddha University et de ses trois instituts. Les programmes sont ouverts à tous et offrent tout au long de l’année, différents séminaires et retraites ainsi que des rencontres transdisciplinaires.

![Karma Ling](./img/KL3.JPG)

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
