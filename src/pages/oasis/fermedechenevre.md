---
title: "Ferme de Chenêvre"
date: "2019-02-25T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/fermedechenevre/"
avancement: "en demande de prêt"
montant: "150 000 euros"
departement: "Jura"
image: ./img/chenevre1.jpg
description: "Située dans le Jura, la Ferme de Chenêvre rassemble une dizaine de personnes qui y développe diverses activités agricoles, artisanales et d'accueil."
---

## L'histoire du projet
Mobilisé autour d’une ferme jurassienne exceptionnelle, un collectif a monté, en presque un an, une oasis de vie et d’activité. “Un petit univers qui me fait comprendre le grand monde” résume Yorgos, habitant du lieu.
À l’origine de cette oasis, une mobilisation autour d’un lieu : la ferme isolée de Chenèvre à La Chapelle-sur-Furieuse, dans le Jura. 25 ha de terrain rattachés à un bâtiment anciennement fortifié, dont certaines parties remontent au XIIIe siècle.
Manon, en tant que maraîchère, a accès aux offres de la SAFER. Lorsqu’elle voit que la ferme de Chenèvre est mise en vente, elle propose une réunion ouverte à tous à la Chapelle-sur-Furieuse pour voir si d’autres personnes seraient intéressées pour acheter avec elle... Un petit groupe de locaux, motivés, se constitue. Il est bientôt rejoint par d’autres personnes venues de toute la France via le réseau Colibris.

![Bâtiment](./img/chenevre1.jpg)

## Une SCI propriétaire pour des loyers modérés
Un peu plus d’une année plus tard, le 16 février 2017, un collectif de 10 personnes qui se découvrent à peine achète le lieu grâce à un emprunt auprès de la Nef. “Au total, nous avons déboursé 400 000€ pour l’achat du lieu et le financement de la première tranche de travaux, qui s’élève, elle, à 70 000€” précise Anne, une habitante. Une SCI est propriétaire et les habitants lui versent tous les mois un loyer afin qu’elle puisse rembourser l’emprunt. De 150€ par mois pour un habitat léger à 500€ pour la location du fournil de 100m2, les loyers s’échelonnent en fonction de la surface occupée, de sa nature et de son usage. En plus des loyers, chaque membre du collectif paie une cotisation sociétaire de 50€ par personne et par mois. Une association gère en parallèle les activités du lieu.

![Poterie](./img/chenevre2.jpg)

## Maraîchage, pain, vin nature, céramique, restauration, camping…
Chenèvre ne se limite en effet pas à loger ses habitants. “Nous souhaitions aussi faire de la ferme est un lieu d’activités professionnelles diverses. Si l’on veut être autonome et résilient, l’habitat ne suffit pas…” explique Manon, maraîchère à l’origine du projet. Très rapidement, elle a donc lancé son activité de maraîchage bio en traction animale et Virginie son atelier de céramique. La première récolte a pris le chemin des Biocoop de la région et du marché de Noël de Lyon, tandis que les créations de Virginie ont été vendues dans la France entière. À partir de l’été 2019, de nouvelles activités commencent : Terre de Pains, une SCOP montée par 4 artisans-boulangers bio, s’empare du fournil entièrement retapé. Morgane, une vigneronne voisine disposera sa prochaine vendange dans les caves, renouant ainsi le fil d'une activité vinicole séculaire sur le lieu.
Enfin, d’autres projets sont en cours d’élaboration. Yorgos, amoureux de restauration mais écoeuré des conditions d’exercice de sa passion en ville, relance petit à petit son entreprise sur place, à l’occasion des divers évènements qui y sont organisés. Maryline réfléchit quant à elle au développement d’un accueil touristique.

![Plan du projet](./img/chenevre3.jpg)

## Un collectif au-delà de l’enceinte de la ferme
Dans la tradition d’ouverture de ce lieu à la vie du village, le collectif organise régulièrement des journées portes ouvertes, évènements thématiques ou chantiers participatifs. Un réseau d'une cinquantaine de personnes des alentours s’est ainsi constitué, apportant de l'aide, de l'amitié, du travail bénévole, mais aussi des temps d'échanges à l’occasion des cafés-philo, des cueillettes de plantes sauvages ou des soirées guinguette. C’est aussi hors les murs que la ferme de Chenèvre entend entretenir et valoriser la communauté dans tout ce qu’elle a de précieux...

Yourgos nous présente la dynamique de cette oasis ressource dans le podcast "la voix des oasis" :
<iframe src="https://www.podcastics.com/player/extended/978/34037/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

![Plan du projet](./img/chenevre4.jpg)

## Besoin de financement
Le projet a sollicité la Coopérative Oasis pour un prêt de 100 000 euros sur 7 ans à moyen terme. Il servirait à investir sur le lieu pour continuer la rénovation des bâtiments et l’installation de nouvelles activités de production.


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez la ferme de Chenêvre en participant à la Coopérative
    </a>
</div>
</div>
