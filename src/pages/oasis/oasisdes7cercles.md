---
title: "Oasis des 7 cercles"
date: "2020-06-07T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/oasisdes7cercles/"
avancement: ""
montant: ""
website: https://www.oasisdes7cercles.com/
departement: "Maine-et-Loire"
image: ./img/Oasis7Cercles3.jpg
description: "Un lieu de vie et d’activités situé près d'Angers qui accueille notamment des personnes fragilisées pour des activité artistiques et sensorielles régénérantes"
---

L’Oasis des 7 cercles est un lieu de vie et d’activités situé à Jarzé, près d'Angers. Depuis 2011, Catherine et Laurent, les fondateurs, ont réussi à transformer une friche en une véritable terre d’accueil. Toute l’année, des personnes fragilisées peuvent y pratiquer des activité artistiques et sensorielles régénérantes...

<iframe width="560" height="315" src="https://www.youtube.com/embed/n5mDL6WIi_c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## De la tribu familiale à la terre d’accueil
Les sept cercles, ce sont les sept membres d’une même tribu composée de deux familles recomposées : Laurent et Catherine, les fondateurs, ont en effet cinq enfants issus de précédents mariages. L’Oasis des 7 Cercles est donc, dès le départ, le choix de construire un lieu pour expérimenter la réunion de ces deux familles et de l’ouvrir vers l’extérieur.
Ancien directeur de foyers d’hébergement pour personnes en situation de handicap mental, Laurent, cofondateur de l’oasis, a en effet souhaité avec sa compagne Catherine proposer aux personnes vivant en institution (foyers, centres d’accueil spécialisés…) des séjours de ressourcement permettant de quitter quelques temps un collectif pesant.

![Oasis des 7 Cercles](./img/Oasis7Cercles1.jpg)

## Fragilités et joie de vivre
L’Oasis propose différent types d’accueil pour toute personne ayant besoin de se reconnecter à la nature, en s’offrant une pause dans une vie souvent surchargée et stressante. L’association offre à un public fragilisé un espace sécurisé, contenant et bienveillant permettant le repos, la diminution des tensions, l’apprentissage de la tolérance, l’acceptation des différences, la régulation de la violence… Elle propose par exemple des weekends d’accueil pour des personnes vivant en institution médico-sociale, des ateliers les mardi ou mercredi pour des jeunes d’institut médico-éducatif autour d’une formation pratique en espaces verts, ou encore des vacances adaptées pour un public porteur de handicap. Le maître mot de ces accueils est le "vivre ensemble", et la vie à l’Oasis s’articule autour de la vie quotidienne et d’activités adaptées aux possibilités du collectif. 

![Oasis des 7 Cercles](./img/Oasis7Cercles3.jpg)

L’Oasis organise aussi de nombreuses autres activités pour "danser la vie", notamment des stages et des cours hebdomadaires de Biodanza. Un Centre d’Éducation Biocentrique a également ouvert en octobre 2015. Il propose des outils pédagogiques soutenant le développement des potentiels humains, et permet de modifier les postures d’accompagnants en intégrant l’affectivité, la bientraitance et le respect des enfants, des humains et de la terre.

## Une oasis « historique »
L’Oasis des 7 Cercles a fait partie du mouvement des "Oasis en tous lieux" qui a transmis à Colibris  puis à la Coopérative Oasis le soin de développer ce mode de vie sobre et collectif. Cette oasis incarne les valeurs de ces "pionniers" avec un engagement très fort en faveur de l’autonomie. Une forêt fournit le bois de chauffage, un potager et des animaux  la majeure partie de la nourriture, des toilettes sèches permettent d’éviter la pollution de l’eau… Quand on arrive, on est étonné de voir comment une famille a pu transformer un espace en friche en un lieu harmonieux et cohérent.

![Oasis des 7 Cercles](./img/Oasis7Cercles2.jpg)

## De l’oasis ressource à l’oasis de vie ?
Une SCI regroupant Catherine et Laurent possède les bâtiments et le terrain depuis son achat en 2011, et a contractualisé pour un prêt, ce qui occasionne de lourdes charges. Une association loi 1901 accueille parallèlement les activités sociales et les stages et paye un loyer à la SCI.
Pour assurer le remboursement de l’emprunt, les fondateurs doivent garantir un haut niveau d’activités du lieu (accueil social, biodanza…), et Laurent et Catherine aspirent maintenant à un rythme moins soutenu.
Ils explorent donc la possibilité d’accueillir sur place d’autres familles avec qui partager l’entretien du lieu, les charges et l’organisation des activités associatives. Le modèle économique d’une oasis de vie est souvent plus facile à équilibrer que celui d’une oasis ressource grâce à l’apport de loyers. Cela va nécessiter des travaux pour libérer la grande maison en espace entièrement dédié aux activités associations, et rénover des bâtiments du site.
Cette transformation en cours permet d’expérimenter une situation assez courante : des lieux, souvent porteurs de patrimoine historique, qui ne pourront être entretenus et exploités pleinement qu’en accueillant d’autres familles.
Dans ce podcast, Catherine et Laurent prennent la parole dans cette émission pour nous présenter l’Oasis des 7 cercles, une terre d’accueil en destination des publics fragiles. Ce lieu leur offre un espace sécurisé et  bienveillant où chacun découvre le vivre ensemble, la tolérance et l’acceptation des différences.

<iframe src="https://www.podcastics.com/player/extended/978/34089/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
