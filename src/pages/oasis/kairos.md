---
title: "Kairos"
date: "2018-01-10T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/kairos/"
avancement: "en demande de prêt"
montant: ""
departement: "Var"
image: ./img/Kairos_Repas.JPG
website: http://kairos1154.fr/
description: "Un habitat participatif de 19 logements à Draguignan avec ateliers musique et arts plastiques et plein d'autres activités vers l'extérieur."
---

## Le groupe
Le projet se prépare sur un foncier de 3 hectares, à 1,5km du centre ville de Draguignan (83). Les bâtiments sont constitués de 300 m2 existants, auxquels s'ajouteront un total de 1 900 m2 neufs, soit 2 200 m2 de bâtiments au total.
Le programme prévoit 19 logements en accession et le groupe souhaite proposer 2 logements locatifs.
Les locaux partagés comprennent notamment un salon, une cuisine, des bureaux communs, 4 chambres pour les visiteurs et une piscine.
![Repas partagé](./img/Kairos_Repas.JPG)

D'ores et déjà le collectif propose tout un tas d'ateliers vers l'extérieur : atelier musique, atelier arts plastiques et vidéo, agriculture et botanique (vignes, oliviers, fruitiers, permaculture et  jardin potager), qi gong...

![Atelier peinture](./img/Kairos_Atelier Peinture_FR05579.JPG)

## La solidarité au coeur du projet
Pour les futurs habitants, la solidarité c’est, bien sûr les tâches réalisées en commun valorisant ce que chacun peut apporter au groupe dans la mesure de ses moyens et de ses compétences. C’est aussi la transmission des savoirs dans tous les domaines, entre pairs, des aînés vers les plus jeunes, ainsi qu'une présence chaleureuse dans les moments difficiles et les accidents de la vie.

Le groupe souhaite en parallèle proposer un logement en location à une personne en situation de handicap, afin de mettre en application ses idées et ses principes sur l’insertion et l’intégration.

Les ateliers de musique, d’arts plastiques et de vidéo déploient des activités à destination des enfants, des adolescents et des jeunes en difficulté.

Les habitants revendiquent enfin un mode de vie qui permet de rester ouvert sur les autres et sur la vie et d'accueillir ainsi les aînés. L'intention est qu'ils puissent rester maîtres de leur destin jusqu’au bout, tout en vivant une solidarité bienveillante.

Plus d’information sur le site internet : [kairos1154.fr](http://kairos1154.fr).

![La Bastide](./img/Kairos_Bastide.JPG)


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
