---
title: "L'oasis de Serendip"
date: "2018-05-15T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/oasideserendip/"
avancement: "en demande de prêt"
montant: "200 000 euros"
departement: "Drôme"
website: https://oasisdeserendip.net/
image: ./img/serendip-groupe.jpg
description: "Située dans la vallée de la Drôme, l'oasis de Serendip est un lieu collectif tourné vers la permaculture et l'éducation, qui vise à expérimenter un nouvel art de vivre et d’entreprendre ensemble."
---

## A l'origine du projet
Porté par la légende des 3 princes de Serendip, qui a donné son nom à la serendipité (le fait de « trouver autre chose que ce que l’on cherchait »), le projet de l’Oasis de Serendip est né en juillet 2014 à l’initiative de Jessica et Samuel Bonvoisin, parents de 3 enfants. Le couple prend contact avec Colibris dès le début et s’inscrit dans la dynamique du réseau Oasis.
Au cours de la première année du projet, une campagne de financement participatif permet de collecter 30 000€, un premier collectif est formé et le lieu d’implantation est identifié dans la vallée de la Drôme. Trois associations font alors collectivement l’acquisition, en novembre 2015, du lieu comprenant 11,5 ha de terrain agricole, partiellement boisé, et des bâtiments à rénover.

## Un écosystème permacole
Désormais porté par une équipe d'une dizaine de personnes, le lieu est conçu sous la forme d’un écosystème associant diverses zones aménagées en fonction des besoins des habitants, professionnels et usagers du lieux : bâtiments, jardin, bois, prairie, forêt comestible, potager, mandala de plantes médicinales, canards, poules, chien… et cochons et ânes à venir…
La permaculture est au coeur de la conception du lieu et est le thème majeur des formations proposées. L'oasis de Serendip est un petit paradis pour les amoureux de la Nature.

![Le tracteur - oasis de Serendip](./img/serendip-tracteur.jpg)

## Trois piliers fondateurs
L’Oasis de Serendip s’est doté de trois piliers fondateurs qui constituent ensemble la base éthique du projet : le texte graine, qui est la charte éthique qui rassemble les valeurs, les 4 accords Toltèques, qui sont une base pour vivre ensemble, et la Communication Non-Violente, qui est la base éthique des échanges.

Plus d’information sur le site internet : [www.oasisdeserendip.net](http://www.oasisdeserendip.net/).

![Groupe à l'oasis de Serendip](./img/serendip-groupe2.jpg)

## Le besoin de financement
L'Oasis de Serendip sollicite auprès de la Coopératve Oasis un prêt d’un montant total de 300 000€ sur 10 ans. Il sera utilisé pour compléter le bouclage financier du programme de rénovation de 680 m² de bâtiment.
Après rénovation et aménagement, il comprendra cinq logements individuels de 50 à 100 m², une buanderie commune, une grande salle de formation de 110 m2, une salle d’activités de 40m2, un espace « restauration » de 40m2, un espace de co-working de 40m2, un espace de stockage de 20m2 pour le matériel éducatif et un gîte de 130m2.
Un espace extérieur de 200m2 comprenant une terrasse, pergolas et des massifs arborés viendra compléter cet ensemble dédié à l’accueil du public. Une serre bioclimatique de 60 m2 permettra d’étendre les activités de jardinage avec le public de l’oasis pendant la période hivernale sera aussi aménagée contre la façade sud de la bâtisse.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez l'oasis de Serendip en participant à la Coopérative
    </a>
</div>
</div>
