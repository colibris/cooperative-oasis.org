---
title: "La Maison Autonome"
date: "2019-03-22T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/maisonautonome/"
avancement: ""
montant: ""
departement: "Loire Atlantique"
image: ./img/maisonautonome1.jpg
website: http://heol2.org/
description: "En 2020, Brigitte et Patrick Baronnet ont fêté leur 50 ans de mariage et leur 74 ans d’engagement au service du vivant dans leur oasis : la Maison Autonome écohameau du Ruisseau située à Moisdon la Rivière en Loire-Atlantique."
---

## Pendant 17 ans, la famille Baronnet de 6 personnes vit sur un seul demi-salaire.
En 1976, à la fin des « 30 glorieuses », Patrick et Brigitte rachètent une maison à rénover en Loire-Atlantique. Avec leurs quatre enfants, ils inventent un autre mode de vie, sobre et autonome. Grâce à un apport personnel au départ, Ils rénovent leur maison, et we mettent à produire une part non négligeable leur consommation en eau et en énergie.

Résultat : pas de facture d’eau, ni d’électricité, ni de déchets-épuration, ni de loyer, ni de prêt d’argent par les banques. L’argent est investi sans intérêt directement dans les matériaux de construction et la vie se construit petit à petit. Pendant 17 ans, la famille Baronnet de 6 personnes vit sur un seul demi-salaire.

![Maison autonome](./img/maisonautonome1.jpg)

## Un écolieu pionnier du réseau
La Maison Autonome fait partie des premiers écolieux en France. Elle participe au réseau des Oasis en Tous Lieux fondé en 2009 par Pierre Rabhi, qui est devenu le réseau des oasis porté aujourd'hui par la Coopérative Oasis. Fidèle à sa posture d'inspirateurs et de pionniers, l'oasis de Moisdon la Rivière a fait partie des tout premiers associés de la Coopérative...

![Maison autonome](./img/maisonautonome2.jpg)

Pendant des années, les Baronnet transmettent leur expérience a travers des stages et des visites. En 2017, des dizaines de milliers de personnes viennent à la Maison Autonome pour se former et en savoir plus sur les moyens de changer de vie. .

![Maison autonome](./img/maisonautonome3.jpg)

Dans le podcast ci-dessous, Patrick et Brigitte Baronnet donnent leur vison de l’autonomie, de l’importance d’une alimentation saine mais aussi d’une pensée exemplaire pour agir sur la matière !

<iframe src="https://www.podcastics.com/player/extended/978/33915/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
