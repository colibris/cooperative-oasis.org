---
title: "Oasis des Tisserands"
date: "2018-01-07T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/oasisdestisserands/"
avancement: "financé"
montant: "150 000 euros"
website: https://oasisdestisserands.com/
departement: "Charente Maritime"
image: ./img/Tisserands-groupe.jpg
description: "Un habitat participatif à la Rochelle de 8 logements avec un espace coworking, un espace de formation, des ateliers et un jardin en permaculture"
---

## Le projet
L'Oasis des Tisserands vise à créer un éco-lieu sur un domaine de 7500 m2, situé Sainte-Soulle, à 10 minutes de La Rochelle. Cet éco-lieu intègre notamment un habitat participatif de 8 logements dont 4 déjà existants à rénover en éco-construction et 4 prévus pour être bâtis en éco-construction.

Les habitants partageront des espaces collectifs dont certains sont déjà présents sur le site : une cuisine d’été, une buanderie, une piscine, une grande salle dans une longère entièrement rénovée (qui servira également aux formations), une grande cave, un verger avec de multiples arbres fruitiers. D’autres espaces partagés sont prévus pour être créés dès le démarrage du projet: un atelier de bricolage et un jardin nourricier en permaculture de 3000 m2.

![Le lieu](./img/Tisserands-lieu1.JPG)

## Un pôle multidisciplinaire
L’Oasis des Tisserands accueillera de nombreuses formations et conférences multi-disciplinaires, axées autour de 4 axes : le lien au meilleur de soi, le lien aux autres, le lien à la nature et le lien à l’univers.

L’objectif est de créer un lieu inspirant qui propose des expériences d’épanouissement personnel, afin de se reconnecter à soi, aux autres et à ce qui les entoure. Le lieu accueillera ainsi des stages, court-séjours, ateliers... permettant de faire rayonner la philosophie et les valeurs du lieu auprès du plus grand nombre.

Enfin, un espace de coworking sera également proposé pour offrir la possibilité aux entrepreneurs locaux de participer au projet. Il permettra l'accueil d'une petite dizaine de travailleurs indépendants.

![Le lieu](./img/Tisserands-groupe.jpg)

## Le besoin de financement
La Coopérative Oasis a fait un prêt de 150 000 euros en compte courant d'asociés à la SCIC de l'Oasis des Tisserands, qui gère l'ensemble des espaces communs (ateiers d'artistes, coworking, espaces de formation, chambres d'hôte...). Le financement a eu lieu en septembre 2020.


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
