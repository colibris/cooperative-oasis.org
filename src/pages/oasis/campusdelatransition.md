---
title: "Le Campus de la Transition"
date: "2019-01-22T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/campusdelatransition/"
avancement: ""
montant: ""
departement: "Seine et Marne"
image: ./img/Campus-de-la-Transition1.jpg
website: https://campus-transition.org/
description: "Le Campus de la Transition s’est installé en 2018 sur le Domaine de Forges. Avec son Château du XVIIIème siècle et son parc de 12 hectares, cet ancien établissement d’enseignement est devenu un éco-lieu expérimental de la transition écologique et sociale"
---

Situé en Seine-et-Marne et proche de Fontainebleau, le [Campus de la Transition](https://campus-transition.org) s’est installé en 2018 sur le Domaine de Forges. Avec son Château du XVIIIème siècle et son parc de 12 hectares, cet ancien établissement d’enseignement général et horticole est devenu un éco-lieu expérimental de la transition écologique et sociale.

![Campus de la Transition](./img/Campus-de-la-Transition1.jpg)

## Sortir des sentiers battus de la pensée académique
L’histoire commence en 2017 lorsque Cécile Renouard, professeure aux Mines et à l’Essec, visite le château de Forges. Les propriétaires, des religieuses de l’Assomption cherchent alors repreneur.  Cela fait quelque temps que cette enseignante spécialiste d’éthique, elle aussi membre de la congrégation de l'Assomption, nourrit l'envie de créer un lieu laïc consacré à la formation à la transition écologique et à son expérimentation. Un lieu qui s’inspirerait du Schumacher College, établissement du Devon anglais devenu une référence internationale pour l’enseignement de l’écologie.

Le Domaine de Forges semble parfait pour le projet : accessible depuis Paris mais suffisamment éloigné pour se sentir loin ; un lieu déconnecté du cadre lisse des grandes écoles, où les enseignements intégrant les questions écologiques et sociales ne seraient pas contredits dans la salle d’à côté par un professeur apôtre de la finance de marché.

Une fois le château récupéré, le Campus de la Transition s'installe. L’appel à un financement participatif, la mobilisation de mécènes et la mise en place de partenariats avec des établissements ont permis en deux ans de rassembler 800 000 euros, dont une bonne partie a été destinée aux travaux

![Campus de la Transition](./img/Campus-de-la-Transition2.jpg)

## Un enseignement "de la tête, du corps et du coeur"
Depuis 2018, le Campus de la transition reçoit des étudiants envoyés par leurs écoles (Sciences Po, l’IEP de Lille ou l’Essec), ou inscrits de manière individuelle comme dans sa formation-phare, le "[T-Camp](http://tcamp.fr/)", porté avec le mouvement Colibris et la Coopérative Oasis.

« Tête, cœur, corps » : telle est la maxime qui résume l'ambition pédagogique du Campus. Il s’agit d'abord de développer son esprit critique sur le monde en suivant des cours sur le climat, l’éthique, la justice sociale, en décloisonnant les disciplines. Chaque étudiant est en outre invité à travailler sur sa relation à soi et aux autres via des outils comme la Communication Non-Violente ou la gouvernance partagée. Enfin, le corps est pleinement engagé durant le cursus dans des travaux de rénovation du château ou la culture de la terre.
En somme, pour permettre d’accéder à une représentation globale du monde, le Campus propose une éducation holistique faisant appel à différentes ressources conceptuelles, pratiques, spirituelles et relationnelles, s’appuyant sur un réseau de recherche interdisciplinaire.

Une équipe de chercheurs associée constitue le comité scientifique du campus. Parmi eux, Alain Grandjean (fondateur de Carbone 4, cabinet spécialisé dans l’adaptation au changement climatique), l’économiste Gaël Giraud (directeur de recherche au CNRS), le politologue Loïc Blondiaux (professeur à Paris-I), les philosophes Dominique Bourg (université de Lausanne) et Dominique Méda (Paris-Dauphine)…

![Campus de la Transition](./img/Campus-de-la-Transition3.jpg)

Ecoutez Rémi présenter le lieu, l’histoire et les activités du Campus dans le podcast "la voix des oasis" :
<iframe src="https://www.podcastics.com/player/extended/978/33916/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
