---
title: "Les Colibres"
date: "2020-03-31T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/lescolibres/"
avancement: ""
montant: ""
departement: "Alpes-de-Haute-Provence"
image: ./img/LesColibres4.jpg
website: https://lescolibres.jimdo.com
description: "Un habitat participatif écologique à Forcalquier d’une trentaine de personnes âgées de 19 mois à 79 ans vivant dans 11 logements"
---

![Les Colibres](./img/LesColibres4.jpg)

Colibris était déjà pris, alors ils ont opté pour [« Les Colibres »](https://lescolibres.jimdo.com/). Une trentaine d’habitants vivent ensemble depuis 2013 dans un habitat qu’ils ont dessiné et construit à leur image, à Forcalquier (Alpes-de-Haute-Provence).

## 12 ans d'âge
Le 25 novembre 2017 au soir, Pierre danse avec sa famille dans le salon ; 12 ans après le début de l’aventure, ils sont enfin chez eux. Les Colibres, c’est un habitat participatif d’une trentaine de personnes âgées de 19 mois à 79 ans vivant dans 11 logements dont 2 appartements dédiés à des personnes âgées ou handicapées.

![Les Colibres](./img/LesColibres5.jpg)

## Vivre ensemble de 1 à 79 ans...
Ce que veulent  avant tout les Colibres, c’est associer l’écologie et la solidarité. Grâce à une SCIA en jouissance, les habitants ne sont pas propriétaires mais sociétaires : chacun acquiert le nombre de parts qui correspond à la valeur de son logement ainsi que sa quote-part des espaces communs. Quant aux bâtiments, ils sont construits sur des critères entièrement écologiques et des panneaux solaires sont installés sur les toits.

![Les Colibres](./img/LesColibres1.jpg)

Pierre Lévy a co-fondé les Colibres et il est accompagnateur de projets d’habitat participatif.

_« Le collectif a progressivement intégré les professionnels qui l’accompagnaient – architectes, juriste, accompagnateur… c’est dire le pouvoir d’attraction du projet !»_ raconte Pierre. _« Pouvoir d’attraction… et de diffusion. De l’autre côté de la route, la Mairie de Forcalquier envisage la réalisation d’un “Colibres II” d’une quarantaine de logements. »_

_« Ce qui a marqué la mise en place de notre projet, c’est une foi inébranlable dans le fait que ça allait marcher. D’ailleurs, on a écrit et régulièrement actualisé une  lettre à la vie . Et c’est incroyable de voir aujourd’hui que tout ce qu’on rêvait de faire… s’est réalisé. »_

![Les Colibres](./img/LesColibres6.jpg)

Dans cet épisode de la Voix des Oasis, il nous parle de leur modèle, il revient sur l’historique de ce projet pilote, et nous explique avec enthousiasme les joies du collectif.


<iframe src="https://www.podcastics.com/player/extended/978/34054/" frameborder="0" height="200" width="100%" style="border-radius: 10px; »></iframe>


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez Grain&sens en participant à la Coopérative.
    </a>
</div>
</div>
