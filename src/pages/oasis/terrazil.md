---
title: "Terr'Azïl"
date: "2019-03-25T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/terrazil/"
avancement: "financé"
montant: "120 000 euros"
departement: "Ariège"
website: https://www.terrazil.org/
image: ./img/Terrazil5.JPG
description: "Terr'Azïl, est un lieu de vie et d'accueil, sur 74 hectares en Ariège, où l'on apprend à prendre soin du vivant. Les relations humaines, en particulier aux enfants, sont au coeur de la vocation du lieu."
---

## Le coeur du projet
A l’origine, deux familles se sont regroupées pour partager le quotidien d’une vie sans école. De cette rencontre est né fin 2018 le projet de créer un écolieu où vivre, incarner et transmettre les valeurs de soutien humain et de soin du vivant, et ainsi contribuer à un avenir plus doux et plus juste. Au printemps 2020, ce sont 14 individus de 18 mois à 45 ans qui investissent un magnifique terrain de 74 hectares en Ariège (Occitanie). Plusieurs autres foyers sont en chemin pour les y rejoindre, afin d’atteindre un équilibre d’une quarantaine de personnes dans les années à venir.

![terrazil](./img/Terrazil4.jpg)

Terr'Azïl est un projet permacuturel au sens global du terme, porteur d'une éthique respectueuse de l’environnement.
Son écosystème s'articule autour de ces différents axes interconnectés:
 - Permettre aux humains de trouver leur juste place au sein de la nature grâce à l'élaboration d'un design permaculturel cohérent, et un objectif de souveraineté alimentaire, énergétique et hydrique.
 - Incarner la sobriété grâce à des constructions passives, une consommation raisonnée, un projet de maraîchage bio, ou encore la mutualisation des espaces et du matériel.
 - Expérimenter des relations humaines équitables, avec la mise en place d' une gouvernance partagée, en évolution constante; ainsi que la mise en place d’espaces d’expression invitant à la remise en questions et l'accueil des différences.  
 - Revisiter les conditionnements éducatifs: ici les plus jeunes sont inclus dans la vie quotidienne du lieu, les activités et les prises de décisions les concernant. Le partage des savoirs faire et talents de chacun·e est valorisé pour une réelle ""école de la vie" à tout âge. Terr'Azïl a pour vocation de communiquer lors d'ateliers et conférences sur les thèmes de la communication en famille, le développement cognitif et l’accueil des émotions au regard des neurosciences.
 - Privilégier l'ouverture et les échanges au travers de projets d'accueil, de camps nature pour jeunes, d’une ludothèque, d’ateliers de parents, de stages et formations autour du bien-être et du vivre ensemble, d'immersions pour des vacances, des séjours de ruptures, ou des chantiers, tout en goûtant à la vie en collectif et ses enjeux.
 - Soutenir le développement d'une agriculture bio locale en portant une activité de couveuse, afin de faire profiter de parcelles de terre à des personnes désireuses de se faire la main avant de créer leur propre entreprise.

Terr'Azïl fait le pari que des humains respectés et libres de s’épanouir à leurs rythmes dès le plus jeune âge seront connectés à leur désir intrinsèque de contribuer pour la communauté et à plus grande échelle pour le monde.

![terrazil](./img/Terrazil2.JPG)

## Un bien commun sur un bel espace de nature
Le lieu répond parfaitement aux multi-facettes de ce projet, avec 7 bâtiments à rénover dont une bâtisse de 450m2 pour l’accueil en gîte et les communs, une grange, une bergerie, un vieux fournil, un pigeonnier, une stabulation…
Les familles résidentes y construisent leurs habitats en accord avec la raison d’être, afin de préserver un équilibre entre les sphères privées et collectives.

Terr'Azïl est une structure associative choisissant de faire vivre le bien commun pour contribuer à expérimenter un nouveau paradigme. Les habitant·es en ont l'usage sans pour autant en être propriétaires. Ce lieu se veut être un lieu d'accueil où chacun puisse se sentir bienvenue.

![terrazil](./img/Terrazil6.jpg)

<iframe src="https://www.podcastics.com/player/extended/978/34092/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>

## Le besoin de financement
La Coopérative Oasis a prêté 120 000 euros sur 10 ans à l'association Terr'azïl en janvier 2020. Le prêt avait pour but de finaliser l'achat du lieu En parallèle la Coopérative Oasis accompagne Terrazil pendant les 10 années de la convention. Lors de la première année, cela inclut notamment un soutien dans les démarches d'urbanisme pour les habitats légers, un soutien sur le cadre légal de l'instruction en familles et une aide à la création d'un fonds de dotation.

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
