---
title: "L'Arche de Saint-Antoine"
date: "2020-02-22T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/archesaintantoine/"
avancement: ""
montant: ""
departement: "Isère"
image: ./img/SaintAntoine1.jpg
website: https://www.arche-de-st-antoine.com/
highlighted: true
description: "Cette communauté non-violente d’une quarantaine de personnes a été fondée en 1987 dans un bâtiment de l'ancienne Abbaye de Saint-Antoine."
---

**La [Communauté de l’Arche de Saint-Antoine](https://www.arche-sta.com/), en Isère, a été fondée en 1987 dans un bâtiment de l'ancienne Abbaye de Saint-Antoine. Elle réunit une quarantaine de personnes unies autour de la non-violence, outil de transformation de soi et de transformation de la société. Toutes et tous expérimentent une vie communautaire faite de travail, de simplification des besoins, de conciliation et de réconciliation, de prière et méditation, ainsi que de chants, de danses et de fêtes.**

![Arche Saint-Antoine](./img/SaintAntoine2.jpg)

La communauté de Saint-Antoine a été fondée en septembre 1987, quand une équipe de 13 adultes et 11 enfants) provenant de la Communauté de l’Arche de Bonnecombe (Aveyron), vient s’installer dans la partie du bâtiment de l’ancienne abbaye de Saint-Antoine, qui leur a été proposée par les anciens propriétaires, les Pères de la Sainte Famille.

![Arche Saint-Antoine](./img/SaintAntoine1.jpg)
_La communauté en 2020 : engagés, postulants, stagiaires de longue ou moyenne durée_

## La non-violence comme philosophie
Plus de trente ans plus tard, la Communauté de l’Arche de Saint-Antoine est formée d’une soixantaine de personnes – célibataires, couples et familles – parmi lesquels les permanents engagés et des personnes qui y passent un temps plus ou moins long pour expérimenter la vie communautaire. La direction de vie est la non-violence comme philosophie et outil de transformation de soi et de la société.
La vie quotidienne est une vie simple de partage et de service, nourrie par le travail sur soi et sur les relations, l’intelligence collective, le chant, la danse et les fêtes, et par l’approfondissement chrétien et interspirituel.

_Dans cet épisode de Paillettes et Grelinette, une série qui donne à voir les oasis de l’intérieur, découvrez notamment le célèbre potager fleuri de l’Arche de Saint-Antoine !_
<iframe width="560" height="315" src="https://www.youtube.com/embed/LHcwbxyfXFw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Agriculture, accueil, construction, formations...
En plus de l’animation de la Maison d’accueil (accueil des groupes, hôtellerie, restauration), les activités quotidiennes se partagent entre le soin de nos jardins maraîchers, fruitiers et fleuris, l’entretien et la restauration de nos bâtiments (classés au patrimoine historique), l’accueil et la transmission et formation auprès de nos stagiaires et dans des formations ou interventions extérieures, la transformation de nos diverses plantes pour leur vente en boutique, l’accompagnement personnel et bien sûr l’animation de notre vie communautaire sous toutes ses formes.

![Arche Saint-Antoine](./img/SaintAntoine3.jpg)

[La Fève](http://www.feve-nv.com/) est un projet de la communauté de l’Arche de Saint-Antoine créé en 2010. Sa finalité est de transmettre, partager et encourager les pratiques et expérimentations du vivre-ensemble  qui favorisent une transition vers une société résiliente, respectueuse et bienveillante dans la relation humaine comme dans notre rapport au vivant, dans un esprit de non-violence.

L’Arche de Saint-Antoine fait partie de la [communauté internationale de l’Arche Non-Violence et Spiritualité](https://www.archecom.org/), fondée en 1948 par Lanza del Vasto.

_Guillem a grandi à l'Arche, qu'il a quittée pour s'y réinstaller des années plus tard avec sa famille. Magali a rejoint l'Arche il y a quelques années, et y a rencontré son compagnon. Ils racontent dans cet entretien le fonctionnement, les contraintes et les joie de la vie quotidienne de l'Arche :_
<iframe width="560" height="315" src="https://www.youtube.com/embed/asTSt0dtrLg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../investir/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez les oasis en participant à la Coopérative
    </a>
</div>
</div>
