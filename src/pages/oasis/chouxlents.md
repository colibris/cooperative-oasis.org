---
title: "Les Choux Lents"
date: "2019-10-06T22:12:03.284Z"
layout: oasis
type: oasis
path: "/oasis/chouxlents/"
avancement: ""
montant: ""
departement: "Rhône"
image: ./img/choux lents groupe.jpg
website: https://leschouxlents.potager.org/
description: "Sept foyers vivent aux Choux Lents, un habitat participatif situé en périphérie de Lyon qu'ils ont eux-mêmes rénové de façon écologique"
---

**[Les Choux Lents](https://leschouxlents.potager.org/), c'est un habitat participatif situé en périphérie de Lyon, à Saint-Germain-au-Mont-d'Or. Sept foyers y vivent avec des espaces privatifs et des espaces communs. Une partie des travaux ont été réalisés en auto-construction avec des matériaux durables.**

## "Bref, vivre moins con !"

Cet habitat groupé de 7 foyers s'est installé dans une maison de ville avec dépendances, réhabilitée en logement collectif. Les valeurs qui rassemblent ce petit groupe sont l'écologie, la sobriété, la mutualisation d'espace et d'objets (buanderie, flotte de véhicule...), l'ouverture sur le village, la bienveillance dans la communication.

Less valeurs ont été formalisées dans une « recette » : simplicité, sobriété, écologie, faire ensemble, prise de décision en équivalence, partage des espaces et des tâches, liberté et indépendance, ouverture sur le village. "Bref, vivre moins con !"

![Choux lents groupe](./img/choux lents groupe.jpg)

## Un faux départ qui n'empêche pas un montage éclair

Tout a commencé en 2011, quand 5 foyers lyonnais unis par une volonté de vivre en habitat groupé se rencontrent. Leur idée: partager des espaces et des moments de vie. Faire bourgeonner une nouvelle manière de vivre ensemble dans un climat de confiance, de solidarité et d’entraide. Pendant un an, le projet se construit autour d'une maison de retraite de prêtres, en vente sur la colline de St-Just à Lyon, impasse Choulans. En mars 2012, alors qu'une petite quinzaine d'habitants est prête à acheter, les vendeurs choisissent un autre acquéreur plus "offrant". Après quelques semaines de recherche à Lyon et aux alentours, une partie de ce groupe rebondit sur un nouveau lieu : une belle maison ancienne et sa grange à St-Germain-au-Mont-d'Or.

Profitant de la dynamique et de l'expérience du projet de St-Just, ce nouveau groupe des Choux Lents avance rapidement dans les étapes du projet : la SCI "Les   Choux  Lents" est propriétaire   du lieu quelques mois plus tard, en septembre 2012. Contrairement aux autres projets d’habitat groupé qui mettent 5 à 7 ans pour sortir, il s'agit d'un projet "éclair".

![Choux lents dej](./img/choux lents dej.jpg)

## Sortir une bâtisse de 1850 du marché de l'immobilier

Dans ces bâtiments datant de 1850 et achetés 835 000€ en 2012, 7 logements ont été auto-rénovés par le groupe sur 700m2 de surface habitable. Les travaux ont été réalisés en grande partie en autoconstruction lors de chantiers participatifs.

Le bâtiment est la propriété de la SCI Les Choux Lents, détenue très majoritairement par une association d'habitants (loi 1901). Cela permet de sortir le bien du marché de l'immobilier. Par ailleurs, les habitants détiennent chacun le même nombre de parts dans la SCI, ce qui leur permet de jouir du bâtiment et d'y habiter. Le montage ne permet pas aux habitants de toucher des allocations logement, car il n'y a pas de location ni de bail.

![Choux lents batisse](./img/choux lents batisse.jpg)

## Des choux allant de 1 à 55 ans

Le groupe comprend 10 adultes (7 foyers) de 32 à 55 ans. Des couples, des célibataires, 5 enfants de 1 à 18 ans. Les professions sont diverses :  professeur de math, prof de français, chercheur, entrepreneur, accompagnatrice d’habitat participatif...

Plusieurs sont de formation ingénieur, certains sont reconvertis. Le groupe se caractérise par une expérience de l’habitat groupé (expérience dans d’autres projets, formations à des méthodes de communication, vie en collectif...) et un fort pragmatisme. Certains que l'expérience fait grandir, les Choux Lents font germer ce projet même s’il n’est pas parfait.

![Choux lents travaux 2](./img/choux lents travaux 2 .jpg)

## Être et faire ensemble

Le niveau d'implication est assez fort depuis la création du groupe : tant en terme de réunions collectives,  que des travaux (entre 50 et 100 jours de travaux par an pour chaque habitant), du vivre-ensemble (gestion d'un groupement d'achat et participation à une AMAP, préparation des repas communs, entretien du lieu), et de l'ouverture (organisation d'événements -concerts, projections, stages, portes ouvertes).

En règle générale, le collectif se réunit chaque semaine (un soir entre lundi et vendredi) pour quelques heures, en partageant un repas.   En   plus, un dimanche par mois en moyenne permet travaux  en sous  groupe,  des prises  de décision, pauses, jeux de groupe...

![Choux Lents travaux](./img/choux lents travaux.jpg)

Audrey Gicquel nous accueille pour nous présenter le fonctionnement du lieu. Audrey s’est spécialisée dans l’accompagnement de collectifs, elle nous donne des clefs pour mieux comprendre le fonctionnement de ce mode de vie. Dans ce podcast elle partage avec nous quelques unes de ses expérience du vivre ensemble.

<iframe src="https://www.podcastics.com/player/extended/978/34048/" frameborder="0" height="200" width="100%" style="border-radius:10px"></iframe>
