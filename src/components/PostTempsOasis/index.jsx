import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import Couv from './Couv2.jpg'
import Logo from './Logo.png'
import Photo01 from './1.ferme-veranda.jpg'
import Photo02 from './2. Produire sa nourriture.jpg'
import Photo03 from './3.Moins d_argent.jpg'
import Photo04 from './4.Maison-autonome.jpg'
import Photo05 from './5.VivreEnsemble-Chirols.jpg'
import Photo06 from './6.Patrimoine-Moulinbleu.jpg'
import './style.scss'

class PostTempsOasis extends React.Component {
  render() {
    const { site, data, isIndex } = this.props
    const title = get(data, 'frontmatter.title')
    const path = get(data, 'frontmatter.path')
    const date = get(data, 'frontmatter.date')
    const html = get(data, 'html')

    return (
      <div className="temps-oasis">
        <div
          className="cover"
          style={{
            backgroundImage: `url(${Couv})`,
          }}
        >
          <img
            className="temps-oasis-logo"
            src={Logo}
            alt="Logo Le temps des Oasis"
          />
        </div>
        <div className="container">
          <div className="blog-article">
            <div className="article-wrap" key={path}>
              <h2 className="title size-1 blue">
                Et si nous habitions le monde autrement ?
              </h2>
              <div
                className="page-content"
                dangerouslySetInnerHTML={{
                  __html: html,
                }}
              />
              <div className="accordion mb-5">
                <ul>
                  <li>
                    <input type="checkbox" defaultChecked />
                    <i></i>
                    <h4>Pour un autre monde, cliquez ici !</h4>
                    <div className="accordion-content page-content">
                      <p className="mt-4">
                        <span className="font-italic">
                          “Nous avons tous connu la longue solitude et nous
                          avons appris que la seule solution est l’amour et que
                          l’amour vient de la communauté”.{' '}
                        </span>{' '}
                        Dorothy Day,{' '}
                        <span className="font-italic">
                          From Union Square to Rome
                        </span>
                      </p>
                      <p className="is-bold">Nous avons encore le choix.</p>
                      <p>
                        En octobre 2019, la philosophe Catherine Larrère osait
                        le dire :{' '}
                        <span className="font-italic">
                          “Nous avons encore le choix”
                        </span>
                        . Un an et demi plus tard, le coronavirus nous prouvait
                        qu’elle avait raison.
                      </p>
                      <p>
                        Cette pandémie n’a pas, comme on a pu le dire, rendu les
                        hommes “impuissants face à un virus”. Cette pandémie a,
                        bien au contraire, redonné aux hommes leur puissance
                        d’imagination : en flinguant, en quelques semaines, le
                        “business as usual”, ce virus a prouvé que le monde
                        pouvait tourner autrement. Et que ça ne dépendait que de
                        nous.
                      </p>
                      <p>
                        Tout à coup, ce système auquel on pensait qu’il n’y
                        avait pas d’alternative, sinon celle de l’effondrement,
                        s’est arrêté du fait de notre volonté. La croissance
                        économique, les flux internationaux, le développement
                        des technologies, la connaissance du monde - notre
                        inexorable quotidien en somme, nous y avons mis fin.
                      </p>
                      <p>
                        Tout à coup, on s’est rendu compte que ce monde-là
                        n’était pas inévitable. Qu’on pouvait peut-être, si ce
                        n’est inverser, du moins faire dévier son cours...
                      </p>
                      <p className="is-bold">
                        Les oasis, une boussole pour imaginer la suite
                      </p>
                      <p>
                        Il existe aujourd’hui en France de plus en plus
                        d’écolieux qu’on appelle, entre autres, les oasis. Ils
                        ont déjà commencé à inverser le cours du monde, à leur
                        échelle. Pour habiter autrement, ces collectifs
                        cultivent l’autonomie et le soin du vivant. Ils vivent
                        dans la nature, avec de l’espace, de la fraternité et du
                        labeur au grand air. Ils amènent du travail et de la
                        fête dans nos territoires dépeuplés. Et surtout, ils
                        vivent comme ils souhaitent vivre, en cohérence avec
                        leurs valeurs et leurs besoins.
                      </p>
                      <p>
                        Le temps de ces oasis est peut-être arrivé. Amenés aux
                        confins de nous-mêmes au fil de ces semaines, on se
                        demande si l’on veut continuer à vivre comme avant.
                        Parce qu’on en a peut-être marre de courir après le
                        temps, l’argent et le calme ; parce qu’on en a peut-être
                        assez d’être conscient que le monde disparaît sans
                        savoir comment le préserver.
                      </p>
                      <p>
                        Qu’il s’agisse de continuer à se poser des questions ou
                        de quitter son appartement pour changer radicalement de
                        quotidien, les oasis sont une piste à suivre. Ces lieux
                        sont autant d’étoiles qui pourront guider chacun et
                        chacune d’entre nous dans l’imagination et la
                        réalisation de ce tant attendu “monde d’après”.
                      </p>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="discover-oasis">
                <h3 className="title size-3 blue">Découvrez les oasis</h3>
                <div className="page-content">
                  <p>
                    Tout au long de l’été, nous vous enverrons des portraits
                    d’oasis, des reportages, des recommandations de lieux à
                    découvrir mais aussi des invitations à des événements,
                    portes ouvertes, formations et rencontres autour du sujet.{' '}
                    <span className="is-bold">
                      Prenez le temps de découvrir ce mode de vie, vous pourriez
                      bien avoir envie d’y goûter vous aussi !
                    </span>
                  </p>
                </div>
                <iframe
                  width="100%"
                  height="350"
                  src="https://aa25fbc2.sibforms.com/serve/MUIEAAiqljOjoi_7Z20NEHr06lX9uFVa6vZ-HtmIg4AOBOFe5HRby7dws2WgsGFCWwRZ25NKVWFwf83o-TpshV-1QLs8s_iiV1OFS4P8wTMIBkUg5eKwqvAb-LSMbx42xTzlI6UvRXnm0O0-qoc6GHoK57DzKmOvhWKq5evKv_BR2yYwa6v6b7RpCtJGCCcUxL79RTe-qqNGdpLG"
                  frameborder="0"
                  scrolling="no"
                  allowfullscreen
                  style={{
                    display: 'block',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginTop: '-30px',
                    maxwidth: '100%',
                  }}
                ></iframe>
              </div>
              <h2 className="title size-2 pink mt-5 mb-4">
                À quoi ressemble la vie dans un écolieu ?
              </h2>
              <div>
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/KypVB_Kj4_Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <script src="https://player.vimeo.com/api/player.js"></script>
              <h2 className="title size-2 pink mt-5">
                Vivre en oasis c’est...
              </h2>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Vivre avec plus d’espace</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img className="" src={Photo01} alt="Photo Ferme Légère" />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">La Ferme Légère,</span>{' '}
                        Clément Osé, Béarn
                      </p>
                      <p>
                        <span className="font-italic">
                          “Quand il voulait respirer un air plus distant et plus
                          sauvage, son chez eux faisait 110 000 m2, 11 hectares,
                          c’est à dire une quinzaine de terrain de foot ou 10
                          000 chambres de bonnes. Il n’avait jamais eu autant
                          d’espace. Il s’enfonçait dans ses bottes et marchait
                          jusque derrière les serres, dépassait les ruches de
                          l’orée du bois et descendait dans l’arène des feuillus
                          de la clairière. Il n’y avait pas une habitation
                          visible, pas la moindre de trace d’homme.”
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/ferme-legere-histoire-collective/"
                    target="_blank"
                  >
                    1400m2 par personne
                  </a>
                </div>
              </div>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Produire sa propre nourriture</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img
                      className=""
                      src={Photo02}
                      alt="Photo Produire sa propre nourriture"
                    />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">
                          La Ferme de Chenèvre,
                        </span>{' '}
                        Manon, Jura
                      </p>
                      <p>
                        <span className="font-italic">
                          “Nous souhaitions aussi faire de la ferme un lieu
                          d’activités professionnelles diverses. Si l’on veut
                          être autonome et résilient, l’habitat ne suffit pas…
                          J’ai donc lancé mon activité de maraîchage bio en
                          traction animale pendant que Virginie installait son
                          atelier de céramique. Ma première récolte a pris le
                          chemin des Biocoop de la région, et les créations de
                          Virginie ont été vendues dans la France entière.”
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/ferme-chenevre/"
                    target="_blank"
                  >
                    Gagner en autonomie alimentaire
                  </a>
                </div>
              </div>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Vivre avec moins d’argent et plus de temps</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img
                      className=""
                      src={Photo03}
                      alt="Photo Vivre avec moins d’argent et plus de temps"
                    />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">
                          Domaine des Possibles,
                        </span>{' '}
                        Alain, Puy de Dôme
                      </p>
                      <p>
                        <span className="font-italic">
                          “La solidarité était une notion clef pour chacun
                          d’entre nous. Nous l’avons donc déclinée sur plusieurs
                          plans. Dans le financement d’abord : chaque foyer
                          devait faire un apport de 130 000€. Pour permettre à
                          tous d’intégrer le projet, la SCI a contracté un
                          emprunt pour ceux qui n’avaient pas accès aux banques.
                          Ces derniers se sont engagés à rembourser au fil du
                          temps à la SCI directement. Quant aux logements, ils
                          ont été distribués en fonction des besoins de chacun,
                          et non des moyens : plus le foyer est nombreux, plus
                          l’appartement est grand, avec la règle d’une chambre
                          par enfant.”
                        </span>{' '}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/domaine-possibles/"
                    target="_blank"
                  >
                    Payer un loyer de 60€ par mois
                  </a>
                </div>
              </div>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Diviser par 10 sa consommation d’énergie</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img
                      className=""
                      src={Photo04}
                      alt="Photo Diviser par 10 sa consommation d’énergie"
                    />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">
                          L’écohameau du Ruisseau,
                        </span>{' '}
                        Patrick, Loire-Atlantique
                      </p>
                      <p>
                        <span className="font-italic">
                          “Depuis les 14 et 15 juin 1997, on est complètement
                          autonomes en électricité. Alors ce qui est
                          intéressant, c’est de savoir qu’on a tout le confort :
                          de la lumière, l’aspirateur, internet, la télévision,
                          une machine-à-laver et tout l’électroménager…”
                        </span>{' '}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/film-vivre-hameau-autonome/"
                    target="_blank"
                  >
                    Produire sa propre électricité
                  </a>
                </div>
              </div>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Vivre ensemble au quotidien</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img
                      className=""
                      src={Photo05}
                      alt="Vivre ensemble au quotidien"
                    />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">
                          Le Moulinage de Chirols,
                        </span>{' '}
                        Guy, Ardèche
                      </p>
                      <p>
                        {' '}
                        <span className="font-italic">
                          “Il y a une générosité et une joie ici, qui invitent à
                          s’engager totalement – et donnent le sentiment d’être
                          à sa place. Vivre, avancer et apprendre ensemble sont
                          plus important que l’objectif. Je sais pas encore
                          combien de temps je resterai, mais je sais que j’aurai
                          beaucoup grandi.”
                        </span>{' '}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/revolution-chirols/"
                    target="_blank"
                  >
                    Se sentir entouré et en sécurité
                  </a>
                </div>
              </div>
              <div className="oasis-feature">
                <div className="oasis-feature__header">
                  <h3>Redonner vie au patrimoine</h3>
                </div>
                <div className="row">
                  <div className="col-sm-7">
                    <img
                      className=""
                      src={Photo06}
                      alt="Redonner vie au patrimoine"
                    />
                  </div>
                  <div className="col-sm-5">
                    <div className="">
                      <p className="is-bold">
                        <span className="font-italic">Moulin Bleu,</span> Flore,
                        Loir-et-Cher
                      </p>
                      <p>
                        {' '}
                        <span className="font-italic">
                          “Avec la mise en place du confinement, le groupe de 15
                          porteurs de projet se sont installés sur place d’un
                          coup. Une occasion en or d'accélérer l'installation.
                          Découverte, nettoyage, réhabilitation, premiers
                          aménagements... la prise en main du lieu nous laisse
                          déjà entrevoir les potentielles futures activités pour
                          ce Moulin.”
                        </span>{' '}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <a
                    className="btn"
                    href="https://cooperative-oasis.org/moulin-bleu-etre-resilient/"
                    target="_blank"
                  >
                    Restaurer un ancien Moulin
                  </a>
                </div>
              </div>
              <div className="discover-oasis">
                <h3 className="title size-3 blue">Découvrez les oasis</h3>
                <div className="page-content">
                  <p>
                    Tout au long de l’été, nous vous enverrons des portraits
                    d’oasis, des reportages, des recommandations de lieux à
                    découvrir mais aussi des invitations à des événements,
                    portes ouvertes, formations et rencontres autour du sujet.{' '}
                    <span className="is-bold">
                      Prenez le temps de découvrir ce mode de vie, vous pourriez
                      bien avoir envie d’y goûter vous aussi !
                    </span>
                  </p>
                </div>
                <iframe
                  width="100%"
                  height="350"
                  src="https://aa25fbc2.sibforms.com/serve/MUIEAAiqljOjoi_7Z20NEHr06lX9uFVa6vZ-HtmIg4AOBOFe5HRby7dws2WgsGFCWwRZ25NKVWFwf83o-TpshV-1QLs8s_iiV1OFS4P8wTMIBkUg5eKwqvAb-LSMbx42xTzlI6UvRXnm0O0-qoc6GHoK57DzKmOvhWKq5evKv_BR2yYwa6v6b7RpCtJGCCcUxL79RTe-qqNGdpLG"
                  frameborder="0"
                  scrolling="no"
                  allowfullscreen
                  style={{
                    display: 'block',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginTop: '-30px',
                    maxwidth: '100%',
                  }}
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default PostTempsOasis
