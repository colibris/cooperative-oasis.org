import React from 'react'
import { graphql, StaticQuery } from 'gatsby'
import RowPeople from '../RowPeople'
import './style.scss'

class SectionComite extends React.Component {
  render() {
    const comites = this.props.comites

    return (
      <div>
        {comites.map((item, index) => {
          let comite = item.node
          return (
            <div key={`comite_${index}`}>
              <h2>{comite.titre}</h2>
              <p>{comite.description}</p>
              {comite.membres ? <RowPeople people={comite.membres}/> : ''}
            </div>
          )
        })}
      </div>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allEquipeYaml {
          edges {
            node {
              titre
              description
              id
              membres {
                description
                titre
                photo {
                  childImageSharp {
                    fluid {
                      src
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => <SectionComite comites={data.allEquipeYaml.edges} />}
  />
)
