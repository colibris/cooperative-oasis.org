import React from 'react'
import { graphql, StaticQuery } from 'gatsby'
import './style.scss'

class RowPeople extends React.Component {

  togglePerson = event => {
    const clickedPerson = event.target.closest('.person')
    if (
      clickedPerson.classList.contains('openend') ||
      clickedPerson.classList.contains('displayed')
    ) {
      this.closePerson(clickedPerson)
    } else {
      this.closeOpenedPersons()
      this.openPerson(clickedPerson)
    }
  }

  openPerson = element => {
    element.classList.add('opened')
    setTimeout(function() {
      element.classList.add('displayed')
    }, 100)
  }

  closePerson = element => {
    element.classList.remove('displayed')
    setTimeout(function() {
      element.classList.remove('opened')
    }, 20)
  }

  closeOpenedPersons = () => {
    const openedPeople = document.querySelectorAll('.person.displayed')
    for (let openedPerson of openedPeople) {
      this.closePerson(openedPerson)
    }
  }

  render() {
    const component = this
    const people = this.props.people

    return (
      <div className="row">
        {people.map((person, index) => {
          {console.log(person)}
          // {console.log(person.node)}
          // {console.log(person.node.photo)}
          return (
            <div className="person" key={`person-${index}`}>
              <div
                className="person-wrapper"
                key={`person-wrapper-${index}`}
                onClick={component.togglePerson}
              >
                <img
                  src={person.photo.childImageSharp.fluid.src}
                  alt={person.titre}
                />
                <h3>{person.titre}</h3>
              </div>
              <div className="person-metadata">
                <button
                  type="button"
                  className="close"
                  aria-label="Close"
                  onClick={component.togglePerson}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <p>{person.description}</p>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default RowPeople