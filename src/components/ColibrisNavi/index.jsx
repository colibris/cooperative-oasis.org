import React from 'react'
import './style.scss'

class ColibrisNavi extends React.Component {
  render() {
    return (
      <>
        <div id="archipel-section">
          <div id="archipel" className="archipel">
            <div className="archipel__wrapper">
              <div className="archipel__islands islands">
                <input
                  type="checkbox"
                  value="1"
                  id="islands__toggle"
                  className="islands__toggle"
                  aria-controls="islands__list"
                />
                <label className="islands__toggle-label" htmlFor="islands__toggle">
                  <span>Les sites partenaires de la</span>coopérative Oasis
                  <i className="fa fa-bars ml-2" aria-hidden="true"></i>
                </label>
                <ul className="islands__list" id="islands__list">
                  <li className="islands__list__item">
                    <a href="https://colibris-lemouvement.org/">Colibris</a>
                  </li>
                  <li className="islands__list__item">
                    <a href="https://colibris.cc/oasis/?MoocIntroduction">Le Mooc Oasis</a>
                  </li>
                  <li className="islands__list__item">
                    <a href="https://colibris-lafabrique.org/">Fabrique des colibris</a>
                  </li>
                  <li className="islands__list__item">
                    <a href="https://www.colibris-outilslibres.org/">Outils libres</a>
                  </li>
                  <li className="islands__list__item">
                    <a href="https://www.habitatparticipatif-france.fr/">Habitat participatif France</a>
                  </li>
                  <li className="islands__list__item">
                    <a href="https://gen-europe.org/">GEN-Europe</a>
                  </li>
                </ul>
              </div>
              <div className="other-list">
                <ul className="list-unstyled">
                  <li className="first">
                    <a href="https://cooperative-oasis.org/wiki/?PagePrincipale">
                      INTRANET / WIKI
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default ColibrisNavi
