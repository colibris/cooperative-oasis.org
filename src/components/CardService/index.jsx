import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import size from 'lodash/size'
import './style.scss'

class CardService extends React.Component {
  render() {
    const { site, data, isIndex } = this.props
    const title = get(data, 'frontmatter.title')
    const path = get(data, 'frontmatter.path')
    const description = get(data, 'frontmatter.description')
    const color = get(data, 'frontmatter.color')
    const image = get(data, 'frontmatter.image')
    const serviceUrl = get(data, 'frontmatter.serviceUrl')
    const serviceUrlTitle = get(data, 'frontmatter.serviceUrlTitle')
    
    return (
      <div className="card-service">
        <div className="row align-items-center">
          <div className="col">
            <a className="link-service" href={serviceUrl} title={title} style={{ backgroundColor: color }}>
              <img className="thumb-service" src={image.publicURL} alt="image service" />
            </a>
          </div>
          <div className="col-7">
            <div className="pr-4">
              <Link style={{ boxShadow: 'none' }} to={path}>
                <h1>{title}</h1>
              </Link>
              <div className="page-description">
              {description}
              </div>
              <Link className="link-more" style={{ boxShadow: 'none' }} to={path}>
                En savoir +
              </Link>
            </div>
          </div>
          <div className="col-2">
            <a href={serviceUrl} title={title} className="link-service">
              <div className="link-service-content">
                <i className="fa fa-magic" aria-hidden="true"></i>
                <p className="service-title">{serviceUrlTitle}</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default CardService
