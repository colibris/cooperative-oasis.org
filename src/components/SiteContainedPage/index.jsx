import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import size from 'lodash/size'
import './style.scss'

class SiteContainedPage extends React.Component {
  render() {
    const { site, data, isIndex } = this.props
    const title = get(data, 'frontmatter.title')
    const path = get(data, 'frontmatter.path')
    //const date = get(data, 'frontmatter.date')
    const html = get(data, 'html')

    return (
      <div className="contained-page container">
        <div className="page-header">
          <Link style={{ boxShadow: 'none' }} to={path}>
            <h1>{title}</h1>
          </Link>
        </div>
        <div
          className="page-content"
          dangerouslySetInnerHTML={{ __html: html }}
        />
      </div>
    )
  }
}

export default SiteContainedPage
