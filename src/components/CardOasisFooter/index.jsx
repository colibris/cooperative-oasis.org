import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import size from 'lodash/size'
import Img from 'gatsby-image'
import './style.scss'

class CardOasisFooter extends React.Component {
  render() {
    const { data, isInvestir } = this.props
    const montant = get(data, 'frontmatter.montant')
    const avancement = get(data, 'frontmatter.avancement')

    let classAvancement = 'avancement'
    if (avancement == 'financé') {
      classAvancement = 'avancement financed'
    }
    if (isInvestir) {
      return (
        <div className="group-sticky">
          <div className={classAvancement}>{avancement}</div>
          <div className="montant">{montant}</div>
        </div>
      )
    } else {
      return (
        <div className="group-sticky">
          <div className="footer-line"></div>
        </div>
      )
    }
  }
}

export default CardOasisFooter