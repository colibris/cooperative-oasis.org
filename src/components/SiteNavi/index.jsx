import React from 'react'
import Link from 'gatsby-link'
import logo from './img/logo-cooperative-oasis.png'; // Tell Webpack this JS file uses this image
import './style.scss'; // Tell Webpack this JS file uses this image


class SiteNavi extends React.Component {
  render() {
    const { location, title } = this.props
    return (
      <nav className="navbar navbar-default my-3">
        <div className="container nav-container">
          <Link className="logo text-center" to="/">
            <h1 className="navbar-brand">
              <img src={logo} alt={title} />
            </h1>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarToggler"
            aria-controls="navbarToggler"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={() =>
              document.getElementById('navbarToggler').classList.toggle('show')
            }
          >
            <i className="fa fa-bars" aria-hidden="true"></i> MENU
          </button>
          <div className="navbar-nav-scroll" id="navbarToggler">
            <ul className="navbar-nav bd-navbar-nav flex-row d-none d-lg-flex">
              <li className="nav-item cta">
                <a
                  href="https://cooperative-oasis.org/wiki/?JournalduprojetOasis"
                  className="nav-link"
                  target="_blank"
                >
                  Recevoir des infos
                </a>
              </li>
              <li
                className={
                  location.pathname === '/investir/'
                    ? 'nav-item cta active'
                    : 'nav-item cta'
                }
              >
                <Link to="/investir/" className="nav-link">
                  Placer son épargne
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav bd-navbar-nav flex-row">
              <li
                className={
                  location.pathname === '/les-oasis/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/les-oasis/" className="nav-link">
                  Les oasis
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/investir/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/investir/" className="nav-link">
                  Investir
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/creer-son-oasis/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/creer-son-oasis/" className="nav-link">
                  Créer son oasis
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/pepiniere-oasis/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/pepiniere-oasis/" className="nav-link">
                  Pépinière
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/actus/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/actus/" className="nav-link">
                  Actus
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/qui-sommes-nous/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/qui-sommes-nous/" className="nav-link">
                  Qui sommes-nous ?
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/des-questions/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/des-questions/" className="nav-link">
                  Des questions ?
                </Link>
              </li>
              <li className="nav-item cta d-lg-none">
                <a
                  href="https://cooperative-oasis.org/wiki/?JournalduprojetOasis"
                  className="nav-link"
                  target="_blank"
                >
                  Recevoir des infos
                </a>
              </li>
              <li className="nav-item cta d-lg-none">
                <Link to="/investir/" className="nav-link">
                  Placer son épargne
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

export default SiteNavi
