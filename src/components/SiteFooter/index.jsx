import React from 'react'
import Link from 'gatsby-link'
import logo from './img/logo-align-right.png'; // Tell Webpack this JS file uses this image
import './style.scss'

class SiteFooter extends React.Component {
  render() {
    return (
      <footer className="footer" role="banner">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <iframe
                width="540"
                height="305"
                src="https://aa25fbc2.sibforms.com/serve/MUIEAMdj37RZv3sPHLH9OiNhKfMtYX4SdjTPVgyAzuPZT-Ko024ssQWXl9F7UPjsBYGrjrnTsVlJtQDPdFtV3k6QgH917kAnA_NiLtbnyve-fOVFuFE-yahNgJsG6zmNrWxEMURMo7OcBZpD_LlhFwPDSVNPspLnXxQkUh37rhqnRU6Pwo0IS9wYvLXMhASMNBGHlf4DyDkZOtlH"
                frameBorder="0"
                scrolling="no"
                allowFullScreen
                style={{
                  display: 'block',
                  marginLeft: 'auto',
                  marginRight: 'auto',
                  maxWidth: '100%',
                }}
              ></iframe>
            </div>

            <div className="col-md-3 py-large" style={{ fontSize: 0.9 + 'em' }}>
              La Coopérative Oasis est une société coopérative d'intérêt
              collectif créée en 2018. Elle vise à développer des oasis partout
              en France. Elle a pris le relais du mouvement Colibris pour porter
              le Projet Oasis et animer le réseau des oasis.
              <br />
              <br />
            </div>

            <div className="col-md-3 text-right py-large">
              <ul className="menu">
                <li>
                  <Link to="/contact">Contact</Link>
                </li>
                <li>
                  <Link to="/mentions-legales">Mentions légales</Link>
                </li>
                <li>
                  <a
                    href="https://cooperative-oasis.org/wiki/"
                    title="Le wiki du réseau oasis"
                  >
                    Le wiki du réseau oasis
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.colibris-lemouvement.org/"
                    title="Le site du mouvement Colibris"
                  >
                    Le site de Colibris
                  </a>
                </li>
                <li>
                  <a
                    href="https://cooperative-oasis.org/investir/"
                    title="Investir"
                  >
                    Investir dans la Coopérative Oasis
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-3 text-right py-large">
              <a
                href="https://www.cooperative-oasis.org/"
                title="Le site de la coopérative"
              >
                <img src={logo} alt="logo droite" />
              </a>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default SiteFooter
