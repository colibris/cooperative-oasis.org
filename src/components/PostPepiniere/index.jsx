import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import Couv from './Couv.jpg'
import Logo from './Logo.png'
import Photo01 from './photo01.jpg'
import Photo02 from './photo02.jpg'
import Photo03 from './photo03.jpg'
import AG from './AudreyGicquel.jpg'
import DV from './DaphneVialan.jpg'
import EG from './EliseGruntz.jpg'
import FB from './FredericBosque.jpg'
import FJ from './FredJozon.jpg'
import JPC from './Jean-PhilippeCieslak.jpg'
import JO from './JorgeOchoa2.jpg'
import MP from './MaelPondaven.jpg'
import MG from './MaiteGayet.jpg'
import MB from './Marc-Bodinier.jpg'
import Programme from './Programme_pepiniere_oasis.pdf'
import './style.scss'

class PostPepiniere extends React.Component {
  render() {
    const { site, data, isIndex } = this.props
    const title = get(data, 'frontmatter.title')
    const path = get(data, 'frontmatter.path')
    const date = get(data, 'frontmatter.date')
    const html = get(data, 'html')

    return (
      <div className="pepiniere">
        <div
          className="cover"
          style={{
            backgroundImage: `url(${Couv})`,
          }}
        >
          <img
            className="pepiniere-logo"
            src={Logo}
            alt="logo La pépinière Oasis, Créez votre projet d'écolieu collectif"
          />
        </div>
        <div className="container">
          <div className="blog-article">
            <div className="article-wrap" key={path}>
              <div className="page-content">
                <div className="text-center">
                  <p>
                    Vous souhaitez créer un écolieu collectif ? <br />
                    Vous souhaitez développer de nouvelles connaissances et
                    compétences sur le montage et la gestion de ces lieux ?
                  </p>
                  <p className="is-bold">
                    La Pépinière Oasis vous propose, durant 6 mois, de vous
                    accompagner dans votre projet et de vous apprendre des
                    fondamentaux pour développer une oasis.
                  </p>
                  <a
                    className="btn"
                    href="mailto:pepiniere@cooperative-oasis.org"
                    target="_blank"
                  >
                    Je candidate
                  </a>
                  <p className="is-extralight-italic">
                    Pour participer, envoyez-nous un email avec
                    <br /> la présentation de votre projet et de votre profil
                    <br /> à l'adresse pepiniere@cooperative-oasis.org.
                  </p>
                </div>
              </div>
              <div>
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/oNR0EvZoRPY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <div className="page-content">
                <h2>Ce cursus va vous apporter :</h2>
                <ul>
                  <li>
                    Une formation qualitative et poussée à un ensemble de savoir-faire et
                    savoir-être utiles pour créer une Oasis
                  </li>
                  <li>
                  L’expérimentation de la vie en Oasis par plusieurs séjours d'immersions dans des oasis
                  </li>
                  <li>
                    La mise en lien avec le réseau de la Coopérative Oasis
                  </li>
                  <li>
                    Le développement de votre projet tout au long du parcours
                    grâce à un processus d’intelligence collective et l’accompagnement de mentors
                  </li>
                </ul>
                <p className="is-bold text-center">
                  Au terme des 6 mois, vous aurez appris de nombreuses connaissances <br />
                  et des outils pour piloter votre projet avec discernement et méthode.
                </p>
              </div>
              <div className="page-content text-center">
                <a className="btn" href={Programme} target="_blank">
                  Télécharger le programme complet
                </a>
              </div>
              <img
                className=""
                src={Photo01}
                alt="Pépinière Oasis en 2021"
              />
              <div className="page-content">
                <h2>Lieu de formation</h2>
                <p>
                  Les conférences et le mentorat de votre projet auront lieu en
                  ligne, via une application de visioconférence. Les séjours
                  d’immersion se passeront dans différentes oasis abouties du réseau.
                </p>
                <h2>Période de formation</h2>
                <p>Le programme durera d'avril à octobre 2021 (6 mois).</p>
                <h2>Public concerné</h2>
                <p>
                  Ce programme s’adresse particulièrement à des porteurs de
                  projet d’oasis. Vous avez toute votre place si :
                </p>
                <ul>
                  <li>
                    Vous réfléchissez à un projet seul mais vous n’avez pas de
                    groupe
                  </li>
                  <li>
                    Vous réfléchissez à un projet avec un collectif (peu importe
                    sa taille)
                  </li>
                  <li>
                    Vous avez commencé à monter votre projet mais vous avez besoin
                    d’outils et de connaissances supplémentaires
                  </li>
                  <li>
                    Vous vivez déjà en oasis ou souhaitez en rejoindre une
                    prochainement et vous souhaitez en comprendre le fonctionnement.
                  </li>
                </ul>
                  La formation se fait en partie en ligne et nécessite de
                  maîtriser quelques outils informatiques (visioconférences)
                  et avoir une bonne connexion internet.
              </div>
              <img
                className=""
                src={Photo02}
                alt="Pépinière Oasis en 2021"
              />
              <h2>Coût et programme de la formation</h2>
              Le coût du parcours est de 2100 € HT, soit 2500 € TTC par personne.
              Nous vous demandons un acompte de 20% (500 €) pour
              réserver votre place puis un virement du reste de la somme avant
              le premier rassemblement.
              <br />
              <br />
              Le programme comprend notamment :
              <br />
              <b>La participation à 4 événements majeurs</b>
              <ul>
                <li>Les 24-25 avril au Domaine de Chardenoux</li>
                <li>Les 19-20 juin à l'Arche de Saint Antoine</li>
                <li>Les 11-12 septembre au Campus de la Transition</li>
                <li>Le Festival Oasis (1 place incluse)</li>
              </ul>
              <b>Au moins 18 jours lors de 3 séjours immersifs</b> à choisir parmi une quinzaine de propositions entre juin et septembre 2021 (voir la carte plus bas)
              <br />
              <br />
              <b>Une formation et un accompagnement en ligne sur 6 mois</b>
              <ul>
                <li>
                  plus de 30 heures de formations en ligne entre avril et septembre
                </li>
                <li>
                  Suivi des projets personnels sous la forme de groupes de
                  travail accompagné par des mentors (6 réunions)
                </li>
                <li>Tous les 15 jours, une lettre d’information dédiée</li>
              </ul>
              <br />
              <div className="page-content text-center">
                <a className="btn" href={Programme} target="_blank">
                  Télécharger le programme complet
                </a>
              </div>
              <br />
              Les déplacements jusqu’aux lieux de séjour restent à la charge des participants.
              <br />
              Des bourses sont possibles en fonction de votre situation.
              La formation peut bénéficier d’une prise en charge dans le
              cadre de la formation professionnelle. Contactez-nous.
              <br />
              <br />
              <h2>Comment candidater ?</h2>
              La formation est à destination de 60 personnes maximum.
              <br />
              <br />
              Pour participer, envoyez la présentation de votre
              projet et de votre profil à pepiniere@cooperative-oasis.org.
              Cela peut prendre la forme que vous souhaitez !
              <br />
              <br />
              <div className="page-content text-center">
                <a
                  className="btn"
                  href="mailto:pepiniere@cooperative-oasis.org"
                  target="_blank"
                >
                  Je candidate
                </a>
              </div>
              <iframe
                width="100%"
                height="500"
                src="https://pepiniere-oasis.gogocarto.fr/annuaire?iframe=1#/carte/@46.33,2.50,5z?cat=all"
                frameBorder="0"
                marginHeight="0"
                marginWidth="0"
              ></iframe>
            </div>
            <div className="page-content">
              <h2>Des mentors de qualité pour vous accompagner</h2>
              <p>
                Durant toute cette formation vous serez accompagnés par des
                mentors qui vous questionneront et vous aideront à structurer
                votre projet.
              </p>
            </div>
            <div className="page-content">
              <div className="intervenants">
                <div className="intervenant">
                  <figure>
                    <img src={JPC} alt="Photo Jean-Philippe Cieslak" />
                  </figure>
                  <p className="text-center">Jean-Philippe Cieslak</p>
                </div>
                <div className="intervenant">
                  <figure>
                    <img src={DV} alt="Photo Daphné Vialant" />
                  </figure>
                  <p className="text-center">Daphné Vialan</p>
                </div>
                <div className="intervenant">
                  <img src={EG} alt="Photo Elise Gruntz" />
                  <p className="text-center">Elise Gruntz</p>
                </div>
                <div className="intervenant">
                  <img src={AG} alt="Photo Audrey Gicquel" />
                  <p className="text-center">Audrey Gicquel</p>
                </div>
                <div className="intervenant">
                  <img src={FB} alt="Photo FredericBosque" />
                  <p className="text-center">Frédéric Bosqué</p>
                </div>
                <div className="intervenant">
                  <img src={FJ} alt="Photo Fred Jozon" />
                  <p className="text-center">Frédéric Jozon</p>
                </div>
                <div className="intervenant">
                  <img src={MP} alt="Photo Mael Pondaven" />
                  <p className="text-center">Mael Pondaven</p>
                </div>
                <div className="intervenant">
                  <img src={MG} alt="Photo Maite Gayet" />
                  <p className="text-center">Maïté Gayet</p>
                </div>
                <div className="intervenant">
                  <img src={JO} alt="Photo JorgeOchoa" />
                  <p className="text-center">Jorge Ochoa</p>
                </div>
                <div className="intervenant">
                  <img src={MB} alt="Photo Marc Bodinier" />
                  <p className="text-center">Marc Bodinier</p>
                </div>
              </div>
            </div>
            <br />
            <br />
            <div className="page-content text-center">
              <a
                className="btn"
                href="mailto:pepiniere@cooperative-oasis.org"
                target="_blank"
              >
                Je candidate
              </a>
            </div>
            <center>
              <p className="is-extralight-italic">
                Pour participer, envoyez-nous un email avec votre
                <br /> la présentation de votre projet et votre profil.
              </p>
            </center>
          </div>
        </div>
      </div>
    )
  }
}




export default PostPepiniere
