import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Slider from 'react-slick'
import './style.scss'

class OasisSlider extends React.Component {
  addVideo(item, slides) {
    slides.push(
      <div className="sliderImageContainer" key={item.id}>
        <iframe
          width="730"
          height="486"
          src={item.link}
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          frameBorder="0"
        ></iframe>
      </div>
    )
    return slides
  }

  addImage(item, slides) {
    let image = item.image
    if (image == null) {
      image = 'https://via.placeholder.com/750x500'
    } else {
      image = image.childImageSharp.resize.src
    }
    slides.push(
      <div className="sliderImageContainer" key={item.id}>
        <Link to={'/' + item.link} className="sliderImageLink">
          <img src={image} />
          <div className="sliderImageFooter">
            <h3>{item.legende}</h3>
          </div>
        </Link>
      </div>
    )
    return slides
  }

  render() {
    const items = get(this, 'props.items')

    let slides = []
    for (const item of items) {
      let type = item.type
      if (type == 'image') {
        slides = this.addImage(item, slides)
      } else if (type == 'video') {
        slides = this.addVideo(item, slides)
      }
    }

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    }
    return (
      <div>
        <Slider {...settings}>{slides}</Slider>
      </div>
    )
  }
}
export default OasisSlider
