import React from 'react'
import Link from 'gatsby-link'
import forEach from 'lodash/forEach'
import get from 'lodash/get'
import size from 'lodash/size'
import SectionComite from '../SectionComite'
import './style.scss'

class PostQuiSommesNous extends React.Component {

  render() {
    const { site, data, isIndex } = this.props
    const title = get(data, 'frontmatter.title')
    const path = get(data, 'frontmatter.path')
    const html = get(data, 'html')

    return (
      <div className="contained-page container">
        <div className="page-header">
          <Link style={{ boxShadow: 'none' }} to={path}>
            <h1>{title}</h1>
          </Link>
        </div>
        <div
          className="page-content"
          dangerouslySetInnerHTML={{ __html: html }}
        />
        <div className="page-content">
          <SectionComite />
        </div>
      </div>
    )
  }
}

export default PostQuiSommesNous
